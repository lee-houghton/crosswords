# 2019-09-09

* Add Facebook login

# 2019-08-21

* Add auditing for Google sign in

# 2019-08-19

* Add Google sign in

# 2019-08-15

* Improve crossword solving UX on small screens

# 2019-08-08

* Allow downloading word lists

# 2019-08-07

* Allow printing crosswords

# 2020-08-01

* Redesign the home screen

# 2020-07-19

* Update to Apollo Client 3

# 2020-07-18

* Add a GraphQL query to return recent activity from followed users

# 2020-07-17

* Added the ability to follow other users

# 2020-07-02

* Added a favicon

# 2020-07-01

* Added the ability to view another user's profile

# 2020-06-23

* Add a What's New screen and an unread news indicator

# 2020-06-14

* Allow resetting your password if you forget it and you still have access to your email address

# 2020-06-08

* Allow changing your password

# 2020-06-07

* Allow choosing size of quick create crossword from the Home screen

# 2020-06-06

* Allow changing the crossword size when adding a crossword from the Add Crossword menu item
* Improved the layout of the crossword screen

# 2020-05-17

* Fixed IME issue in Firefox

# 2020-05-14

* Improved IME support for different browsers

# 2020-05-13

* Added support for IME composition events (support typing characters like é or 中)

# 2020-05-01

* Added recent activity section to Home screen

# 2020-04-30

* Show red/amber/green scores for terms you've practiced before when viewing a word list.

# 2020-04-29

* Add basic Elo-style scoring system and stats for each user vs each term

# 2020-04-25

* Add support for Node.js v14

# 2020-04-05

* Improve security configuration
* Allow long press to edit in main word list view

# 2020-03-30

* Add Swipe To Delete when editing a word list

# 2020-03-28

* Cosmetic changes to the profile screen
* Cosmetic changes to edit word list screen

# 2020-03-27

* Allow creating a crossword from specific word lists (of your own)
* Add images to user language card (selected languages)

# 2020-03-26

* De-emphasise the "show answers" button when all the cells are not yet filled in

# 2020-03-22

* Improved performance while solving a crossword
* Improved user experience solving a crossword on mobile browsers

# 2020-03-08

* Keep track of user stats per language
* Allow adding crosswords for multiple word lists (currently just picks all your word lists for a language)
* Added quick create crossword buttons to home screen
* Improved display of some flag icons

# 2020-03-07

* Show author of a crossword
* Add author name when viewing a word list

# 2020-03-06

* Add "swap term/definition" when editing a single term
* Show language flags when editing a single term

# 2020-03-04

* First public release (add/edit word lists, create crosswords)
* Show which word lists a crossword was generated from
* Allow editing terms after doing a crossword (e.g. if it is wrong or ambiguous)
