variable aws_access_key_id { 
    type = string
}

variable aws_secret_access_key { 
    type = string
}

provider aws {
    region = "eu-west-2"
    version = "~> 2.42"
    access_key = var.aws_access_key_id
    secret_key = var.aws_secret_access_key
}

provider aws {
    alias = "cloudfront"
    region = "us-east-1"
    version = "~> 2.42"
    access_key = var.aws_access_key_id
    secret_key = var.aws_secret_access_key
}
