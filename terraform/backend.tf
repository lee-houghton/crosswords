terraform {
  backend "remote" {
    organization = "crosswords"

    workspaces {
      name = "dev"
    }
  }
}
