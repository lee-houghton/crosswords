variable server_ipv4 {
    description = "IPv4 address of the development server"
}

variable server_ipv6 {
    description = "IPv6 address of the development server"
}

resource aws_iam_user deploy-dev {
    name = "deploy-dev"
}

resource aws_iam_access_key deploy-dev {
    user = aws_iam_user.deploy-dev.name
}

output dev-access-key-id {
    value = aws_iam_access_key.deploy-dev.id
}

output dev-access-secret-key {
    value = aws_iam_access_key.deploy-dev.secret
    sensitive = true
}

data aws_iam_policy_document deploy-dev-policy {
    statement {
        actions = [
            "ecr:GetAuthorizationToken",
            "ecr:GetDownloadUrlForLayer",
            "ecr:BatchGetImage",
            "ecr:BatchCheckLayerAvailability",
        ]
        resources = ["*"]
    }
}

resource aws_iam_user_policy deploy-dev-policy {
    user = aws_iam_user.deploy-dev.name
    policy = data.aws_iam_policy_document.deploy-dev-policy.json
}

resource aws_route53_record a-dev {
    zone_id = aws_route53_zone.app.id
    name = "api.crosswords.leehoughton.dev"
    
    type = "A"
    records = [var.server_ipv4]
    ttl = 3600
}

resource aws_route53_record aaaa-dev {
    zone_id = aws_route53_zone.app.id
    name = "api.crosswords.leehoughton.dev"
    
    type = "AAAA"
    records = [var.server_ipv6]
    ttl = 3600
}