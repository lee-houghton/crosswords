variable gitlab_token {
    type = string
}

provider gitlab {
    token = var.gitlab_token
}

data gitlab_project crosswords {
    id = 5160373
}

data aws_region current {}
data aws_caller_identity current {}

resource gitlab_project_variable aws_access_key_id {
    project = data.gitlab_project.crosswords.id
    environment_scope = "*"
    key = "AWS_ACCESS_KEY_ID"
    value = aws_iam_access_key.gitlab-ci.id
}

resource gitlab_project_variable aws_secret_access_key {
    project = data.gitlab_project.crosswords.id
    environment_scope = "*"
    key = "AWS_SECRET_ACCESS_KEY"
    value = aws_iam_access_key.gitlab-ci.secret
}

resource gitlab_project_variable aws_default_region {
    project = data.gitlab_project.crosswords.id
    environment_scope = "*"
    key = "AWS_DEFAULT_REGION"
    value = "eu-west-2"
}

resource gitlab_project_variable ci_registry {
    project = data.gitlab_project.crosswords.id
    environment_scope = "*"
    key = "CI_REGISTRY"
    value = "${data.aws_caller_identity.current.account_id}.dkr.ecr.${data.aws_region.current.name}.amazonaws.com"
}

resource gitlab_project_variable ssh_ip {
    project = data.gitlab_project.crosswords.id
    environment_scope = "*"
    key = "SSH_IP"
    value = var.server_ipv4
}

resource gitlab_project_variable vite-api-url {
    project = data.gitlab_project.crosswords.id
    environment_scope = "*"
    key = "VITE_API_URL"
    value = "https://api.crosswords.leehoughton.dev"
}

resource gitlab_project_variable cloudfront_distribution_id {
    project = data.gitlab_project.crosswords.id
    environment_scope = "*"
    key = "CLOUDFRONT_DISTRIBUTION_ID"
    value = aws_cloudfront_distribution.website.id
}

resource aws_iam_user gitlab-ci {
    name = "gitlab-ci"
}

resource aws_iam_access_key gitlab-ci {
    user = aws_iam_user.gitlab-ci.name
}

data aws_iam_policy_document gitlab-ci-policy {
    statement {
        actions = [
            "cloudfront:CreateInvalidation",
            "ecr:GetAuthorizationToken",
            "ecr:GetDownloadUrlForLayer",
            "ecr:BatchGetImage",
            "ecr:BatchCheckLayerAvailability",
            "ecr:PutImage",
            "ecr:InitiateLayerUpload",
            "ecr:UploadLayerPart",
            "ecr:CompleteLayerUpload",
            "ecs:UpdateService"
        ]
        resources = ["*"]
    }

    statement {
        actions = ["s3:GetObject", "s3:PutObject", "s3:PutObjectAcl", "s3:ListBucket"]
        resources = [
            aws_s3_bucket.website.arn,
            "${aws_s3_bucket.website.arn}/*",
        ]
    }
}

resource aws_iam_user_policy gitlab-ci-policy {
    user = aws_iam_user.gitlab-ci.name
    policy = data.aws_iam_policy_document.gitlab-ci-policy.json
}
