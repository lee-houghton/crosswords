#!/usr/bin/env bash 
set +e 
pushd edge
npm install
npm run deploy:dev
popd
terraform apply 
./invalidate.sh
