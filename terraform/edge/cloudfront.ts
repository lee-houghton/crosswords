import type { CloudFrontResponseHandler, CloudFrontResponseResult, CloudFrontRequestHandler } from "aws-lambda";
import { randomBytes } from "crypto";
import S3 from "aws-sdk/clients/s3";

type Env = "dev" | "prod";

// Provided by webpack
declare const ENV_NAME: Env;
declare const API_URL: string;

const s3 = new S3({ region: "eu-west-2" });
const indexCache = new Map<Env, string>();

export const addSecurityHeaders: CloudFrontResponseHandler = async (event) => {
    const { request, response } = event.Records[0].cf;
    console.log(request);

    const headers = request.uri === "/service-worker.js" ? getServiceWorkerHeaders() : getAssetHeaders();
    for (const [key, value] of Object.entries(headers)) response.headers[key.toLowerCase()] = [{ key, value }];

    return response;
};

export const serveIndexPage: CloudFrontRequestHandler = async (event) => {
    const nonce = randomBytes(16).toString("base64");
    const headers = {
        ...getIndexHeaders(nonce),
        "cache-control": "public,must-revalidate",
        "content-type": "text/html",
        "content-encoding": "UTF-8",
    };

    const body = await getStaticContent("index.html");
    const metaTag = `<meta property="csp-nonce" content="${nonce}" />`; // For material-ui

    const response: CloudFrontResponseResult = {
        status: "200",
        statusDescription: "OK",
        body: body.replace("<head>", "<head>" + metaTag),
        bodyEncoding: "text",
        headers: {},
    };

    for (const [key, value] of Object.entries(headers)) response.headers![key.toLowerCase()] = [{ key, value }];

    return response;
};

async function getStaticContent(path: string) {
    const cached = indexCache.get(ENV_NAME);
    if (cached) return cached;

    const data = await s3
        .getObject({
            Bucket: "crosswords-web",
            Key: `${ENV_NAME}/${path}`,
        })
        .promise();

    const html = (data.Body as Buffer).toString("utf8");
    indexCache.set(ENV_NAME, html);
    return html;
}

const baseHeaders: Record<string, string> = {
    "X-Content-Type-Options": "nosniff",
    "X-Frame-Options": "DENY",
    "X-XSS-Protection": "1; mode=block",
    "Referrer-Policy": "same-origin",
    "Strict-Transport-Security": "max-age=63072000; includeSubDomains; preload",
};

function getAssetHeaders(): Record<string, string> {
    return {
        ...baseHeaders,
        "Content-Security-Policy": "default-src 'none'; script-src 'self'",
    };
}

function getServiceWorkerHeaders(): Record<string, string> {
    return {
        ...baseHeaders,
        "Content-Security-Policy": ["default-src 'none'", "script-src 'self'", "connect-src 'self'"].join("; "),
    };
}

function getIndexHeaders(nonce: string): Record<string, string> {
    return {
        ...baseHeaders,
        "Content-Security-Policy": [
            `default-src 'none'`,
            `img-src 'self' ${API_URL}`,
            `script-src 'self'`,
            `script-src-elem 'self'`,
            `style-src 'self' https://fonts.googleapis.com 'nonce-${nonce}'`,
            `font-src https://fonts.gstatic.com`,
            `connect-src 'self' ${API_URL} ${API_URL.replace(/^https\:/, "wss:")}`,
            `form-action 'none'`,
            `manifest-src 'self'`,
            `base-uri 'self'`,
            `child-src 'none'`,
            `frame-ancestors 'none'`,
            `object-src 'none'`,
            `worker-src 'self'`,
        ].join("; "),
    };
}
