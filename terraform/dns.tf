resource aws_route53_zone app {
    name = "crosswords.leehoughton.dev."

    tags = {
        env = "all"
        component = "infra"
    }
}

resource aws_route53_record mailgun_sending_verification {
    # Just hard-code this, because it apparently doesn't work even if you
    # apply just mailgun_domain.app first.
    count = 3 # length(mailgun_domain.app.sending_records)

    zone_id = aws_route53_zone.app.id
    name = "${mailgun_domain.app.sending_records[count.index].name}."
    type = mailgun_domain.app.sending_records[count.index].record_type
    ttl = 300

    // Ouch! The mailgun DNS records don't correspond exactly with what GCP needs.
    // - TXT records need to be surrounded with \"\" otherwise they will be split by whitespace
    // - CNAME records need to end with a .
    records = (
        mailgun_domain.app.sending_records[count.index].record_type == "CNAME" 
            ? ["${mailgun_domain.app.sending_records[count.index].value}."] 
            : mailgun_domain.app.sending_records[count.index].record_type == "TXT" 
                ? [mailgun_domain.app.sending_records[count.index].value] 
                : [mailgun_domain.app.sending_records[count.index].value]
    )
}
