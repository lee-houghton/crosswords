provider external {
    version = "~> 1.2"
}

resource aws_s3_bucket website {
    bucket = "crosswords-web"
    acl = "private"
    
    lifecycle {
        prevent_destroy = true
    }

    tags = {
        env = "dev"
        component = "frontend"
    }
}

resource aws_cloudfront_origin_access_identity website {
    comment = "Allows CloudFront to access the website's S3 bucket"
}

data aws_iam_policy_document s3_policy {
    statement {
        actions   = ["s3:GetObject", "s3:ListBucket"]
        resources = [
            aws_s3_bucket.website.arn,
            "${aws_s3_bucket.website.arn}/*",
        ]

        principals {
            type = "AWS"
            identifiers = [aws_cloudfront_origin_access_identity.website.iam_arn]
        }
    }
}

resource aws_s3_bucket_policy website {
    bucket = aws_s3_bucket.website.id
    policy = data.aws_iam_policy_document.s3_policy.json
}

resource aws_acm_certificate website {
    provider = aws.cloudfront

    domain_name = "crosswords.leehoughton.dev"
    validation_method = "DNS"

    tags = {
        env = "dev"
    }

    options { 
        certificate_transparency_logging_preference = "ENABLED"
    }

    lifecycle {
        create_before_destroy = true
    }
}

resource aws_route53_record validation {
    // Update if necessary, this should be derived from the 
    // certificate resource but terraform doesn't like that
    count = 1 

    zone_id = aws_route53_zone.app.id

    ttl = 60
    name = aws_acm_certificate.website.domain_validation_options[count.index].resource_record_name
    type = aws_acm_certificate.website.domain_validation_options[count.index].resource_record_type
    records = [aws_acm_certificate.website.domain_validation_options[count.index].resource_record_value]
}

resource aws_route53_record website {
    zone_id = aws_route53_zone.app.id

    name = "crosswords.leehoughton.dev."
    type = "A"

    alias {
        name = aws_cloudfront_distribution.website.domain_name
        zone_id = aws_cloudfront_distribution.website.hosted_zone_id
        evaluate_target_health = false
    }
}

resource aws_route53_record website_ipv6 {
    zone_id = aws_route53_zone.app.id

    name = "crosswords.leehoughton.dev."
    type = "AAAA"

    alias {
        name = aws_cloudfront_distribution.website.domain_name
        zone_id = aws_cloudfront_distribution.website.hosted_zone_id
        evaluate_target_health = false
    }
}

resource aws_acm_certificate_validation website {
    provider = aws.cloudfront

    certificate_arn = aws_acm_certificate.website.arn
    validation_record_fqdns = aws_route53_record.validation[*].name

    depends_on = [aws_acm_certificate.website]
}

data aws_lambda_function security_headers {
    provider = aws.cloudfront
    function_name = "crosswords-dev-security-headers"
}

data aws_lambda_function index_page {
    provider = aws.cloudfront
    function_name = "crosswords-dev-index-page"
}

data "external" "headers_lambda_version" {
  program = [
    "sh",
    "-c",
    "AWS_ACCESS_KEY_ID=${var.aws_access_key_id} AWS_SECRET_ACCESS_KEY=${var.aws_secret_access_key} aws --region us-east-1 lambda publish-version --function-name crosswords-dev-security-headers --query '{Version: Version}'",
  ]
}

data "external" "index_page_lambda_version" {
  program = [
    "sh",
    "-c",
    "AWS_ACCESS_KEY_ID=${var.aws_access_key_id} AWS_SECRET_ACCESS_KEY=${var.aws_secret_access_key} aws --region us-east-1 lambda publish-version --function-name crosswords-dev-index-page --query '{Version: Version}'",
  ]
}

resource aws_cloudfront_distribution website {
    origin {
        domain_name = aws_s3_bucket.website.bucket_regional_domain_name
        origin_id = "s3"
        origin_path = "/dev"

        s3_origin_config {
            origin_access_identity = aws_cloudfront_origin_access_identity.website.cloudfront_access_identity_path
        }
    }

    enabled = true
    is_ipv6_enabled = true 
    comment = "crosswords.leehoughton.dev"
    default_root_object = "index.html"
    price_class = "PriceClass_100" // Europe & US 
    aliases = ["crosswords.leehoughton.dev"]

    default_cache_behavior {
        allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
        cached_methods   = ["GET", "HEAD"]
        target_origin_id = "s3"
        compress = true

        forwarded_values {
            query_string = false

            cookies {
                forward = "none"
            }
        }

        lambda_function_association {
            event_type   = "origin-response"
            lambda_arn   = "${data.aws_lambda_function.security_headers.arn}:${data.external.headers_lambda_version.result.Version}"
            include_body = false
        }

        viewer_protocol_policy = "redirect-to-https"
        min_ttl                = 0
        default_ttl            = 3600
        max_ttl                = 86400
    }

    # The index.html page needs to change every time due to the CSP nonce
    ordered_cache_behavior {
        path_pattern = "/index.html"
        allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
        cached_methods   = ["GET", "HEAD"]
        target_origin_id = "s3"
        compress = true

        forwarded_values {
            query_string = false

            cookies {
                forward = "none"
            }
        }

        lambda_function_association {
            event_type   = "viewer-request"
            lambda_arn   = "${data.aws_lambda_function.index_page.arn}:${data.external.index_page_lambda_version.result.Version}"
            include_body = false
        }

        viewer_protocol_policy = "redirect-to-https"
        min_ttl                = 0
        default_ttl            = 3600
        max_ttl                = 86400
    }

    restrictions {
        geo_restriction {
            restriction_type = "none"
        }
    }

    viewer_certificate {
        acm_certificate_arn = aws_acm_certificate.website.arn
        ssl_support_method = "sni-only"
        minimum_protocol_version = "TLSv1.2_2018"
    }

    

    tags = {
        env = "dev"
        component = "frontend"
    }

    wait_for_deployment = false
}
