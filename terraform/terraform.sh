#!/bin/sh
set +e
docker build . -t crosswords/terraform
docker run --rm -it -u $(id -u):$(id -g) -v ~/.aws/credentials:/tmp/.aws/credentials:ro -v $(realpath ~/.terraformrc):/tmp/.terraformrc:ro -v $(realpath ./.terraform.d):/tmp/.terraform.d -v $(pwd):/workspace crosswords/terraform $@
