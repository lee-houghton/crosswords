variable "mailgun_key" {
    type = string
}

provider "mailgun" {
    api_key = var.mailgun_key
}

resource random_password mailgun {
    length = 32
    special = true
}

resource "mailgun_domain" "app" {
    name = "crosswords.leehoughton.dev"
    region = "eu"
    spam_action = "disabled"
    wildcard = false
    smtp_password = random_password.mailgun.result
}
