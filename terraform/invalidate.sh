#!/bin/sh
set +e
ID=$(aws --profile crosswords cloudfront list-distributions --query "DistributionList.Items[?Comment=='crosswords.leehoughton.dev'].Id" --output text)
[ "$ID" == "" ] && echo "Distribution not found" && exit 1
aws --profile crosswords cloudfront create-invalidation --distribution-id $ID --paths /index.html /service-worker.js /favicon.ico
