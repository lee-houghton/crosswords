/**
 * Creates a VPC with a /16 block. Each subnet gets a /20 which is 4096 IPs.
 */

variable name { type = string }

variable number { 
    description = "Unique VPC number (1-255)"
    type = string
}

variable internet_gateway {
    description = "If true, create an internet gateway"
    default = false
}

locals {
    tags = {
        Name = var.name
        env = "shared"
        app = "shared"
    }
}

resource aws_vpc vpc {
    cidr_block = "10.${var.number}.0.0/16"
    tags = local.tags

    enable_dns_hostnames = true
    enable_dns_support = true
}

data aws_availability_zones azs {
    state = "available"
}

resource aws_subnet subnets {
    count = length(data.aws_availability_zones.azs.names)

    vpc_id = aws_vpc.vpc.id
    availability_zone = data.aws_availability_zones.azs.names[count.index]
    cidr_block = "10.${var.number}.${16 * count.index}.0/20"

    tags = merge(local.tags, {
        Name = "${var.name}-${data.aws_availability_zones.azs.names[count.index]}"
    })
}

resource aws_route_table main_route_table {
    vpc_id = aws_vpc.vpc.id
    tags = local.tags
}

resource aws_internet_gateway default {
    count = var.internet_gateway ? 1 : 0
    vpc_id = aws_vpc.vpc.id
    tags = local.tags
}

resource aws_route internet_via_gateway {
    count = var.internet_gateway ? 1 : 0
    route_table_id = aws_route_table.main_route_table.id
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default[count.index].id
}

resource aws_route_table_association routes {
    count = var.internet_gateway ? length(data.aws_availability_zones.azs.names) : 0
    subnet_id = aws_subnet.subnets[count.index].id
    route_table_id = aws_route_table.main_route_table.id
}

output vpc { value = aws_vpc.vpc }
output subnet_ids { value = aws_subnet.subnets[*].id }
output route_table { value = aws_route_table.main_route_table }
