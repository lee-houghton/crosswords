resource aws_ssm_parameter tag {
    name = "/${var.env}/crosswords/tag"
    type = "String"
    value = "latest"
    lifecycle {
        ignore_changes = [value]
    }
}

resource aws_ssm_parameter db-password {
    name = "/${var.env}/crosswords/db-password"
    type = "SecureString"
    value = var.environment.TYPEORM_PASSWORD
}

resource aws_ssm_parameter session-secret {
    name = "/${var.env}/crosswords/session-secret"
    type = "SecureString"
    value = var.environment.SESSION_SECRET
}

resource aws_ssm_parameter mailgun-api-key {
    name = "/${var.env}/crosswords/mailgun-api-key"
    type = "SecureString"
    value = var.environment.MAILGUN_API_KEY
}

data aws_ssm_parameter google-client-id {
    name = "/${var.env}/crosswords/google-client-id"
}

data aws_ssm_parameter google-client-secret {
    name = "/${var.env}/crosswords/google-client-secret"
}

data aws_ssm_parameter facebook-client-id {
    name = "/${var.env}/crosswords/facebook-client-id"
}

data aws_ssm_parameter facebook-client-secret {
    name = "/${var.env}/crosswords/facebook-client-secret"
}
