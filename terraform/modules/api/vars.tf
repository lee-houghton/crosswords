variable env { type = string }

variable environment {
    description = "Environment variables for the service"
    type = object({
        TYPEORM_HOST = string
        TYPEORM_PORT = string
        TYPEORM_USERNAME = string
        TYPEORM_DATABASE = string
        REDIS_HOST = string
        MAIL_DOMAIN = string
        CORS_ORIGIN = string
        FRONTEND_URL = string
        API_URL = string
        TYPEORM_PASSWORD = string
        SESSION_SECRET = string
        MAILGUN_API_KEY = string
        ELASTICSEARCH_NODE = string
    })
}

variable vpc { type = object({ id = string }) }
variable subnet_ids { type = list(string) }
variable security_group_ids { type = list(string) }
variable listener { type = object({ arn = string }) }
variable priority { type = number }
variable domain { type = string }
variable zone { type = object({ id = string}) }

variable load_balancer {
    type = object({
        dns_name = string
        zone_id = string
        vpc_id = string
    })
}

variable repo {
    description = "ECR repository for docker images (aws_ecr_repository)"
    type = object({
        repository_url = string
    })
}

locals {
    tags = {
        env = var.env
        app = "crosswords"
        component = "api"
    }
}
