resource aws_acm_certificate service {
    domain_name = var.domain
    validation_method = "DNS"
    tags = local.tags
    lifecycle { create_before_destroy = true }
}

resource aws_route53_record validation {
    // Update if necessary, this should be derived from the 
    // certificate resource but terraform doesn't like that
    count = 1 

    zone_id = var.zone.id

    ttl = 60
    name = aws_acm_certificate.service.domain_validation_options[count.index].resource_record_name
    type = aws_acm_certificate.service.domain_validation_options[count.index].resource_record_type
    records = [aws_acm_certificate.service.domain_validation_options[count.index].resource_record_value]
}

resource aws_acm_certificate_validation service {
    certificate_arn = aws_acm_certificate.service.arn
    validation_record_fqdns = aws_route53_record.validation[*].name
}

resource aws_lb_listener_certificate https {
    certificate_arn = aws_acm_certificate.service.arn
    listener_arn = var.listener.arn
    depends_on = [aws_acm_certificate_validation.service]
}

output certificate {
    value = aws_acm_certificate.service
}