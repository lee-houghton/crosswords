resource aws_lb_target_group api {
    name_prefix = "${substr(var.env, 0, 3)}-"
    target_type = "ip"
    port = 8080 // ECS will override this anyway, I think
    protocol = "HTTP"
    vpc_id = var.load_balancer.vpc_id

    health_check {
        enabled = true
        interval = 10
        protocol = "HTTP"
        path = "/status"
        port = "traffic-port"
        timeout = 5
        healthy_threshold = 2
        unhealthy_threshold = 2
        matcher = "200-299"
    }

    tags = local.tags
}

resource aws_lb_listener_rule api {
    listener_arn = var.listener.arn
    priority = var.priority

    action {
        type = "forward"
        target_group_arn = aws_lb_target_group.api.arn
    }

    condition {
        host_header {
            values = [var.domain]
        }
    }
}
