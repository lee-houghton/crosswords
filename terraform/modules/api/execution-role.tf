resource aws_iam_role execution {
    name_prefix = "crosswords-api-${var.env}-exec-"

    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

    lifecycle { 
        create_before_destroy = true
    }
}

data aws_iam_policy_document execution {
    statement {
        actions = [
            "ecr:BatchCheckLayerAvailability",
            "ecr:GetDownloadUrlForLayer",
            "ecr:BatchGetImage",
            "ecr:GetAuthorizationToken"
        ]
        resources = ["*"]
    }
    statement {
        actions = [
            "ecr:GetAuthorizationToken"
        ]
        resources = ["*"]
    }
    statement {
        actions = ["logs:CreateLogStream", "logs:PutLogEvents"]
        resources = ["arn:aws:logs:*:*:log-group:/${var.env}/crosswords/api:*"]
    }
    statement {
        actions = ["ssm:GetParameter*"]
        resources = ["arn:aws:ssm:*:*:parameter/${var.env}/crosswords/*"]
    }
}

resource aws_iam_role_policy execution {
    role = aws_iam_role.execution.name
    policy = data.aws_iam_policy_document.execution.json
}
