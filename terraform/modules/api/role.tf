resource aws_iam_role task {
    name_prefix = "api-task-${var.env}-"

    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

    tags = {
        env = var.env
        app = "crosswords"
        component = "api"
    }
}

output task-role {
    value = aws_iam_role.task
}
