data aws_region current {}

resource aws_ecs_cluster api {
    name = "crosswords-${var.env}"
    tags = local.tags
    capacity_providers = ["FARGATE", "FARGATE_SPOT"]
    setting {
        name = "containerInsights"
        value = "disabled"
    }
    default_capacity_provider_strategy {
        capacity_provider = "FARGATE_SPOT"
        weight = 1
        base = 1
    }
}

// TODO Store in SSM
variable image { 
    type = string
    default = "latest"
}

resource aws_ecs_task_definition api {
    family = "crosswords-api"
    cpu = 256
    memory = 512
    requires_compatibilities = ["FARGATE"]
    network_mode = "awsvpc"
    execution_role_arn = aws_iam_role.execution.arn
    task_role_arn = aws_iam_role.task.arn
    container_definitions = <<EOF
[
    {
        "name": "api",
        "image": "${var.repo.repository_url}:${var.image}",
        "cpu": 256,
        "memory": 512,
        "essential": true,
        "portMappings": [{ "containerPort": 8080, "hostPort": 8080 }],
        "environment": [
            { "name": "TYPEORM_CONNECTION", "value": "postgres" },
            { "name": "TYPEORM_HOST", "value": "${var.environment.TYPEORM_HOST}" },
            { "name": "TYPEORM_PORT", "value": "${var.environment.TYPEORM_PORT}" },
            { "name": "TYPEORM_USERNAME", "value": "${var.environment.TYPEORM_USERNAME}" },
            { "name": "TYPEORM_DATABASE", "value": "${var.environment.TYPEORM_DATABASE}" },
            { "name": "TYPEORM_SYNCHRONIZE", "value": "false" },
            { "name": "TYPEORM_LOGGING", "value": "true" },
            { "name": "REDIS_HOST", "value": "${var.environment.REDIS_HOST}" },
            { "name": "MAIL_DOMAIN", "value": "${var.environment.MAIL_DOMAIN}" },
            { "name": "CORS_ORIGIN", "value": "${var.environment.CORS_ORIGIN}" },
            { "name": "FRONTEND_URL", "value": "${var.environment.FRONTEND_URL}" },
            { "name": "API_URL", "value": "${var.environment.API_URL}" },
            { "name": "ELASTICSEARCH_NODE", "value": "${var.environment.ELASTICSEARCH_NODE}" }
        ],
        "secrets": [
            { "name": "TYPEORM_PASSWORD", "valueFrom": "${aws_ssm_parameter.db-password.name}" },
            { "name": "SESSION_SECRET", "valueFrom": "${aws_ssm_parameter.session-secret.name}" },
            { "name": "MAILGUN_API_KEY", "valueFrom": "${aws_ssm_parameter.mailgun-api-key.name}" },
            { "name": "GOOGLE_CLIENT_ID", "valueFrom": "${data.aws_ssm_parameter.google-client-id.name}" },
            { "name": "GOOGLE_CLIENT_SECRET", "valueFrom": "${data.aws_ssm_parameter.google-client-secret.name}" },
            { "name": "FACEBOOK_CLIENT_ID", "valueFrom": "${data.aws_ssm_parameter.facebook-client-id.name}" },
            { "name": "FACEBOOK_CLIENT_SECRET", "valueFrom": "${data.aws_ssm_parameter.facebook-client-secret.name}" }
        ],
        "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
                "awslogs-group": "${aws_cloudwatch_log_group.api.name}",
                "awslogs-region": "${data.aws_region.current.name}",
                "awslogs-stream-prefix": "task/"
            }
        }
    }
]
EOF
    tags = local.tags
}

resource aws_ecs_service api {
    name = "crosswords-${var.env}"
    cluster = aws_ecs_cluster.api.arn
    launch_type = "FARGATE"
    propagate_tags = "TASK_DEFINITION"
    enable_ecs_managed_tags = true 
    desired_count = 1
    task_definition = "${aws_ecs_task_definition.api.family}:${aws_ecs_task_definition.api.revision}"
    
    network_configuration {
        subnets = var.subnet_ids
        security_groups = var.security_group_ids
        assign_public_ip = true
    }

    load_balancer {
        container_name = "api"
        container_port = "8080"
        target_group_arn = aws_lb_target_group.api.arn
    }

    lifecycle {
        ignore_changes = [desired_count]
    }
}
