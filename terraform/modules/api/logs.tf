resource aws_cloudwatch_log_group api {
    name = "/${var.env}/crosswords/api"
    retention_in_days = 14
}

