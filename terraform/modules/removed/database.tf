resource "google_sql_database_instance" "instance" {
  name = "crosswords-db"
  region = "europe-west2"
  database_version = "POSTGRES_11"
  settings {
    tier = "db-f1-micro"
    ip_configuration {
      ipv4_enabled = false
      private_network = google_compute_network.vpc.self_link
    }
  }
  depends_on = [google_service_networking_connection.private_vpc_connection]
}

resource "google_sql_database" "database" {
  name     = "crosswords-db"
  instance = google_sql_database_instance.instance.name
}

resource "random_password" "database_user_password" {
  length = 32
  special = false
}

resource "google_sql_user" "user" {
  instance = google_sql_database_instance.instance.name
  name = "crosswords"
  password = random_password.database_user_password.result
}
