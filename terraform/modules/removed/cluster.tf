provider random {
}

resource random_password cluster_password {
    length = 16
    special = true
}

resource "google_container_cluster" "primary" {
    name = "crosswords"

    remove_default_node_pool = true
    initial_node_count = 1

    master_auth {
        username = "crosswords"
        password = random_password.cluster_password.result
    }
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
    name = "crosswords-nodes"
    cluster = google_container_cluster.primary.name
    
    node_count = 1
    autoscaling {
        min_node_count = 0
        max_node_count = 4
    }

    node_config {
        preemptible  = true
        machine_type = "g1-small"

        metadata = {
            disable-legacy-endpoints = "true"
        }

        oauth_scopes = [
            "https://www.googleapis.com/auth/logging.write",
            "https://www.googleapis.com/auth/monitoring",
        ]
    }
}
