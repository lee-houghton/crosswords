resource google_storage_bucket website {
    name = "crosswords-web"

    website {
        main_page_suffix = "index.html"
        not_found_page = "index.html"
    }

    location = "EU"

    lifecycle {
        prevent_destroy = true
    }
}

resource google_storage_bucket_iam_member gitlab_website_upload {
    bucket = google_storage_bucket.website.name
    role   = "roles/storage.admin"
    member = "serviceAccount:${google_service_account.gitlab-ci.email}"
}

