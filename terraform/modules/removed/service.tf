resource kubernetes_config_map api {
    metadata {
        name = "api-config"
    }

    data = {
        TYPEORM_CONNECTION = "postgres"
        TYPEORM_HOST = google_sql_database_instance.instance.private_ip_address
        TYPEORM_PORT = "5432"
        TYPEORM_USERNAME = google_sql_user.user.name
        TYPEORM_DATABASE = google_sql_database.database.name
        TYPEORM_SYNCHRONIZE = "false"
        TYPEORM_LOGGING = "true"
        REDIS_HOST = "redis"
        NODE_ENV = "production"
        MAIL_DOMAIN = mailgun_domain.app.name
    }
}

resource random_password api_session_secret {
    length = 32
    special = true
}

resource kubernetes_secret api {
    metadata {
        name = "api-secrets"
    }

    data = {
        TYPEORM_PASSWORD = google_sql_user.user.password
        SESSION_SECRET = random_password.api_session_secret.result
        MAILGUN_API_KEY = var.mailgun_key
    }
}

resource kubernetes_deployment api {
    metadata {
        name = "api"

        labels = {
            app = "crosswords"
            tier = "backend"
        }
    }

    spec {
        replicas = 2
        selector {
            match_labels = {
                app = "crosswords"
                tier = "backend"
            }
        }
        template {
            metadata {
                labels = {
                    app = "crosswords"
                    tier = "backend"
                }
            }
            spec {
                container {
                    name = "api"
                    image = "gcr.io/crosswords/api:latest" // TODO
                    port {
                        container_port = 80
                    }
                    env_from {
                        config_map_ref {
                            name = "api-config"
                        }
                    }
                    env_from {
                        secret_ref {
                            name = "api-secrets"
                        }
                    }
                    resources {
                        requests {
                            cpu = "100m"
                            memory = "100Mi"
                        } 
                    }
                }
            }
        }
    }
}

resource kubernetes_service api {
    metadata {
        name = "api"

        labels = {
            app = "crosswords"
            tier = "api"
        }
    }
    spec {
        selector = {
            app = "crosswords"
        }
        session_affinity = "ClientIP"
        port {
            port = 8080
            target_port = 80
        } 
        type = "LoadBalancer"
    }
}

resource aws_route53_record api {
    zone_id = aws_route53_zone.app.id

    name = "api.${aws_route53_zone.app.name}"
    type = "A"
    ttl = 60
    records = kubernetes_service.api.load_balancer_ingress[*].ip
}