resource google_service_account gitlab-ci {
    account_id = "gitlab-ci"
    display_name = "gitlab-ci"
    description = "Account to allow GitLab CI to update images to GCR"
}

resource google_service_account_key gitlab-ci {
    service_account_id = google_service_account.gitlab-ci.account_id
}

// GCR repositories aren't real resources.
// For example, there isn't a gcloud command to create one.
//
// To control access you simply control access to the underlying bucket.
resource "google_storage_bucket_iam_member" "member" {
    bucket = "artifacts.crosswords.appspot.com"
    role   = "roles/storage.admin"
    member = "serviceAccount:${google_service_account.gitlab-ci.email}"
}
