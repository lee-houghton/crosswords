data "google_client_config" "default" {}

provider "kubernetes" {
    host = google_container_cluster.primary.endpoint
    // username = google_container_cluster.primary.master_auth[0].username
    // password = google_container_cluster.primary.master_auth[0].password
    load_config_file = false
    token = data.google_client_config.default.access_token

    client_certificate = base64decode(google_container_cluster.primary.master_auth[0].client_certificate)
    client_key = base64decode(google_container_cluster.primary.master_auth[0].client_key)
    cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth[0].cluster_ca_certificate)
}
