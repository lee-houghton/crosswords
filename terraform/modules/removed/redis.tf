resource kubernetes_deployment redis {
    metadata {
        name = "redis"
        labels = {
            app = "redis"
            role = "master"
            tier = "api"
        }
    }
    spec {
        replicas = 1
        selector {
            match_labels = {
                app = "redis"
                role = "master"
                tier = "backend"
            }
        }
        template {
            metadata {
                labels = {
                    app = "redis"
                    role = "master"
                    tier = "backend"
                }
            }
            spec {
                container {
                    name = "master"
                    image = "k8s.gcr.io/redis:e2e"
                    port {
                        container_port = 6379
                    }
                    resources {
                        requests {
                            cpu = "50m"
                            memory = "50Mi"
                        }
                    }
                }
            }
        }
    }
}

resource kubernetes_service redis-master {
    metadata {
        name = "redis-master"

        labels = {
            app = "redis"
            role = "master"
            tier = "backend"
        }
    }

    spec {
        selector = {
            app = "redis"
            role = "master"
            tier = "backend"
        }

        port {
            port = 6379
            target_port = 6379
        }
    }
}