# How to run terraform

You must have docker installed on a Linux system. Then, run `./terraform.sh` to run a docker container. Your working directory is mounted into the container.

## Applying the templates

Run `terraform apply` to apply any changes made to the templates.

If it is your first time running terraform on this machine, or you made changes to any `provider` blocks, you need to run `terraform init` first. This downloads all the required providers for creating resources in the various clouds.

## Gotchas

### New environment

If creating a new environment, it is necessary to apply some small parts of the templates
before applying the entire configuration. _You may also have to run these steps when upgrading
to a newer version of terraform_.

Resource|Reason
-|-
`terraform apply -target mailgun_domain.app`|This is needed because it outputs an unknown number of DNS records and Terraform can't plan the rest of the configuration until it knows how many there will be.
`terraform apply -target aws_vpc_peering_connection.dev-internal`|This is needed because of reasons. Maybe it's not even needed?
`terraform apply -target aws_db_instance.rds`|This is needed because the RDS instance's host name is used for the postgresql provider's configuration so it needs to be created first.

### Terraform permissions

Owner permissions may be required for the terraform service account in order to create the `google_service_networking_connection.private_vpc_connection` resource.