provider "google" {
    version = "~> 3.0.0-beta.1"
    project = "crosswords"
    region = "europe-west2"
    zone = "europe-west2-a"
    credentials = file("crosswords-911d9622cec9.json")
}
