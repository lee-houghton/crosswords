FROM node:14 as build

    RUN mkdir /work
    WORKDIR /work

    # Files necessary for caching node_modules
    COPY package.json yarn.lock .yarnrc.yml ./
    COPY .yarn/ ./.yarn/
    COPY api/package.json ./api/
    RUN yarn workspaces focus @asztal/crosswords-api

    # Files necessary for building
    COPY api/assets/ ./api/assets/
    COPY api/webpack.config.js api/tsconfig.json ./api/
    COPY api/src/ ./api/src/

    WORKDIR /work/api
    RUN yarn build

FROM build as install

    RUN rm -rf node_modules && yarn workspaces focus --production @asztal/crosswords-api

FROM node:14 as run

    COPY --from=build /work/api/dist/ /app/dist/
    COPY --from=build /work/api/assets/ /app/assets/
    COPY --from=build /work/api/package.json /work/yarn.lock /app/
    COPY --from=build /work/node_modules /app/node_modules
    COPY --from=build /work/api/node_modules /app/node_modules
    WORKDIR /app

    EXPOSE 3000

    CMD yarn start
