const { spawn } = require("child_process");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const { resolve } = require("path");
const process = require("process");
const TerserPlugin = require("terser-webpack-plugin");
const nodeExternals = require("webpack-node-externals");
const { HotModuleReplacementPlugin } = require("webpack");

const POLL_ENTRY_POINT = "webpack/hot/poll?250";

/** @type {import("webpack").ConfigurationFactory}*/
module.exports = (_env, argv) => {
    const dev = process.env.NODE_ENV || process.env.NODE_ENV === "development";

    return {
        devtool: dev ? "eval-cheap-source-map" : "source-map",
        mode: dev ? "development" : "production",
        entry: [
            dev && POLL_ENTRY_POINT,
            resolve(__dirname, "src/main.ts")
        ].filter(Boolean),
        externals: [
            nodeExternals({ 
                allowlist: dev ? [POLL_ENTRY_POINT] : [],
                additionalModuleDirs: [
                    resolve(__dirname, "../node_modules")
                ],
            })
        ],
        module: {
            rules: [
                {
                    exclude: [resolve(__dirname, "node_modules")],
                    test: /\.ts$/,
                    use: "ts-loader"
                },
                {
                    exclude: /node_modules/,
                    test: /\.graphql$/,
                    use: "graphql-tag/loader"
                }
            ]
        },
        optimization: dev ? {} : {
            minimizer: [
                new TerserPlugin({
                    terserOptions: {
                        keep_fnames: true,
                    }
                })
            ]
        },
        output: {
            filename: "server.js",
            path: resolve(__dirname, "dist")
        },
        plugins: [
            new CleanWebpackPlugin(),
            dev && new HotModuleReplacementPlugin(),
            argv.watch && {
                /** @type {import("child_process").ChildProcessWithoutNullStreams | undefined} */
                child: undefined,
                apply(/** @type {import("webpack").Compiler} */ compiler) {
                    compiler.hooks.afterEmit.tap("RunServerOnEmit", compilation => {
                        if (compilation.errors.length > 0) {
                            if (this.child) {
                                process.stderr.write(`[webpack-development] Killing server process due to errors\n`);
                                this.child.kill("SIGTERM");
                                this.child = undefined;
                            }

                            return;
                        }

                        if (this.child)
                            return;

                        this.child = spawn("node", [resolve(__dirname, "dist/server")]);
                        this.child.stdout.on("data", data => process.stdout.write(data));
                        this.child.stderr.on("data", data => {
                            process.stderr.write(data)
                        });
                        this.child.on("close", code => {
                            process.stderr.write(`[webpack-development] Server process exited with code: ${code}\n`);
                            this.child = undefined;
                        });
                    });
                },
            }
        ].filter(Boolean),
        resolve: {
            extensions: [".ts", ".js"],
        },
        target: "node",
    }
};
