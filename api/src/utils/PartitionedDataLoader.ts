import DataLoader from "dataloader";

export class PartitionedDataLoader<P, K, V, C = K> {
    private loaders = new Map<P, DataLoader<K, V, C>>();

    public constructor(
        private readonly loader: (partition: P, keys: readonly K[]) => Promise<V[]>,
        private readonly options?: DataLoader.Options<K, V, C>
    ) {
    }

    public getLoader(partition: P) {
        let loader = this.loaders.get(partition);
        if (!loader) {
            loader = new DataLoader(keys => this.loader(partition, keys), this.options);
            this.loaders.set(partition, loader);
        }

        return loader;
    }
    
    public load(partition: P, key: K) {
        return this.getLoader(partition).load(key);
    }

    public loadMany(partition: P, keys: K[]) {
        return this.getLoader(partition).loadMany(keys);
    }

    /**
     * Replicate the V1 behaviour of DataLoader, where all loads must succeed.
     */
    public loadAllOrNothing(partition: P, keys: K[]) {
        const loader = this.getLoader(partition);
        return Promise.all(keys.map(key => loader.load(key)));
    }
}
