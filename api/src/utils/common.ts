import { existsSync } from "fs";
import { join, dirname } from "path";

function findPackageRoot(path: string) {
    if (existsSync(join(path, "package.json")))
        return path;
    
    const parent = dirname(path);
    if (parent === path)
        throw new Error("Unable to find package.json");
    
    return findPackageRoot(parent);
}

export const packageRoot = findPackageRoot(__dirname);
