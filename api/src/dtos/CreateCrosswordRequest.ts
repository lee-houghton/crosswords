import { ArrayMinSize, IsInt, IsOptional, Length, Max, Min, IsString } from "class-validator";

export class CreateCrosswordRequest {
    @IsOptional()
    @IsInt({ each: true })
    @ArrayMinSize(1)
    public deckIds?: number[];

    @IsString()
    @IsOptional()
    public languageId?: string;

    @Length(0, 50)
    @IsOptional()
    public title?: string;

    @Length(0, 1000, {  })
    @IsOptional()
    public description?: string;

    @IsOptional()
    @Min(10)
    @Max(28)
    public width?: number;

    @IsOptional()
    @Min(10)
    @Max(28)
    public height?: number;
}
