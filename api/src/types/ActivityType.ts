export enum ActivityType {
    DeckCreated = 100,
    DeckTermsAdded = 101,
    CrosswordsCompleted = 200
}
