import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, Index } from "typeorm";
import { Deck } from "./Deck";

@Entity()
@Index(["deckId", "order"]) // This should be unique, but typeorm temporarily generates duplicates
export class Term {
    public constructor(fields?: Partial<Term>) {
        if (fields) Object.assign(this, fields);
    }

    @PrimaryGeneratedColumn()
    public id!: number;

    // Should this be nullable?
    // I guess if a Term is deleted from a Deck, it should maybe still exist.
    @Column({ nullable: true })
    public deckId!: number;

    /**
     * The Deck which owns this Term.
     */
    @ManyToOne((type) => Deck, (deck) => deck.terms)
    public deck!: Deck & {};

    /**
     * The term which is to be learned.
     * In a crosswords, this the answer.
     */
    @Column()
    term!: string;

    /**
     * The definitions for this term.
     * In a crossword, this is the clue.
     */
    @Column("text", { array: true })
    definitions!: string[];

    /**
     * Guide for pronunciation of the clue.
     */
    @Column()
    pronunciation!: string;

    /**
     * Keep track of what order the terms appear in
     */
    @Column({ nullable: false })
    order!: number;
}
