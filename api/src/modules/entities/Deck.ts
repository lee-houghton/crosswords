import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { LanguageId } from "../services/LanguageService";
import { Crossword } from "./Crossword";
import { Term } from "./Term";
import { User } from "./User";

// @ViewEntity({
//     materialized: true,
//     expression: `
//         select "deckId", count(*) as stars
//         from user_favourite_decks_deck
//         group by "deckId"
//     `
// })
// @Index(["deckId"], { unique: true })
// class DeckStarCount {
//     @ViewColumn()
//     deckId!: number;

//     @ViewColumn()
//     stars!: number;
// }

@Entity()
export class Deck {
    public constructor(id?: number) {
        if (id) this.id = id;
    }

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column()
    public userId!: number;

    @Column()
    public title!: string;

    @Column()
    public description!: string;

    @ManyToOne((type) => User, (u) => u.decks)
    public user!: User & {};

    @OneToMany((type) => Term, (t) => t.deck, { cascade: true })
    public terms!: Term[];

    @Column({ type: "varchar", length: 2, nullable: false })
    public termLanguage!: LanguageId;

    @Column({ type: "varchar", length: 2, nullable: false })
    public definitionLanguage!: LanguageId;

    /** Denormalised count of terms in the set */
    @Column({ type: "int", default: 0 })
    public termCount!: number;

    // Well, that and its relation to other entities
    @ManyToMany((type) => Crossword, (cw) => cw.decks)
    @JoinTable()
    public crosswords!: Crossword[];

    /** How many users have marked this word list as a favourite. */
    @Column({ type: "int", default: 0, nullable: false })
    public stars!: number;

    /** How many crosswords have been made using this word list. */
    @Column({ type: "int", default: 0, nullable: false })
    public crosswordCount!: number;
}
