import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Deck } from "./Deck";
import { Crossword } from "./Crossword";
import { Term } from "./Term";

@Entity()
export class Clue {
    public constructor(fields?: Partial<Clue>) {
        if (fields) Object.assign(this, fields);
    }

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column({ type: "smallint" })
    public x!: number;

    @Column({ type: "smallint" })
    public y!: number;

    @Column({ length: 1, enum: ["a", "d"] })
    public direction!: "a" | "d";

    @Column({ type: "smallint" })
    public number!: number;

    @Column({ length: 50 })
    public lengthHint!: string;

    @Column({ length: 500 })
    public clue!: string;

    // This can be smaller as it has to fit into the crossword
    @Column({ length: 32 })
    public answer!: string;

    // Answer before splitting by , and ; and removing parenthesised parts
    //
    // E.g. if clue is "open" answer might be "ouvert" and originalAnswer
    // might be "ouvert(e) (adj.), ouvrir (v.)"
    @Column({ length: 500 })
    public originalAnswer!: string;

    // Deck from which the clue originated
    @Column()
    public deckId!: number;

    @ManyToOne(() => Deck)
    public deck!: Deck;

    // Term from which the clue originated
    @Column({ nullable: true })
    public termId?: number;

    @ManyToOne(() => Term, { nullable: true })
    public term?: Term;

    @Column()
    public crosswordId!: number;

    @ManyToOne(() => Crossword)
    public crossword!: Crossword & {};
}
