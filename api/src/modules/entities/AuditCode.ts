// Note: when changing these, you must also change the GraphQL enums in src/modules/schema/Admin.graphql

export enum DeckAuditCode {
    DeckCreated = 100,
    DeckUpdated = 101,
    DeckDeleted = 102,
    DeckTermEdited = 103,
}

export enum CrosswordAuditCode {
    CrosswordCreated = 200,
    CrosswordGenerated = 201,
    CrosswordError = 202,
    CrosswordCompleted = 203,
}

export enum SecurityAuditCode {
    PasswordUpdated = 300,
    ForgotPassword = 301,
    PasswordReset = 302,
    PasswordResetSuccess = 303,
    EmailChangeRequested = 310,
    EmailChangeSuccess = 311,
    AccountRegistered = 320,
    AccountActivated = 321,
    GoogleAccountLinked = 330,
    GoogleAccountRemoved = 331,
    FacebookAccountLinked = 340,
    FacebookAccountRemoved = 341,
}

export enum UserAuditCode {
    UserFollowed = 400,
    UserUnfollowed = 401,
}

export type AuditCode = DeckAuditCode | CrosswordAuditCode | SecurityAuditCode | UserAuditCode;

export const AuditCode = {
    ...DeckAuditCode,
    ...CrosswordAuditCode,
    ...SecurityAuditCode,
    ...UserAuditCode
};

// The "& string" excludes the `number` index signature. 
// (Since `AuditCode[303]` is valid JS and returns PasswordResetSuccess)
export type AuditCodeName = keyof typeof AuditCode & string;
