import { Entity } from "typeorm";
import { UserAccountRequest } from "./UserAccountRequest";

@Entity()
export class ResetPasswordRequest extends UserAccountRequest {
    public constructor(fields?: Partial<ResetPasswordRequest>) {
        super(fields);
        if (fields)
            Object.assign(this, fields);
    }
}
