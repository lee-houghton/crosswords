import { Column, Entity, ManyToOne, PrimaryColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { User } from "./User";

@Entity()
export class UserLanguage {
    public constructor(fields?: Partial<UserLanguage>) {
        if (fields) Object.assign(this, fields);
    }

    @PrimaryColumn()
    userId!: number;

    @ManyToOne((type) => User, (user) => user.languages)
    user!: User & {};

    /** ISO-639-1 code for the language */
    @PrimaryColumn({ type: "varchar", length: 2 })
    languageId!: string;

    /** Number of word lists the user has created for this language */
    @Column({ default: 0 })
    decks!: number;

    @Column({ default: 0 })
    favourites!: number;

    @Column({ default: 0 })
    crosswordsCreated!: number;

    @Column({ default: 0 })
    crosswordsCompleted!: number;

    @CreateDateColumn()
    started!: Date;

    @UpdateDateColumn()
    lastActivity!: Date;
}
