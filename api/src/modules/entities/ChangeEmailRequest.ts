import { Column, Entity } from "typeorm";
import { UserAccountRequest } from "./UserAccountRequest";

@Entity()
export class ChangeEmailRequest extends UserAccountRequest {
    public constructor(fields?: Partial<ChangeEmailRequest>) {
        super(fields);
        if (fields)
            Object.assign(this, fields);
    }

    @Column()
    public newEmail!: string;
}
