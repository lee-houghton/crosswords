import { Column, Entity, Index, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Attempt } from "./Attempt";

@Entity()
@Index(["attemptId", "x", "y"], { unique: true })
export class Guess {
    public constructor(fields?: Partial<Guess>) {
        if (fields) Object.assign(this, fields);
    }

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column({ nullable: false })
    public attemptId!: number;

    @ManyToOne(() => Attempt)
    public attempt!: Attempt & {};

    @Column({ nullable: false, type: "smallint" })
    public x!: number;

    @Column({ nullable: false, type: "smallint" })
    public y!: number;

    @Column({ nullable: false, type: "text" })
    public contents!: string;
}
