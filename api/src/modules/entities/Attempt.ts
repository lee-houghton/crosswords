import { Column, CreateDateColumn, Entity, Index, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Crossword } from "./Crossword";
import { Guess } from "./Guess";
import { User } from "./User";

@Entity()
@Index(["crosswordId", "userId"], { unique: true })
export class Attempt {
    public constructor(fields?: Partial<Attempt>) {
        if (fields)
            Object.assign(this, fields);
    }

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column({ nullable: false })
    public crosswordId!: number;

    @Column({ nullable: false })
    public userId!: number;
    
    @CreateDateColumn()
    public started!: Date;
    
    @Column({ nullable: false })
    public lastPlayed!: Date;

    // Computed/denormalised fields
    @Column({ default: false, nullable: false })
    public completed!: boolean;
    
    @Column({ default: 0, nullable: false })
    public percentCompleted!: number;
    
    @Column({ default: 0, nullable: false })
    public score!: number;

    // Relations
    @ManyToOne(() => User)
    public user!: User;
    
    @ManyToOne(() => Crossword)
    public crossword!: Crossword;
    
    @OneToMany(() => Guess, guess => guess.attempt)
    public guesses!: Guess[];
}
