export enum AuditType {
    DeckAudit = 1,
    CrosswordAudit = 2,
    SecurityAudit = 3,
    UserAudit = 4,
}
