import { ChildEntity, Column, CreateDateColumn, Entity, Index, ManyToOne, PrimaryGeneratedColumn, TableInheritance } from "typeorm";
import { AuditCode, CrosswordAuditCode, DeckAuditCode, SecurityAuditCode, UserAuditCode } from "./AuditCode";
import { Crossword } from "./Crossword";
import { Deck } from "./Deck";
import { User } from "./User";
import { AuditType } from "./AuditType";

@Entity()
@TableInheritance({ column: { name: "type", type: "smallint" } })
@Index("audit_code_key", ["code"])
@Index("audit_code_created_key", ["code", "created"])
export class Audit<TAuditCode = AuditCode> {
    protected constructor(fields?: Partial<Audit<TAuditCode>>) {
        if (fields)
            Object.assign(this, fields);
    }

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column({ nullable: false, type: "smallint" })
    public code!: TAuditCode;

    @Column({ type: "text", array: true, nullable: true })
    public parameters?: string[];

    @CreateDateColumn()
    public created!: string;

    // Foreign keys
    @Column({ nullable: true })
    public userId?: number;

    @ManyToOne(type => User, { nullable: true, cascade: true })
    public user?: User;
}

@ChildEntity(AuditType.DeckAudit)
export class DeckAudit extends Audit<DeckAuditCode> {
    protected constructor(fields?: Partial<DeckAudit>) {
        super(fields);
    }

    @Column({ nullable: true })
    public deckId?: number;

    @ManyToOne(type => Deck, { nullable: false, cascade: true })
    public deck?: Deck;

    public static created(deckId: number, userId: number) {
        return new DeckAudit({ deckId, userId, code: DeckAuditCode.DeckCreated });
    }

    public static updated(deckId: number, userId: number) {
        return new DeckAudit({ deckId, userId, code: DeckAuditCode.DeckUpdated });
    }

    public static deleted(deckId: number, userId: number) {
        return new DeckAudit({ deckId, userId, code: DeckAuditCode.DeckDeleted });
    }

    public static termEdited(deckId: number, userId: number) {
        return new DeckAudit({ deckId, userId, code: DeckAuditCode.DeckTermEdited });
    }
}

@ChildEntity(AuditType.CrosswordAudit)
export class CrosswordAudit extends Audit<CrosswordAuditCode> {
    protected constructor(fields?: Partial<CrosswordAudit>) {
        super(fields);
    }

    @Column({ nullable: true })
    public crosswordId?: number;

    @ManyToOne(type => Crossword, { nullable: true, cascade: true })
    public crossword?: Crossword;

    public static created(crosswordId: number, userId: number) {
        return new CrosswordAudit({ crosswordId, userId, code: CrosswordAuditCode.CrosswordCreated });
    }

    public static generated(crosswordId: number) {
        return new CrosswordAudit({ crosswordId, code: CrosswordAuditCode.CrosswordGenerated });
    }

    public static error(crosswordId: number, message: string) {
        return new CrosswordAudit({ crosswordId, parameters: [message], code: CrosswordAuditCode.CrosswordError });
    }

    public static completed(crosswordId: number, userId: number) {
        return new CrosswordAudit({ crosswordId, userId, code: CrosswordAuditCode.CrosswordCompleted });
    }
}

@ChildEntity(AuditType.SecurityAudit)
export class SecurityAudit extends Audit<SecurityAuditCode> {
    protected constructor(fields?: Partial<SecurityAudit>) {
        super(fields);
    }
    
    public static emailChangeRequested(userId: number): SecurityAudit {
        return new SecurityAudit({ userId, code: SecurityAuditCode.EmailChangeRequested });
    }
    
    public static emailChangeSuccess(userId: number): SecurityAudit {
        return new SecurityAudit({ userId, code: SecurityAuditCode.EmailChangeSuccess });
    }
    
    public static forgotPassword(userId: number): SecurityAudit {
        return new SecurityAudit({ userId, code: SecurityAuditCode.ForgotPassword });
    }
    
    public static passwordReset(userId: number): SecurityAudit {
        return new SecurityAudit({ userId, code: SecurityAuditCode.PasswordReset });
    }
    
    public static passwordResetSuccess(userId: number): SecurityAudit {
        return new SecurityAudit({ userId, code: SecurityAuditCode.PasswordResetSuccess });
    }
    
    public static passwordUpdated(userId: number): SecurityAudit {
        return new SecurityAudit({ userId, code: SecurityAuditCode.PasswordUpdated });
    }
    
    public static accountActivated(userId: number): SecurityAudit {
        return new SecurityAudit({ userId, code: SecurityAuditCode.AccountActivated });
    }
    
    public static accountRegistered(userId: number): SecurityAudit {
        return new SecurityAudit({ userId, code: SecurityAuditCode.AccountRegistered });
    }

    public static googleAccountLinked(userId: number, email: string) {
        return new SecurityAudit({ userId, code: SecurityAuditCode.GoogleAccountLinked, parameters: [email] });
    }

    public static googleAccountRemoved(userId: number) {
        return new SecurityAudit({ userId, code: SecurityAuditCode.GoogleAccountRemoved });
    }

    public static facebookAccountLinked(userId: number, displayName: string) {
        return new SecurityAudit({ userId, code: SecurityAuditCode.FacebookAccountLinked, parameters: [displayName] });
    }

    public static facebookAccountRemoved(userId: number) {
        return new SecurityAudit({ userId, code: SecurityAuditCode.FacebookAccountRemoved });
    }
}


@ChildEntity(AuditType.UserAudit)
export class UserAudit extends Audit<UserAuditCode> {
    @Column()
    public otherUserId?: number;

    @ManyToOne(type => User, { cascade: true })
    public otherUser?: User;

    protected constructor(fields?: Partial<UserAudit>) {
        super(fields);
    }
    
    public static followedUser(userId: number, otherUserId: number): UserAudit {
        return new UserAudit({ userId, otherUserId, code: UserAuditCode.UserFollowed });
    }

    public static unfollowedUser(userId: number, otherUserId: number): UserAudit {
        return new UserAudit({ userId, otherUserId, code: UserAuditCode.UserUnfollowed });
    }
}
