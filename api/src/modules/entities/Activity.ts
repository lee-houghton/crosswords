import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { ActivityType } from "../../types";
import { User } from "./User";

@Entity()
export class Activity {
    public constructor(fields?: Partial<Activity>) {
        if (fields)
            Object.assign(this, fields);
    }

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column({ nullable: false, type: "smallint" })
    public type!: ActivityType;

    @Column({ type: "text", array: true, nullable: true })
    public parameters?: string[];

    @UpdateDateColumn()
    public date!: Date;

    // Foreign keys
    @Column()
    public userId!: number;

    @ManyToOne(type => User)
    public user!: User;

    @Column({ type: "int", array: true, nullable: true })
    public deckIds!: number[];

    @Column({ type: "int", array: true, nullable: true })
    public crosswordIds!: number[];
}
