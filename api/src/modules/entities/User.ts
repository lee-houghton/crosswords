import {
    Column,
    CreateDateColumn,
    Entity,
    Index,
    OneToMany,
    PrimaryGeneratedColumn,
    ManyToMany,
    JoinTable,
} from "typeorm";
import { Crossword } from "./Crossword";
import { Deck } from "./Deck";
import { UserLanguage } from "./UserLanguage";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    public id!: number;

    @Column("citext")
    @Index("User_name_key", { unique: true })
    public name!: string;

    @Column("citext")
    public displayName!: string;

    @Column("citext", { nullable: true })
    @Index("User_email_key", { unique: true })
    public email?: string;

    @Column({ default: true })
    public enabled!: boolean;

    @CreateDateColumn()
    public created!: Date;

    @Column({ nullable: true })
    public passwordHash?: string;

    @Column({ nullable: true })
    public googleId?: string;

    @Column({ nullable: true })
    public facebookId?: string;

    @Column({ type: "text", nullable: true })
    @Index("User_activationCode_key", { unique: true })
    public activationCode?: string | null;

    @Column({ nullable: true })
    public activated?: Date;

    @Column({ default: false, nullable: false })
    public superuser!: boolean;

    // Relations
    @OneToMany((type) => Crossword, (crossword) => crossword.user)
    public crosswords!: Promise<Crossword[]>;

    @OneToMany((type) => Deck, (deck) => deck.user)
    public decks!: Deck[];

    @ManyToMany((type) => Deck)
    @JoinTable()
    public favouriteDecks!: Deck[];

    @OneToMany((type) => UserLanguage, (userLanguage) => userLanguage.user)
    public languages!: UserLanguage[];

    @Column({ type: "int", nullable: true })
    public lastReadNewsId?: number;

    @ManyToMany((type) => User, (user) => user.followedBy)
    @JoinTable()
    public followedUsers!: User[];

    @ManyToMany((type) => User, (user) => user.followedUsers)
    public followedBy!: User[];
}
