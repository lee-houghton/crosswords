import { DeckAuditCode, CrosswordAuditCode, SecurityAuditCode, UserAuditCode } from "./AuditCode";
import { DeckAudit, CrosswordAudit, SecurityAudit, UserAudit } from "./Audit";

export const auditTypes = {
    DeckAudit: { codes: DeckAuditCode, class: DeckAudit },
    CrosswordAudit: { codes: CrosswordAuditCode, class: CrosswordAudit },
    SecurityAudit: { codes: SecurityAuditCode, class: SecurityAudit },
    UserAudit: { codes: UserAuditCode, class: UserAudit },
} as const;

export type AuditTypeName = keyof typeof auditTypes;
