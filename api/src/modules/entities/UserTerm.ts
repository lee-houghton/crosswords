import { Column, Entity, Index, ManyToOne, PrimaryColumn } from "typeorm";
import { Term } from "./Term";
import { User } from "./User";

@Entity()
// termId should be an included field rather than part of the key, but typeorm doesn't support it
@Index(["userId", "score", "termId"])
@Index(["userId", "lastPlayed"], { where: `not "lastResultSuccess"` })
export class UserTerm {
    public constructor(fields?: Partial<UserTerm>) {
        if (fields)
            Object.assign(this, fields);
    }

    @PrimaryColumn()
    public userId!: number;

    @PrimaryColumn()
    public termId!: number;

    @ManyToOne(type => User, { onDelete: "CASCADE" })
    public user?: User;

    @ManyToOne(type => Term, { onDelete: "CASCADE" })
    public term?: Term;

    @Column({ default: 0, nullable: false })
    public attempts!: number;

    @Column({ default: 0, nullable: false })
    public successes!: number;

    @Column({ type: "real", default: 0, nullable: false })
    public score!: number;

    @Column({ nullable: false })
    public lastPlayed!: Date;

    @Column({ nullable: false, default: true })
    public lastResultSuccess!: boolean;
}
