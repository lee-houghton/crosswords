import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from "typeorm";
import { User } from "./User";
import { Deck } from "./Deck";
import { Clue } from "./Clue";
import { Cell } from "./Cell";

@Entity()
export class Crossword {
    public constructor(fields?: Partial<Crossword>) {
        if (fields) Object.assign(this, fields);
    }

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column({ type: "varchar", length: 2, nullable: true })
    public languageId?: string;

    @Column({ nullable: true })
    public title?: string;

    @Column({ nullable: true })
    public description?: string;

    @CreateDateColumn()
    public created!: Date;

    @UpdateDateColumn()
    public updated!: Date;

    @Column({ nullable: true })
    public userId?: number;

    @ManyToOne((type) => User, (user) => user.crosswords)
    public user?: User & {};

    @ManyToMany(() => Deck, (deck) => deck.crosswords, { eager: true })
    public decks?: Deck[];

    @Column({ nullable: true })
    public width!: number;

    @Column({ nullable: true })
    public height!: number;

    //
    // Filled in once the puzzle has been generated
    //

    // Date when the crossword was generated
    @Column({ nullable: true })
    public available?: Date;

    @OneToMany(() => Clue, (clue) => clue.crossword)
    public clues?: Clue[];

    @OneToMany(() => Cell, (cell) => cell.crossword)
    public cells?: Cell[];

    //
    // Denormalised fields
    //
    @Column({ default: 0 })
    public stars!: number;

    @Column({ default: 0 })
    public attempts!: number;
}
