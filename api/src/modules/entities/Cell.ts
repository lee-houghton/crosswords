import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, Index } from "typeorm";
import { Clue } from "./Clue";
import { Crossword } from "./Crossword";

@Entity()
@Index(["crosswordId", "x", "y"])
export class Cell {
    public constructor(fields?: Partial<Cell>) {
        if (fields) Object.assign(this, fields);
    }

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column({ nullable: false })
    public crosswordId!: number;

    @ManyToOne(() => Crossword, { nullable: false })
    public crossword!: Crossword & {};

    @Column({ nullable: false, type: "smallint" })
    public x!: number;

    @Column({ nullable: false, type: "smallint" })
    public y!: number;

    @Column({ nullable: false, type: "text" })
    public contents!: string;

    @Column({ nullable: true })
    public acrossClueId?: number;

    @ManyToOne(() => Clue)
    public acrossClue?: Clue;

    @Column({ nullable: true })
    public downClueId?: number;

    @ManyToOne(() => Clue)
    public downClue?: Clue;
}
