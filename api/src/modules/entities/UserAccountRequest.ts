import { Column, Entity, PrimaryGeneratedColumn, Index, CreateDateColumn, ManyToOne, TableInheritance } from "typeorm";
import { User } from "./User";

@Entity()
@TableInheritance({ column: { type: "varchar", name: "type" } })
export abstract class UserAccountRequest {
    public constructor(fields?: Partial<UserAccountRequest>) {
        if (fields)
            Object.assign(this, fields);
    }

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column({ nullable: false })
    public userId!: number;

    @ManyToOne(type => User, { nullable: false })
    public user!: User;

    @Column({ type: "text", nullable: false })
    @Index({ unique: true })
    public code!: string;

    @CreateDateColumn({ type: "timestamptz" })
    public created!: Date;

    @Column({ type: "timestamptz", nullable: false })
    public expires!: Date;
}
