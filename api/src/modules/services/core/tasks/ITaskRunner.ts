import { Task } from "./ITaskScheduler";

export interface TaskRunnerConstructor<TTask extends Task = Task> {
    new(...args: any[]): ITaskRunner<TTask>;
    handledTypes: ReadonlyArray<TTask["type"]>;
}

export interface ITaskRunner<TTask extends Task = Task> {
    runTask(task: TTask): Promise<void>;
}
