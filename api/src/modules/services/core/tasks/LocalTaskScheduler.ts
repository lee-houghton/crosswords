import { Inject, Injectable } from "injection-js";
import { nanoid } from "nanoid";
import { Container } from "../../../../container";
import { requestChildContainerToken, taskRunnerToken } from "../../../../tokens";
import { LoggerService } from "../../LoggerService";
import { ITaskRunner, TaskRunnerConstructor } from "./ITaskRunner";
import { ITaskScheduler, Task } from "./ITaskScheduler";

@Injectable()
export class LocalTaskScheduler extends ITaskScheduler {
    private readonly items = [] as Task[];
    private taskRunners = new Map<string, TaskRunnerConstructor>();
    private busy = false;

    public constructor(
        private readonly logger: LoggerService,
        @Inject(taskRunnerToken) taskRunnerConstructors: TaskRunnerConstructor[],
        @Inject(requestChildContainerToken) private readonly requestChildContainer: () => Container
    ) {
        super();

        for (const constructor of taskRunnerConstructors) {
            const { handledTypes } = constructor;
            if (!handledTypes || handledTypes.length === 0)
                throw new Error("TaskRunnerConstructor does not handle any types: " + constructor.name);

            for (const handledType of handledTypes) this.taskRunners.set(handledType, constructor);
        }
    }

    public async add(item: Task) {
        this.items.push(item);
        if (!this.busy) this.performWork();
    }

    public newId(): string {
        return nanoid();
    }

    private async performWork() {
        const item = this.items.shift();
        if (!item) {
            this.busy = false;
            return;
        }

        this.busy = true;
        try {
            await this.performItem(item);
            this.performWork();
        } catch (err) {
            this.logger.error(`Error processing ${item.type}: ${err}`);
            this.busy = false;
        }
    }

    private async performItem(item: Task) {
        this.logger.debug(`Performing ${item.type} item: ${item.id}`);

        const container = this.requestChildContainer();
        const constructor = this.taskRunners.get(item.type);
        if (!constructor) throw new Error(`No TaskRunner found for type ${item.type}`);

        const runner: ITaskRunner = container.get(constructor);
        await runner.runTask(item);
    }

    public start(): void {
        // Nothing to do: tasks are processed when they are added.
    }

    public stop(): void {
        // Nothing to do: the task queue is fully processed before exiting.
    }
}
