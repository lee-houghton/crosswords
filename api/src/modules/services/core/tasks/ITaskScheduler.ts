export interface Task {
    id: string;
    type: string;
}

export abstract class ITaskScheduler {
    public abstract add<TTask extends Task = Task>(item: TTask): Promise<void>;

    public abstract newId(): string;

    /**
     * This doesn't have a meaning for all task schedulers, but some task schedulers
     * need to explicitly run and check for tasks.
     */
    public abstract start(): void;

    /**
     * This doesn't have a meaning for all task schedulers, but some task schedulers
     * need to explicitly run and check for tasks.
     */
    public abstract stop(): void;
}
