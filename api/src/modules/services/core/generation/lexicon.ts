import { Deck } from "../../../entities";
import { Term } from "../../../entities/Term";

export interface ILexiconEntry {
    term: string;
    definition: string;
    sourceSet: Deck;
    source: Term;
    wordLengths: number[];
}

export class Lexicon {
    protected map: Map<string, ILexiconEntry>;
    protected trigrams = new Map<string, string[]>();

    public readonly entries: ILexiconEntry[];

    public constructor(
        public readonly decks: readonly Deck[],
        public readonly minWordLength: number,
        public readonly maxWordLength: number,
    ) {
        this.entries = [];

        for (const deck of decks)
            for (const term of deck.terms)
                this.add(deck, term);

        this.map = new Map<string, ILexiconEntry>();

        for (const entry of this.entries) {
            const { term } = entry;

            this.map.set(term, entry);

            for (let i = 0; i < term.length - 2; i++) {
                const tri = term.substr(i, 3);
                const list = this.trigrams.get(tri);
                if (list)
                    list.push(term);
                else
                    this.trigrams.set(tri, [term]);
            }
        }
    }

    public add(deck: Deck, term: Term) {
        const parts = term.term.split(term.term.includes(";") ? ";" : ",");
        for (const part of parts) {
            const words = this.getWords(part);
            const newTerm = words.join("");
            if (newTerm.length < this.minWordLength || newTerm.length > this.maxWordLength)
                continue;

            this.entries.push({
                term: newTerm,
                definition: term.definitions.join(", "),
                source: term,
                sourceSet: deck,
                wordLengths: words.map(w => w.length),
            });
        }
    }

    public getWords(term: string) {
        // Remove parenthesised portions
        term = term.replace(/\([^\)]*\)/g, "");

        // Remove punctuation
        term = term.replace(/[^\p{Alphabetic}\p{Mark}\p{Decimal_Number}\s]/gu, "");

        return term.split(/\s+/g);
    }

    public randomEntry() {
        const i = Math.floor(Math.random() * this.entries.length);
        return this.entries[i];
    }

    public isWord(word: string) {
        return this.map.has(word);
    }

    public get(term: string) {
        return this.map.get(term);
    }

    public searchTrigram(trigram: string) {
        return this.trigrams.get(trigram);
    }

    public *searchPrefix(prefix: string) {
        for (const entry of this.entries)
            if (entry.term.startsWith(prefix))
                yield entry ;
    }
}
