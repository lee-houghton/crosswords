import { ILexiconEntry, Lexicon } from "./lexicon";

export type Direction = "across" | "down";
export interface Placement {
    x: number;
    y: number;
    dir: Direction;
}

export interface WordPlacement extends Placement {
    word: string;
    entry: ILexiconEntry;
}

export interface ScoredWordPlacement extends WordPlacement {
    intersections: number;
}

export class Grid {
    protected grid: string[][] = [];
    protected placements: WordPlacement[] = [];
    protected used = new Set<string>();
    protected filled = 0;

    public constructor(
        public readonly lexicon: Lexicon,
        public readonly width: number,
        public readonly height: number
    ) {
        for (let y = 0; y < this.height; y++) {
            this.grid[y] = [];
            for (let x = 0; x < this.width; x++)
                this.grid[y][x] = "";
        }
    }

    public get(x: number, y: number) {
        if (!this.grid[y])
            return "";
        return this.grid[y][x];
    }

    public show() {
        return (
            "+" + this.grid[0].map(c => "-").join("") + "+\n" +
            this.grid.map(row => "|" + row.map(c => c || " ").join("") + "|").join("\n") +
            "\n+" + this.grid[0].map(c => "-").join("") + "+"
        );
    }

    public inBounds(x: number, y: number) {
        return x >= 0 && y >= 0 && x < this.width && y < this.height;
    }

    public place({word, entry, x, y, dir}: WordPlacement) {
        if (!this.inBounds(x, y))
            throw new Error("place(): start point out of bounds");
        const { dx, dy } = this.delta(dir);
        if (!this.inBounds(x + dx * (word.length - 1), y + dy * (word.length - 1)))
            throw new Error("place(): end point out of bounds");

        const placement = { word, entry, x, y, dir };
        this.placements.push(placement);
        this.used.add(word);

        for (const c of word) {
            if (!this.grid[y][x])
                this.filled++;

            this.grid[y][x] = c;
            if (dir === "across")
                x++;
            else
                y++;
        }

        return placement;
    }

    public debug() {
        // tslint:disable:no-console
        console.log(this.show());
        console.log("");
        for (const {word, x, y, dir} of this.placements) {
            console.log(`(${x}, ${y}) ${dir}: ${word}`);
            if (!this.lexicon.isWord(word))
                console.log("Invalid!", word);
        }
        // tslint:enable:no-console
    }

    public placeRandomWord(lexicon: Lexicon) {
        const placements: ScoredWordPlacement[] = [];

        for (const {x: x0, y: y0, dir, word} of this.placements) {
            const { dx, dy } = this.delta(dir);
            for (let i = 0; i < word.length; i++) {
                const x = x0 + i * dx;
                const y = y0 + i * dy;

                const char = this.get(x, y);

                for (const entry of lexicon.searchPrefix(char)) {
                    if (this.used.has(entry.term))
                        continue;
                    if (dx > 0 && x + entry.term.length >= this.width)
                        continue;
                    if (dy > 0 && y + entry.term.length >= this.height)
                        continue;

                    const placement = this.tryPlace({
                        dir: dir === "across" ? "down" : "across",
                        entry,
                        word: entry.term,
                        x,
                        y
                    });

                    if (placement)
                        placements.push(placement);
                }
            }
        }

        if (placements.length > 0)
            return this.place(this.pickPlacement(placements));
    }

    public tryPlaceWord(entry: ILexiconEntry) {
        const placements: ScoredWordPlacement[] = [];
        const word = entry.term;

        const horizontal = () => {
            for (let x = 0; x <= this.width - word.length; x++) {
                for (let y = 0; y < this.height; y++) {
                    const placement = this.tryPlace({ word, entry, x, y, dir: "across" });
                    if (placement)
                        placements.push(placement);
                }
            }
        };

        const vertical = () => {
            for (let x = 0; x < this.width; x++) {
                for (let y = 0; y <= this.height - word.length; y++) {
                    const placement = this.tryPlace({ word, entry, x, y, dir: "down" });
                    if (placement)
                        placements.push(placement);
                }
            }
        };

        if (Math.random() > 0.5) {
            horizontal();
            vertical();
        } else {
            vertical();
            horizontal();
        }

        if (placements.length > 0)
            return this.place(this.pickPlacement(placements));
    }

    /** Pick the placement with the most intersections with existing words */
    private pickPlacement(placements: ScoredWordPlacement[]) {
        placements.sort((p1, p2) => p1.intersections - p2.intersections);
        return placements[0];
    }

    public get density() {
        // let filled = 0;
        // for (let y = 0; y < this.height; y++)
        //     for (let x = 0; x < this.width; x++)
        //         if (this.get(x, y))
        //             filled++;
        return this.filled / (this.width * this.height);
    }

    /** Pick a random item from an array */
    // private pick<T>(array: ArrayLike<T>) {
    //     const i = Math.floor(Math.random() * array.length);
    //     return array[i];
    // }

    /*
    private next({ x, y, dir }: Placement) {
        return {
            x: dir === "across" ? x + 1 : x,
            y: dir === "down" ? y + 1 : y,
            dir
        };
    }

    private prev({ x, y, dir }: Placement) {
        return {
            x: dir === "across" ? x - 1 : x,
            y: dir === "down" ? y - 1 : y,
            dir
        };
    }*/

    private delta(dir: Direction) {
        return {
            dx: dir === "across" ? 1 : 0,
            dy: dir === "down" ? 1 : 0
        };
    }

    protected tryPlace(placement: WordPlacement, first = false): ScoredWordPlacement | undefined {
        const { word, dir } = placement;
        const { dx, dy } = this.delta(dir);

        let { x, y } = placement;
        let intersections = 0;

        // Just before and just after the word should be clear
        if (this.get(x - dx, y - dy))
            return;
        if (this.get(x + dx * word.length, y + dy * word.length))
            return;

        for (const c of word) {
            if (!this.inBounds(x, y))
                return;

            // All the spaces in the word should be the same or match
            const existing = this.get(x, y);
            if (c !== (existing || c))
                return;

            if (existing)
                intersections++;

            // It shouldn't cause any invalid words on the cross axis
            // (TODO: this algorithm is sub-optimal, it rarely produces
            // dense grids because it doesn't consider that the cross-axis
            // might not yet be complete. But this requires backtracking or
            // something.)
            if (!this.checkCrossAxis(c, { x, y, dir }))
                return;

            // It's possible for words to overlap in the same direction - this
            // is crosswords, not scrabble, so we can't allow this.
            //
            // This can only happen if there is already a letter on the square
            // we're trying to place on.
            if (existing)
                if (!this.checkOverlap(c, { x, y, dir }))
                    return;

            x += dx;
            y += dy;
        }

        // Has to intersect with another word, unless it's the first
        if (intersections > 0 || this.placements.length === 0)
            return { ...placement, intersections };
    }

    /**
     * Avoid words intersecting in the same direction, like "avis" + "viser"
     * to become "aviser" which is also a word.
     */
    protected checkOverlap(char: string, { x , y, dir }: Placement) {
        for (const p of this.placements) {
            if (p.dir !== dir)
                continue;

            if (dir === "across") {
                if (y === p.y && x >= p.x && x < p.x + p.word.length)
                    return false;
            } else {
                if (x === p.x && y >= p.y && y < p.y + p.word.length)
                    return false;
            }
        }

        return true;
    }

    protected checkCrossAxis(char: string, { x, y, dir }: Placement) {
        let word = "";

        const dx = dir === "across" ? 0 : 1;
        const dy = 1 - dx;

        // tslint:disable-next-line:no-conditional-assignment
        for (let xx = x - dx, yy = y - dy, c: string; (c = this.get(xx, yy)); xx -= dx, yy -= dy)
            word = c + word;

        word = word + char;

        // tslint:disable-next-line:no-conditional-assignment
        for (let xx = x + dx, yy = y + dy, c: string; (c = this.get(xx, yy)); xx += dx, yy += dy)
            word = word + c;

        return word.length === 1 || this.lexicon.isWord(word);
    }
}
