import express, { Application } from "express";
import { Server } from "http";
import { Inject } from "injection-js";
import { Service } from "../../decorators";
import { env } from "../../env";
import { endpointProviderToken } from "../../tokens";
import { LoggerService } from "./LoggerService";

export interface IEndpointProvider {
    registerEndpoints(app: Application): void;
    addServerHandlers(server: Server, app: Application): void;
}

@Service()
export class Http {
    public readonly app: Application;
    private server?: Server;

    public constructor(
        @Inject(endpointProviderToken)
        private readonly endpointProviders: IEndpointProvider[],
        private readonly logger: LoggerService
    ) {
        this.app = express();

        const corsDomain = env.CORS_ORIGIN;
        if (corsDomain) {
            this.app.use((req, res, next) => {
                res.header("Access-Control-Allow-Origin", corsDomain);
                res.header("Access-Control-Allow-Methods", "GET, HEAD, PUT, POST, DELETE, OPTIONS");
                res.header("Access-Control-Allow-Credentials", "true");
                res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
                if (req.method === "OPTIONS") res.status(204).end();
                else next();
            });
        }

        this.app.disable("x-powered-by");
        this.app.use((req, res, next) => {
            res.header("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
            res.header("X-Frame-Options", "SAMEORIGIN");
            res.header("Referrer-Policy", "same-origin");
            next();
        });

        for (const provider of endpointProviders) provider.registerEndpoints(this.app);
    }

    public async start() {
        await new Promise<void>((resolve) => {
            this.server = new Server(this.app);
            this.server.listen(8080, resolve);
            for (const provider of this.endpointProviders) provider.addServerHandlers(this.server, this.app);
        });
        this.logger.info("Listening on port 8080");
    }

    public async stop() {
        await new Promise<void>((resolve) => {
            if (this.server) {
                this.server.close(() => resolve);
                this.server = undefined;
            } else {
                resolve();
            }
        });
        this.logger.info("Stopped listening on port 8080");
    }
}
