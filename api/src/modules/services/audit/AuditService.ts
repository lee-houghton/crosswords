import { RedisPubSub } from "graphql-redis-subscriptions";
import { Connection, EntityManager } from "typeorm";
import { Service } from "../../../decorators";
import { Audit } from "../../entities/Audit";
import { AuditTypeName, auditTypes } from "../../entities/auditTypes";
import { LoggerService } from "../LoggerService";

@Service()
export class AuditService {
    public constructor(
        private readonly connection: Connection,
        private readonly logger: LoggerService,
        private readonly pubSub: RedisPubSub // We need Redis specifically for pattern subscriptions
    ) {}

    public async add(audit: Audit, manager?: EntityManager) {
        manager = manager ?? this.connection.manager;

        const res = await manager.getRepository(audit.constructor).insert(audit);

        // This will be stringified with JSON.stringify, however, this doesn't include
        // the type of the audit, so we include this in the event name (which also
        // allows for easier filtering).
        const type = audit.constructor.name;
        await this.pubSub.publish(`Audit:${type}`, {
            type,
            audit: { ...audit, id: res.identifiers[0].id },
        });
    }

    public async tryAdd(audit: Audit, manager?: EntityManager) {
        try {
            await this.add(audit, manager);
        } catch (err) {
            this.logger.error(`Unable to save ${audit.constructor.name}: ${err}`);
        }
    }

    public async *asyncIterator(type: AuditTypeName | undefined, prefix = 10, filter?: (audit: Audit) => boolean) {
        type Record = { type: AuditTypeName; audit: Audit };

        if (prefix > 0) {
            const ctor = type ? auditTypes[type]?.class : Audit;
            const repo = this.connection.manager.getRepository(ctor);
            const audits = (await repo.find({
                take: prefix,
                order: { id: "DESC" },
            })) as Audit<any>[];

            for (const audit of audits.reverse()) if (!filter || filter(audit)) yield audit;
        }

        // Work around the incorrectly documented return type
        // https://github.com/apollographql/graphql-subscriptions/issues/192
        const iterable = this.pubSub.asyncIterator<Record>(`Audit:${type || "*"}`, {
            pattern: true,
        }) as AsyncIterableIterator<Record>;

        for await (const item of iterable) {
            const ctor = auditTypes[item.type];
            if (!ctor) {
                this.logger.error(`Dropped audit record of unknown type: ${item.type}`);
                continue;
            }
            const audit = Object.assign(Object.create(ctor.class.prototype), item.audit);
            if (filter && !filter(audit)) continue;

            yield audit;
        }
    }
}
