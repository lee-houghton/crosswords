import DataLoader from "dataloader";
import { subHours } from "date-fns/subHours";
import { Inject } from "injection-js";
import { Connection, In, Repository } from "typeorm";
import { Service, ServiceScope } from "../../../decorators";
import { ActivityType } from "../../../types";
import { Attempt, Cell, Clue, Crossword, CrosswordAudit, Guess, User } from "../../entities";
import { UpdateAttemptRequest, UpdateGuessRequest } from "../../schema/types";
import { ActivityService } from "../activity/ActivityService";
import { AuditService } from "../audit/AuditService";
import { StatsService } from "../StatsService";
import { CrosswordService } from "./CrosswordService";
import { ScoringService } from "./ScoringService";

@Service(ServiceScope.Request)
export class AttemptService {
    private dataLoaders = new Map<number, DataLoader<number, Attempt>>();
    private getDataLoader(userId: number) {
        let loader = this.dataLoaders.get(userId);

        if (!loader) {
            loader = new DataLoader<number, Attempt>(async (crosswordIds) => {
                const attempts = await this.attemptRepo().find({
                    where: { crosswordId: In(crosswordIds as number[]) },
                });
                return crosswordIds.map((id) => attempts.find((a) => a.crosswordId === id)!);
            });
            this.dataLoaders.set(userId, loader);
        }

        return loader;
    }

    public constructor(
        @Inject(Attempt) private readonly attemptRepo: () => Repository<Attempt>,
        @Inject(Cell) private readonly cellRepo: () => Repository<Cell>,
        @Inject(Guess) private readonly guessRepo: () => Repository<Guess>,
        @Inject(Connection) private readonly connection: Connection,
        private readonly crosswordService: CrosswordService,
        private readonly statsService: StatsService,
        private readonly scoringService: ScoringService,
        private readonly auditService: AuditService,
        private readonly activityService: ActivityService
    ) {}

    public getByCrosswordId(crosswordId: number, user: User | undefined) {
        if (!user) return Promise.resolve(undefined);

        return this.getDataLoader(user.id).load(crosswordId);
    }

    public getGuesses(attemptId: number, user: User | undefined) {
        if (!user) return Promise.resolve([]);

        return this.guessRepo().find({
            where: { attemptId },
        });
    }

    public async recentAttempts(count: number, user: User | undefined) {
        if (!user) return [];

        return await this.attemptRepo().find({
            where: { user, completed: false },
            order: { lastPlayed: "DESC" },
            relations: ["crossword"],
            take: count,
        });
    }

    public async update(crossword: Crossword, guesses: UpdateGuessRequest[], user?: User, complete = false) {
        return await this.connection.transaction(async (manager) => {
            const { cells, id: crosswordId } = crossword;

            if (!cells) throw new Error("AttemptService.update(): crossword cells must be loaded");

            const attempt =
                (user &&
                    (await manager.findOne(Attempt, {
                        where: { crosswordId, userId: user.id },
                        relations: ["guesses"],
                    }))) ||
                new Attempt({ crossword, crosswordId, user, guesses: [] });

            if (attempt.completed) throw new Error("Can't save after completing the crossword");

            // Update or remove existing guesses
            for (const existing of attempt.guesses) {
                const update = guesses.find((g) => g.x === existing.x && g.y === existing.y);
                if (update && update.guess) existing.contents = update.guess;
                else existing.contents = ""; // Was removed
            }

            // Add new guesses
            for (const { x, y, guess } of guesses) {
                if (!guess) continue;

                const existing = attempt.guesses.find((g) => g.x === x && g.y === y);
                if (!existing)
                    attempt.guesses.push(new Guess({ attempt, attemptId: attempt.id, x, y, contents: guess }));
            }

            attempt.lastPlayed = new Date();

            // It's easier to calculate this now while we have all the info loaded than to load it all
            // again when clicking "Show Answers".
            attempt.score = calculateScore(cells, guesses);

            if (complete) {
                if (user) {
                    if (!attempt.completed) {
                        const languageId = await this.crosswordService.getCrosswordLanguage(crosswordId);
                        if (languageId)
                            await this.statsService.incrementUserLanguageStats(user.id, languageId, {
                                crosswordsCompleted: 1,
                            });

                        await this.logActivity(attempt, user);
                    }

                    const clues = await manager.find(Clue, { where: { crosswordId } });
                    const { wins, losses } = calculateWinsAndLosses(cells, clues, attempt.guesses);
                    await this.scoringService.updateUserScores(user.id, wins, losses);
                    await this.auditService.add(CrosswordAudit.completed(crosswordId, user.id));
                }

                attempt.completed = true;
                attempt.percentCompleted = 100;
            } else {
                attempt.percentCompleted = Math.floor((attempt.guesses.length * 100) / cells.length);
            }

            if (!attempt.id) {
                // It's a new attempt, increment the crossword's attempt count
                await manager
                    .createQueryBuilder()
                    .update(Crossword)
                    .set({ attempts: () => "attempts + 1" })
                    .whereInIds([crosswordId])
                    .execute();
            }

            if (!user) {
                // We need an id, and this helps avoid weird caching issues, maybe?
                attempt.id = -crosswordId;
                return attempt;
            }

            const newAttempt = await manager.save(attempt);
            await manager.save(attempt.guesses);
            return newAttempt;
        });
    }

    public async save(crosswordId: number, guesses: UpdateGuessRequest[], complete: boolean, user?: User) {
        if (!complete && !user) throw new Error("Must be logged in to save a partially-completed attempt");

        const crossword = await this.crosswordService.getById(crosswordId);
        if (!crossword) throw new Error(`Crossword ${crosswordId} not found`);

        crossword.cells = await this.cellRepo().find({ where: { crosswordId: crosswordId } });

        return await this.update(crossword, guesses, user, complete);
    }

    /** @deprecated */
    public async complete(request: UpdateAttemptRequest, user: User) {
        const attempt = await this.save(request.crosswordId, request.guesses, true, user);

        // Note: the below is necessary to avoid extra DB calls to resolve the Attempt from the Crossword.
        // (checkAnswers does not have this problem)
        this.getDataLoader(user.id).prime(request.crosswordId, attempt);

        return attempt.crossword;
    }

    private async logActivity(attempt: Attempt, user: User) {
        return await this.activityService.addOrUpdate(
            user,
            ActivityType.CrosswordsCompleted,
            subHours(new Date(), 18),
            (activity) => {
                if (!activity.crosswordIds) activity.crosswordIds = [attempt.crosswordId];
                else if (!activity.crosswordIds.includes(attempt.crosswordId))
                    activity.crosswordIds.push(attempt.crosswordId);
            }
        );
    }
}

const collator = Intl.Collator(undefined, { sensitivity: "accent" });

function calculateScore(cells: Cell[], guesses: UpdateGuessRequest[]) {
    let score = 0;

    for (const cell of cells) {
        const guess = guesses.find((g) => g.x === cell.x && g.y === cell.y);

        if (guess && collator.compare(guess.guess, cell.contents) === 0) score++;
    }

    return Math.floor((score * 100) / cells.length);
}

/**
 * Given a set of clues and a set of guesses, figure out which guesses correspond
 * to which clues, and thus which terms.
 *
 * @returns A list of correctly-guessed term ids and a list of incorrectly-guessed term ids.
 */
function calculateWinsAndLosses(cells: Cell[], clues: Clue[], guesses: Guess[]) {
    const termsByClueId = new Map(clues.filter((c) => c.termId).map((c) => [c.id, c.termId!]));
    const wins = new Set(termsByClueId.values()); // Assume correct until we see otherwise
    const losses = new Set<number>();

    for (const cell of cells) {
        const guess = guesses.find((g) => g.x === cell.x && g.y === cell.y);
        if (guess && collator.compare(guess.contents, cell.contents) === 0) continue;

        for (const clueId of [cell.acrossClueId, cell.downClueId]) {
            if (!clueId) continue;

            const termId = termsByClueId.get(clueId);
            if (termId) {
                wins.delete(termId);
                losses.add(termId);
            }
        }
    }

    return { wins, losses };
}
