import { Inject } from "injection-js";
import { Repository } from "typeorm";
import { Service, ServiceScope } from "../../../decorators";
import { CreateCrosswordRequest } from "../../../dtos";
import { Clue, Crossword, CrosswordAudit, Deck, User } from "../../entities";
import { AuditService } from "../audit/AuditService";
import { ITaskScheduler as ITaskScheduler } from "../core/tasks/ITaskScheduler";
import { GenerateCrosswordTask } from "./GenerateCrossword.task";
import { TASK_GENERATE_CROSSWORD } from "./GenerateCrosswordService";
import { UserInputError } from "../../../errors";

@Service(ServiceScope.Request)
export class CrosswordService {
    public constructor(
        @Inject(Deck) private readonly deckRepo: () => Repository<Deck>,
        @Inject(Clue) private readonly clueRepo: () => Repository<Clue>,
        @Inject(Crossword) private readonly crosswordRepo: () => Repository<Crossword>,
        private readonly taskScheduler: ITaskScheduler,
        private readonly auditService: AuditService
    ) {}

    public getById(id: number) {
        return this.crosswordRepo().findOne({ where: { id }, relations: ["user"] });
    }

    public getAll() {
        return this.crosswordRepo().find();
    }

    public async getByDeckId(deckId: number) {
        const deck = await this.deckRepo().findOne({ where: { id: deckId }, relations: ["crosswords"] });
        return deck ? deck.crosswords : [];
    }

    public async getByUserId(userId: number) {
        return await this.crosswordRepo().find({ where: { userId } });
    }

    public async getCrosswordLanguage(crosswordId: number) {
        return await this.getById(crosswordId).then((c) => c?.languageId);
    }

    public async create(request: CreateCrosswordRequest, user?: User): Promise<Crossword> {
        if (!request.deckIds && !request.languageId)
            throw new UserInputError("Must specify either a languageId or a list of deckIds");

        let crossword = new Crossword();
        crossword.title = request.title;
        crossword.user = user;
        crossword.width = request.width || 14;
        crossword.height = request.height || 14;
        crossword.languageId = request.languageId;

        const crosswordRepo = this.crosswordRepo();
        crossword = await crosswordRepo.save(crossword);

        let { deckIds, languageId } = request;

        if (!deckIds) {
            if (!user) throw new UserInputError("Must specify a list of deckIds if not logged in");

            deckIds = await this.deckRepo()
                .createQueryBuilder("deck")
                .where("deck.userId = :userId and deck.termLanguage = :languageId", { userId: user.id, languageId })
                .select("deck.id")
                .getMany()
                .then((decks) => decks.map((deck) => deck.id));

            if (deckIds!.length === 0) throw new UserInputError("You do not have any decks for this language");
        }

        // TODO: Validation of deck ids if supplied by the user? Or are we just being optimistic?

        await crosswordRepo.manager.createQueryBuilder().relation(Crossword, "decks").of(crossword).add(deckIds);

        await this.taskScheduler.add<GenerateCrosswordTask>({
            id: this.taskScheduler.newId(),
            type: TASK_GENERATE_CROSSWORD,
            crosswordId: crossword.id,
        });

        if (user) await this.auditService.add(CrosswordAudit.created(crossword.id, user.id));

        return crossword;
    }

    public async getClues(crosswordId: number): Promise<Clue[]> {
        return this.clueRepo().findBy({ crosswordId });
    }
}
