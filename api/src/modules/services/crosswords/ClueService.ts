import { Inject } from "injection-js";
import { Repository } from "typeorm";
import { Service, ServiceScope } from "../../../decorators";
import { Clue } from "../../entities";

@Service(ServiceScope.Request)
export class ClueService {
    public constructor(@Inject(Clue) private readonly clueRepo: () => Repository<Clue>) {}

    public getById(id: number) {
        return this.clueRepo().findOneOrFail({ where: { id } });
    }
}
