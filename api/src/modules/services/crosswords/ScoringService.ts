import DataLoader from "dataloader";
import { Inject } from "injection-js";
import { In, Repository } from "typeorm";
import { Service, ServiceScope } from "../../../decorators";
import { UserTerm } from "../../entities";

const DEFAULT_SCORE = 0;
const K = 40;
const MIN_SCORE = -400;
const MAX_SCORE = 400;

/**
 * Scoring system
 * 
 * The scoring system is inspired by the Elo rating system, with the User and
 * the Term being treated as opponents. However, we don't actually need to save
 * both ratings, we just save the rating _difference_ between the User and the Term,
 * which naturally starts off at zero.
 * 
 * A positive score (up to 400) means the user is "good" at a particular term, a negative score 
 * (down to -400) means the user is "bad" at that term. In theory 0 means that the user gets the 
 * word correct about 50% of the time, 200 means the user gets it right about 3/4 of the time.
 */
@Service(ServiceScope.Request)
export class ScoringService {
    #dataLoaders = new Map<number, DataLoader<number, UserTerm | undefined>>();

    public constructor(
        @Inject(UserTerm) private readonly userTermRepo: () => Repository<UserTerm>,
    ) {
    }
    
    private getDataLoader(userId: number) {
        let loader = this.#dataLoaders.get(userId);
        if (!loader)
            this.#dataLoaders.set(userId, loader = new DataLoader(async termIds => {
                const scores = await this.getUserScoresInternal(this.userTermRepo(), userId, termIds);
                return termIds.map(termId => scores.find(s => s.termId === termId));
            }));
        return loader;
    }

    private getUserScoresInternal(repo: Repository<UserTerm>, userId: number, termIds: readonly number[]) {
        return repo.find({ 
            where: { 
                userId, 
                termId: In(termIds as number[])
            } 
        });
    }

    public getUserLowestScoring(userId: number, count: number) {
        return this.userTermRepo().find({
            where: { userId },
            order: { score: "ASC" },
            take: count,
        })
    }


    public getUserRecentFailures(userId: number, count: number) {
        return this.userTermRepo().find({
            where: { userId, lastResultSuccess: false },
            order: { lastPlayed: "ASC" },
            take: count,
        })
    }

    /**
     * Gets a user's score against a particular term.
     * 
     * @param userId The ID of the user whose scores should be updated.
     * @param termIds The IDs of the terms to get the scores for.
     * 
     * @remarks Calls are batched using DataLoader.
     */
    public getUserScore(userId: number, termId: number) {
        return this.getDataLoader(userId).load(termId);
    }

    /**
     * Gets a list of scores for the specified user and the specified terms.
     * 
     * @param userId The ID of the user whose scores should be updated.
     * @param termIds The IDs of the terms to get the scores for.
     */
    public getUserScores(userId: number, termIds: number[]) {
        return this.getUserScoresInternal(this.userTermRepo(), userId, termIds);
    }

    /**
     * Updates a user's scores against the specified terms.
     * 
     * @param userId The ID of the user whose scores should be updated.
     * @param wins The IDs of the terms which were entered correctly into the crossword.
     * @param losses The IDs of the terms which were entered incorrectly into the crossword.
     */
    public async updateUserScores(userId: number, wins: Iterable<number>, losses: Iterable<number>) {
        const repo = this.userTermRepo();
        const userTerms = await this.getUserScoresInternal(repo, userId, [...wins, ...losses]);

        const now = new Date();

        // Do stuff
        for (const list of [wins, losses]) {
            for (const termId of list) {
                let userTerm = userTerms.find(t => t.termId === termId);
                if (!userTerm) {
                    userTerm = new UserTerm({
                        userId,
                        termId,
                        attempts: 0,
                        successes: 0,
                        score: DEFAULT_SCORE,
                    });
                    userTerms.push(userTerm);
                }

                userTerm.lastPlayed = now;
                userTerm.attempts++;
                userTerm.lastResultSuccess = (list === wins);
                if (list === wins)
                    userTerm.successes++;

                // TODO: Calculate scores on a continuum? E.g. Correct = 1. Nearly correct = 0.5, Wrong = 0
                const score = list === wins ? 1 : 0;
                const expectedScore = 1 / (1 + 10 ** (userTerm.score / 400));
                userTerm.score += K * (score - expectedScore);
                if (userTerm.score < MIN_SCORE)
                    userTerm.score = MIN_SCORE;
                else if (userTerm.score > MAX_SCORE)
                    userTerm.score = MAX_SCORE;
            }
        }

        // TODO Here we're assuming there won't be any insert conflicts from concurrent operations.
        // It seems pretty unlikely because conflicts could only be scoped to a single user.
        // 
        // A more efficient and conflict-avoiding method could be to use the postgres ON CONFLICT
        // clause to move the updates into there - it would be simple enough to perform the 
        // Elo calculations in SQL.
        await repo.save(userTerms);
    }
}
