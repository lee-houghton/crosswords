import { Task } from "../core/tasks/ITaskScheduler";
import { TASK_GENERATE_CROSSWORD } from "./GenerateCrosswordService";

export interface GenerateCrosswordTask extends Task {
    type: typeof TASK_GENERATE_CROSSWORD;
    crosswordId: number;
}
