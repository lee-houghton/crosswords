import { Inject } from "injection-js";
import { Repository } from "typeorm";
import { Service, ServiceScope } from "../../../decorators";
import { Crossword } from "../../entities";

@Service(ServiceScope.Request)
export class CrosswordSvgService {
    public constructor(@Inject(Crossword) private readonly crosswordRepo: () => Repository<Crossword>) {}

    public async generateSvgFromCrossword(crossword: Crossword, background = false, realAnswers = false) {
        const grid: string[][] = [];
        for (let y = 0; y < crossword.height!; y++) {
            const row: string[] = [];
            for (let x = 0; x < crossword.width!; x++) row.push("");
            grid.push(row);
        }

        const numbers = grid.map((row) => row.map((s) => undefined)) as Array<Array<number | undefined>>;

        for (const clue of crossword.clues!) {
            let { x, y } = clue;
            numbers[y][x] = clue.number;
            for (const c of clue.answer) {
                grid[y][x] = c;
                if (clue.direction === "a") x++;
                else y++;
            }
        }

        const size = 48;
        const fontSize = 26;

        const bgcolor = background ? "#666" : "#000";

        return `<svg xmlns="http://www.w3.org/2000/svg" width="${crossword.width! * size}" height="${
            crossword.height! * size
        }">
            <style>
                .box {
                    stroke: ${bgcolor};
                    fill: #fff;
                    stroke-width: 2px;
                }
                .num {
                    font-size: ${fontSize / 2}px;
                    dominant-baseline: hanging;
                }
                .ans {
                    font-size: ${fontSize}px;
                    dominant-baseline: middle;
                    text-anchor: middle;
                    font-family: sans-serif;
                    text-transform: uppercase
                }
            </style>
            <rect width="${crossword.width! * size}" height="${crossword.height! * size}" fill="${bgcolor}"/>
            ${grid.map((row, y) =>
                row
                    .map((char, x) =>
                        char
                            ? `<rect class="box" width="${size}" height="${size}" x="${x * size}" y="${y * size}"/>
                ${
                    numbers[y][x]
                        ? `<text class="num" x="${x * size + 2}" y="${y * size + 2}">${numbers[y][x]}</text>`
                        : ""
                }
                <text class="ans" x="${x * size + size / 2}" y="${y * size + size / 2}">${
                                  realAnswers ? char : randomChar()
                              }</text>`
                            : ""
                    )
                    .join("\n")
            )}
        </svg>`
            .replace(/\s+/g, " ")
            .replace(/> </g, "><");
    }

    public async generateSvg(crosswordId: number, background = false) {
        const crossword = await this.crosswordRepo().findOne({ where: { id: crosswordId }, relations: ["clues"] });
        if (!crossword) throw new Error(`Crossword ${crosswordId} does not exist`);

        if (!crossword.available) throw new Error(`Crossword ${crosswordId} has not been generated yet`);

        return this.generateSvgFromCrossword(crossword, background);
    }
}

/**
 * Returns a random uppercase character from A to Z.
 */
function randomChar() {
    return String.fromCharCode(Math.floor(Math.random() * 26 + 65));
}
