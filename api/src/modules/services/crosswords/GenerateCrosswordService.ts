import { Inject } from "injection-js";
import { Repository } from "typeorm";
import { Service, ServiceScope } from "../../../decorators";
import { Clue, Crossword, CrosswordAudit, Deck } from "../../entities";
import { Cell } from "../../entities/Cell";
import { AuditService } from "../audit/AuditService";
import { Grid, WordPlacement } from "../core/generation/grid";
import { ILexiconEntry, Lexicon } from "../core/generation/lexicon";
import { ITaskRunner } from "../core/tasks/ITaskRunner";
import { LoggerService } from "../LoggerService";
import { StatsService } from "../StatsService";
import { GenerateCrosswordTask } from "./GenerateCrossword.task";
import { ScoringService } from "./ScoringService";

export const TASK_GENERATE_CROSSWORD = "generate-crossword";

@Service(ServiceScope.Request)
export class GenerateCrosswordService implements ITaskRunner<GenerateCrosswordTask> {
    public static handledTypes = [TASK_GENERATE_CROSSWORD];

    public constructor(
        @Inject(Crossword) private readonly crosswordRepo: () => Repository<Crossword>,
        @Inject(Clue) private readonly clueRepo: () => Repository<Clue>,
        @Inject(Cell) private readonly cellRepo: () => Repository<Cell>,
        @Inject(Deck) private readonly deckRepo: () => Repository<Deck>,
        private logger: LoggerService,
        private statsService: StatsService,
        private scoringService: ScoringService,
        private auditService: AuditService
    ) {}

    public async runTask(task: GenerateCrosswordTask) {
        await this.generateCrossword(task.crosswordId, true);
    }

    private makeClue(crossword: Crossword | undefined, placement: WordPlacement) {
        return new Clue({
            crossword,
            clue: placement.entry.definition,
            answer: placement.entry.term,
            originalAnswer: placement.entry.source.term,
            direction: placement.dir === "across" ? "a" : "d",
            deckId: placement.entry.sourceSet.id,
            x: placement.x,
            y: placement.y,
            lengthHint: placement.entry.wordLengths.filter((len) => len > 0).join(", "),
            termId: placement.entry.source.id,
        });
    }

    private async getScoresMap(userId: number, decks: Deck[]) {
        const termIds = decks.flatMap((d) => d.terms).flatMap((t) => t.id);
        return new Map((await this.scoringService.getUserScores(userId, termIds)).map((s) => [s.termId, s.score]));
    }

    private weightedShuffle(entries: ILexiconEntry[], scoresMap?: Map<number, number>) {
        const buckets: ILexiconEntry[][] = [];

        for (const entry of entries) {
            const score = scoresMap?.get(entry.source.id) ?? 0;
            const bucket = Math.floor((score + 400) / 100);
            buckets[bucket] = buckets[bucket] || [];
            buckets[bucket].push(entry);
        }

        for (const bucket of buckets) if (bucket) shuffle(bucket);

        return buckets.flat();
    }

    private async generateClues(
        crossword: Crossword | undefined,
        userId: number | undefined,
        decks: Deck[],
        width: number,
        height: number
    ) {
        if (decks.length === 0) throw new Error("Need at least one word list to make a crossword");

        const scoresMap = userId ? await this.getScoresMap(userId, decks) : undefined;
        const lexicon = new Lexicon(decks, 2, Math.max(width, height) - 1);
        const grid = new Grid(lexicon, width, height);

        if (lexicon.entries.length === 0) throw new Error("Need at least one term to make a crossword");

        const clues: Clue[] = [];
        const entries = this.weightedShuffle(lexicon.entries, scoresMap);

        const seedTerm = lexicon.randomEntry();
        const dir = Math.random() > 0.5 ? "across" : "down";
        const seed = grid.place({
            word: seedTerm.term,
            entry: seedTerm,
            x: Math.floor(width / 2) - (dir === "across" ? Math.ceil(seedTerm.term.length / 2) : 0),
            y: Math.floor(height / 2) - (dir === "down" ? Math.ceil(seedTerm.term.length / 2) : 0),
            dir,
        });
        clues.push(this.makeClue(crossword, seed));

        while (
            entries.some((entry, i) => {
                const placement = grid.tryPlaceWord(entry);
                if (placement) {
                    entries.splice(i, 1);
                    clues.push(this.makeClue(crossword, placement));
                    return true;
                }
                return false;
            })
        );

        // Assign numbers to to the acrosses and downs in (hopefully) the correct order.
        // Two clues can have the same number if they start on the same square (one across, one down.)
        //
        // So sorting the squares top to bottom, left to right, across then down, then iterating through
        // them should generate clue numbers in the correct order (ensuring that the same square doesn't
        // get two clue numbers).
        //
        // This will also generate the Clue entities in ascending ID order, well... hopefully.
        // It's not actually required, just helps debugging.
        clues.sort((c1, c2) => c1.y - c2.y || c1.x - c2.x || (c1.direction > c2.direction ? 1 : -1));

        let clueNumber = 0;
        let last = "";
        for (const clue of clues) {
            const current = `${clue.x},${clue.y}`;

            if (current !== last) {
                clueNumber++;
                last = current;
            }

            clue.number = clueNumber;
        }

        return clues;
    }

    public async generateCrosswordFromDecks(userId: number | undefined, decks: Deck[], width: number, height: number) {
        var crossword = new Crossword({
            title: decks.map((d) => d.title).join(", "),
            width,
            height,
            clues: await this.generateClues(undefined, userId, decks, width, height),
            available: new Date(),
        });

        crossword.cells = getCells(crossword);
        return crossword;
    }

    public async generateCrossword(id: number, save: boolean) {
        try {
            this.logger.debug(`Generating crossword ${id}`);
            const crosswordRepo = this.crosswordRepo();
            const crossword = await crosswordRepo.findOne({
                where: { id },
                join: {
                    alias: "crossword",
                    leftJoinAndSelect: {
                        decks: "crossword.decks",
                        terms: "decks.terms",
                        user: "crossword.user",
                    },
                },
            });

            if (!crossword) throw new Error(`Invalid crossword ID: ${id}`);

            if (!crossword.title) {
                crossword.title = crossword.decks!.map((d) => d.title).join(", ");
                if (crossword.title.length > 200) crossword.title = crossword.title.substring(0, 199) + "\u2026";
            }

            crossword.clues = await this.generateClues(
                crossword,
                crossword.userId,
                crossword.decks!,
                crossword.width,
                crossword.height
            );
            crossword.cells = getCells(crossword);
            crossword.available = new Date();

            const isLanguageCrossword = Boolean(crossword.languageId);

            if (save) {
                const languageIds = new Set(crossword.decks?.map((d) => d.termLanguage));
                if (languageIds.size === 1) [crossword.languageId] = languageIds;

                const usedDeckIds = new Set(crossword.clues.map((c) => c.deckId));

                // Check if any decks weren't actually used
                //
                // (Unless it's a crossword for specific word lists, in which case we preserve it to
                // ensure that "play again" works correctly)
                if (isLanguageCrossword) {
                    const unusedDecks = crossword.decks!.filter((d) => !usedDeckIds.has(d.id));

                    if (unusedDecks.length > 0) {
                        this.logger.info(
                            `Crossword ${id} has ${unusedDecks?.length} decks (out of ${crossword.decks?.length}) which did not contribute any terms, removing`
                        );

                        await crosswordRepo
                            .createQueryBuilder("crossword")
                            .relation("decks")
                            .of(crossword)
                            .remove(unusedDecks);

                        // Remove from crossword.decks as well to avoid it being reverted on crosswordRepo.save(crossword)
                    }
                }

                // It's more efficient to save the clues first and then cells, because the cells contain
                // "acrossClueId" and "downClueId" fields. Doing it the other way around results in it
                // executing an UPDATE statement for every cell, whereas this way it executes as two
                // simple INSERT statements.
                crossword.clues = await this.clueRepo().save(crossword.clues);
                crossword.cells = await this.cellRepo().save(crossword.cells);
                crossword.decks = undefined; // Avoid this being overwritten
                await crosswordRepo.save(crossword);
                this.logger.debug(`Generated crossword: ${id}`);

                if (crossword.userId && crossword.languageId)
                    await this.statsService.incrementUserLanguageStats(crossword.userId, crossword.languageId, {
                        crosswordsCreated: 1,
                    });

                await this.deckRepo()
                    .createQueryBuilder()
                    .update()
                    .set({ crosswordCount: () => `"crosswordCount" + 1` })
                    .whereInIds(usedDeckIds)
                    .execute();
            }

            await this.auditService.add(CrosswordAudit.generated(crossword.id));

            return crossword;
        } catch (err) {
            this.auditService.tryAdd(CrosswordAudit.error(id, err instanceof Error ? err.message : `${err}`));
            throw err;
        }
    }
}

function shuffle<T>(array: T[]) {
    for (let i = array.length - 1; i > 0; i--) {
        const temp = array[i];
        const j = Math.floor(Math.random() * (i + 1));
        array[i] = array[j];
        array[j] = temp;
    }
}

function getCells(crossword: Crossword) {
    if (!crossword.clues) throw new Error("getCells: crossword.clues is not set");

    const cells = new Map<string, Cell>();
    for (const clue of crossword.clues) {
        const { x, y, direction, answer } = clue;
        const dx = clue.direction === "a" ? 1 : 0;
        const dy = 1 - dx;
        for (let i = 0; i < answer.length; i++) {
            const key = `${x + dx * i},${y + dy * i}`;
            const existing = cells.get(key);
            if (existing) {
                existing[direction === "a" ? "acrossClue" : "downClue"] = clue;
                continue;
            }

            cells.set(
                key,
                new Cell({
                    crossword,
                    x: x + dx * i,
                    y: y + dy * i,
                    contents: answer[i],
                    [direction === "a" ? "acrossClue" : "downClue"]: clue,
                })
            );
        }
    }

    return [...cells.values()];
}
