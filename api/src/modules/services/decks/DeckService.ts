import { subHours } from "date-fns";
import { Inject } from "injection-js";
import { Connection, In, Repository } from "typeorm";
import { Service, ServiceScope } from "../../../decorators";
import { ActivityType } from "../../../types";
import { Activity, Deck, DeckAudit, Term, User } from "../../entities";
import { CreateDeckRequest, UpdateDeckRequest, UpdateTermRequest } from "../../schema/types";
import { ActivityService } from "../activity";
import { AuditService } from "../audit/AuditService";
import { LoggerService } from "../LoggerService";
import { StatsService } from "../StatsService";
import { isDeepStrictEqual } from "util";
import { SearchService } from "../search";
import DataLoader from "dataloader";
import { ForbiddenError, NotFoundError } from "../../../errors";

@Service(ServiceScope.Request)
export class DeckService {
    private deckDataLoader = new DataLoader(
        async (ids: readonly number[]) => {
            const decks = await this.deckRepo().find({ where: { id: In(ids) } });
            return ids.map((id) => decks.find((d) => d.id === id) || new Error(`Deck ${id} not found`));
        },
        { maxBatchSize: 250 }
    );

    private deckDataLoaderWithTerms = new DataLoader(
        async (ids: readonly number[]) => {
            const decks = await this.deckRepo().find({ where: { id: In(ids) }, relations: ["terms"] });
            return ids.map((id) => decks.find((d) => d.id === id) || new Error(`Deck ${id} not found`));
        },
        { maxBatchSize: 250 }
    );

    private termDataLoader = new DataLoader(
        async (ids: readonly number[]) => {
            const terms = await this.termRepo().findByIds(ids as number[]);
            return ids.map((id) => terms.find((t) => t.id === id) || new Error(`Term ${id} not found`));
        },
        { maxBatchSize: 500 }
    );

    public constructor(
        private readonly connection: Connection,
        @Inject(Deck) private readonly deckRepo: () => Repository<Deck>,
        @Inject(Term) private readonly termRepo: () => Repository<Term>,
        @Inject(User) private readonly userRepo: () => Repository<User>,
        private readonly statsService: StatsService,
        private readonly auditService: AuditService,
        private readonly activityService: ActivityService,
        private readonly logger: LoggerService,
        private readonly search: SearchService
    ) {}

    public getById(id: number, includeTerms = false): Promise<Deck> {
        const loader = includeTerms ? this.deckDataLoaderWithTerms : this.deckDataLoader;
        return loader.load(id);
    }

    public getTermById(id: number): Promise<Term> {
        return this.termDataLoader.load(id);
    }

    public getByIds(ids: number[]): Promise<Deck[]> {
        return this.deckRepo().findByIds(ids);
    }

    /**
     * Updates whether or not the user has marked this deck as a favourite, and
     * updates the `stars` field on the deck.
     */
    public async toggleDeckFavourite(id: number, favourite: boolean, user: User) {
        if (favourite)
            await this.userRepo().query(
                `
                with ins as (
                    insert into user_favourite_decks_deck ("deckId", "userId")
                    values ($1, $2)
                    on conflict do nothing
                    returning "deckId"
                )
                update deck set stars = stars + 1 where id in (select "deckId" from ins);

            `,
                [id, user.id]
            );
        else
            await this.userRepo().query(
                `
                with del as (
                    delete from user_favourite_decks_deck
                    where "deckId" = $1 and "userId" = $2
                    returning "deckId"
                )
                update deck set stars = stars - 1 where id in (select "deckId" from del);
            `,
                [id, user.id]
            );
    }

    public async createDeck(request: CreateDeckRequest, user: User) {
        let deck = new Deck();

        deck.termLanguage = request.termLangId;
        deck.definitionLanguage = request.definitionLangId;

        deck.userId = user.id;
        deck.title = request.title;
        deck.description = request.description || "";
        deck.termCount = request.terms?.length || 0;

        deck = await this.connection.manager.transaction(async (manager) => {
            deck = await manager.save(deck);

            if (request.terms) {
                const terms = request.terms.map(
                    (t, i) =>
                        new Term({
                            order: i + 1,
                            deckId: deck.id,
                            term: t.term,
                            definitions: t.definitions,
                            pronunciation: "",
                        })
                );
                await manager.save(terms);
            }

            return deck;
        });

        await this.statsService.calculateUserDeckStats(user.id);

        this.tryIndexDeck(deck, undefined);

        this.auditService.tryAdd(DeckAudit.created(deck.id, user.id));
        this.logActivity(deck.id, ActivityType.DeckCreated, user);

        return deck;
    }

    private getDeckChanges(deck: Deck, req: UpdateDeckRequest) {
        let diff: Partial<Deck> | undefined;
        check("title", "title");
        check("description", "description");
        check("termLanguage", "termLangId");
        check("definitionLanguage", "definitionLangId");
        if (req.terms && deck.terms.length !== req.terms.length) {
            diff = diff || {};
            diff.termCount = req.terms.length;
        }

        return diff;

        function check(key: keyof Deck, reqKey: keyof UpdateDeckRequest) {
            if (req[reqKey] == null || deck[key] === req[reqKey]) return;

            if (!diff) diff = {};
            diff[key] = req[reqKey] as any;
        }
    }

    public async updateDeck(request: UpdateDeckRequest, user: User) {
        const repo = this.deckRepo();
        const deck = await repo.findOne({ where: { id: request.id }, relations: ["terms"] });
        if (!deck) throw new NotFoundError(`Deck ${request.id} not found`);

        if (user.id !== deck.userId) throw new ForbiddenError("You can only edit your own decks");

        let termsChanged = false;

        const newTerms: Term[] = [];
        const updatedTerms: Term[] = [];
        const removedTermIds: number[] = [];

        if (request.terms) {
            request.terms.forEach((termReq, i) => {
                let term = termReq.id ? deck.terms.find((t) => t.id === termReq.id) : undefined;

                const fields: Partial<Term> = {
                    id: termReq.id || undefined,
                    term: termReq.term,
                    definitions: termReq.definitions,
                    order: i + 1,
                    pronunciation: "",
                    deckId: deck.id,
                };

                if (!term) {
                    newTerms.push(new Term(fields));
                    return;
                }

                let updated = false;
                for (const key in fields) {
                    if (isDeepStrictEqual(fields[key], term[key])) continue;
                    updated = true;
                    term[key] = fields[key];
                }

                if (updated) updatedTerms.push(term);
            });

            for (const term of deck.terms)
                if (!request.terms.some((t) => t.id === term.id)) removedTermIds.push(term.id);

            const termRepo = this.termRepo();
            await termRepo.insert(newTerms);
            await termRepo.save(updatedTerms);
            if (removedTermIds.length > 0)
                await termRepo
                    .createQueryBuilder()
                    .update()
                    .set({ deckId: null! })
                    .whereInIds(removedTermIds)
                    .execute();

            // Avoid creating an audit when nothing changed
            termsChanged = newTerms.length > 0 || updatedTerms.length > 0 || removedTermIds.length > 0;

            if (newTerms.length > 0) this.logTermsAdded(deck.id, user, newTerms.length);

            // This is updated below by getDeckChanges but we also need to return the correct amount from
            // the mutation so that the Apollo cache is correctly updated
            deck.termCount = request.terms.length;
        }

        const deckChanges = this.getDeckChanges(deck, request);
        if (deckChanges) await repo.update(deck.id, deckChanges);

        // This can be avoided most of the time, since usually the term language won't change
        if (request.termLangId && deck.termLanguage !== request.termLangId)
            await this.statsService.calculateUserDeckStats(user.id);

        if (deckChanges || termsChanged) this.auditService.tryAdd(DeckAudit.updated(deck.id, user.id));

        // TODO: newTerms don't have IDs
        this.tryIndexDeck(deck, [...newTerms, ...updatedTerms]);

        if (removedTermIds.length > 0)
            this.search
                .deindexTermsById(removedTermIds)
                .catch((err) => this.logger.error(`Failed to deindex terms for deck "${deck.title}": ${err.message}`));

        return deck;
    }

    /**
     * Try to index the deck, but do so asynchronously (don't block the current request from returning.)
     *
     * This also prevents errors when updating decks, which is good as we wouldn't want indexing errors
     * to cause problems using the app.
     */
    private tryIndexDeck(deck: Deck, terms?: Term[]) {
        this.search
            .indexDeck(deck, terms)
            .catch((err) => this.logger.error(`Failed to index deck "${deck.title}": ${err.message}`));
    }

    public async updateTerm(request: UpdateTermRequest, user: User) {
        const repo = this.termRepo();
        const deckRepo = this.deckRepo();
        const deck = await deckRepo.findOneOrFail({ where: { id: request.deckId } });
        const term = request.id
            ? await repo.findOneOrFail({ where: { id: request.id } })
            : new Term({ deckId: request.deckId, pronunciation: "", order: deck.termCount + 1 });

        if (deck.userId !== user.id) throw new ForbiddenError("You can only edit your own terms");

        if (term.deckId !== request.deckId) throw new ForbiddenError("Incorrect word list ID specified");

        term.term = request.term;
        term.definitions = request.definitions;

        await this.auditService.add(DeckAudit.termEdited(deck.id, user.id));
        if (!request.id) {
            await deckRepo.update(deck.id, { termCount: () => `"termCount" + 1` });
            this.logTermsAdded(deck.id, user, 1);
        }

        this.search
            .indexTerms(deck.termLanguage, deck.definitionLanguage, [term])
            .catch((err) =>
                this.logger.error(`Failed to index term "${term.term}=${term.definitions}": ${err.message}`)
            );

        return await repo.save(term);
    }

    public async getTerms(deckId: number) {
        const deck = await this.deckRepo().findOne({ where: { id: deckId }, relations: ["terms"] });
        return deck ? deck.terms : [];
    }

    private async logActivity(deckId: number, type: ActivityType, user: User, updater?: (activity: Activity) => void) {
        try {
            this.activityService.addOrUpdate(user, type, subHours(new Date(), 18), (activity) => {
                activity.deckIds = activity.deckIds || [];
                if (!activity.deckIds.includes(deckId)) activity.deckIds.push(deckId);
                updater?.(activity);
            });
        } catch (err) {
            this.logger.error(`Error logging ${ActivityType[type]} activity: ${err}`);
        }
    }

    private async logTermsAdded(deckId: number, user: User, count: number) {
        return this.logActivity(deckId, ActivityType.DeckTermsAdded, user, (activity) => {
            const termCount = activity.parameters?.[0];
            if (termCount) activity.parameters = [`${(parseInt(termCount) || 0) + count}`];
            else activity.parameters = [`${count}`];
        });
    }

    public async topDecks(termLang: string, definitionLang: string | undefined, count: number) {
        let query = this.deckRepo()
            .createQueryBuilder("deck")
            .limit(Math.min(count, 25))
            .where("deck.termLanguage = :termLang", { termLang })
            .andWhere("deck.termCount > 0");

        if (definitionLang) query = query.andWhere("deck.definitionLanguage = :definitionLang", { definitionLang });

        return query.orderBy(`deck.stars * 10 + deck."crosswordCount"`, "DESC").getMany();
    }
}
