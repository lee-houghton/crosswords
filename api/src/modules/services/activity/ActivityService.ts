import { subHours } from "date-fns";
import { Inject } from "injection-js";
import { LessThan, MoreThanOrEqual, Repository } from "typeorm";
import { Service } from "../../../decorators";
import { ActivityType } from "../../../types";
import { Activity, User } from "../../entities";
import { PageInfo } from "../../schema/types";
import { UserInputError } from "../../../errors";

@Service()
export class ActivityService {
    public constructor(@Inject(Activity) private activityRepo: () => Repository<Activity>) {}

    public async add(activity: Activity) {
        this.activityRepo().save(activity);
    }

    public async find(
        userId: number,
        type: ActivityType,
        minDate: Date = subHours(new Date(), 1),
        repo = this.activityRepo()
    ) {
        const [activity] = await repo.find({
            where: {
                type,
                userId,
                date: MoreThanOrEqual(minDate),
            },
        });
        return activity;
    }

    public async updateOrCreate<T>(
        user: User,
        type: ActivityType,
        minDate: Date = subHours(new Date(), 1),
        updater: (activity: Activity, repo: Repository<Activity>) => Promise<T>
    ) {
        const repo = this.activityRepo();
        let activity = await this.find(user.id, type, minDate, repo);
        if (!activity)
            activity = new Activity({
                type,
                userId: user.id,
            });

        return updater(activity, repo);
    }

    public async addOrUpdate(
        user: User,
        type: ActivityType,
        minDate: Date = subHours(new Date(), 1),
        updater: (activity: Activity) => void
    ) {
        return this.updateOrCreate(user, type, minDate, (activity, repo) => {
            updater(activity);
            return repo.save(activity);
        });
    }

    public async getRecentActivity(count = 3, cursor: string | undefined, user: User): Promise<Activities> {
        let builder = this.activityRepo()
            .createQueryBuilder("activity")
            .innerJoin("activity.user", "user")
            .innerJoin("user.followedBy", "follower", "follower.id = :userId")
            .setParameter("userId", user.id);

        if (cursor) {
            const lastId = parseInt(cursor, 10);
            if (isNaN(lastId)) throw new UserInputError("Cursor is invalid");
            builder = builder.where({ id: LessThan(lastId) });
        }

        const activities = await builder.limit(count).orderBy("activity.id", "DESC").getMany();
        return {
            activities,
            page: {
                startCursor: activities[0]?.id?.toString(),
                endCursor: activities[activities.length - 1]?.id?.toString(),
            },
        };
    }
}

export interface Activities {
    activities: Activity[];
    page: PageInfo;
}
