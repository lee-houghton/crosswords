import { Inject } from "injection-js";
import { Repository } from "typeorm";
import { Service } from "../../decorators";
import { Deck, UserLanguage } from "../entities";

@Service()
export class StatsService {
    public constructor(
        @Inject(UserLanguage)
        private readonly userLanguageRepository: () => Repository<UserLanguage>,
        @Inject(Deck) private readonly deckRepository: () => Repository<Deck>
    ) {}

    // This could be done as an insert().onConflict(), maybe. It might even be easier?
    public async incrementUserLanguageStats(
        userId: number,
        languageId: string,
        fields: Partial<UserLanguage>
    ) {
        const repo = this.userLanguageRepository();
        const userLanguage =
            (await repo.findOne({ where: { userId, languageId } })) ||
            new UserLanguage({ userId, languageId });

        for (const field of [
            "crosswordsCompleted",
            "crosswordsCreated",
            "decks",
            "favourites",
        ])
            if (fields[field] > 0)
                userLanguage[field] =
                    (userLanguage[field] ?? 0) + fields[field];

        await repo.save(userLanguage);
    }

    // This seems like far too much work given that databases can do this automatically with indexed views
    // TODO: Use an indexed view?
    public async calculateUserDeckStats(userId: number) {
        const statsRepo = this.userLanguageRepository();
        const deckRepo = this.deckRepository();

        const counts: Array<{ termLanguage: string; count: number }> =
            await deckRepo
                .createQueryBuilder("deck")
                .select("deck.termLanguage, count(*) :: int as count")
                .where("deck.userId = :userId", { userId })
                .andWhere("deck.termLanguage is not null")
                .groupBy("deck.termLanguage")
                .getRawMany();

        const userLanguages = await statsRepo.find({ where: { userId } });

        // Handle any languages that used to be linked to this user but no longer are
        // (probably happened by changing the language of their last word list in this language)
        for (const lang of userLanguages) lang.decks = 0;

        for (const item of counts) {
            let lang = userLanguages.find(
                (ul) => ul.languageId === item.termLanguage
            );
            if (!lang) {
                lang = new UserLanguage({
                    userId,
                    languageId: item.termLanguage,
                });
                userLanguages.push(lang);
            }

            lang.decks = item.count;
        }

        await statsRepo.save(userLanguages);
    }
}
