declare module "*.graphql" {
    const schema: import("graphql").DocumentNode;
    export = schema;
}

declare module "iso-639" {
    export interface Language1 {
        "639-1": string;
        "639-2": string;
        family: string;
        name: string;
        nativeName: string;
        wikiUrl: string;
    }

    export interface Language2 {
        "639-1": string;
        "639-2": string;
        de: string[];
        en: string[];
        fr: string[];
        wikiUrl: string;
    }

    export const iso_639_1: Language1[];
    export const iso_639_2: Language2[];
}

declare namespace Express {
    interface User {
        id: number;
    }
}
