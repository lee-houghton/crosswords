import { ApolloServer } from "@apollo/server";
import { expressMiddleware } from "@apollo/server/express4";
import { ApolloServerPluginDrainHttpServer } from "@apollo/server/plugin/drainHttpServer";
import { makeExecutableSchema } from "@graphql-tools/schema";
import type { IResolvers } from "@graphql-tools/utils";
import cors from "cors";
import { Application, Request, json } from "express";
import { GraphQLScalarType, GraphQLSchema } from "graphql";
import { GraphQLDate, GraphQLDateTime, GraphQLTime } from "graphql-iso-date";
import { GraphQLJSON } from "graphql-type-json";
import { useServer } from "graphql-ws/lib/use/ws";
import { Server } from "http";
import { Inject } from "injection-js";
import { Server as WebSocketServer } from "ws";
import { Container } from "../../container";
import { Service } from "../../decorators";
import { Context } from "../../interfaces";
import { ISchemaProvider } from "../../interfaces/ISchemaProvider";
import { requestChildContainerToken } from "../../tokens";
import {
    adminVisibleDirective,
    adminOnlyDirective,
    authDirective,
    authNullDirective,
    selfOnlyDirective,
    selfVisibleDirective,
} from "../directives";
import { User } from "../entities/User";
import { schemaTypeDefs } from "../schema/typeDefs";
import type { IEndpointProvider } from "./Http";
import { AuthService } from "./auth";
import { frontEndUrlToken } from "../../config";

@Service()
export class GraphQL implements IEndpointProvider {
    private readonly server: ApolloServer;
    private readonly schema: GraphQLSchema;

    public constructor(
        @Inject(ISchemaProvider) schemaProviders: ISchemaProvider[],
        @Inject(requestChildContainerToken)
        private readonly childContainerFactory: (user?: User) => Container,
        private readonly authService: AuthService,
        @Inject(frontEndUrlToken) private readonly frontEndUrl: string
    ) {
        const resolvers: IResolvers = {
            Date: GraphQLDate,
            Time: GraphQLTime,
            DateTime: GraphQLDateTime,
            JSON: GraphQLJSON,
            Null: new GraphQLScalarType({
                name: "Null",
                description: "Null",
                serialize: (x) => null,
                parseValue: (x) => null,
                parseLiteral: (ast) => null,
            }),
        };

        for (const schema of schemaProviders) {
            for (const typeName in schema.resolvers) {
                if (!schema.resolvers.hasOwnProperty(typeName)) continue;

                resolvers[typeName] = resolvers[typeName] || {};
                Object.assign(resolvers[typeName], schema.resolvers[typeName]);
            }
        }

        this.schema = makeExecutableSchema<Context>({
            typeDefs: schemaTypeDefs,
            resolvers,
            inheritResolversFromInterfaces: true,
        });

        this.schema = adminOnlyDirective(this.schema);
        this.schema = adminVisibleDirective(this.schema);
        this.schema = authDirective(this.schema);
        this.schema = selfOnlyDirective(this.schema);
        this.schema = selfVisibleDirective(this.schema);
        this.schema = authNullDirective(this.schema);

        this.server = new ApolloServer({
            schema: this.schema,
            formatError: (err) => {
                console.error("GraphQL error:", err.message, err.extensions?.code || "");
                // const stack = err.extensions?.exception?.stacktrace;
                // if (stack) console.error(stack);
                return {
                    message: err.message,
                    path: err.path,
                    locations: err.locations,
                };
            },
        });
    }

    public registerEndpoints(app: Application) {}

    private getDynamicContext(user: User | undefined, req?: Request) {
        const { childContainerFactory } = this;
        const container = childContainerFactory(user);
        const resolve = (serviceIdentifier: unknown) => container.get(serviceIdentifier);

        if (!req) return { user, container, resolve };

        const context = {
            user,
            container,
            resolve,
            login(user: User) {
                return new Promise<void>((resolve, reject) =>
                    req.login(user, (err) => {
                        if (err) reject(err);

                        this.setUser(user as User);
                        resolve();
                    })
                );
            },
            logout() {
                this.setUser(undefined);
                req.logout(() => {});
            },
            setUser(user: User | undefined) {
                this.user = user;
                this.container = childContainerFactory(user);
                this.resolve = (serviceIdentifier) => this.container.get(serviceIdentifier);
            },
        };

        return context;
    }

    public addServerHandlers(server: Server, app: Application) {
        this.server.addPlugin(ApolloServerPluginDrainHttpServer({ httpServer: server }));

        const wsServer = new WebSocketServer({
            server,
        });
        const serverCleanup = useServer(
            {
                schema: this.schema,
                onConnect: async (ctx) => {
                    // Authenticate the user using the passport middleware
                    const req = ctx.extra.request as Request;
                    await this.authService.runSessionMiddleware(req as Request);

                    return { user: req.user };
                },
                context: async (ctx, msg, args) => {
                    const req = ctx.extra.request as Request;

                    // You can define your own function for setting a dynamic context
                    // or provide a static value
                    return this.getDynamicContext(req.user as User | undefined);
                },
            },
            wsServer as any // XXX Type clashes?
        );

        this.server.addPlugin({
            serverWillStart: async () => ({
                async drainServer() {
                    await serverCleanup.dispose();
                },
            }),
        });

        this.server.start().then(
            () =>
                app.use(
                    "/graphql",
                    cors({ origin: new URL(this.frontEndUrl).origin, credentials: true }),
                    json(),
                    expressMiddleware(this.server, {
                        context: async ({ req }) => this.getDynamicContext(req.user as User | undefined, req),
                    })
                ),
            (error) => {
                console.error(error);
            }
        );
    }
}
