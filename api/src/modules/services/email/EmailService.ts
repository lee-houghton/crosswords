import { Inject, InjectionToken } from "injection-js";
import Mailgun from "mailgun-js";
import { Service } from "../../../decorators";

export const mailDomainToken = new InjectionToken<string>("MAIL_DOMAIN");
export const mailgunApiKeyToken = new InjectionToken<string>("MAILGUN_API_KEY");

@Service()
export class EmailService {
    private mailgun: Mailgun.Mailgun;

    public constructor(
        @Inject(mailDomainToken) private readonly domain: string,
        @Inject(mailgunApiKeyToken) apiKey: string,
    ) {
        if (!domain)
            throw new Error("MAIL_DOMAIN is not defined");
        if (!apiKey)
            throw new Error("MAILGUN_API_KEY is not defined");

        this.mailgun = new Mailgun({
            apiKey,
            domain,
            host: "api.eu.mailgun.net", // TODO: Env var, and use official mailgun SDK
        });
    }

    public sendEmail(email: string, subject: string, body: string) {
        return this.mailgun.messages().send({
            from: `noreply@${this.domain}`,
            to: email,
            subject,
            text: body,
        });
    }
}
