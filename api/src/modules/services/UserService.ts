import DataLoader from "dataloader";
import { Inject } from "injection-js";
import { In, Repository } from "typeorm";
import { Service, ServiceScope } from "../../decorators";
import { PartitionedDataLoader } from "../../utils/PartitionedDataLoader";
import { Deck, User, UserAudit, UserLanguage } from "../entities";
import { AuditService } from "./audit/AuditService";
import { UserInputError } from "../../errors";

@Service(ServiceScope.Request)
export class UserService {
    private userDataLoader = new DataLoader<number, User>(async (userIds) => {
        const users = await this.userRepository().find({
            where: { id: In(userIds as number[]) },
        });
        return userIds.map((id) => users.find((u) => u.id === id) || new Error(`User ${id} not found`));
    });

    private userDecksDataLoader = new DataLoader<number, Deck[]>(async (userIds) => {
        const decks = await this.deckRepository().find({
            where: {
                userId: In(userIds as number[]),
            },
        });
        return userIds.map((id) => decks.filter((d) => d.userId === id));
    });

    private isFollowedDataLoader = new PartitionedDataLoader<User, number, boolean>(async (user, userIds) => {
        // Simple but inefficient
        // const values = await this.userRepository()
        //     .createQueryBuilder()
        //     .relation("followedUsers")
        //     .of(user)
        //     .loadMany();

        // Still slightly inefficient - there's extra joins, but that's ORMs for you.
        // TypeORM doesn't provide a way to efficiently check if a relation exists,
        // neither for a single pair of entities or a set of entities.
        const values = await this.userRepository()
            .createQueryBuilder("user")
            .leftJoinAndSelect("user.followedUsers", "followed")
            .where("user.id = :id and followed.id in (:...userIds)", {
                id: user.id,
                userIds,
            })
            .select("followed.id", "id")
            .getRawMany();

        const followedUserIds = new Set<number>(values.map((v) => v.id));
        return userIds.map((id) => followedUserIds.has(id));
    });

    public constructor(
        @Inject(User) private readonly userRepository: () => Repository<User>,
        @Inject(UserLanguage)
        private readonly userLanguageRepository: () => Repository<UserLanguage>,
        @Inject(Deck) private readonly deckRepository: () => Repository<Deck>,
        private readonly auditService: AuditService
    ) {}

    public getById(id: number) {
        return this.userDataLoader.load(id);
    }

    public async getByName(name: string) {
        return this.userRepository().findOne({ where: { name } });
    }

    public async createByName(name: string) {
        let user = new User();
        user.name = name;

        const repo = this.userRepository();
        user = await repo.save(user);
        // Users follow themselves
        await repo.createQueryBuilder("user").relation("followedUsers").of(user.id).add(user.id);
        return user;
    }

    public async getDecks(userId: number) {
        return this.userDecksDataLoader.load(userId);
    }

    public async getFavouriteDecks(userId: number) {
        return this.userRepository().createQueryBuilder().relation("favouriteDecks").of(userId).loadMany<Deck>();
    }

    public async getUserLanguages(userId: number) {
        return await this.userLanguageRepository().find({ where: { userId } });
    }

    public async markNewsRead(user: User, id: number): Promise<User> {
        if (user.lastReadNewsId === undefined || id > user.lastReadNewsId) {
            user.lastReadNewsId = id;
            await this.userRepository().update(user.id, { lastReadNewsId: id });
        }
        return user;
    }

    public async followUser(id: number, follow: boolean, user: User) {
        if (user.id === id) throw new UserInputError("You cannot unfollow yourself");

        const relation = this.userRepository().createQueryBuilder().relation("followedUsers");
        if (follow)
            // XXX: .add(id) would be cleaner, but can fail with a unique key violation.
            // XXX: including the same id in both `added` and `removed` is relying on internals
            // TODO: Replace with a helper for upserting into the join table
            await relation.of(user).addAndRemove(id, id);
        else await relation.of(user).remove(id);

        this.auditService.tryAdd(UserAudit[follow ? "followedUser" : "unfollowedUser"](user.id, id));
    }

    // There's no point using a DataLoader here.
    // You can't call this method for other users.
    public async getFollowedUsers(userId: number) {
        return this.userRepository().createQueryBuilder().relation("followedUsers").of(userId).loadMany();
    }

    public async isFollowed(subjectUserId: number, follower: User) {
        return this.isFollowedDataLoader.load(follower, subjectUserId);
    }
}
