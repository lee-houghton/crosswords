import { Service } from "../../decorators";

// tslint:disable:no-console

@Service()
export class LoggerService {
    private debugFormat: string;
    private infoFormat: string;
    private errorFormat: string;
    public readonly includeDebug = true;

    // TODO: Per-service loggers?
    // See container.ts for details
    public constructor(
        // context: string | undefined,
        // public readonly includeDebug: boolean,
    ) {
        const context: string | undefined = undefined;
        this.debugFormat = context ? `[${context}] %s` : "%s";
        this.infoFormat = context ? `[${context}] %s` : "%s";
        this.errorFormat = context ? `*** [${context}] %s ***` : `*** %s ***`;
    }

    public debug(text: string) {
        if (this.includeDebug)
            console.log(this.debugFormat, text);
    }

    public info(text: string) {
        console.log(this.infoFormat, text);
    }

    public error(text: string) {
        console.error(this.errorFormat, text);
    }
}
