import { addDays } from "date-fns";
import { Inject } from "injection-js";
import { Connection, Repository } from "typeorm";
import { frontEndUrlToken } from "../../../config";
import { Service } from "../../../decorators";
import { ChangeEmailRequest, SecurityAudit, User } from "../../entities";
import { AuditService } from "../audit/AuditService";
import { EmailService } from "../email/EmailService";
import { RequestCodeService } from "./RequestCodeService";
import { ForbiddenError, UserInputError } from "../../../errors";

export const CHANGE_EMAIL_REQUEST_VALIDITY_PERIOD_DAYS = 30;

@Service()
export class ChangeEmailService {
    public constructor(
        @Inject(ChangeEmailRequest)
        private readonly requestRepo: () => Repository<ChangeEmailRequest>,
        @Inject(User) private readonly userRepo: () => Repository<User>,
        @Inject(Connection) private readonly connection: Connection,
        @Inject(frontEndUrlToken) private readonly frontEndUrl: string,
        private readonly requestCodeService: RequestCodeService,
        private readonly emailService: EmailService,
        private readonly auditService: AuditService
    ) {}

    public async getRequestForUser(userId: number, active?: boolean) {
        const request = (await this.requestRepo().findOne({ where: { userId } })) || undefined;
        if (active !== undefined && active !== this.requestCodeService.isValid(request)) return undefined;

        return request;
    }

    public getByCodeForUser(userId: number, code: string) {
        return this.requestRepo().findOne({ where: { userId, code } });
    }

    public async create(user: User, newEmail: string) {
        const repo = this.requestRepo();
        const existing = await this.getRequestForUser(user.id);
        if (existing) {
            if (this.requestCodeService.isValid(existing))
                throw new UserInputError(
                    "You have already requested to change your email address. Please cancel the request first."
                );
            await repo.remove(existing);
        }

        if (newEmail === user.email)
            throw new UserInputError(
                "The new email address is the same as your current email address. Please choose a different email address."
            );

        const otherUser = await this.userRepo().findOne({
            where: { email: newEmail },
        });
        if (otherUser)
            throw new UserInputError(
                "The email address you have selected is currently in use by a different account. Please select a different email address."
            );

        let request = new ChangeEmailRequest({
            userId: user.id,
            newEmail,
            code: this.requestCodeService.createCode(),
            expires: addDays(new Date(), CHANGE_EMAIL_REQUEST_VALIDITY_PERIOD_DAYS),
        });

        await this.emailService.sendEmail(
            newEmail,
            "Confirm your new email address",
            `You have requested to change your email address to ${newEmail}.
To confirm this request, visit the following URL:
${this.frontEndUrl}#/profile/manage/email/${request.code}

If this request wasn't you, then you can cancel the request by visiting the same URL.
`
        );

        await this.auditService.add(SecurityAudit.emailChangeRequested(user.id));

        return await repo.save(request);
    }

    public async apply(code: string, user: User) {
        const repo = this.requestRepo();
        const request = (await repo.findOne({ where: { code } })) || undefined;

        this.requestCodeService.validateRequest(request);
        if (request.userId !== user.id) throw new ForbiddenError("Cannot confirm change of email for a different user");

        return await this.connection.transaction(async (manager) => {
            // Try to get a better error message in this scenario.
            // If that fails, database constraints will ensure emails are not duplicated.
            const otherUser = await manager.findOne(User, {
                where: { email: request.newEmail },
            });
            if (otherUser) {
                await manager.remove(request);
                throw new UserInputError(
                    "This email address has been taken by another user. Please choose another email address."
                );
            }

            await manager.update(User, user.id, { email: request.newEmail });
            user.email = request.newEmail;
            await manager.delete(ChangeEmailRequest, request.id);

            await this.auditService.add(SecurityAudit.emailChangeSuccess(user.id), manager);

            return user;
        });
    }

    public async cancel(id: number, user: User) {
        const repo = this.requestRepo();
        const request = await repo.findOneBy({ id });
        if (!request) throw new UserInputError("The specified request was not found");

        if (request.userId !== user.id) throw new ForbiddenError("You cannot cancel another user's request");

        await repo.remove(request);
        return user;
    }
}
