import { addDays } from "date-fns";
import { Inject } from "injection-js";
import { Connection, Repository } from "typeorm";
import { frontEndUrlToken } from "../../../config";
import { Service } from "../../../decorators";
import { SecurityAudit, User } from "../../entities";
import { ResetPasswordRequest } from "../../entities/ResetPasswordRequest";
import { AuditService } from "../audit/AuditService";
import { IPasswordHasher } from "../auth/IPasswordHasher";
import { EmailService } from "../email/EmailService";
import { RequestCodeService } from "./RequestCodeService";

export const RESET_PASSWORD_REQUEST_VALIDITY_PERIOD_DAYS = 30;

@Service()
export class ResetPasswordService {
    public constructor(
        @Inject(ResetPasswordRequest)
        private readonly requestRepo: () => Repository<ResetPasswordRequest>,
        @Inject(User) private readonly userRepo: () => Repository<User>,
        @Inject(Connection) private readonly connection: Connection,
        @Inject(frontEndUrlToken) private readonly frontEndUrl: string,
        private readonly requestCodeService: RequestCodeService,
        private readonly emailService: EmailService,
        private readonly passwordHasher: IPasswordHasher,
        private readonly auditService: AuditService
    ) {}

    public async getRequestForUser(userId: number, active?: boolean) {
        const request = (await this.requestRepo().findOne({ where: { userId } })) || undefined;
        if (active !== undefined && active !== this.requestCodeService.isValid(request)) return undefined;

        return request;
    }

    public getByCodeForUser(userId: number, code: string) {
        return this.requestRepo().findOne({ where: { userId, code } });
    }

    public async createRequest(email: string) {
        const user = await this.userRepo().findOne({ where: { email } });

        // If the user isn't found, there's nothing to do.
        //
        // But we should avoid revealing the existence or non-existence of a user
        // in this method, so a small delay is a crude way to achieve that.
        if (!user) return await new Promise((r) => setTimeout(r, 250 + Math.random() * 250));

        const request = new ResetPasswordRequest({
            userId: user.id,
            code: this.requestCodeService.createCode(),
            expires: addDays(new Date(), RESET_PASSWORD_REQUEST_VALIDITY_PERIOD_DAYS),
        });

        await this.emailService.sendEmail(
            email,
            "Reset your Crosswords password",
            `You have requested to change your reset your password for ${user.name}.
To confirm this request, visit the following URL:
${this.frontEndUrl}#/reset-password/${request.code}

If this request wasn't you, please just ignore this email.
`
        );

        await this.auditService.add(SecurityAudit.passwordReset(user.id));

        return await this.requestRepo().save(request);
    }

    public async fetch(code: string) {
        return this.requestRepo().findOne({ where: { code } });
    }

    public async apply(code: string, password: string) {
        const repo = this.requestRepo();
        const request =
            (await repo.findOne({
                where: { code },
                relations: ["user"],
            })) || undefined;

        this.requestCodeService.validateRequest(request);

        return await this.connection.transaction(async (manager) => {
            request.user.passwordHash = await this.passwordHasher.hashPassword(password);
            const user = await manager.save(request.user);
            await manager.remove(request);

            await this.auditService.add(SecurityAudit.passwordResetSuccess(user.id));

            return user;
        });
    }
}
