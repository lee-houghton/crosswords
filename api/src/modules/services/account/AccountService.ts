import { Inject } from "injection-js";
import { Repository } from "typeorm";
import { frontEndUrlToken } from "../../../config";
import { Service } from "../../../decorators";
import { SecurityAudit, User } from "../../entities";
import { AuditService } from "../audit/AuditService";
import { AuthUserCache } from "../auth/AuthUserCache";
import { IPasswordHasher } from "../auth/IPasswordHasher";
import { EmailService } from "../email/EmailService";
import { RequestCodeService } from "./RequestCodeService";
import { AuthenticationError } from "../../../errors";

@Service()
export class AccountService {
    public constructor(
        @Inject(User) private readonly userRepo: () => Repository<User>,
        @Inject(frontEndUrlToken) private readonly frontEndUrl: string,
        private readonly passwordHasher: IPasswordHasher,
        private readonly emailService: EmailService,
        private readonly requestCodeService: RequestCodeService,
        private readonly auditService: AuditService,
        private readonly authUserCache: AuthUserCache
    ) {}

    private validateUsername(username: string) {
        if (username.length < 3) throw new Error("User name must contain at least 3 characters.");

        if (username.length > 20) throw new Error("User name must contain at most 20 characters.");

        if (!/^[\w\d_]+$/.test(username))
            throw new Error("User name must contain only letters, digits, and underscores.");
    }

    public async register(username: string, email: string) {
        const repo = this.userRepo();
        const existingUser = await repo.find({ where: { name: username } });
        if (existingUser.length > 0) throw new Error(`User name "${username}" is already taken`);

        this.validateUsername(username);

        let user = new User();
        user.name = user.displayName = username;
        user.email = email;
        user.activationCode = this.requestCodeService.createCode();
        user = await repo.save(user);
        this.authUserCache.invalidate(user.id);

        // console.log(`Activation code for ${username}: ${user.activationCode}`);

        // TODO: Add email templating of some sort
        // TODO: Add environment variable for front end domain
        await this.emailService.sendEmail(
            email,
            "Activate your crosswords account",
            `Welcome to crosswords!

Please activate your account at the following address: ${this.frontEndUrl}#/activate/${user.activationCode}
`
        );

        await this.auditService.add(SecurityAudit.accountRegistered(user.id));
        return user;
    }

    public async activateAccount(activationCode: string, password: string, displayName: string) {
        const repo = this.userRepo();
        const user = await repo.findOne({ where: { activationCode } });
        if (!user) throw new AuthenticationError("Invalid activation code");

        user.enabled = true;
        user.displayName = displayName;
        user.activationCode = null;
        user.activated = new Date();
        user.passwordHash = await this.passwordHasher.hashPassword(password);
        await repo.save(user);
        this.authUserCache.invalidate(user.id);
        await this.auditService.add(SecurityAudit.accountActivated(user.id));
        return user;
    }

    public async changePassword(userId: number, currentPassword: string, newPassword: string) {
        const repo = this.userRepo();
        let user = await repo.findOneOrFail({ where: { id: userId } });

        // Users who signed up via Google or Facebook authentication might not have a password
        if (user.passwordHash) {
            const result = await this.passwordHasher.verifyPasswordHash(user.passwordHash, currentPassword);
            if (!result.valid) throw new Error("Current password is incorrect");
        }

        const hash = await this.passwordHasher.hashPassword(newPassword);
        user.passwordHash = hash;

        await this.auditService.add(SecurityAudit.passwordReset(userId));

        user = await repo.save(user);

        this.authUserCache.invalidate(user.id);

        if (user.email)
            await this.emailService.sendEmail(
                user.email,
                "Your Crosswords password was changed",
                `Your Crosswords password (for the account ${user.name}) has been changed. ` +
                    `You can manage your account at: ${new URL("#/profile/manage", this.frontEndUrl).href}`
            );

        return user;
    }
}
