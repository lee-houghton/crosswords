import { randomBytes } from "crypto";
import { Service } from "../../../decorators";
import { UserAccountRequest } from "../../entities/UserAccountRequest";
import { ChangeEmailRequest } from "../../entities";
import { UserInputError } from "../../../errors";

type RequestWithCode = UserAccountRequest | ChangeEmailRequest;

@Service()
export class RequestCodeService {
    public createCode(): string {
        return randomBytes(32).toString("hex");
    }

    private getValidationMessage(request?: RequestWithCode): string | undefined {
        if (!request) return "Code is not valid";

        if (request.expires < new Date()) return "Code is expired";
    }

    public validateRequest(request?: RequestWithCode): asserts request {
        const message = this.getValidationMessage(request);
        if (message) throw new UserInputError(message);
    }

    public isValid(request?: RequestWithCode) {
        return this.getValidationMessage(request) === undefined;
    }
}
