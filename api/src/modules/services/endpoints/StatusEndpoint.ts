import { Application } from "express";
import { IEndpointProvider } from "../Http";

export class StatusEndpoint implements IEndpointProvider {
    public registerEndpoints(app: Application) {
        app.get(`/status`, async (req, res) => {
            res.status(200).json({ ok: true });
        });
    }

    public addServerHandlers() {}
}
