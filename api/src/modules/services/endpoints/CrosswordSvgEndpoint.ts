import { Application } from "express";
import { Inject } from "injection-js";
import { Container } from "../../../container";
import { requestChildContainerToken } from "../../../tokens";
import { CrosswordSvgService } from "../crosswords/CrosswordSvgService";
import { IEndpointProvider } from "../Http";
import { DeckService } from "../decks";
import { GenerateCrosswordService } from "../crosswords";

export class CrosswordSvgEndpoint implements IEndpointProvider {
    public constructor(@Inject(requestChildContainerToken) private readonly createChildContainer: () => Container) {}

    public registerEndpoints(app: Application) {
        app.get(`/crosswords/:id/svg`, async (req, res) => {
            const child = this.createChildContainer();
            const crosswordSvgService = child.get(CrosswordSvgService);
            try {
                const svg = await crosswordSvgService.generateSvg(
                    parseInt(req.params.id),
                    req.query.background === "true"
                );
                res.header("content-disposition", "inline");
                res.type("image/svg+xml");
                res.send(svg);
            } catch (err: any) {
                res.status(400).json({ error: err?.message, stack: err?.stack });
            }
        });

        app.get(`/decks/:id/debug`, async (req, res) => {
            try {
                const child = this.createChildContainer();
                const crosswordSvgService = child.get(CrosswordSvgService);
                const generateCrosswordService = child.get(GenerateCrosswordService);
                const deckService = child.get(DeckService);

                const deck = await deckService.getById(parseInt(req.params.id, 10), true);
                if (!deck) throw new Error(`Deck ${req.params.id} not found`);
                const crossword = await generateCrosswordService.generateCrosswordFromDecks(undefined, [deck], 14, 14);
                const svg = await crosswordSvgService.generateSvgFromCrossword(
                    crossword,
                    req.query.background === "true",
                    true
                );
                res.header("content-disposition", "inline");
                res.type("image/svg+xml");
                res.send(svg);
            } catch (err: any) {
                res.status(400).json({ error: err?.message, stack: err?.stack });
            }
        });
    }

    public addServerHandlers() {}
}
