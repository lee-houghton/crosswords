import DataLoader from "dataloader";
import { Inject } from "injection-js";
import { Service } from "../../decorators";
import { LoggerService } from "./LoggerService";
import { redisClientToken } from "../../tokens";
import { createClient } from "redis";

type RedisClient = ReturnType<typeof createClient>;

@Service()
export class CacheService {
    private getDataLoader: DataLoader<string, any>;

    public constructor(
        @Inject(redisClientToken) private readonly client: RedisClient | undefined,
        private readonly logger: LoggerService
    ) {
        if (!client) console.warn("[CacheService] No redis client");

        this.getDataLoader = new DataLoader(async (keys) => {
            if (!this.client) return [];

            const start = Date.now();
            const values = await this.client.mGet(keys as string[]);
            if (this.logger.includeDebug) {
                const hits = values.reduce((sum, value) => sum + (value ? 1 : 0), 0);
                this.logger.debug(`get ${keys} ${Date.now() - start}ms (${hits}/${keys.length})`);
            }

            return values.map((str) => (str ? JSON.parse(str) : undefined));
        });
    }

    public getObject<T>(key: string): Promise<T | undefined> {
        return this.getDataLoader.load(key);
    }

    public getObjects<T>(keys: string[]): Promise<Array<T | undefined>> {
        return Promise.all(keys.map((key) => this.getDataLoader.load(key)));
    }

    public async setObject(key: string, value: any, ttlSecs?: number) {
        if (!this.client) return;

        const start = Date.now();
        if (ttlSecs !== undefined) await this.client.setEx(key, ttlSecs, JSON.stringify(value));
        else await this.client.set(key, JSON.stringify(value));

        if (this.logger.includeDebug) this.logger.debug(`set ${key} ${Date.now() - start}ms`);
    }

    public async setObjects<T>(objects: T[], getKey: (obj: T) => string, ttlSecs?: number) {
        if (!this.client) return;

        if (objects.length === 0) return;

        const start = Date.now();
        const args: string[] = [];

        for (const object of objects) args.push(getKey(object), JSON.stringify(object));

        await this.client.mSet(args);

        if (ttlSecs !== undefined) for (let i = 0; i < args.length; i += 2) this.client.expire(args[i], ttlSecs);

        if (this.logger.includeDebug) this.logger.debug(`set ${objects.map(getKey)} ${Date.now() - start}ms`);
    }

    public async deleteObject(key: string) {
        if (!this.client) return;

        const start = Date.now();
        await this.client.del(key);

        if (this.logger.includeDebug) this.logger.debug(`del ${key} ${Date.now() - start}ms`);
    }
}
