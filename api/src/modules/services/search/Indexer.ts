import { Inject } from "injection-js";
import { Repository } from "typeorm";
import { Service } from "../../../decorators";
import { Deck, User } from "../../entities";
import { LoggerService } from "../LoggerService";
import { SearchService } from "./SearchService";

@Service()
export class Indexer {
    public constructor(
        @Inject(User) private readonly userRepo: () => Repository<User>,
        @Inject(Deck) private readonly deckRepo: () => Repository<Deck>,
        private readonly searchService: SearchService,
        private readonly logger: LoggerService
    ) {}

    public async indexAllUsers(verbose = false) {
        const users = await this.userRepo().find();
        for (const user of users) {
            if (verbose) this.logger.info(`Indexing user: ${user.name}`);

            await this.searchService.indexUser(user);
        }
    }

    public async indexAllDecks(verbose = false) {
        const decks = await this.deckRepo().find({ relations: ["terms"] });
        for (const deck of decks) {
            if (verbose) this.logger.info(`Indexing deck: ${deck.title}`);

            try {
                await this.searchService.indexDeck(deck);
            } catch (err) {
                this.logger.error(`Failed to index deck ${deck.title}: ${err}`);
            }
        }
    }
}
