import { Service } from "../../../decorators";
import { Classifier } from "fasttext";
import { packageRoot } from "../../../utils";
import { join } from "path";

/**
 * FastText returns labels prefix with __label__, for example __label__fr, 
 * so we need to strip the prefix.
 */
const LABEL_PREFIX_LENGTH = "__label__".length;

/**
 * Use by `getTextLanguage` to give higher weighting to "expected" values.
 * 
 * For example, for a Deck, the `title` and `description` fields use the `termLang` and `definitionLang`
 * values as hints, because it's highly likely that the title and description are written in one of those 
 * two languages.
 * 
 * The value 2.5 has no scientific or empirical basis :) 
 */
const SUGGESTION_VALUE_FACTOR = 2.5;

@Service()
export class LanguageIdentifier {
    // TODO Use lid.176.bin if it's available?
    private classifier = new Classifier(join(packageRoot, "assets/fasttext/lid.176.ftz"));

    /**
     * Attempts to determine the language of a piece of text.
     * @param suggestedLanguages If specified, s
     */
    public async getTextLanguage(text: string, suggestedLanguages?: string[], supportedLanguages?: Set<string>) {
        const labels = await this.classifier.predict(text, 3);

        let langId: string | undefined; // The ISO-639-1 code of the current "best" language
        let langValue: number | undefined; // The adjusted value of the current "best" language
        let confidence: number | undefined; // The original value of the current "best" language

        // Go through the language labels returned by fasttext and find one with the highest `value`.
        // Ordinarily this would just be the first one in the list, but that might not be the case
        // if `suggestedLanguages` causes one label to be weighted more highly than another.
        for (const {label, value} of labels) {
            const lang = label.substring(LABEL_PREFIX_LENGTH);
            if (supportedLanguages && !supportedLanguages.has(lang))
                continue;

            const adjustedValue = value * (suggestedLanguages?.includes(lang) ? SUGGESTION_VALUE_FACTOR : 1);

            if (langValue === undefined || adjustedValue > langValue) {
                langId = lang;
                langValue = adjustedValue;
                confidence = value;
            }
        }

        // It's not a supported language, but at least it's something
        if (!langId && labels.length > 0)
            return { langId: labels[0].label.substring(LABEL_PREFIX_LENGTH), confidence: labels[0].value };

        return { langId, confidence };
    }
}