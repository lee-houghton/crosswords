import AWS from "aws-sdk";
import { Client } from "@elastic/elasticsearch";
import { Inject } from "injection-js";
import { createConfigToken, envToken } from "../../../config";
import { Service } from "../../../decorators";
import { required } from "../../../env";
import { Deck, Term, User } from "../../entities";
import { In, Repository } from "typeorm";
import { LanguageIdentifier } from "./LanguageIdentifier";
import { LoggerService } from "../LoggerService";
import createAwsElasticsearchConnector from "aws-elasticsearch-connector";

export interface SearchConfig {
    elasticSearchNode: string;
}

export const searchConfigToken = createConfigToken(
    "SearchService",
    envToken
)((env) => ({
    elasticSearchNode: required(env, "ELASTICSEARCH_NODE"),
}));

const builtIn = {
    ar: "arabic",
    hy: "armenian",
    eu: "basque",
    bn: "bengali",
    // "pt-BR": "brazilian" // We don't current support regional variants anyway
    bg: "bulgarian",
    ca: "catalan",
    // "???": "cjk" // We don't need the cjk one as we have smartcn/nori/kuromoji
    cs: "czech",
    da: "danish",
    nl: "dutch",
    en: "english",
    et: "estonian",
    fi: "finnish",
    fr: "french",
    gl: "galician",
    de: "german",
    el: "greek",
    hi: "hindi",
    hu: "hungarian",
    id: "indonesian",
    ga: "irish",
    it: "italian",
    lv: "latvian",
    lt: "lithuanian",
    no: "norwegian",
    fa: "persian",
    pt: "portuguese",
    ro: "romanian",
    ru: "russian",
    // "???": "sorani" // Sorani support is built-in to ElasticSearch, but there is no ISO-639-1 code for it
    es: "spanish",
    sv: "swedish",
    tr: "turkish",
    th: "thai",
} as const;

const extra = {
    ja: "kuromoji",
    // "ko": "nori", // Technically AWS has Seunjeon but it looks hard to install for dev
    zh: "smartcn",
    pl: "polish",
    // TODO: Find more
} as const;

const allLanguages = { ...builtIn, ...extra };

const supportedIsoCodes = new Set(Object.keys(allLanguages));

const searchableTextFieldConfig = {
    properties: {
        // "language": { "type": "keyword" }, // I don't see why we would need this, if we do then we can re-index
        // "supported": { "type": "boolean" }, // Or this
        default: {
            type: "text",
            analyzer: "default",

            // TODO: I don't know what this does, I just copy-pasted it.
            // It doesn't work anyway.
            // Error! analyzer [icu_analyzer] not found for field [icu]
            // // "fields": {
            // //     "icu": {
            // //         "type": "text",
            // //         "analyzer": "icu_analyzer"
            // //     }
            // // }
        },
        ...Object.fromEntries(Object.entries(allLanguages).map(([iso, analyzer]) => [iso, { type: "text", analyzer }])),

        // Simple version for testing
        // "en": { type: "text", analyzer: "english" },
        // "fr": { type: "text", analyzer: "french" },
    },
} as const;

const termIndexSettings = {
    analysis: {
        analyzer: {
            autocomplete: {
                tokenizer: "autocomplete",
                filter: ["lowercase"],
            },
            autocomplete_search: {
                tokenizer: "lowercase",
            },
        },
        tokenizer: {
            autocomplete: {
                type: "edge_ngram",
                min_gram: 2,
                max_gram: 20,
                token_chars: ["letter"],
            },
        },
    },
};

@Service()
export class SearchService {
    private client: Client;

    public constructor(
        @Inject(searchConfigToken) config: SearchConfig,
        @Inject(User) private readonly userRepo: () => Repository<User>,
        @Inject(Deck) private readonly deckRepo: () => Repository<Deck>,
        @Inject(Term) private readonly termRepo: () => Repository<Term>,
        private readonly languageIdentifier: LanguageIdentifier,
        private readonly logger: LoggerService
    ) {
        this.client = new Client({
            ...(config.elasticSearchNode.endsWith("amazonaws.com") ? createAwsElasticsearchConnector(AWS.config) : {}),
            node: config.elasticSearchNode,
        });
    }

    private async getIndexLanguage(text?: string, hints?: string[]) {
        if (!text) return undefined;

        const { langId } = await this.languageIdentifier.getTextLanguage(text, hints, supportedIsoCodes);

        return langId && supportedIsoCodes.has(langId) ? langId : undefined;
    }

    private async createIndex(index: string, settings?: any) {
        const exists = await this.client.indices.exists({ index });
        if (exists === true) return this.logger.debug("Index already exists: " + index);

        this.logger.info("Creating ElasticSearch index: " + index);
        await this.client.indices.create({ index, body: { settings } });
    }

    public async createMappings() {
        await this.createIndex("decks");

        // XXX No longer required?
        // // If the index exists with the old schema (no analyzers) remove it
        // // since it will not be possible
        // const termsExists = await this.client.indices.exists({ index: "terms" });
        // if (termsExists) {
        //     const settings = await this.client.indices.getSettings({ index: "terms" });
        //     if (!settings.terms?.analysis?.analyzer?.autocomplete) await this.client.indices.delete({ index: "terms" });
        // }

        await this.createIndex("terms", termIndexSettings);

        this.logger.info("Updating mappings for decks index");
        await this.client.indices.putMapping({
            index: "decks",
            body: {
                dynamic: "strict",
                properties: {
                    termLang: { type: "keyword" },
                    definitionLang: { type: "keyword" },
                    title: searchableTextFieldConfig,
                    description: searchableTextFieldConfig,
                },
            },
        });

        this.logger.info("Updating mappings for terms index");
        await this.client.indices.putMapping({
            index: "terms",
            dynamic: "strict",
            properties: {
                deckId: { type: "long" },
                termLang: { type: "keyword" },
                definitionLang: { type: "keyword" },
                term: searchableTextFieldConfig,
                definitions: searchableTextFieldConfig,
                termSuggest: {
                    type: "text",
                    analyzer: "autocomplete",
                    search_analyzer: "autocomplete_search",
                },
                definitionSuggest: {
                    type: "text",
                    analyzer: "autocomplete",
                    search_analyzer: "autocomplete_search",
                },
            },
        });
    }

    public async searchUsers(search: string) {
        if (search.trim() === "") return [];

        const results = await this.client.search<{ id: number }>({
            index: "users",
            q: search,
        });

        const userIds = results.hits?.hits?.map((user) => user?._source?.id);
        const users = await this.userRepo().find({ where: { id: In(userIds) } });
        return users;
    }

    public async executeDebugSearch(index: string | undefined, q: string | undefined, body?: any) {
        const results = await this.client.search({
            index,
            q,
            body,
        });
        return results;
    }

    public async suggestTerms(search: string, termLang?: string, excludeDeckId?: number) {
        const match = {
            termSuggest: {
                query: search,
                operator: "and",
                fuzziness: "AUTO",
            },
        } as const;

        const query = {
            bool: {
                must: { match },
                must_not: excludeDeckId !== undefined ? { term: { deckId: excludeDeckId } } : undefined,
                filter: termLang ? { term: { termLang } } : undefined,
            },
        };

        const results = await this.client.search({
            index: ["terms"],
            size: 5,
            body: {
                query,
            },
        });

        const hits = results.hits?.hits as ReadonlyArray<{ _index: string; _id: string }>;
        if (!hits) return [];

        const termIds = hits.map((hit) => parseInt(hit._id));
        const terms = await this.termRepo().find({ where: { id: In(termIds) } });
        return termIds.map((id) => terms.find((t) => t.id === id)).filter((term) => term) as Term[];
    }

    public async searchDecksAndTerms(search: string, termLang = "*", definitionLang = "*") {
        if (search.trim() === "") return [];

        const results = await this.client.search({
            index: ["decks", "terms"],
            size: 25,
            body: {
                query: {
                    multi_match: {
                        query: search,

                        // Treat deck title matches as most important, followed by term, definition,
                        // and finally deck description.
                        fields: ["title.*^5", "term.*^4", "definitions.*^3", "description.*"],
                    },
                },
            },
        });

        const hits = results.hits?.hits as ReadonlyArray<{ _index: string; _id: string }>;
        const termIds: number[] = [];
        const deckIds: number[] = [];

        for (const hit of hits) {
            const array = hit._index === "decks" ? deckIds : termIds;
            array.push(parseInt(hit._id));
        }

        const [decks, terms] = await Promise.all([
            this.deckRepo().find({ where: { id: In(deckIds) } }),
            this.termRepo().find({ where: { id: In(termIds) } }),
        ]);

        // TypeORM returns the results in a random order...
        // Now we need to re-rank them again by score
        const decksById = new Map<number, Deck>();
        const termsById = new Map<number, Term>();
        for (const deck of decks) decksById.set(deck.id, deck);
        for (const term of terms) termsById.set(term.id, term);

        return hits
            .map((hit) => {
                if (hit._index === "decks") return decksById.get(parseInt(hit._id));
                if (hit._index === "terms") return termsById.get(parseInt(hit._id));
            })
            .filter((item) => item) as Array<Deck | Term>;
    }

    public async indexUser(user: User) {
        await this.client.update({
            index: "users",
            id: `${user.id}`,
            body: {
                doc: {
                    name: user.name,
                    displayName: user.displayName,
                },
                doc_as_upsert: true,
            },
        });
    }

    public async indexDeck(deck: Deck, terms: Term[] | undefined = deck.terms) {
        const [titleLang, descLang] = await Promise.all([
            this.getIndexLanguage(deck.title, [deck.termLanguage, deck.definitionLanguage]),
            this.getIndexLanguage(deck.description, [deck.termLanguage, deck.definitionLanguage]),
        ]);

        this.logger.debug(`Deck ${deck.id} ${deck.title}: title is ${titleLang}, description is ${descLang}`);

        await this.client.update({
            index: "decks",
            id: `${deck.id}`,
            body: {
                doc: {
                    termLang: deck.termLanguage,
                    definitionLang: deck.definitionLanguage,
                    title: { [titleLang || "default"]: deck.title },
                    description: { [descLang || "default"]: deck.description },
                },
                doc_as_upsert: true,
            },
        });

        if (terms) await this.indexTerms(deck.termLanguage, deck.definitionLanguage, terms);
    }

    public deindexTermsById(ids: number[]) {
        return this.client.deleteByQuery({
            index: "terms",
            body: {
                query: {
                    terms: {
                        _id: ids.map((id) => id.toString()),
                    },
                },
            },
        });
    }

    private mapLanguage(lang: string) {
        return supportedIsoCodes.has(lang) ? lang : "default";
    }

    public async indexTerms(termLang: string, definitionLang: string, terms: Term[]) {
        if (terms.length === 0) return;

        const body = terms.flatMap((term) => [
            { index: { _index: "terms", _id: `${term.id}` } },
            {
                deckId: term.deckId,
                termLang,
                definitionLang,
                term: { [this.mapLanguage(termLang)]: term.term },
                definitions: { [this.mapLanguage(definitionLang)]: term.definitions },
                termSuggest: term.term,
                definitionSuggest: term.definitions,
            },
        ]);

        await this.client.bulk({ body });
    }
}
