import { Application, Request } from "express";
import { Inject } from "injection-js";
import { customAlphabet } from "nanoid";
import passport, { Profile } from "passport";
import { Strategy as FacebookStrategy } from "passport-facebook";
import { Like, Repository } from "typeorm";
import { apiUrlToken, createConfigToken, frontEndUrlToken } from "../../../config";
import { Service } from "../../../decorators";
import { AccountProvider, IAccountProvider } from "../../../decorators/AccountProvider";
import { SecurityAudit, User } from "../../entities";
import { ConnectedAccountType } from "../../schema/types";
import { AuditService } from "../audit";
import { EmailService } from "../email";
import { IEndpointProvider } from "../Http";
import { ForbiddenError } from "../../../errors";

const generateUserNameSuffix = customAlphabet("abcdefghijklmnoprstuvwxyz1234567890", 8);

interface FacebookAuthConfig {
    clientId: string;
    clientSecret: string;
}

const facebookAuthConfigToken = createConfigToken("FacebookAuthService")(() =>
    process.env.FACEBOOK_CLIENT_ID && process.env.FACEBOOK_CLIENT_SECRET
        ? {
              clientId: process.env.FACEBOOK_CLIENT_ID,
              clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
          }
        : undefined
);

type DoneFunction = (error: any, user?: any, info?: any) => void;

@Service()
@AccountProvider(ConnectedAccountType.Facebook)
export class FacebookAuthService implements IEndpointProvider, IAccountProvider {
    private manageURL: string;

    public constructor(
        @Inject(facebookAuthConfigToken) private readonly config: FacebookAuthConfig | undefined,
        @Inject(User) private readonly userRepo: () => Repository<User>,
        @Inject(frontEndUrlToken) private readonly frontEndUrl: string,
        @Inject(apiUrlToken) private readonly apiUrl: string,
        private readonly emailService: EmailService,
        private readonly auditService: AuditService
    ) {
        this.manageURL = new URL("#/profile/manage/accounts", this.frontEndUrl).href;
    }

    public registerEndpoints(app: Application) {
        app.get("/auth/facebook", passport.authenticate("facebook"));

        app.get("/connect/facebook", passport.authorize("facebook-connect"));

        app.get(
            "/auth/facebook/callback",
            passport.authenticate("facebook", { failureRedirect: this.frontEndUrl }),
            (req, res) => res.redirect(this.frontEndUrl)
        );

        app.get(
            "/connect/facebook/callback",
            passport.authorize("facebook-connect", { failureRedirect: this.frontEndUrl }),
            async (req, res) => res.redirect(this.manageURL)
        );
    }

    public addServerHandlers() {
        if (!this.config) return console.warn("No config for FacebookAuthService");

        passport.use(
            new FacebookStrategy(
                {
                    clientID: this.config.clientId,
                    clientSecret: this.config.clientSecret,
                    // Add the hash to the URL to stop Facebook overriding it
                    // https://stackoverflow.com/a/41917323
                    callbackURL: new URL("auth/facebook/callback", this.apiUrl).href + "#/",
                    passReqToCallback: true,
                },
                this.verify
            )
        );

        passport.use(
            "facebook-connect",
            new FacebookStrategy(
                {
                    clientID: this.config.clientId,
                    clientSecret: this.config.clientSecret,
                    callbackURL: new URL("connect/facebook/callback", this.apiUrl).href + "#/",
                    passReqToCallback: true,
                },
                this.verify
            )
        );
    }

    private verify = async (
        req: Request,
        _accessToken: string,
        _refreshToken: string,
        profile: Profile,
        done: DoneFunction
    ) => {
        try {
            if (req.user) {
                const user = req.user as User;
                user.facebookId = profile.id;
                await this.auditService.add(SecurityAudit.facebookAccountLinked(user.id, user.displayName));
                await this.userRepo().update(user.id, { facebookId: profile.id });
                return done(undefined, user);
            }

            const repo = this.userRepo();

            // If we find a User with the same `facebookId`, use that.
            const users = await repo.find({ where: { facebookId: profile.id } });

            // TODO: What if there is more than one?
            if (users.length > 0) return done(undefined, users[0]);

            let user = new User();
            user.facebookId = profile.id;
            user.displayName = profile.displayName || "";

            const baseName = user.displayName.replace(/\W/g, "");
            const similarUsers = await repo.find({ where: { name: Like(baseName + "%") } });
            const names = new Set(similarUsers.map((u) => u.name.toLowerCase()));

            for (let i = 0; i < 1000; i++) {
                const name = baseName + (i ? i : "");
                if (!names.has(name.toLowerCase())) {
                    user.name = name;
                    break;
                }
            }

            if (!user.name) user.name = baseName + generateUserNameSuffix();

            await this.auditService.add(SecurityAudit.facebookAccountLinked(user.id, user.displayName));
            user = await repo.save(user);

            done(undefined, user);
        } catch (err) {
            done(err);
        }
    };

    public get accountType() {
        return ConnectedAccountType.Facebook;
    }

    public isConnected(user: User): boolean {
        return !!user.facebookId;
    }

    public async disconnectAccount(user: User) {
        if (!user.passwordHash)
            throw new ForbiddenError(
                "You must set a password for your account before removing the linked Facebook account."
            );

        await this.auditService.add(SecurityAudit.facebookAccountRemoved(user.id));
        await this.userRepo().update(user.id, { facebookId: undefined });

        if (user.email)
            await this.emailService.sendEmail(
                user.email,
                `Facebook account unlinked`,
                `Your Facebook account has been unlinked from your Crosswords account. ` +
                    `You can view your account settings here: ${
                        new URL("#/profile/manage/password", this.frontEndUrl).href
                    }`
            );

        user.facebookId = undefined;
        return user;
    }
}
