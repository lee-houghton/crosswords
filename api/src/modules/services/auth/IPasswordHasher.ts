import { Injectable } from "injection-js";

export interface VerifyPasswordResult {
    valid: boolean;
    hashUpgradeRequired: boolean;
}

@Injectable()
export abstract class IPasswordHasher {
    public abstract hashPassword(password: string): Promise<string>;
    public abstract verifyPasswordHash(hash: string, password: string): Promise<VerifyPasswordResult>;
}
