import RedisStore from "connect-redis";
import { Application, Request, RequestHandler } from "express";
import expressSession from "express-session";
import { Inject } from "injection-js";
import passport from "passport";
import { Repository } from "typeorm";
import { Service } from "../../../decorators";
import { env, required } from "../../../env";
import { redisClientToken } from "../../../tokens";
import { User } from "../../entities/User";
import { IEndpointProvider } from "../Http";
import { AuthUserCache } from "./AuthUserCache";
import { IPasswordHasher } from "./IPasswordHasher";
import { createClient } from "redis";

type RedisClient = ReturnType<typeof createClient>;

// TODO: Quizlet requires a &state parameter which we currently set to a static string.
//
// Instead we should use HMAC(gettime() % 300 + req.ip) or something similar -
// Something that is unpredictable to attackers, but predictable to us, and
// changes over time.

@Service()
export class AuthService implements IEndpointProvider {
    public readonly middleware: RequestHandler[];

    public constructor(
        @Inject(User) private readonly userRepo: () => Repository<User>,
        private readonly passwordHasher: IPasswordHasher,
        @Inject(redisClientToken) private readonly redisClient: RedisClient,
        private readonly authUserCache: AuthUserCache
    ) {
        this.middleware = [
            expressSession({
                secret: required(env, "SESSION_SECRET"),
                cookie: {
                    sameSite: "lax",
                    httpOnly: true,
                    maxAge: 30 * 86400 * 1000,
                    signed: true,
                },
                saveUninitialized: false,
                resave: false,
                store: new RedisStore({
                    client: this.redisClient,
                }),
            }),
            passport.initialize(),
            passport.session(),
        ];
    }

    public async runSessionMiddleware(req: Request) {
        // TODO: What if the session middleware tries to call res.send()?
        const res = req.res || ({} as any);

        for (const handler of this.middleware) {
            await new Promise<void>((resolve, reject) =>
                handler(req, res, (err) => {
                    if (err) reject(err);
                    else resolve();
                })
            );
        }
    }

    public registerEndpoints(app: Application) {
        passport.serializeUser<number>((user, done) => done(undefined, user.id));
        passport.deserializeUser<number>((id, done) =>
            this.authUserCache
                .get(id)
                .then((user) => done(undefined, user))
                .catch(done)
        );

        app.use(...this.middleware);
    }

    public addServerHandlers() {}

    public async login(username: string, password: string) {
        const userRepo = this.userRepo();
        const user = await userRepo.findOne({ where: { name: username } });
        if (!user || !user.passwordHash || !user.enabled) return undefined;

        this.authUserCache.put(user);

        const { valid, hashUpgradeRequired } = await this.passwordHasher.verifyPasswordHash(
            user.passwordHash,
            password
        );
        if (hashUpgradeRequired) {
            user.passwordHash = await this.passwordHasher.hashPassword(password);
            await userRepo.save(user);
        }

        return valid ? user : undefined;
    }
}
