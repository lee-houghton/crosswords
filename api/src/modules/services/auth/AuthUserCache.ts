import { PubSubEngine } from "graphql-subscriptions";
import { Inject } from "injection-js";
import ms from "ms";
import { Repository } from "typeorm";
import { Service } from "../../../decorators";
import { User } from "../../entities/User";
import { LoggerService } from "../LoggerService";

const CACHE_VALIDITY_PERIOD_MS = ms("15m");

interface CacheRecord {
    user: User | undefined;
    expires: number;
}

/**
 * Caches a user's information for performance reasons.
 *
 * This is also the information in `ctx.user` in GraphQL resolvers,
 * so we should take reasonable effort to ensure it is fresh. This is
 * done by a combination of:
 * - Expiring the cached user info after a certain amount of time (see above)
 * - Removing cached user info when the user record is updated.
 *   This is opt-in, implemented by services such as AccountService calling
 *   authUserCache.invalidate(...)
 */
@Service()
export class AuthUserCache {
    private cache = new Map<number, CacheRecord>();

    public constructor(
        private readonly pubSub: PubSubEngine,
        private logger: LoggerService,
        @Inject(User) private readonly userRepo: () => Repository<User>
    ) {
        this.pubSub.subscribe(
            "UserUpdated",
            (id) => {
                this.logger.info(`Removing user id ${id} from cache`);
                this.cache.delete(id);
            },
            {}
        );
    }

    public invalidate(id: number) {
        this.pubSub.publish("UserUpdated", id);
    }

    public async get(id: number) {
        const cached = this.cache.get(id);
        const now = Date.now();

        if (cached && now < cached.expires) return cached.user;

        const user = (await this.userRepo().findOneBy({ id })) || undefined;
        this.cache.set(id, { user, expires: now + CACHE_VALIDITY_PERIOD_MS });
        return user;
    }

    public put(user: User) {
        this.cache.set(user.id, { user, expires: Date.now() + CACHE_VALIDITY_PERIOD_MS });
    }
}
