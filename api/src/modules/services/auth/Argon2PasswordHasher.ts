import { hash, verify, needsRehash, Options } from "argon2";
import { IPasswordHasher } from "./IPasswordHasher";
import { Injectable } from "injection-js";

// TODO: Allow customising Argon2 options
const options: Options = {};

@Injectable()
export class Argon2PasswordHasher extends IPasswordHasher {
    public hashPassword(password: string) {
        return hash(password, { raw: false });
    }

    public async verifyPasswordHash(hash: string, password: string) {
        const valid = await verify(hash, password);
        return {
            valid,
            hashUpgradeRequired: valid && needsRehash(hash, options),
        };
    }
}
