import { Application, Request } from "express";
import { Inject } from "injection-js";
import { customAlphabet } from "nanoid";
import passport, { Profile } from "passport";
import { OAuth2Strategy as GoogleStrategy, VerifyFunction } from "passport-google-oauth";
import { Like, Repository } from "typeorm";
import { apiUrlToken, createConfigToken, frontEndUrlToken } from "../../../config";
import { Service } from "../../../decorators";
import { AccountProvider, IAccountProvider } from "../../../decorators/AccountProvider";
import { SecurityAudit, User } from "../../entities";
import { ConnectedAccountType } from "../../schema/types";
import { AuditService } from "../audit";
import { EmailService } from "../email";
import { IEndpointProvider } from "../Http";
import { ForbiddenError } from "../../../errors";

const generateUserNameSuffix = customAlphabet("abcdefghijklmnoprstuvwxyz1234567890", 8);

interface GoogleAuthConfig {
    clientId: string;
    clientSecret: string;
}

const googleAuthConfigToken = createConfigToken("GoogleAuthService")(() =>
    process.env.GOOGLE_CLIENT_ID && process.env.GOOGLE_CLIENT_SECRET
        ? {
              clientId: process.env.GOOGLE_CLIENT_ID,
              clientSecret: process.env.GOOGLE_CLIENT_SECRET,
          }
        : undefined
);

@Service()
@AccountProvider(ConnectedAccountType.Google)
export class GoogleAuthService implements IEndpointProvider, IAccountProvider {
    private manageURL: string;

    public constructor(
        @Inject(googleAuthConfigToken) private readonly config: GoogleAuthConfig | undefined,
        @Inject(User) private readonly userRepo: () => Repository<User>,
        @Inject(frontEndUrlToken) private readonly frontEndUrl: string,
        @Inject(apiUrlToken) private readonly apiUrl: string,
        private readonly emailService: EmailService,
        private readonly auditService: AuditService
    ) {
        this.manageURL = new URL("#/profile/manage/accounts", this.frontEndUrl).href;
    }

    private scopes = [
        "https://www.googleapis.com/auth/plus.login",
        "https://www.googleapis.com/auth/userinfo.email",
        "https://www.googleapis.com/auth/userinfo.profile",
    ];

    public registerEndpoints(app: Application) {
        app.get(
            "/auth/google",
            passport.authenticate("google", {
                scope: this.scopes,
            })
        );

        app.get(
            "/connect/google",
            passport.authorize("google-connect", {
                scope: this.scopes,
            })
        );

        app.get(
            "/auth/google/callback",
            passport.authenticate("google", { failureRedirect: this.frontEndUrl }),
            (req, res) => res.redirect(this.frontEndUrl)
        );

        app.get(
            "/connect/google/callback",
            passport.authorize("google-connect", { failureRedirect: this.frontEndUrl }),
            async (req, res) => res.redirect(this.manageURL)
        );
    }

    public addServerHandlers() {
        if (!this.config) return console.warn("No config for GoogleAuthService");

        passport.use(
            new GoogleStrategy(
                {
                    clientID: this.config.clientId,
                    clientSecret: this.config.clientSecret,
                    callbackURL: new URL("auth/google/callback", this.apiUrl).href,
                    passReqToCallback: true,
                },
                this.verify
            )
        );

        passport.use(
            "google-connect",
            new GoogleStrategy(
                {
                    clientID: this.config.clientId,
                    clientSecret: this.config.clientSecret,
                    callbackURL: new URL("connect/google/callback", this.apiUrl).href,
                    passReqToCallback: true,
                },
                this.verify
            )
        );
    }

    private verify = async (
        req: Request,
        _accessToken: string,
        _refreshToken: string,
        profile: Profile,
        done: VerifyFunction
    ) => {
        try {
            const email = profile.emails?.[0].value;
            if (!email) throw new Error("No email address provided"); // Should never happen

            if (req.user) {
                const user = req.user as User;
                user.googleId = profile.id;
                await this.auditService.add(SecurityAudit.googleAccountLinked(user.id, email));
                await this.userRepo().update(user.id, { googleId: profile.id });
                return done(undefined, user);
            }

            const repo = this.userRepo();

            // If we find a User with the same `googleId`, use that.
            // Failing that, if we find a user with the same `email`, it must be the
            // same person, so use that.
            const users = await repo.find({
                where: [{ googleId: profile.id }, { email }],
            });

            if (users.length > 0) {
                // Is there a user already linked to this account?
                const exact = users.find((u) => u.googleId === profile.id);
                if (exact) return done(undefined, exact);

                // If not, is there a user with the same email address?
                const user = users[0];
                user.googleId = profile.id;

                await this.auditService.add(SecurityAudit.googleAccountLinked(user.id, email));
                await repo.update(user.id, { googleId: profile.id });
                await this.emailService.sendEmail(
                    email,
                    "A Google account was linked with your Crosswords account",
                    `Your Crosswords user (${user.name}) is now linked with the Google account for ${email}. ` +
                        `You can manage your linked accounts at: ${
                            new URL("#/profile/manage/accounts", this.frontEndUrl).href
                        }`
                );

                return done(undefined, user);
            }

            let user = new User();
            user.googleId = profile.id;
            user.displayName = profile.displayName || "";
            user.email = email;

            const baseName = user.displayName.replace(/\W/g, "");
            const similarUsers = await repo.find({ where: { name: Like(baseName + "%") } });
            const names = new Set(similarUsers.map((u) => u.name.toLowerCase()));

            for (let i = 0; i < 1000; i++) {
                const name = baseName + (i ? i : "");
                if (!names.has(name.toLowerCase())) {
                    user.name = name;
                    break;
                }
            }

            if (!user.name) user.name = baseName + generateUserNameSuffix();

            await this.auditService.add(SecurityAudit.googleAccountLinked(user.id, email));
            user = await repo.save(user);

            await this.emailService.sendEmail(
                email,
                `Welcome to Crosswords, ${user.displayName}!`,
                `You have signed up to Crosswords using your Google account. ` +
                    `Your user name is ${user.name}. ` +
                    `If you wish, you can later set a password for your account by visiting the ` +
                    `settings page: ${new URL("#/profile/manage/password", this.frontEndUrl).href}`
            );

            done(undefined, user);
        } catch (err) {
            done(err);
        }
    };

    public get accountType() {
        return ConnectedAccountType.Google;
    }

    public isConnected(user: User): boolean {
        return !!user.googleId;
    }

    public async disconnectAccount(user: User) {
        if (!user.passwordHash)
            throw new ForbiddenError(
                "You must set a password for your account before removing the linked Google account."
            );

        await this.auditService.add(SecurityAudit.googleAccountRemoved(user.id));
        await this.userRepo().update(user.id, { googleId: undefined });

        if (user.email)
            await this.emailService.sendEmail(
                user.email,
                `Google account unlinked`,
                `Your Google account has been unlinked from your Crosswords account. ` +
                    `You can view your account settings here: ${
                        new URL("#/profile/manage/password", this.frontEndUrl).href
                    }`
            );

        user.googleId = undefined;
        return user;
    }
}
