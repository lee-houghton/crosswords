import { iso_639_1 } from "iso-639";
import { Service } from "../../decorators";

export type LanguageId = string;

export interface Language {
    id: LanguageId;
    name: string; 
    nativeName: string;
}

@Service()
export class LanguageService {
    private languages: Map<LanguageId, Language>;

    public constructor() {
        this.languages = new Map<LanguageId, Language>();
        const collator = new Intl.Collator();

        // Sort these because we would like these to be in alphabetical order.
        // (Well, some sort of alphabetical order, anyway - in order of the English name)
        const languages = Object.entries(iso_639_1).map(([id, lang]) => ({
            id,
            name: lang.name,
            nativeName: lang.nativeName,
        })).sort((l1, l2) => collator.compare(l1.name, l2.name));

        for (const lang of languages)
            this.languages.set(lang.id, lang);
    }

    public getName(id: LanguageId): string {
        // Fall back to the ID - at least it's better than nothing.
        return this.languages.get(id)?.name ?? id;
    }

    public getNativeName(id: LanguageId): string {
        // Fall back to the ID - at least it's better than nothing.
        return this.languages.get(id)?.nativeName ?? id;
    }

    public getLanguage(id: LanguageId): Language {
        return this.languages.get(id) ?? { id, name: id, nativeName: id };
    }

    public getLanguageIds(): LanguageId[] {
        return [...this.languages.keys()];
    }

    public getLanguages(): Language[] {
        return [...this.languages.values()];
    }
}
