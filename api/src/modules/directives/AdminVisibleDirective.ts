import { Context } from "../../interfaces";
import { fieldWrapperDirective } from "./fieldWrapperDirective";

/**
 * Describes a field or object which can only be accessed by the user is a superuser.
 * If the user is not a superuser, the field resolves to `null`.
 */
export const adminVisibleDirective = fieldWrapperDirective(
    "adminVisible",
    async ({}, _src, _args, context: Context) => {
        if (!context.user || !context.user.superuser) return null;
    }
);
