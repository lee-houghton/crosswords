import { AuthenticationError } from "../../errors";
import { Context } from "../../interfaces";
import { fieldWrapperDirective } from "./fieldWrapperDirective";

export const authDirective = fieldWrapperDirective("auth", async ({}, _src, _args, context: Context) => {
    if (!context.user) throw new AuthenticationError("You are not logged in. You must log in to do this.");
});
