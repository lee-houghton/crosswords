import { Context } from "../../interfaces";
import { fieldWrapperDirective } from "./fieldWrapperDirective";

export const authNullDirective = fieldWrapperDirective("authNull", async ({}, _src, _args, context: Context) => {
    if (!context.user) return null;
});
