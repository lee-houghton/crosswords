import { AuthenticationError } from "../../errors";
import { Context } from "../../interfaces";
import { User } from "../entities/User";
import { fieldWrapperDirective } from "./fieldWrapperDirective";

/**
 * Describes a field or object which can only be accessed by the user it's for.
 * Should only be used on fields of User.
 */
export const selfOnlyDirective = fieldWrapperDirective<{ admin: boolean }>(
    "selfOnly",
    async ({ admin }, src: number | User, _args, context: Context) => {
        const userId = typeof src === "number" ? src : src.id;

        if (!context.user) throw new AuthenticationError("You are not logged in. You must log in to do this.");

        if (admin === true && context.user.superuser) return;

        if (userId !== context.user.id)
            throw new AuthenticationError("You can only call this function for yourself, not for other users.");
    }
);
