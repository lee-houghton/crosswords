import { AuthenticationError } from "../../errors";
import { Context } from "../../interfaces";
import { fieldWrapperDirective } from "./fieldWrapperDirective";

/**
 * Describes a field or object which can only be accessed by the user is a superuser.
 */
export const adminOnlyDirective = fieldWrapperDirective("adminOnly", async ({}, _src, _args, context: Context) => {
    if (!context.user || !context.user.superuser)
        throw new AuthenticationError("You must be logged in as a superuser to view/do this.");
});
