import { MapperKind, getDirective, mapSchema } from "@graphql-tools/utils";
import { GraphQLResolveInfo, GraphQLSchema, defaultFieldResolver } from "graphql";
import { Context } from "../../interfaces";

export function fieldWrapperDirective<TDirective extends {} = {}>(
    directiveName: string,
    resolveDirective: (
        directive: TDirective,
        src: any,
        args: Record<string, any>,
        context: Context,
        info: GraphQLResolveInfo
    ) => Promise<any>
) {
    const typeDirectiveArgumentMaps: Record<string, TDirective> = {};
    return (schema: GraphQLSchema) =>
        mapSchema(schema, {
            [MapperKind.TYPE]: (type) => {
                const directive = getDirective(schema, type, directiveName)?.[0];
                if (directive) {
                    typeDirectiveArgumentMaps[type.name] = directive as TDirective;
                }
                return undefined;
            },
            [MapperKind.OBJECT_FIELD]: (fieldConfig, _fieldName, typeName) => {
                const directive =
                    (getDirective(schema, fieldConfig, directiveName)?.[0] as TDirective | undefined) ??
                    typeDirectiveArgumentMaps[typeName];
                if (!directive) return;

                const { resolve = defaultFieldResolver, subscribe } = fieldConfig;

                fieldConfig.resolve = async (src, args, ctx, info) => {
                    const result = await resolveDirective(directive, src, args, ctx, info);
                    if (result !== undefined) return result;

                    return resolve(src, args, ctx, info);
                };

                if (subscribe) {
                    fieldConfig.subscribe = async (src, args, ctx, info) => {
                        const result = await resolveDirective(directive, src, args, ctx, info);
                        if (result !== undefined) return result;
                        return subscribe(src, args, ctx, info);
                    };
                }

                return fieldConfig;
            },
        });
}
