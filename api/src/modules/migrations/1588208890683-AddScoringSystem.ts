import {MigrationInterface, QueryRunner} from "typeorm";

export class AddScoringSystem1588208890683 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "user_term" ("userId" integer NOT NULL, "termId" integer NOT NULL, "attempts" integer NOT NULL DEFAULT 0, "successes" integer NOT NULL DEFAULT 0, "score" real NOT NULL DEFAULT 0, "lastPlayed" TIMESTAMP NOT NULL, CONSTRAINT "PK_97165521823cafd883faba4c447" PRIMARY KEY ("userId", "termId"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_77e401f1e2c97fa6c28ab2ec1f" ON "user_term" ("userId", "score", "termId") `, undefined);
        await queryRunner.query(`ALTER TABLE "deck" ALTER COLUMN "termLanguage" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "deck" ALTER COLUMN "definitionLanguage" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "user_term" ADD CONSTRAINT "FK_8138efbb02e084eeb044f8ca02d" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "user_term" ADD CONSTRAINT "FK_3f8c222c31f2dfa85e05085184c" FOREIGN KEY ("termId") REFERENCES "term"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "user_term" DROP CONSTRAINT "FK_3f8c222c31f2dfa85e05085184c"`, undefined);
        await queryRunner.query(`ALTER TABLE "user_term" DROP CONSTRAINT "FK_8138efbb02e084eeb044f8ca02d"`, undefined);
        await queryRunner.query(`ALTER TABLE "deck" ALTER COLUMN "definitionLanguage" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "deck" ALTER COLUMN "termLanguage" SET NOT NULL`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_77e401f1e2c97fa6c28ab2ec1f"`, undefined);
        await queryRunner.query(`DROP TABLE "user_term"`, undefined);
    }

}
