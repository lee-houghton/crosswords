import {MigrationInterface, QueryRunner} from "typeorm";

export class AddAuditEntity1593130971889 implements MigrationInterface {
    name = 'AddAuditEntity1593130971889'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "audit" ("id" SERIAL NOT NULL, "code" smallint NOT NULL, "parameters" text array, "created" TIMESTAMP NOT NULL DEFAULT now(), "userId" integer, "deckId" integer, "crosswordId" integer, "type" smallint NOT NULL, CONSTRAINT "PK_1d3d120ddaf7bc9b1ed68ed463a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "audit_code_created_key" ON "audit" ("code", "created") `);
        await queryRunner.query(`CREATE INDEX "audit_code_key" ON "audit" ("code") `);
        await queryRunner.query(`CREATE INDEX "IDX_36af2200b489c9dcef2e1ebbea" ON "audit" ("type") `);
        await queryRunner.query(`ALTER TABLE "audit" ADD CONSTRAINT "FK_7ae389e858ad6f2c0c63112e387" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "audit" ADD CONSTRAINT "FK_5da7307d18c90b7b074def10ee2" FOREIGN KEY ("deckId") REFERENCES "deck"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "audit" ADD CONSTRAINT "FK_fff10d8f3df14606fd96a52be51" FOREIGN KEY ("crosswordId") REFERENCES "crossword"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "audit" DROP CONSTRAINT "FK_fff10d8f3df14606fd96a52be51"`);
        await queryRunner.query(`ALTER TABLE "audit" DROP CONSTRAINT "FK_5da7307d18c90b7b074def10ee2"`);
        await queryRunner.query(`ALTER TABLE "audit" DROP CONSTRAINT "FK_7ae389e858ad6f2c0c63112e387"`);
        await queryRunner.query(`DROP INDEX "IDX_36af2200b489c9dcef2e1ebbea"`);
        await queryRunner.query(`DROP INDEX "audit_code_key"`);
        await queryRunner.query(`DROP INDEX "audit_code_created_key"`);
        await queryRunner.query(`DROP TABLE "audit"`);
    }

}
