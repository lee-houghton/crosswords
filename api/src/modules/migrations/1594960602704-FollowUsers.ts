import {MigrationInterface, QueryRunner} from "typeorm";

export class FollowUsers1594960602704 implements MigrationInterface {
    name = 'FollowUsers1594960602704'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user_followed_users_user" ("userId_1" integer NOT NULL, "userId_2" integer NOT NULL, CONSTRAINT "PK_66df5e74a6bd310d05ec72cad3c" PRIMARY KEY ("userId_1", "userId_2"))`);
        await queryRunner.query(`CREATE INDEX "IDX_d9054dc181f1fed03d71a966a5" ON "user_followed_users_user" ("userId_1") `);
        await queryRunner.query(`CREATE INDEX "IDX_c2d031d9399a2428cedbb14e81" ON "user_followed_users_user" ("userId_2") `);
        await queryRunner.query(`ALTER TABLE "user_followed_users_user" ADD CONSTRAINT "FK_d9054dc181f1fed03d71a966a5b" FOREIGN KEY ("userId_1") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_followed_users_user" ADD CONSTRAINT "FK_c2d031d9399a2428cedbb14e81e" FOREIGN KEY ("userId_2") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_followed_users_user" DROP CONSTRAINT "FK_c2d031d9399a2428cedbb14e81e"`);
        await queryRunner.query(`ALTER TABLE "user_followed_users_user" DROP CONSTRAINT "FK_d9054dc181f1fed03d71a966a5b"`);
        await queryRunner.query(`DROP INDEX "IDX_c2d031d9399a2428cedbb14e81"`);
        await queryRunner.query(`DROP INDEX "IDX_d9054dc181f1fed03d71a966a5"`);
        await queryRunner.query(`DROP TABLE "user_followed_users_user"`);
    }

}
