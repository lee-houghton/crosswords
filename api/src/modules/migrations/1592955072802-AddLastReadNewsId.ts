import {MigrationInterface, QueryRunner} from "typeorm";

export class AddLastReadNewsId1592955072802 implements MigrationInterface {
    name = 'AddLastReadNewsId1592955072802'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "reset_password_request" ("id" SERIAL NOT NULL, "userId" integer NOT NULL, "code" text NOT NULL, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "expires" TIMESTAMP WITH TIME ZONE NOT NULL, CONSTRAINT "PK_74675f940551b34f6e321247b81" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_403fcf601d273e91c7e2203276" ON "reset_password_request" ("code") `);
        await queryRunner.query(`ALTER TABLE "user" ADD "lastReadNewsId" integer`);
        await queryRunner.query(`ALTER TABLE "reset_password_request" ADD CONSTRAINT "FK_f49e9196528497e5762e5fac23d" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "reset_password_request" DROP CONSTRAINT "FK_f49e9196528497e5762e5fac23d"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "lastReadNewsId"`);
        await queryRunner.query(`DROP INDEX "IDX_403fcf601d273e91c7e2203276"`);
        await queryRunner.query(`DROP TABLE "reset_password_request"`);
    }

}
