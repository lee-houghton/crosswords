import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialDatabase1577566708052 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "term" ("id" SERIAL NOT NULL, "term" character varying NOT NULL, "definitions" text array NOT NULL, "pronunciation" character varying NOT NULL, "order" integer NOT NULL, "deckId" integer, CONSTRAINT "PK_55b0479f0743f2e5d5ec414821e" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_0421496075293ad1340b09b350" ON "term" ("deckId", "order") `, undefined);
        await queryRunner.query(`CREATE TABLE "deck" ("id" SERIAL NOT NULL, "userId" integer NOT NULL, "title" character varying NOT NULL, "description" character varying NOT NULL, "termLanguage" character varying NOT NULL, "definitionLanguage" character varying NOT NULL, "termCount" integer NOT NULL DEFAULT 0, CONSTRAINT "PK_99f8010303acab0edf8e1df24f9" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "user" ("id" SERIAL NOT NULL, "name" citext NOT NULL, "displayName" citext NOT NULL, "email" citext NOT NULL, "enabled" boolean NOT NULL DEFAULT true, "created" TIMESTAMP NOT NULL DEFAULT now(), "passwordHash" character varying, "activationCode" text, "activated" TIMESTAMP, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE UNIQUE INDEX "User_name_key" ON "user" ("name") `, undefined);
        await queryRunner.query(`CREATE UNIQUE INDEX "User_email_key" ON "user" ("email") `, undefined);
        await queryRunner.query(`CREATE UNIQUE INDEX "User_activationCode_key" ON "user" ("activationCode") `, undefined);
        await queryRunner.query(`CREATE TABLE "clue" ("id" SERIAL NOT NULL, "x" smallint NOT NULL, "y" smallint NOT NULL, "direction" character varying(1) NOT NULL, "number" smallint NOT NULL, "lengthHint" character varying(50) NOT NULL, "clue" character varying(500) NOT NULL, "answer" character varying(32) NOT NULL, "originalAnswer" character varying(500) NOT NULL, "deckId" integer NOT NULL, "crosswordId" integer NOT NULL, CONSTRAINT "PK_fdfc59f3731665ca6fd7e7682d4" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "cell" ("id" SERIAL NOT NULL, "crosswordId" integer NOT NULL, "x" smallint NOT NULL, "y" smallint NOT NULL, "contents" text NOT NULL, "acrossClueId" integer, "downClueId" integer, CONSTRAINT "PK_6f34717c251843e5ca32fc1b2b8" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_6915fb80dff15cc26886914c8b" ON "cell" ("crosswordId", "x", "y") `, undefined);
        await queryRunner.query(`CREATE TABLE "crossword" ("id" SERIAL NOT NULL, "title" character varying, "description" character varying, "created" TIMESTAMP NOT NULL DEFAULT now(), "updated" TIMESTAMP NOT NULL DEFAULT now(), "userId" integer NOT NULL, "width" integer, "height" integer, "available" TIMESTAMP, "stars" integer NOT NULL DEFAULT 0, "attempts" integer NOT NULL DEFAULT 0, CONSTRAINT "PK_b3bdd474534ec7c74704d0f43c8" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "guess" ("id" SERIAL NOT NULL, "attemptId" integer NOT NULL, "x" smallint NOT NULL, "y" smallint NOT NULL, "contents" text NOT NULL, CONSTRAINT "PK_3a695f50b71c117a9fb5b8ff67c" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_9b73f3b99fa1a7c0df2b8a81c2" ON "guess" ("attemptId", "x", "y") `, undefined);
        await queryRunner.query(`CREATE TABLE "attempt" ("id" SERIAL NOT NULL, "crosswordId" integer NOT NULL, "userId" integer NOT NULL, "started" TIMESTAMP NOT NULL DEFAULT now(), "lastPlayed" TIMESTAMP NOT NULL, "completed" boolean NOT NULL DEFAULT false, "percentCompleted" integer NOT NULL DEFAULT 0, "score" integer NOT NULL DEFAULT 0, CONSTRAINT "PK_5f822b29b3128d1c65d3d6c193d" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_12d68a3e4d356d67c167ce2c50" ON "attempt" ("crosswordId", "userId") `, undefined);
        await queryRunner.query(`CREATE TABLE "user_account_request" ("id" SERIAL NOT NULL, "userId" integer NOT NULL, "code" text NOT NULL, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "expires" TIMESTAMP WITH TIME ZONE NOT NULL, "type" character varying NOT NULL, CONSTRAINT "PK_acea753b0c017079fd09834a771" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_b9dfcc9e384c86ddaf3cec5c51" ON "user_account_request" ("code") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_9f364fd445e5ab73abba3c0fde" ON "user_account_request" ("type") `, undefined);
        await queryRunner.query(`CREATE TABLE "change_email_request" ("id" SERIAL NOT NULL, "userId" integer NOT NULL, "code" text NOT NULL, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "expires" TIMESTAMP WITH TIME ZONE NOT NULL, "newEmail" character varying NOT NULL, CONSTRAINT "PK_d170e30bae3913df69b68541f1b" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_3f82660f51a340a8b2589f7396" ON "change_email_request" ("code") `, undefined);
        await queryRunner.query(`CREATE TABLE "deck_crosswords_crossword" ("deckId" integer NOT NULL, "crosswordId" integer NOT NULL, CONSTRAINT "PK_1b238be6c0a88ecea1601a68b1d" PRIMARY KEY ("deckId", "crosswordId"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_ccbd54a8b39d2c052047df271c" ON "deck_crosswords_crossword" ("deckId") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_11afd00097edab18adeab9f891" ON "deck_crosswords_crossword" ("crosswordId") `, undefined);
        await queryRunner.query(`CREATE TABLE "user_favourite_decks_deck" ("userId" integer NOT NULL, "deckId" integer NOT NULL, CONSTRAINT "PK_cb76e482c5b700bdd90d31b3045" PRIMARY KEY ("userId", "deckId"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_3da7e3aafd421c791562c82a7b" ON "user_favourite_decks_deck" ("userId") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_e4768c94e60bc3a46785e9f39f" ON "user_favourite_decks_deck" ("deckId") `, undefined);
        await queryRunner.query(`ALTER TABLE "term" ADD CONSTRAINT "FK_127076afe7b4fb19a7473835bc8" FOREIGN KEY ("deckId") REFERENCES "deck"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "deck" ADD CONSTRAINT "FK_09e8a376bab70b9737c839b2e24" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "clue" ADD CONSTRAINT "FK_16292b7d880bd7a9e92ac16f6c6" FOREIGN KEY ("deckId") REFERENCES "deck"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "clue" ADD CONSTRAINT "FK_c80c53ee31cf36e0aa450c81754" FOREIGN KEY ("crosswordId") REFERENCES "crossword"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "cell" ADD CONSTRAINT "FK_c32d6e2df4beae06ae7056c7f74" FOREIGN KEY ("crosswordId") REFERENCES "crossword"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "cell" ADD CONSTRAINT "FK_2ee9d2e8f0a6b5a55e7cb720922" FOREIGN KEY ("acrossClueId") REFERENCES "clue"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "cell" ADD CONSTRAINT "FK_2d9a5ddc63443d983f2ed18ae3d" FOREIGN KEY ("downClueId") REFERENCES "clue"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "crossword" ADD CONSTRAINT "FK_d99cda925bdc565e5bd566e28a5" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "guess" ADD CONSTRAINT "FK_e0ff710b5c5ba564ca6b4ec50cc" FOREIGN KEY ("attemptId") REFERENCES "attempt"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "attempt" ADD CONSTRAINT "FK_dd8844876037b478f5bb859512e" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "attempt" ADD CONSTRAINT "FK_eec67ab04261ee84eea131a834b" FOREIGN KEY ("crosswordId") REFERENCES "crossword"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "user_account_request" ADD CONSTRAINT "FK_fd9b05f61570ccae1ef6f754acc" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "change_email_request" ADD CONSTRAINT "FK_58d4db661673d147ce84f8d3f6d" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "deck_crosswords_crossword" ADD CONSTRAINT "FK_ccbd54a8b39d2c052047df271c1" FOREIGN KEY ("deckId") REFERENCES "deck"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "deck_crosswords_crossword" ADD CONSTRAINT "FK_11afd00097edab18adeab9f8910" FOREIGN KEY ("crosswordId") REFERENCES "crossword"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "user_favourite_decks_deck" ADD CONSTRAINT "FK_3da7e3aafd421c791562c82a7bc" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "user_favourite_decks_deck" ADD CONSTRAINT "FK_e4768c94e60bc3a46785e9f39f3" FOREIGN KEY ("deckId") REFERENCES "deck"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "user_favourite_decks_deck" DROP CONSTRAINT "FK_e4768c94e60bc3a46785e9f39f3"`, undefined);
        await queryRunner.query(`ALTER TABLE "user_favourite_decks_deck" DROP CONSTRAINT "FK_3da7e3aafd421c791562c82a7bc"`, undefined);
        await queryRunner.query(`ALTER TABLE "deck_crosswords_crossword" DROP CONSTRAINT "FK_11afd00097edab18adeab9f8910"`, undefined);
        await queryRunner.query(`ALTER TABLE "deck_crosswords_crossword" DROP CONSTRAINT "FK_ccbd54a8b39d2c052047df271c1"`, undefined);
        await queryRunner.query(`ALTER TABLE "change_email_request" DROP CONSTRAINT "FK_58d4db661673d147ce84f8d3f6d"`, undefined);
        await queryRunner.query(`ALTER TABLE "user_account_request" DROP CONSTRAINT "FK_fd9b05f61570ccae1ef6f754acc"`, undefined);
        await queryRunner.query(`ALTER TABLE "attempt" DROP CONSTRAINT "FK_eec67ab04261ee84eea131a834b"`, undefined);
        await queryRunner.query(`ALTER TABLE "attempt" DROP CONSTRAINT "FK_dd8844876037b478f5bb859512e"`, undefined);
        await queryRunner.query(`ALTER TABLE "guess" DROP CONSTRAINT "FK_e0ff710b5c5ba564ca6b4ec50cc"`, undefined);
        await queryRunner.query(`ALTER TABLE "crossword" DROP CONSTRAINT "FK_d99cda925bdc565e5bd566e28a5"`, undefined);
        await queryRunner.query(`ALTER TABLE "cell" DROP CONSTRAINT "FK_2d9a5ddc63443d983f2ed18ae3d"`, undefined);
        await queryRunner.query(`ALTER TABLE "cell" DROP CONSTRAINT "FK_2ee9d2e8f0a6b5a55e7cb720922"`, undefined);
        await queryRunner.query(`ALTER TABLE "cell" DROP CONSTRAINT "FK_c32d6e2df4beae06ae7056c7f74"`, undefined);
        await queryRunner.query(`ALTER TABLE "clue" DROP CONSTRAINT "FK_c80c53ee31cf36e0aa450c81754"`, undefined);
        await queryRunner.query(`ALTER TABLE "clue" DROP CONSTRAINT "FK_16292b7d880bd7a9e92ac16f6c6"`, undefined);
        await queryRunner.query(`ALTER TABLE "deck" DROP CONSTRAINT "FK_09e8a376bab70b9737c839b2e24"`, undefined);
        await queryRunner.query(`ALTER TABLE "term" DROP CONSTRAINT "FK_127076afe7b4fb19a7473835bc8"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_e4768c94e60bc3a46785e9f39f"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_3da7e3aafd421c791562c82a7b"`, undefined);
        await queryRunner.query(`DROP TABLE "user_favourite_decks_deck"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_11afd00097edab18adeab9f891"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_ccbd54a8b39d2c052047df271c"`, undefined);
        await queryRunner.query(`DROP TABLE "deck_crosswords_crossword"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_3f82660f51a340a8b2589f7396"`, undefined);
        await queryRunner.query(`DROP TABLE "change_email_request"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_9f364fd445e5ab73abba3c0fde"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_b9dfcc9e384c86ddaf3cec5c51"`, undefined);
        await queryRunner.query(`DROP TABLE "user_account_request"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_12d68a3e4d356d67c167ce2c50"`, undefined);
        await queryRunner.query(`DROP TABLE "attempt"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_9b73f3b99fa1a7c0df2b8a81c2"`, undefined);
        await queryRunner.query(`DROP TABLE "guess"`, undefined);
        await queryRunner.query(`DROP TABLE "crossword"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_6915fb80dff15cc26886914c8b"`, undefined);
        await queryRunner.query(`DROP TABLE "cell"`, undefined);
        await queryRunner.query(`DROP TABLE "clue"`, undefined);
        await queryRunner.query(`DROP INDEX "User_activationCode_key"`, undefined);
        await queryRunner.query(`DROP INDEX "User_email_key"`, undefined);
        await queryRunner.query(`DROP INDEX "User_name_key"`, undefined);
        await queryRunner.query(`DROP TABLE "user"`, undefined);
        await queryRunner.query(`DROP TABLE "deck"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_0421496075293ad1340b09b350"`, undefined);
        await queryRunner.query(`DROP TABLE "term"`, undefined);
    }

}
