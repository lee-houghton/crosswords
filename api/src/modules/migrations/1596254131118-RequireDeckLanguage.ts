import {MigrationInterface, QueryRunner} from "typeorm";

export class RequireDeckLanguage1596254131118 implements MigrationInterface {
    name = 'RequireDeckLanguage1596254131118'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "deck" ALTER COLUMN "termLanguage" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deck" ALTER COLUMN "definitionLanguage" SET NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "deck" ALTER COLUMN "definitionLanguage" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deck" ALTER COLUMN "termLanguage" DROP NOT NULL`);
    }

}
