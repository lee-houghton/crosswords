import {MigrationInterface, QueryRunner} from "typeorm";

export class AddGoogleId1597702968270 implements MigrationInterface {
    name = 'AddGoogleId1597702968270'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "googleId" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "googleId"`);
    }

}
