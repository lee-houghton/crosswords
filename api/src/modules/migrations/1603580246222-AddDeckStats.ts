import {MigrationInterface, QueryRunner} from "typeorm";

export class AddDeckStats1603580246222 implements MigrationInterface {
    name = 'AddDeckStats1603580246222'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "deck" ADD "stars" integer NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "deck" ADD "crosswordCount" integer NOT NULL DEFAULT 0`);

        await queryRunner.query(`
            update deck d 
            set stars = (
                select count(*) 
                from user_favourite_decks_deck ud 
                where ud."deckId" = d.id
            );
        `);

        await queryRunner.query(`
            update deck d 
            set "crosswordCount" = (
                select count(*) 
                from deck_crosswords_crossword dc 
                where dc."deckId" = d.id
            );
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "deck" DROP COLUMN "crosswordCount"`);
        await queryRunner.query(`ALTER TABLE "deck" DROP COLUMN "stars"`);
    }

}
