import {MigrationInterface, QueryRunner} from "typeorm";

export class AddUserTermLastResultSuccess1588368114854 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "user_term" ADD "lastResultSuccess" boolean NOT NULL DEFAULT true`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_6d4fecd588408f72217aca5c67" ON "user_term" ("userId", "lastPlayed") WHERE not "lastResultSuccess"`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP INDEX "IDX_6d4fecd588408f72217aca5c67"`, undefined);
        await queryRunner.query(`ALTER TABLE "user_term" DROP COLUMN "lastResultSuccess"`, undefined);
    }

}
