import {MigrationInterface, QueryRunner} from "typeorm";

export class FollowSelf1596256598214 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        queryRunner.query(`
            insert into user_followed_users_user("userId_1", "userId_2")
            select u.id, u.id 
            from "user" u
            on conflict ("userId_1", "userId_2")
                do nothing;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
