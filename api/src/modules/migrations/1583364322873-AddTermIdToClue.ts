import {MigrationInterface, QueryRunner} from "typeorm";

export class AddTermIdToClue1583364322873 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "clue" ADD "termId" integer`, undefined);
        await queryRunner.query(`ALTER TABLE "clue" ADD CONSTRAINT "FK_2dd9b602f420848417b3490032d" FOREIGN KEY ("termId") REFERENCES "term"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "clue" DROP CONSTRAINT "FK_2dd9b602f420848417b3490032d"`, undefined);
        await queryRunner.query(`ALTER TABLE "clue" DROP COLUMN "termId"`, undefined);
    }

}
