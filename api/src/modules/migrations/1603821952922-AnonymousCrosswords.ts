import {MigrationInterface, QueryRunner} from "typeorm";

export class AnonymousCrosswords1603821952922 implements MigrationInterface {
    name = 'AnonymousCrosswords1603821952922'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "crossword" DROP CONSTRAINT "FK_d99cda925bdc565e5bd566e28a5"`);
        await queryRunner.query(`ALTER TABLE "crossword" ALTER COLUMN "userId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "crossword" ADD CONSTRAINT "FK_d99cda925bdc565e5bd566e28a5" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "crossword" DROP CONSTRAINT "FK_d99cda925bdc565e5bd566e28a5"`);
        await queryRunner.query(`ALTER TABLE "crossword" ALTER COLUMN "userId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "crossword" ADD CONSTRAINT "FK_d99cda925bdc565e5bd566e28a5" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
