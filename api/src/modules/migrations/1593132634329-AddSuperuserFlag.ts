import {MigrationInterface, QueryRunner} from "typeorm";

export class AddSuperuserFlag1593132634329 implements MigrationInterface {
    name = 'AddSuperuserFlag1593132634329'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "superuser" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "superuser"`);
    }

}
