import {MigrationInterface, QueryRunner} from "typeorm";

export class AddUserLanguage1583649459305 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "user_language" ("userId" integer NOT NULL, "languageId" character varying(2) NOT NULL, "decks" integer NOT NULL DEFAULT 0, "favourites" integer NOT NULL DEFAULT 0, "crosswordsCreated" integer NOT NULL DEFAULT 0, "crosswordsCompleted" integer NOT NULL DEFAULT 0, "started" TIMESTAMP NOT NULL DEFAULT now(), "lastActivity" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_af83763d8a9fe1919ca7441b7f0" PRIMARY KEY ("userId", "languageId"))`, undefined);
        await queryRunner.query(`ALTER TABLE "crossword" ADD "languageId" character varying(2)`, undefined);
        await queryRunner.query(`ALTER TABLE "deck" ALTER COLUMN "termLanguage" TYPE character varying(2)`, undefined);
        await queryRunner.query(`ALTER TABLE "deck" ALTER COLUMN "definitionLanguage" TYPE character varying(2)`, undefined);
        await queryRunner.query(`ALTER TABLE "user_language" ADD CONSTRAINT "FK_43d5b919c56d00a9f61ae8bf4cf" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "user_language" DROP CONSTRAINT "FK_43d5b919c56d00a9f61ae8bf4cf"`, undefined);
        await queryRunner.query(`ALTER TABLE "deck" ALTER COLUMN "termLanguage" TYPE text`, undefined);
        await queryRunner.query(`ALTER TABLE "deck" ALTER COLUMN "definitionLanguage" TYPE text`, undefined);
        await queryRunner.query(`ALTER TABLE "crossword" DROP COLUMN "languageId"`, undefined);
        await queryRunner.query(`DROP TABLE "user_language"`, undefined);
    }

}
