import {MigrationInterface, QueryRunner} from "typeorm";

export class AddActivities1595127366207 implements MigrationInterface {
    name = 'AddActivities1595127366207'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "activity" ("id" SERIAL NOT NULL, "type" smallint NOT NULL, "parameters" text array, "date" TIMESTAMP NOT NULL DEFAULT now(), "userId" integer NOT NULL, "deckIds" integer array, "crosswordIds" integer array, CONSTRAINT "PK_24625a1d6b1b089c8ae206fe467" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "audit" ADD "otherUserId" integer`);
        await queryRunner.query(`ALTER TABLE "activity" ADD CONSTRAINT "FK_3571467bcbe021f66e2bdce96ea" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "audit" ADD CONSTRAINT "FK_654ac05389d9212d54e3bcd21d8" FOREIGN KEY ("otherUserId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "audit" DROP CONSTRAINT "FK_654ac05389d9212d54e3bcd21d8"`);
        await queryRunner.query(`ALTER TABLE "activity" DROP CONSTRAINT "FK_3571467bcbe021f66e2bdce96ea"`);
        await queryRunner.query(`ALTER TABLE "audit" DROP COLUMN "otherUserId"`);
        await queryRunner.query(`DROP TABLE "activity"`);
    }

}
