import { transformAndValidateSync } from "class-transformer-validator";
import { Injectable } from "injection-js";
import { CreateCrosswordRequest } from "../../dtos";
import { SchemaProvider } from "../../interfaces/ISchemaProvider";
import { CrosswordService, DeckService } from "../services";
import { AttemptService } from "../services/crosswords/AttemptService";

@Injectable()
export class CrosswordSchema extends SchemaProvider {
    public constructor() {
        super({
            Crossword: {
                user: src => src.userId,

                // TODO: Perhaps set the context to DeckSchema to be number|Deck
                // TODO: This implementation makes zero sense
                decks: async (src, args, ctx) =>
                    // TODO: Remove eager loading of Crossword.decks
                    src.decks ? src.decks.map(deck => ctx.resolve(DeckService).getById(deck.id)) : [],

                clues: (src, args, ctx) => ctx.resolve(CrosswordService).getClues(src.id),

                imageUrl: (src, args, ctx) => `/crosswords/${src.id}/svg`,

                attempt: (src, _args, ctx) => 
                    ctx.resolve(AttemptService).getByCrosswordId(src.id, ctx.user),

                language: src => src.languageId,
            },
            Query: {
                crossword: (src, {id}, ctx) => ctx.resolve(CrosswordService).getById(id),
            },
            Mutation: {
                addCrossword: (src, req: object, ctx) => ctx.resolve(CrosswordService).create(
                    transformAndValidateSync(CreateCrosswordRequest, req),
                    ctx.user
                ),
            }
        });
    }
}
