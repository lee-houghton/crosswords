import { AuthenticationError } from "../../errors";
import { SchemaProvider } from "../../interfaces";
import { AuthService } from "../services";
import { AccountService } from "../services/account";
import { ResetPasswordService } from "../services/account/ResetPasswordService";

export class AuthSchema extends SchemaProvider {
    public constructor() {
        super({
            Mutation: {
                async login(_src, { username, password }, ctx) {
                    const user = await ctx.resolve(AuthService).login(username, password);
                    if (!user) throw new AuthenticationError("Incorrect user name or password");

                    await ctx.login(user);
                    return user;
                },
                async logout(_src, _args, ctx) {
                    await ctx.logout();
                },
                async register(_src, { username, email }, ctx) {
                    await ctx.resolve(AccountService).register(username, email);
                },
                async activateAccount(_src, { activationCode, password, displayName }, ctx) {
                    const user = await ctx
                        .resolve(AccountService)
                        .activateAccount(activationCode, password, displayName);
                    await ctx.login(user);
                    return user;
                },
                async resetPassword(_src, { email }, ctx) {
                    await ctx.resolve(ResetPasswordService).createRequest(email);
                },
                async confirmResetPassword(_src, { code, password }, ctx) {
                    const user = await ctx.resolve(ResetPasswordService).apply(code, password);
                    await ctx.login(user);
                    return user;
                },
            },
        });
    }
}
