import { Injectable } from "injection-js";
import { SchemaProvider } from "../../interfaces/ISchemaProvider";
import { LanguageService } from "../services/LanguageService";

@Injectable()
export class LanguageSchema extends SchemaProvider {
    public constructor() {
        super({
            Language: {
                id: id => id,
                name: (id, _args, ctx) => ctx.resolve(LanguageService).getName(id),
                nativeName: (id, _args, ctx) => ctx.resolve(LanguageService).getNativeName(id),
            },
            Query: {
                languages: (src, args, ctx) => ctx.resolve(LanguageService).getLanguageIds(),
                language: (src, {id}, ctx) => id,
            }
        });
    }
}
