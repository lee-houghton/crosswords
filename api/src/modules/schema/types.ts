import { ActivityType } from '../../types';
import { AuditType } from '../entities/AuditType';
import { AuditCode } from '../entities/AuditCode';
import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
import { Activity as ActivityModel, Attempt as AttemptModel, ChangeEmailRequest as ChangeEmailRequestModel, Clue as ClueModel, Crossword as CrosswordModel, CrosswordAudit as CrosswordAuditModel, Deck as DeckModel, DeckAudit as DeckAuditModel, Guess as GuessModel, Term as TermModel, UserTerm as UserTermModel, UserAudit as UserAuditModel, UserLanguage as UserLanguageModel } from '../entities';
import { UserSource as UserSourceModel } from '.';
import { Context } from '../../interfaces';
export type Maybe<T> = T | null | undefined;
export type InputMaybe<T> = T | null | undefined;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type EnumResolverSignature<T, AllowedValues = any> = { [key in keyof T]?: AllowedValues };
export type RequireFields<T, K extends keyof T> = Omit<T, K> & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  Date: { input: any; output: any; }
  DateTime: { input: any; output: any; }
  JSON: { input: any; output: any; }
  Long: { input: number; output: number; }
  Null: { input: any; output: any; }
  Time: { input: any; output: any; }
};

export type Activities = {
  __typename?: 'Activities';
  activities: Array<Activity>;
  page: PageInfo;
};

export type Activity = {
  __typename?: 'Activity';
  crosswordIds?: Maybe<Array<Scalars['Int']['output']>>;
  cursor: Scalars['String']['output'];
  date: Scalars['DateTime']['output'];
  deckIds?: Maybe<Array<Scalars['Int']['output']>>;
  decks?: Maybe<Array<Deck>>;
  id: Scalars['Int']['output'];
  parameters?: Maybe<Array<Scalars['String']['output']>>;
  type: ActivityType;
  user: User;
};

export { ActivityType };

export type Attempt = {
  __typename?: 'Attempt';
  completed: Scalars['Boolean']['output'];
  crossword: Crossword;
  guesses: Array<Guess>;
  id: Scalars['Long']['output'];
  lastPlayed: Scalars['Date']['output'];
  percentCompleted: Scalars['Int']['output'];
  score?: Maybe<Scalars['Int']['output']>;
  started: Scalars['Date']['output'];
  user: User;
  userName: Scalars['String']['output'];
};

export type Audit = {
  code: AuditCode;
  created: Scalars['DateTime']['output'];
  id: Scalars['Int']['output'];
  parameters?: Maybe<Array<Scalars['String']['output']>>;
  type: AuditType;
  user?: Maybe<User>;
  userId?: Maybe<Scalars['Int']['output']>;
};

export { AuditCode };

export { AuditType };

export type ChangeEmailRequest = UserAccountRequest & {
  __typename?: 'ChangeEmailRequest';
  cancelled?: Maybe<Scalars['DateTime']['output']>;
  created: Scalars['DateTime']['output'];
  expires: Scalars['DateTime']['output'];
  id: Scalars['Int']['output'];
  newEmail: Scalars['String']['output'];
  used?: Maybe<Scalars['DateTime']['output']>;
  userId: Scalars['Int']['output'];
  valid: Scalars['Boolean']['output'];
};

export type Clue = {
  __typename?: 'Clue';
  answer?: Maybe<Scalars['String']['output']>;
  clue: Scalars['String']['output'];
  crossword: Crossword;
  deck: Deck;
  deckId: Scalars['Int']['output'];
  direction: Direction;
  id: Scalars['Int']['output'];
  length: Scalars['Int']['output'];
  lengthHint: Scalars['String']['output'];
  number: Scalars['Int']['output'];
  originalAnswer?: Maybe<Scalars['String']['output']>;
  term?: Maybe<Term>;
  termId?: Maybe<Scalars['Int']['output']>;
  x: Scalars['Int']['output'];
  y: Scalars['Int']['output'];
};

export enum ConnectedAccountType {
  Apple = 'APPLE',
  Facebook = 'FACEBOOK',
  Google = 'GOOGLE'
}

export type CreateDeckRequest = {
  definitionLangId: Scalars['String']['input'];
  description?: InputMaybe<Scalars['String']['input']>;
  termLangId: Scalars['String']['input'];
  terms?: InputMaybe<Array<UpdateDeckTerm>>;
  title: Scalars['String']['input'];
};

export type Crossword = {
  __typename?: 'Crossword';
  attempt?: Maybe<Attempt>;
  available?: Maybe<Scalars['DateTime']['output']>;
  clues?: Maybe<Array<Clue>>;
  created: Scalars['DateTime']['output'];
  decks: Array<Deck>;
  description?: Maybe<Scalars['String']['output']>;
  height?: Maybe<Scalars['Int']['output']>;
  id: Scalars['Int']['output'];
  imageUrl: Scalars['String']['output'];
  language?: Maybe<Language>;
  stars: Scalars['Int']['output'];
  title?: Maybe<Scalars['String']['output']>;
  updated: Scalars['DateTime']['output'];
  user?: Maybe<User>;
  width?: Maybe<Scalars['Int']['output']>;
};

export type CrosswordAudit = Audit & {
  __typename?: 'CrosswordAudit';
  code: AuditCode;
  created: Scalars['DateTime']['output'];
  crossword?: Maybe<Crossword>;
  crosswordId?: Maybe<Scalars['Int']['output']>;
  id: Scalars['Int']['output'];
  parameters?: Maybe<Array<Scalars['String']['output']>>;
  type: AuditType;
  user?: Maybe<User>;
  userId?: Maybe<Scalars['Int']['output']>;
};

export enum CrosswordAuditCode {
  CrosswordCompleted = 'CrosswordCompleted',
  CrosswordCreated = 'CrosswordCreated',
  CrosswordError = 'CrosswordError',
  CrosswordGenerated = 'CrosswordGenerated'
}

export type Deck = {
  __typename?: 'Deck';
  createdBy: User;
  crosswordCount: Scalars['Int']['output'];
  crosswords: Array<Crossword>;
  definitionLang: Language;
  description?: Maybe<Scalars['String']['output']>;
  id: Scalars['Long']['output'];
  stars: Scalars['Int']['output'];
  termCount: Scalars['Int']['output'];
  termLang: Language;
  terms: Array<Term>;
  title: Scalars['String']['output'];
  url: Scalars['String']['output'];
};

export type DeckAudit = Audit & {
  __typename?: 'DeckAudit';
  code: AuditCode;
  created: Scalars['DateTime']['output'];
  deck?: Maybe<Deck>;
  deckId?: Maybe<Scalars['Int']['output']>;
  id: Scalars['Int']['output'];
  parameters?: Maybe<Array<Scalars['String']['output']>>;
  type: AuditType;
  user?: Maybe<User>;
  userId?: Maybe<Scalars['Int']['output']>;
};

export enum DeckAuditCode {
  DeckCreated = 'DeckCreated',
  DeckDeleted = 'DeckDeleted',
  DeckTermEdited = 'DeckTermEdited',
  DeckUpdated = 'DeckUpdated'
}

export type DeckSearchResult = Deck | Term;

export enum Direction {
  Across = 'ACROSS',
  Down = 'DOWN'
}

export type Guess = {
  __typename?: 'Guess';
  attemptId: Scalars['Long']['output'];
  guess?: Maybe<Scalars['String']['output']>;
  id: Scalars['Long']['output'];
  x: Scalars['Int']['output'];
  y: Scalars['Int']['output'];
};

export type Language = {
  __typename?: 'Language';
  id: Scalars['String']['output'];
  name: Scalars['String']['output'];
  nativeName: Scalars['String']['output'];
};

export type Mutation = {
  __typename?: 'Mutation';
  activateAccount: User;
  addCrossword: Crossword;
  cancelChangeEmailRequest: User;
  changeEmail: ChangeEmailRequest;
  changePassword: User;
  /**
   * Same as checkAnswers, but deprecated because it is better to return the Attempt rather than the Crossword.
   * @deprecated Field no longer supported
   */
  completeAttempt: Crossword;
  confirmChangeEmail: User;
  confirmResetPassword: User;
  createDeck: Deck;
  disconnectAccount: User;
  followUser?: Maybe<Scalars['Null']['output']>;
  login: User;
  logout?: Maybe<Scalars['Null']['output']>;
  markNewsRead: User;
  register?: Maybe<Scalars['Null']['output']>;
  reindex?: Maybe<Scalars['Null']['output']>;
  resetPassword?: Maybe<Scalars['Null']['output']>;
  saveAttempt: Attempt;
  toggleSetFavourite?: Maybe<Scalars['Null']['output']>;
  updateDeck: Deck;
  updateTerm: Term;
};


export type MutationActivateAccountArgs = {
  activationCode: Scalars['String']['input'];
  displayName: Scalars['String']['input'];
  password: Scalars['String']['input'];
};


export type MutationAddCrosswordArgs = {
  deckIds?: InputMaybe<Array<Scalars['Long']['input']>>;
  description?: InputMaybe<Scalars['String']['input']>;
  height?: InputMaybe<Scalars['Int']['input']>;
  languageId?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  width?: InputMaybe<Scalars['Int']['input']>;
};


export type MutationCancelChangeEmailRequestArgs = {
  id: Scalars['Int']['input'];
};


export type MutationChangeEmailArgs = {
  email: Scalars['String']['input'];
};


export type MutationChangePasswordArgs = {
  currentPassword: Scalars['String']['input'];
  newPassword: Scalars['String']['input'];
};


export type MutationCompleteAttemptArgs = {
  attempt: UpdateAttemptRequest;
};


export type MutationConfirmChangeEmailArgs = {
  code: Scalars['String']['input'];
};


export type MutationConfirmResetPasswordArgs = {
  code: Scalars['String']['input'];
  password: Scalars['String']['input'];
};


export type MutationCreateDeckArgs = {
  request: CreateDeckRequest;
};


export type MutationDisconnectAccountArgs = {
  type: ConnectedAccountType;
};


export type MutationFollowUserArgs = {
  follow: Scalars['Boolean']['input'];
  userId: Scalars['Int']['input'];
};


export type MutationLoginArgs = {
  password: Scalars['String']['input'];
  username: Scalars['String']['input'];
};


export type MutationMarkNewsReadArgs = {
  id: Scalars['Int']['input'];
};


export type MutationRegisterArgs = {
  email: Scalars['String']['input'];
  username: Scalars['String']['input'];
};


export type MutationResetPasswordArgs = {
  email: Scalars['String']['input'];
};


export type MutationSaveAttemptArgs = {
  attempt: UpdateAttemptRequest;
  complete?: InputMaybe<Scalars['Boolean']['input']>;
};


export type MutationToggleSetFavouriteArgs = {
  favourite: Scalars['Boolean']['input'];
  id: Scalars['Long']['input'];
};


export type MutationUpdateDeckArgs = {
  request: UpdateDeckRequest;
};


export type MutationUpdateTermArgs = {
  request: UpdateTermRequest;
};

export type PageInfo = {
  __typename?: 'PageInfo';
  endCursor?: Maybe<Scalars['String']['output']>;
  startCursor?: Maybe<Scalars['String']['output']>;
};

export type Query = {
  __typename?: 'Query';
  crossword?: Maybe<Crossword>;
  debugSearch: Scalars['JSON']['output'];
  deck: Deck;
  language?: Maybe<Language>;
  languages: Array<Language>;
  me?: Maybe<User>;
  recentActivity: Activities;
  searchDecksAndTerms: Array<DeckSearchResult>;
  session?: Maybe<Session>;
  /** Given part of a term, find other terms with similar text. */
  suggestTerms: Array<Term>;
  term: Term;
  /** Returns the most popular decks for a particular language, optionally filtered by definition language. */
  topDecks: Array<Deck>;
  user?: Maybe<User>;
  users: Array<User>;
};


export type QueryCrosswordArgs = {
  id: Scalars['Long']['input'];
};


export type QueryDebugSearchArgs = {
  body?: InputMaybe<Scalars['JSON']['input']>;
  index?: InputMaybe<Scalars['String']['input']>;
  q?: InputMaybe<Scalars['String']['input']>;
};


export type QueryDeckArgs = {
  id: Scalars['Long']['input'];
};


export type QueryLanguageArgs = {
  id: Scalars['String']['input'];
};


export type QueryRecentActivityArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  count?: InputMaybe<Scalars['Int']['input']>;
};


export type QuerySearchDecksAndTermsArgs = {
  search: Scalars['String']['input'];
};


export type QuerySuggestTermsArgs = {
  excludeDeckId?: InputMaybe<Scalars['Int']['input']>;
  search: Scalars['String']['input'];
  termLang?: InputMaybe<Scalars['String']['input']>;
};


export type QueryTermArgs = {
  id: Scalars['Long']['input'];
};


export type QueryTopDecksArgs = {
  count?: InputMaybe<Scalars['Int']['input']>;
  definitionLang?: InputMaybe<Scalars['String']['input']>;
  termLang: Scalars['String']['input'];
};


export type QueryUserArgs = {
  id?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};


export type QueryUsersArgs = {
  search: Scalars['String']['input'];
};

export type Score = {
  __typename?: 'Score';
  attempts: Scalars['Int']['output'];
  lastPlayed: Scalars['Date']['output'];
  lastResultSuccess: Scalars['Boolean']['output'];
  score: Scalars['Float']['output'];
  successes: Scalars['Int']['output'];
  term: Term;
  termId: Scalars['Int']['output'];
  userId: Scalars['Int']['output'];
};

export type SecurityAudit = Audit & {
  __typename?: 'SecurityAudit';
  code: AuditCode;
  created: Scalars['DateTime']['output'];
  id: Scalars['Int']['output'];
  parameters?: Maybe<Array<Scalars['String']['output']>>;
  type: AuditType;
  user?: Maybe<User>;
  userId?: Maybe<Scalars['Int']['output']>;
};

export enum SecurityAuditCode {
  AccountActivated = 'AccountActivated',
  AccountRegistered = 'AccountRegistered',
  EmailChangeRequested = 'EmailChangeRequested',
  EmailChangeSuccess = 'EmailChangeSuccess',
  FacebookAccountLinked = 'FacebookAccountLinked',
  FacebookAccountRemoved = 'FacebookAccountRemoved',
  ForgotPassword = 'ForgotPassword',
  GoogleAccountLinked = 'GoogleAccountLinked',
  GoogleAccountRemoved = 'GoogleAccountRemoved',
  PasswordReset = 'PasswordReset',
  PasswordResetSuccess = 'PasswordResetSuccess',
  PasswordUpdated = 'PasswordUpdated'
}

export type Session = {
  __typename?: 'Session';
  expires: Scalars['Date']['output'];
};

export type Subscription = {
  __typename?: 'Subscription';
  auditAdded: Audit;
};


export type SubscriptionAuditAddedArgs = {
  type?: InputMaybe<AuditType>;
};

export type Term = {
  __typename?: 'Term';
  deck?: Maybe<Deck>;
  definitions: Array<Scalars['String']['output']>;
  example?: Maybe<Scalars['String']['output']>;
  id: Scalars['Long']['output'];
  order: Scalars['Int']['output'];
  pronunciation: Scalars['String']['output'];
  score?: Maybe<Score>;
  term: Scalars['String']['output'];
};

export type UpdateAttemptRequest = {
  crosswordId: Scalars['Long']['input'];
  guesses: Array<UpdateGuessRequest>;
};

export type UpdateDeckRequest = {
  definitionLangId?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  id: Scalars['Long']['input'];
  termLangId?: InputMaybe<Scalars['String']['input']>;
  terms?: InputMaybe<Array<UpdateDeckTerm>>;
  title?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateDeckTerm = {
  definitions: Array<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['Long']['input']>;
  term: Scalars['String']['input'];
};

export type UpdateGuessRequest = {
  guess: Scalars['String']['input'];
  x: Scalars['Int']['input'];
  y: Scalars['Int']['input'];
};

/** Request to add or update a term. If `id` is specified, edit that term, otherwise add a new term. */
export type UpdateTermRequest = {
  deckId: Scalars['Long']['input'];
  definitions: Array<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['Long']['input']>;
  term: Scalars['String']['input'];
};

export type User = {
  __typename?: 'User';
  changeEmailRequest?: Maybe<ChangeEmailRequest>;
  connectedAccounts: Array<ConnectedAccountType>;
  crosswords: Array<Crossword>;
  decks: Array<Deck>;
  displayName: Scalars['String']['output'];
  email?: Maybe<Scalars['String']['output']>;
  favouriteDecks: Array<Deck>;
  followed?: Maybe<Scalars['Boolean']['output']>;
  followedUsers: Array<User>;
  hardestTerms: Array<Score>;
  hasPassword?: Maybe<Scalars['Boolean']['output']>;
  id: Scalars['Int']['output'];
  isMe: Scalars['Boolean']['output'];
  languages: Array<UserLanguage>;
  lastReadNewsId?: Maybe<Scalars['Int']['output']>;
  name: Scalars['String']['output'];
  profileImage?: Maybe<Scalars['String']['output']>;
  recentAttempts: Array<Attempt>;
  recentFailures: Array<Score>;
  signUpDate: Scalars['Date']['output'];
  superuser: Scalars['Boolean']['output'];
};


export type UserChangeEmailRequestArgs = {
  active?: InputMaybe<Scalars['Boolean']['input']>;
};


export type UserHardestTermsArgs = {
  count?: InputMaybe<Scalars['Int']['input']>;
};


export type UserRecentAttemptsArgs = {
  count: Scalars['Int']['input'];
};


export type UserRecentFailuresArgs = {
  count?: InputMaybe<Scalars['Int']['input']>;
};

export type UserAccountRequest = {
  created: Scalars['DateTime']['output'];
  expires: Scalars['DateTime']['output'];
  id: Scalars['Int']['output'];
  used?: Maybe<Scalars['DateTime']['output']>;
  valid: Scalars['Boolean']['output'];
};

export type UserAudit = Audit & {
  __typename?: 'UserAudit';
  code: AuditCode;
  created: Scalars['DateTime']['output'];
  id: Scalars['Int']['output'];
  otherUser?: Maybe<User>;
  otherUserId?: Maybe<Scalars['Int']['output']>;
  parameters?: Maybe<Array<Scalars['String']['output']>>;
  type: AuditType;
  user?: Maybe<User>;
  userId?: Maybe<Scalars['Int']['output']>;
};

export enum UserAuditCode {
  UserFollowed = 'UserFollowed',
  UserUnfollowed = 'UserUnfollowed'
}

export type UserLanguage = {
  __typename?: 'UserLanguage';
  crosswordsCompleted: Scalars['Int']['output'];
  crosswordsCreated: Scalars['Int']['output'];
  decks: Scalars['Int']['output'];
  language: Language;
  languageId: Scalars['String']['output'];
  lastActivity: Scalars['DateTime']['output'];
  started: Scalars['DateTime']['output'];
  user: User;
  userId: Scalars['Int']['output'];
};



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping of union types */
export type ResolversUnionTypes<RefType extends Record<string, unknown>> = {
  DeckSearchResult: ( DeckModel ) | ( TermModel );
};

/** Mapping of interface types */
export type ResolversInterfaceTypes<RefType extends Record<string, unknown>> = {
  Audit: ( CrosswordAuditModel ) | ( DeckAuditModel ) | ( Omit<SecurityAudit, 'user'> & { user?: Maybe<RefType['User']> } ) | ( UserAuditModel );
  UserAccountRequest: ( ChangeEmailRequestModel );
};

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  Activities: ResolverTypeWrapper<Omit<Activities, 'activities'> & { activities: Array<ResolversTypes['Activity']> }>;
  Activity: ResolverTypeWrapper<ActivityModel>;
  ActivityType: ActivityType;
  Attempt: ResolverTypeWrapper<AttemptModel>;
  Audit: ResolverTypeWrapper<ResolversInterfaceTypes<ResolversTypes>['Audit']>;
  AuditCode: AuditCode;
  AuditType: AuditType;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']['output']>;
  ChangeEmailRequest: ResolverTypeWrapper<ChangeEmailRequestModel>;
  Clue: ResolverTypeWrapper<ClueModel>;
  ConnectedAccountType: ConnectedAccountType;
  CreateDeckRequest: CreateDeckRequest;
  Crossword: ResolverTypeWrapper<CrosswordModel>;
  CrosswordAudit: ResolverTypeWrapper<CrosswordAuditModel>;
  CrosswordAuditCode: CrosswordAuditCode;
  Date: ResolverTypeWrapper<Scalars['Date']['output']>;
  DateTime: ResolverTypeWrapper<Scalars['DateTime']['output']>;
  Deck: ResolverTypeWrapper<DeckModel>;
  DeckAudit: ResolverTypeWrapper<DeckAuditModel>;
  DeckAuditCode: DeckAuditCode;
  DeckSearchResult: ResolverTypeWrapper<ResolversUnionTypes<ResolversTypes>['DeckSearchResult']>;
  Direction: ResolverTypeWrapper<string>;
  Float: ResolverTypeWrapper<Scalars['Float']['output']>;
  Guess: ResolverTypeWrapper<GuessModel>;
  Int: ResolverTypeWrapper<Scalars['Int']['output']>;
  JSON: ResolverTypeWrapper<Scalars['JSON']['output']>;
  Language: ResolverTypeWrapper<string>;
  Long: ResolverTypeWrapper<Scalars['Long']['output']>;
  Mutation: ResolverTypeWrapper<{}>;
  Null: ResolverTypeWrapper<Scalars['Null']['output']>;
  PageInfo: ResolverTypeWrapper<PageInfo>;
  Query: ResolverTypeWrapper<{}>;
  Score: ResolverTypeWrapper<UserTermModel>;
  SecurityAudit: ResolverTypeWrapper<Omit<SecurityAudit, 'user'> & { user?: Maybe<ResolversTypes['User']> }>;
  SecurityAuditCode: SecurityAuditCode;
  Session: ResolverTypeWrapper<Session>;
  String: ResolverTypeWrapper<Scalars['String']['output']>;
  Subscription: ResolverTypeWrapper<{}>;
  Term: ResolverTypeWrapper<TermModel>;
  Time: ResolverTypeWrapper<Scalars['Time']['output']>;
  UpdateAttemptRequest: UpdateAttemptRequest;
  UpdateDeckRequest: UpdateDeckRequest;
  UpdateDeckTerm: UpdateDeckTerm;
  UpdateGuessRequest: UpdateGuessRequest;
  UpdateTermRequest: UpdateTermRequest;
  User: ResolverTypeWrapper<UserSourceModel>;
  UserAccountRequest: ResolverTypeWrapper<ResolversInterfaceTypes<ResolversTypes>['UserAccountRequest']>;
  UserAudit: ResolverTypeWrapper<UserAuditModel>;
  UserAuditCode: UserAuditCode;
  UserLanguage: ResolverTypeWrapper<UserLanguageModel>;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Activities: Omit<Activities, 'activities'> & { activities: Array<ResolversParentTypes['Activity']> };
  Activity: ActivityModel;
  Attempt: AttemptModel;
  Audit: ResolversInterfaceTypes<ResolversParentTypes>['Audit'];
  Boolean: Scalars['Boolean']['output'];
  ChangeEmailRequest: ChangeEmailRequestModel;
  Clue: ClueModel;
  CreateDeckRequest: CreateDeckRequest;
  Crossword: CrosswordModel;
  CrosswordAudit: CrosswordAuditModel;
  Date: Scalars['Date']['output'];
  DateTime: Scalars['DateTime']['output'];
  Deck: DeckModel;
  DeckAudit: DeckAuditModel;
  DeckSearchResult: ResolversUnionTypes<ResolversParentTypes>['DeckSearchResult'];
  Float: Scalars['Float']['output'];
  Guess: GuessModel;
  Int: Scalars['Int']['output'];
  JSON: Scalars['JSON']['output'];
  Language: string;
  Long: Scalars['Long']['output'];
  Mutation: {};
  Null: Scalars['Null']['output'];
  PageInfo: PageInfo;
  Query: {};
  Score: UserTermModel;
  SecurityAudit: Omit<SecurityAudit, 'user'> & { user?: Maybe<ResolversParentTypes['User']> };
  Session: Session;
  String: Scalars['String']['output'];
  Subscription: {};
  Term: TermModel;
  Time: Scalars['Time']['output'];
  UpdateAttemptRequest: UpdateAttemptRequest;
  UpdateDeckRequest: UpdateDeckRequest;
  UpdateDeckTerm: UpdateDeckTerm;
  UpdateGuessRequest: UpdateGuessRequest;
  UpdateTermRequest: UpdateTermRequest;
  User: UserSourceModel;
  UserAccountRequest: ResolversInterfaceTypes<ResolversParentTypes>['UserAccountRequest'];
  UserAudit: UserAuditModel;
  UserLanguage: UserLanguageModel;
};

export type AdminOnlyDirectiveArgs = { };

export type AdminOnlyDirectiveResolver<Result, Parent, ContextType = Context, Args = AdminOnlyDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type AdminVisibleDirectiveArgs = { };

export type AdminVisibleDirectiveResolver<Result, Parent, ContextType = Context, Args = AdminVisibleDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type AuthDirectiveArgs = { };

export type AuthDirectiveResolver<Result, Parent, ContextType = Context, Args = AuthDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type AuthNullDirectiveArgs = { };

export type AuthNullDirectiveResolver<Result, Parent, ContextType = Context, Args = AuthNullDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type SelfOnlyDirectiveArgs = {
  admin?: Maybe<Scalars['Boolean']['input']>;
};

export type SelfOnlyDirectiveResolver<Result, Parent, ContextType = Context, Args = SelfOnlyDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type SelfVisibleDirectiveArgs = {
  admin?: Maybe<Scalars['Boolean']['input']>;
};

export type SelfVisibleDirectiveResolver<Result, Parent, ContextType = Context, Args = SelfVisibleDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type ActivitiesResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Activities'] = ResolversParentTypes['Activities']> = {
  activities?: Resolver<Array<ResolversTypes['Activity']>, ParentType, ContextType>;
  page?: Resolver<ResolversTypes['PageInfo'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ActivityResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Activity'] = ResolversParentTypes['Activity']> = {
  crosswordIds?: Resolver<Maybe<Array<ResolversTypes['Int']>>, ParentType, ContextType>;
  cursor?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  date?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  deckIds?: Resolver<Maybe<Array<ResolversTypes['Int']>>, ParentType, ContextType>;
  decks?: Resolver<Maybe<Array<ResolversTypes['Deck']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  parameters?: Resolver<Maybe<Array<ResolversTypes['String']>>, ParentType, ContextType>;
  type?: Resolver<ResolversTypes['ActivityType'], ParentType, ContextType>;
  user?: Resolver<ResolversTypes['User'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ActivityTypeResolvers = EnumResolverSignature<{ CrosswordsCompleted?: any, DeckCreated?: any, DeckTermsAdded?: any }, ResolversTypes['ActivityType']>;

export type AttemptResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Attempt'] = ResolversParentTypes['Attempt']> = {
  completed?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  crossword?: Resolver<ResolversTypes['Crossword'], ParentType, ContextType>;
  guesses?: Resolver<Array<ResolversTypes['Guess']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Long'], ParentType, ContextType>;
  lastPlayed?: Resolver<ResolversTypes['Date'], ParentType, ContextType>;
  percentCompleted?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  score?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  started?: Resolver<ResolversTypes['Date'], ParentType, ContextType>;
  user?: Resolver<ResolversTypes['User'], ParentType, ContextType>;
  userName?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type AuditResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Audit'] = ResolversParentTypes['Audit']> = {
  __resolveType?: TypeResolveFn<'CrosswordAudit' | 'DeckAudit' | 'SecurityAudit' | 'UserAudit', ParentType, ContextType>;
  code?: Resolver<ResolversTypes['AuditCode'], ParentType, ContextType>;
  created?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  parameters?: Resolver<Maybe<Array<ResolversTypes['String']>>, ParentType, ContextType>;
  type?: Resolver<ResolversTypes['AuditType'], ParentType, ContextType>;
  user?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType>;
  userId?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
};

export type AuditCodeResolvers = EnumResolverSignature<{ AccountActivated?: any, AccountRegistered?: any, CrosswordCompleted?: any, CrosswordCreated?: any, CrosswordError?: any, CrosswordGenerated?: any, DeckCreated?: any, DeckDeleted?: any, DeckTermEdited?: any, DeckUpdated?: any, EmailChangeRequested?: any, EmailChangeSuccess?: any, FacebookAccountLinked?: any, FacebookAccountRemoved?: any, ForgotPassword?: any, GoogleAccountLinked?: any, GoogleAccountRemoved?: any, PasswordReset?: any, PasswordResetSuccess?: any, PasswordUpdated?: any, UserFollowed?: any, UserUnfollowed?: any }, ResolversTypes['AuditCode']>;

export type AuditTypeResolvers = EnumResolverSignature<{ CrosswordAudit?: any, DeckAudit?: any, SecurityAudit?: any, UserAudit?: any }, ResolversTypes['AuditType']>;

export type ChangeEmailRequestResolvers<ContextType = Context, ParentType extends ResolversParentTypes['ChangeEmailRequest'] = ResolversParentTypes['ChangeEmailRequest']> = {
  cancelled?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  created?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  expires?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  newEmail?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  used?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  userId?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  valid?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ClueResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Clue'] = ResolversParentTypes['Clue']> = {
  answer?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  clue?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  crossword?: Resolver<ResolversTypes['Crossword'], ParentType, ContextType>;
  deck?: Resolver<ResolversTypes['Deck'], ParentType, ContextType>;
  deckId?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  direction?: Resolver<ResolversTypes['Direction'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  length?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  lengthHint?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  number?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  originalAnswer?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  term?: Resolver<Maybe<ResolversTypes['Term']>, ParentType, ContextType>;
  termId?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  x?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  y?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CrosswordResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Crossword'] = ResolversParentTypes['Crossword']> = {
  attempt?: Resolver<Maybe<ResolversTypes['Attempt']>, ParentType, ContextType>;
  available?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  clues?: Resolver<Maybe<Array<ResolversTypes['Clue']>>, ParentType, ContextType>;
  created?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  decks?: Resolver<Array<ResolversTypes['Deck']>, ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  height?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  imageUrl?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  language?: Resolver<Maybe<ResolversTypes['Language']>, ParentType, ContextType>;
  stars?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  updated?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  user?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType>;
  width?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CrosswordAuditResolvers<ContextType = Context, ParentType extends ResolversParentTypes['CrosswordAudit'] = ResolversParentTypes['CrosswordAudit']> = {
  code?: Resolver<ResolversTypes['AuditCode'], ParentType, ContextType>;
  created?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  crossword?: Resolver<Maybe<ResolversTypes['Crossword']>, ParentType, ContextType>;
  crosswordId?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  parameters?: Resolver<Maybe<Array<ResolversTypes['String']>>, ParentType, ContextType>;
  type?: Resolver<ResolversTypes['AuditType'], ParentType, ContextType>;
  user?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType>;
  userId?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface DateScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Date'], any> {
  name: 'Date';
}

export interface DateTimeScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['DateTime'], any> {
  name: 'DateTime';
}

export type DeckResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Deck'] = ResolversParentTypes['Deck']> = {
  createdBy?: Resolver<ResolversTypes['User'], ParentType, ContextType>;
  crosswordCount?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  crosswords?: Resolver<Array<ResolversTypes['Crossword']>, ParentType, ContextType>;
  definitionLang?: Resolver<ResolversTypes['Language'], ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Long'], ParentType, ContextType>;
  stars?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  termCount?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  termLang?: Resolver<ResolversTypes['Language'], ParentType, ContextType>;
  terms?: Resolver<Array<ResolversTypes['Term']>, ParentType, ContextType>;
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  url?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type DeckAuditResolvers<ContextType = Context, ParentType extends ResolversParentTypes['DeckAudit'] = ResolversParentTypes['DeckAudit']> = {
  code?: Resolver<ResolversTypes['AuditCode'], ParentType, ContextType>;
  created?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  deck?: Resolver<Maybe<ResolversTypes['Deck']>, ParentType, ContextType>;
  deckId?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  parameters?: Resolver<Maybe<Array<ResolversTypes['String']>>, ParentType, ContextType>;
  type?: Resolver<ResolversTypes['AuditType'], ParentType, ContextType>;
  user?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType>;
  userId?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type DeckSearchResultResolvers<ContextType = Context, ParentType extends ResolversParentTypes['DeckSearchResult'] = ResolversParentTypes['DeckSearchResult']> = {
  __resolveType?: TypeResolveFn<'Deck' | 'Term', ParentType, ContextType>;
};

export type DirectionResolvers = EnumResolverSignature<{ ACROSS?: any, DOWN?: any }, ResolversTypes['Direction']>;

export type GuessResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Guess'] = ResolversParentTypes['Guess']> = {
  attemptId?: Resolver<ResolversTypes['Long'], ParentType, ContextType>;
  guess?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Long'], ParentType, ContextType>;
  x?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  y?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface JsonScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['JSON'], any> {
  name: 'JSON';
}

export type LanguageResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Language'] = ResolversParentTypes['Language']> = {
  id?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  nativeName?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface LongScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Long'], any> {
  name: 'Long';
}

export type MutationResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = {
  activateAccount?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationActivateAccountArgs, 'activationCode' | 'displayName' | 'password'>>;
  addCrossword?: Resolver<ResolversTypes['Crossword'], ParentType, ContextType, Partial<MutationAddCrosswordArgs>>;
  cancelChangeEmailRequest?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationCancelChangeEmailRequestArgs, 'id'>>;
  changeEmail?: Resolver<ResolversTypes['ChangeEmailRequest'], ParentType, ContextType, RequireFields<MutationChangeEmailArgs, 'email'>>;
  changePassword?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationChangePasswordArgs, 'currentPassword' | 'newPassword'>>;
  completeAttempt?: Resolver<ResolversTypes['Crossword'], ParentType, ContextType, RequireFields<MutationCompleteAttemptArgs, 'attempt'>>;
  confirmChangeEmail?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationConfirmChangeEmailArgs, 'code'>>;
  confirmResetPassword?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationConfirmResetPasswordArgs, 'code' | 'password'>>;
  createDeck?: Resolver<ResolversTypes['Deck'], ParentType, ContextType, RequireFields<MutationCreateDeckArgs, 'request'>>;
  disconnectAccount?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationDisconnectAccountArgs, 'type'>>;
  followUser?: Resolver<Maybe<ResolversTypes['Null']>, ParentType, ContextType, RequireFields<MutationFollowUserArgs, 'follow' | 'userId'>>;
  login?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationLoginArgs, 'password' | 'username'>>;
  logout?: Resolver<Maybe<ResolversTypes['Null']>, ParentType, ContextType>;
  markNewsRead?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationMarkNewsReadArgs, 'id'>>;
  register?: Resolver<Maybe<ResolversTypes['Null']>, ParentType, ContextType, RequireFields<MutationRegisterArgs, 'email' | 'username'>>;
  reindex?: Resolver<Maybe<ResolversTypes['Null']>, ParentType, ContextType>;
  resetPassword?: Resolver<Maybe<ResolversTypes['Null']>, ParentType, ContextType, RequireFields<MutationResetPasswordArgs, 'email'>>;
  saveAttempt?: Resolver<ResolversTypes['Attempt'], ParentType, ContextType, RequireFields<MutationSaveAttemptArgs, 'attempt' | 'complete'>>;
  toggleSetFavourite?: Resolver<Maybe<ResolversTypes['Null']>, ParentType, ContextType, RequireFields<MutationToggleSetFavouriteArgs, 'favourite' | 'id'>>;
  updateDeck?: Resolver<ResolversTypes['Deck'], ParentType, ContextType, RequireFields<MutationUpdateDeckArgs, 'request'>>;
  updateTerm?: Resolver<ResolversTypes['Term'], ParentType, ContextType, RequireFields<MutationUpdateTermArgs, 'request'>>;
};

export interface NullScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Null'], any> {
  name: 'Null';
}

export type PageInfoResolvers<ContextType = Context, ParentType extends ResolversParentTypes['PageInfo'] = ResolversParentTypes['PageInfo']> = {
  endCursor?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  startCursor?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type QueryResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
  crossword?: Resolver<Maybe<ResolversTypes['Crossword']>, ParentType, ContextType, RequireFields<QueryCrosswordArgs, 'id'>>;
  debugSearch?: Resolver<ResolversTypes['JSON'], ParentType, ContextType, Partial<QueryDebugSearchArgs>>;
  deck?: Resolver<ResolversTypes['Deck'], ParentType, ContextType, RequireFields<QueryDeckArgs, 'id'>>;
  language?: Resolver<Maybe<ResolversTypes['Language']>, ParentType, ContextType, RequireFields<QueryLanguageArgs, 'id'>>;
  languages?: Resolver<Array<ResolversTypes['Language']>, ParentType, ContextType>;
  me?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType>;
  recentActivity?: Resolver<ResolversTypes['Activities'], ParentType, ContextType, Partial<QueryRecentActivityArgs>>;
  searchDecksAndTerms?: Resolver<Array<ResolversTypes['DeckSearchResult']>, ParentType, ContextType, RequireFields<QuerySearchDecksAndTermsArgs, 'search'>>;
  session?: Resolver<Maybe<ResolversTypes['Session']>, ParentType, ContextType>;
  suggestTerms?: Resolver<Array<ResolversTypes['Term']>, ParentType, ContextType, RequireFields<QuerySuggestTermsArgs, 'search'>>;
  term?: Resolver<ResolversTypes['Term'], ParentType, ContextType, RequireFields<QueryTermArgs, 'id'>>;
  topDecks?: Resolver<Array<ResolversTypes['Deck']>, ParentType, ContextType, RequireFields<QueryTopDecksArgs, 'count' | 'termLang'>>;
  user?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType, Partial<QueryUserArgs>>;
  users?: Resolver<Array<ResolversTypes['User']>, ParentType, ContextType, RequireFields<QueryUsersArgs, 'search'>>;
};

export type ScoreResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Score'] = ResolversParentTypes['Score']> = {
  attempts?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  lastPlayed?: Resolver<ResolversTypes['Date'], ParentType, ContextType>;
  lastResultSuccess?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  score?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  successes?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  term?: Resolver<ResolversTypes['Term'], ParentType, ContextType>;
  termId?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  userId?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SecurityAuditResolvers<ContextType = Context, ParentType extends ResolversParentTypes['SecurityAudit'] = ResolversParentTypes['SecurityAudit']> = {
  code?: Resolver<ResolversTypes['AuditCode'], ParentType, ContextType>;
  created?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  parameters?: Resolver<Maybe<Array<ResolversTypes['String']>>, ParentType, ContextType>;
  type?: Resolver<ResolversTypes['AuditType'], ParentType, ContextType>;
  user?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType>;
  userId?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SessionResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Session'] = ResolversParentTypes['Session']> = {
  expires?: Resolver<ResolversTypes['Date'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SubscriptionResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Subscription'] = ResolversParentTypes['Subscription']> = {
  auditAdded?: SubscriptionResolver<ResolversTypes['Audit'], "auditAdded", ParentType, ContextType, Partial<SubscriptionAuditAddedArgs>>;
};

export type TermResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Term'] = ResolversParentTypes['Term']> = {
  deck?: Resolver<Maybe<ResolversTypes['Deck']>, ParentType, ContextType>;
  definitions?: Resolver<Array<ResolversTypes['String']>, ParentType, ContextType>;
  example?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Long'], ParentType, ContextType>;
  order?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  pronunciation?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  score?: Resolver<Maybe<ResolversTypes['Score']>, ParentType, ContextType>;
  term?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface TimeScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Time'], any> {
  name: 'Time';
}

export type UserResolvers<ContextType = Context, ParentType extends ResolversParentTypes['User'] = ResolversParentTypes['User']> = {
  changeEmailRequest?: Resolver<Maybe<ResolversTypes['ChangeEmailRequest']>, ParentType, ContextType, Partial<UserChangeEmailRequestArgs>>;
  connectedAccounts?: Resolver<Array<ResolversTypes['ConnectedAccountType']>, ParentType, ContextType>;
  crosswords?: Resolver<Array<ResolversTypes['Crossword']>, ParentType, ContextType>;
  decks?: Resolver<Array<ResolversTypes['Deck']>, ParentType, ContextType>;
  displayName?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  email?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  favouriteDecks?: Resolver<Array<ResolversTypes['Deck']>, ParentType, ContextType>;
  followed?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  followedUsers?: Resolver<Array<ResolversTypes['User']>, ParentType, ContextType>;
  hardestTerms?: Resolver<Array<ResolversTypes['Score']>, ParentType, ContextType, Partial<UserHardestTermsArgs>>;
  hasPassword?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  isMe?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  languages?: Resolver<Array<ResolversTypes['UserLanguage']>, ParentType, ContextType>;
  lastReadNewsId?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  profileImage?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  recentAttempts?: Resolver<Array<ResolversTypes['Attempt']>, ParentType, ContextType, RequireFields<UserRecentAttemptsArgs, 'count'>>;
  recentFailures?: Resolver<Array<ResolversTypes['Score']>, ParentType, ContextType, Partial<UserRecentFailuresArgs>>;
  signUpDate?: Resolver<ResolversTypes['Date'], ParentType, ContextType>;
  superuser?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type UserAccountRequestResolvers<ContextType = Context, ParentType extends ResolversParentTypes['UserAccountRequest'] = ResolversParentTypes['UserAccountRequest']> = {
  __resolveType?: TypeResolveFn<'ChangeEmailRequest', ParentType, ContextType>;
  created?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  expires?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  used?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  valid?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
};

export type UserAuditResolvers<ContextType = Context, ParentType extends ResolversParentTypes['UserAudit'] = ResolversParentTypes['UserAudit']> = {
  code?: Resolver<ResolversTypes['AuditCode'], ParentType, ContextType>;
  created?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  otherUser?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType>;
  otherUserId?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  parameters?: Resolver<Maybe<Array<ResolversTypes['String']>>, ParentType, ContextType>;
  type?: Resolver<ResolversTypes['AuditType'], ParentType, ContextType>;
  user?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType>;
  userId?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type UserLanguageResolvers<ContextType = Context, ParentType extends ResolversParentTypes['UserLanguage'] = ResolversParentTypes['UserLanguage']> = {
  crosswordsCompleted?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  crosswordsCreated?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  decks?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  language?: Resolver<ResolversTypes['Language'], ParentType, ContextType>;
  languageId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  lastActivity?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  started?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  user?: Resolver<ResolversTypes['User'], ParentType, ContextType>;
  userId?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type Resolvers<ContextType = Context> = {
  Activities?: ActivitiesResolvers<ContextType>;
  Activity?: ActivityResolvers<ContextType>;
  ActivityType?: ActivityTypeResolvers;
  Attempt?: AttemptResolvers<ContextType>;
  Audit?: AuditResolvers<ContextType>;
  AuditCode?: AuditCodeResolvers;
  AuditType?: AuditTypeResolvers;
  ChangeEmailRequest?: ChangeEmailRequestResolvers<ContextType>;
  Clue?: ClueResolvers<ContextType>;
  Crossword?: CrosswordResolvers<ContextType>;
  CrosswordAudit?: CrosswordAuditResolvers<ContextType>;
  Date?: GraphQLScalarType;
  DateTime?: GraphQLScalarType;
  Deck?: DeckResolvers<ContextType>;
  DeckAudit?: DeckAuditResolvers<ContextType>;
  DeckSearchResult?: DeckSearchResultResolvers<ContextType>;
  Direction?: DirectionResolvers;
  Guess?: GuessResolvers<ContextType>;
  JSON?: GraphQLScalarType;
  Language?: LanguageResolvers<ContextType>;
  Long?: GraphQLScalarType;
  Mutation?: MutationResolvers<ContextType>;
  Null?: GraphQLScalarType;
  PageInfo?: PageInfoResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Score?: ScoreResolvers<ContextType>;
  SecurityAudit?: SecurityAuditResolvers<ContextType>;
  Session?: SessionResolvers<ContextType>;
  Subscription?: SubscriptionResolvers<ContextType>;
  Term?: TermResolvers<ContextType>;
  Time?: GraphQLScalarType;
  User?: UserResolvers<ContextType>;
  UserAccountRequest?: UserAccountRequestResolvers<ContextType>;
  UserAudit?: UserAuditResolvers<ContextType>;
  UserLanguage?: UserLanguageResolvers<ContextType>;
};

export type DirectiveResolvers<ContextType = Context> = {
  adminOnly?: AdminOnlyDirectiveResolver<any, any, ContextType>;
  adminVisible?: AdminVisibleDirectiveResolver<any, any, ContextType>;
  auth?: AuthDirectiveResolver<any, any, ContextType>;
  authNull?: AuthNullDirectiveResolver<any, any, ContextType>;
  selfOnly?: SelfOnlyDirectiveResolver<any, any, ContextType>;
  selfVisible?: SelfVisibleDirectiveResolver<any, any, ContextType>;
};
