import { Injectable } from "injection-js";
import { SchemaProvider } from "../../interfaces";
import { ActivityService, DeckService, UserService } from "../services";
import { enumResolver } from "./enumResolver";
import { ActivityType } from "../../types";

@Injectable()
export class ActivitySchema extends SchemaProvider {
    public constructor() {
        super({
            Activity: {
                user: (src, _, ctx) => ctx.resolve(UserService).getById(src.userId),
                decks: (src, _, ctx) => src.deckIds && ctx.resolve(DeckService).getByIds(src.deckIds),
                cursor: src => src.id.toString(),
            },
            Query: {
                recentActivity: (src, { count, after }, ctx) => 
                    ctx.user
                        ? ctx.resolve(ActivityService)
                            .getRecentActivity(count ?? undefined, after ?? undefined, ctx.user)
                        : { activities: [], page: {} }
            },
            ActivityType: enumResolver(ActivityType)
        });
    }
}
 