import { GraphQLScalarType, Kind } from "graphql";
import { SchemaProvider } from "../../interfaces/ISchemaProvider";
import { UserInputError } from "../../errors";

export const GraphQLLong = new GraphQLScalarType({
    name: "Long",
    description: "53-bit integer",
    serialize: (x) => x,
    parseValue: (x) => x,
    parseLiteral: (ast) => {
        if (ast.kind === Kind.INT) return parseInt(ast.value, 10);
        throw new UserInputError("Invalid Long value: expected INT, got " + ast.kind);
    },
});

export class GraphQLLongSchema extends SchemaProvider {
    public constructor() {
        super({
            Long: GraphQLLong,
        });
    }
}
