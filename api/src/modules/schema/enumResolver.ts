export function enumResolver(enumMap: Record<string | number, string | number>) {
    return Object.fromEntries(
        Object.entries(enumMap)
            .filter(([k, v]) => typeof v === "number")
    );
}
