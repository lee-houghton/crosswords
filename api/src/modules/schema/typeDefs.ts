import { DocumentNode, Kind } from "graphql";

export const schemaTypeDefs: DocumentNode = {
    kind: Kind.DOCUMENT,
    "definitions": [
        {
            kind: Kind.DIRECTIVE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "adminOnly"
            },
            "arguments": [],
            "repeatable": false,
            "locations": [
                {
                    kind: Kind.NAME,
                    "value": "OBJECT"
                },
                {
                    kind: Kind.NAME,
                    "value": "FIELD_DEFINITION"
                }
            ]
        },
        {
            kind: Kind.DIRECTIVE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "adminVisible"
            },
            "arguments": [],
            "repeatable": false,
            "locations": [
                {
                    kind: Kind.NAME,
                    "value": "OBJECT"
                },
                {
                    kind: Kind.NAME,
                    "value": "FIELD_DEFINITION"
                }
            ]
        },
        {
            kind: Kind.DIRECTIVE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "auth"
            },
            "arguments": [],
            "repeatable": false,
            "locations": [
                {
                    kind: Kind.NAME,
                    "value": "OBJECT"
                },
                {
                    kind: Kind.NAME,
                    "value": "FIELD_DEFINITION"
                }
            ]
        },
        {
            kind: Kind.DIRECTIVE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "authNull"
            },
            "arguments": [],
            "repeatable": false,
            "locations": [
                {
                    kind: Kind.NAME,
                    "value": "OBJECT"
                },
                {
                    kind: Kind.NAME,
                    "value": "FIELD_DEFINITION"
                }
            ]
        },
        {
            kind: Kind.DIRECTIVE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "selfOnly"
            },
            "arguments": [
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "admin"
                    },
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Boolean"
                        }
                    },
                    "directives": []
                }
            ],
            "repeatable": false,
            "locations": [
                {
                    kind: Kind.NAME,
                    "value": "OBJECT"
                },
                {
                    kind: Kind.NAME,
                    "value": "FIELD_DEFINITION"
                }
            ]
        },
        {
            kind: Kind.DIRECTIVE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "selfVisible"
            },
            "arguments": [
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "admin"
                    },
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Boolean"
                        }
                    },
                    "directives": []
                }
            ],
            "repeatable": false,
            "locations": [
                {
                    kind: Kind.NAME,
                    "value": "OBJECT"
                },
                {
                    kind: Kind.NAME,
                    "value": "FIELD_DEFINITION"
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Activities"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "activities"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Activity"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "page"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "PageInfo"
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Activity"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "type"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "ActivityType"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "date"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "user"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "User"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "deckIds"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.LIST_TYPE,
                        "type": {
                            kind: Kind.NON_NULL_TYPE,
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Int"
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "decks"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.LIST_TYPE,
                        "type": {
                            kind: Kind.NON_NULL_TYPE,
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Deck"
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "crosswordIds"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.LIST_TYPE,
                        "type": {
                            kind: Kind.NON_NULL_TYPE,
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Int"
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "parameters"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.LIST_TYPE,
                        "type": {
                            kind: Kind.NON_NULL_TYPE,
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "cursor"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.ENUM_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "ActivityType"
            },
            "directives": [],
            "values": [
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "DeckCreated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "DeckTermsAdded"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "CrosswordsCompleted"
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Attempt"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Long"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "userName"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "user"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "User"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "started"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Date"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "lastPlayed"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Date"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "percentCompleted"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "guesses"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Guess"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "crossword"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Crossword"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "completed"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Boolean"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "score"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.INTERFACE_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Audit"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "created"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "type"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "AuditType"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "code"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "AuditCode"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "parameters"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.LIST_TYPE,
                        "type": {
                            kind: Kind.NON_NULL_TYPE,
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "userId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "user"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "User"
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.ENUM_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "AuditCode"
            },
            "directives": [],
            "values": [
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "DeckCreated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "DeckUpdated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "DeckDeleted"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "DeckTermEdited"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "CrosswordCreated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "CrosswordGenerated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "CrosswordError"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "CrosswordCompleted"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "PasswordUpdated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "ForgotPassword"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "PasswordReset"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "PasswordResetSuccess"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "EmailChangeRequested"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "EmailChangeSuccess"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "AccountRegistered"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "AccountActivated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "GoogleAccountLinked"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "GoogleAccountRemoved"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "FacebookAccountLinked"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "FacebookAccountRemoved"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "UserFollowed"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "UserUnfollowed"
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.ENUM_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "AuditType"
            },
            "directives": [],
            "values": [
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "DeckAudit"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "CrosswordAudit"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "SecurityAudit"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "UserAudit"
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "ChangeEmailRequest"
            },
            "interfaces": [
                {
                    kind: Kind.NAMED_TYPE,
                    "name": {
                        kind: Kind.NAME,
                        "value": "UserAccountRequest"
                    }
                }
            ],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "userId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "created"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "used"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "DateTime"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "cancelled"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "DateTime"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "expires"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "valid"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Boolean"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "newEmail"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Clue"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "clue"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "length"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "answer"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "originalAnswer"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "x"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "y"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "direction"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Direction"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "number"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "lengthHint"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "crossword"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Crossword"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "deckId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "deck"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Deck"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "termId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "term"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Term"
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.ENUM_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "ConnectedAccountType"
            },
            "directives": [],
            "values": [
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "GOOGLE"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "APPLE"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "FACEBOOK"
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.INPUT_OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "CreateDeckRequest"
            },
            "directives": [],
            "fields": [
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "title"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "description"
                    },
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "termLangId"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "definitionLangId"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "terms"
                    },
                    "type": {
                        kind: Kind.LIST_TYPE,
                        "type": {
                            kind: Kind.NON_NULL_TYPE,
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "UpdateDeckTerm"
                                }
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Crossword"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "title"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "description"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "user"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "User"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "created"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "updated"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "decks"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Deck"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "language"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Language"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "available"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "DateTime"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "width"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "height"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "clues"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.LIST_TYPE,
                        "type": {
                            kind: Kind.NON_NULL_TYPE,
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Clue"
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "attempt"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Attempt"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "imageUrl"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "stars"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "CrosswordAudit"
            },
            "interfaces": [
                {
                    kind: Kind.NAMED_TYPE,
                    "name": {
                        kind: Kind.NAME,
                        "value": "Audit"
                    }
                }
            ],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "created"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "type"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "AuditType"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "code"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "AuditCode"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "parameters"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.LIST_TYPE,
                        "type": {
                            kind: Kind.NON_NULL_TYPE,
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "userId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "user"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "User"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "crosswordId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "crossword"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Crossword"
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.ENUM_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "CrosswordAuditCode"
            },
            "directives": [],
            "values": [
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "CrosswordCreated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "CrosswordGenerated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "CrosswordError"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "CrosswordCompleted"
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.SCALAR_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Date"
            },
            "directives": []
        },
        {
            kind: Kind.SCALAR_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "DateTime"
            },
            "directives": []
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Deck"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Long"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "title"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "description"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "url"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "terms"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Term"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "termCount"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "crosswords"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Crossword"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "createdBy"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "User"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "stars"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "crosswordCount"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "termLang"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Language"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "definitionLang"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Language"
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "DeckAudit"
            },
            "interfaces": [
                {
                    kind: Kind.NAMED_TYPE,
                    "name": {
                        kind: Kind.NAME,
                        "value": "Audit"
                    }
                }
            ],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "created"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "type"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "AuditType"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "code"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "AuditCode"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "parameters"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.LIST_TYPE,
                        "type": {
                            kind: Kind.NON_NULL_TYPE,
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "userId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "user"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "User"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "deckId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "deck"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Deck"
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.ENUM_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "DeckAuditCode"
            },
            "directives": [],
            "values": [
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "DeckCreated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "DeckUpdated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "DeckDeleted"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "DeckTermEdited"
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.UNION_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "DeckSearchResult"
            },
            "directives": [],
            "types": [
                {
                    kind: Kind.NAMED_TYPE,
                    "name": {
                        kind: Kind.NAME,
                        "value": "Deck"
                    }
                },
                {
                    kind: Kind.NAMED_TYPE,
                    "name": {
                        kind: Kind.NAME,
                        "value": "Term"
                    }
                }
            ]
        },
        {
            kind: Kind.ENUM_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Direction"
            },
            "directives": [],
            "values": [
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "ACROSS"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "DOWN"
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Guess"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Long"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "attemptId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Long"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "x"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "y"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "guess"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.SCALAR_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "JSON"
            },
            "directives": []
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Language"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "name"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "nativeName"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.SCALAR_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Long"
            },
            "directives": []
        },
        {
            "name": {
                kind: Kind.NAME,
                "value": "Mutation"
            },
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "activateAccount"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "activationCode"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "password"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "displayName"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "User"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "addCrossword"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "deckIds"
                            },
                            "type": {
                                kind: Kind.LIST_TYPE,
                                "type": {
                                    kind: Kind.NON_NULL_TYPE,
                                    "type": {
                                        kind: Kind.NAMED_TYPE,
                                        "name": {
                                            kind: Kind.NAME,
                                            "value": "Long"
                                        }
                                    }
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "languageId"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "title"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "description"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "width"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Int"
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "height"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Int"
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Crossword"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "cancelChangeEmailRequest"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "id"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Int"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "User"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "auth"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "changeEmail"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "email"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "ChangeEmailRequest"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "auth"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "changePassword"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "currentPassword"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "newPassword"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "User"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "auth"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "description": {
                        kind: Kind.STRING,
                        "value": "Same as checkAnswers, but deprecated because it is better to return the Attempt rather than the Crossword.",
                        "block": false
                    },
                    "name": {
                        kind: Kind.NAME,
                        "value": "completeAttempt"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "attempt"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "UpdateAttemptRequest"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Crossword"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "auth"
                            },
                            "arguments": []
                        },
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "deprecated"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "confirmChangeEmail"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "code"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "User"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "auth"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "confirmResetPassword"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "code"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "password"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "User"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "createDeck"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "request"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "CreateDeckRequest"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Deck"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "auth"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "disconnectAccount"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "type"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "ConnectedAccountType"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "User"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "auth"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "followUser"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "userId"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Int"
                                    }
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "follow"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Boolean"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Null"
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "auth"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "login"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "username"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "password"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "User"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "logout"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Null"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "markNewsRead"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "id"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Int"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "User"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "auth"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "register"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "username"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "email"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Null"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "reindex"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Null"
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "adminOnly"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "resetPassword"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "email"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Null"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "saveAttempt"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "attempt"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "UpdateAttemptRequest"
                                    }
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "complete"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Boolean"
                                }
                            },
                            "defaultValue": {
                                kind: Kind.BOOLEAN,
                                "value": false
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Attempt"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "toggleSetFavourite"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "id"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Long"
                                    }
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "favourite"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Boolean"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Null"
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "auth"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "updateDeck"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "request"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "UpdateDeckRequest"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Deck"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "auth"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "updateTerm"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "request"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "UpdateTermRequest"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Term"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "auth"
                            },
                            "arguments": []
                        }
                    ]
                }
            ],
            "directives": [],
            "interfaces": []
        },
        {
            kind: Kind.SCALAR_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Null"
            },
            "directives": []
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "PageInfo"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "startCursor"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "endCursor"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            "name": {
                kind: Kind.NAME,
                "value": "Query"
            },
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "crossword"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "id"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Long"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Crossword"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "debugSearch"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "index"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "q"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "body"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "JSON"
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "JSON"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "adminOnly"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "deck"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "id"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Long"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Deck"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "language"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "id"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Language"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "languages"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Language"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "me"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "User"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "recentActivity"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "count"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Int"
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "after"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Activities"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "searchDecksAndTerms"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "search"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "DeckSearchResult"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "session"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Session"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "description": {
                        kind: Kind.STRING,
                        "value": "Given part of a term, find other terms with similar text.",
                        "block": false
                    },
                    "name": {
                        kind: Kind.NAME,
                        "value": "suggestTerms"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "search"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "description": {
                                kind: Kind.STRING,
                                "value": "If specified, only returns terms from the specified language.",
                                "block": false
                            },
                            "name": {
                                kind: Kind.NAME,
                                "value": "termLang"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "description": {
                                kind: Kind.STRING,
                                "value": "If specified, excludes terms from the specified set (to avoid suggesting words that are already in it)",
                                "block": false
                            },
                            "name": {
                                kind: Kind.NAME,
                                "value": "excludeDeckId"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Int"
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Term"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "term"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "id"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Long"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Term"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "description": {
                        kind: Kind.STRING,
                        "value": "Returns the most popular decks for a particular language, optionally filtered by definition language.",
                        "block": false
                    },
                    "name": {
                        kind: Kind.NAME,
                        "value": "topDecks"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "termLang"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "definitionLang"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "count"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Int"
                                }
                            },
                            "defaultValue": {
                                kind: Kind.INT,
                                "value": "10"
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Deck"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "user"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "id"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Int"
                                }
                            },
                            "directives": []
                        },
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "name"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "User"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "users"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "search"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "User"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                }
            ],
            "directives": [],
            "interfaces": []
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Score"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "attempts"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "successes"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "score"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Float"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "lastPlayed"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Date"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "lastResultSuccess"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Boolean"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "userId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "termId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "term"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Term"
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "SecurityAudit"
            },
            "interfaces": [
                {
                    kind: Kind.NAMED_TYPE,
                    "name": {
                        kind: Kind.NAME,
                        "value": "Audit"
                    }
                }
            ],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "created"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "type"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "AuditType"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "code"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "AuditCode"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "parameters"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.LIST_TYPE,
                        "type": {
                            kind: Kind.NON_NULL_TYPE,
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "userId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "user"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "User"
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.ENUM_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "SecurityAuditCode"
            },
            "directives": [],
            "values": [
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "PasswordUpdated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "ForgotPassword"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "PasswordReset"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "PasswordResetSuccess"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "EmailChangeRequested"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "EmailChangeSuccess"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "AccountRegistered"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "AccountActivated"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "GoogleAccountLinked"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "GoogleAccountRemoved"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "FacebookAccountLinked"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "FacebookAccountRemoved"
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Session"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "expires"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Date"
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            "name": {
                kind: Kind.NAME,
                "value": "Subscription"
            },
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "auditAdded"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "type"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "AuditType"
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Audit"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "adminOnly"
                            },
                            "arguments": []
                        }
                    ]
                }
            ],
            "directives": [],
            "interfaces": []
        },
        {
            "name": {
                kind: Kind.NAME,
                "value": "Term"
            },
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "deck"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Deck"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "definitions"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "example"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Long"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "order"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "pronunciation"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "score"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Score"
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "authNull"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "term"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                }
            ],
            "directives": [],
            "interfaces": []
        },
        {
            kind: Kind.SCALAR_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "Time"
            },
            "directives": []
        },
        {
            kind: Kind.INPUT_OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "UpdateAttemptRequest"
            },
            "directives": [],
            "fields": [
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "crosswordId"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Long"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "guesses"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "UpdateGuessRequest"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.INPUT_OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "UpdateDeckRequest"
            },
            "directives": [],
            "fields": [
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Long"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "title"
                    },
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "description"
                    },
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "termLangId"
                    },
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "definitionLangId"
                    },
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "terms"
                    },
                    "type": {
                        kind: Kind.LIST_TYPE,
                        "type": {
                            kind: Kind.NON_NULL_TYPE,
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "UpdateDeckTerm"
                                }
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.INPUT_OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "UpdateDeckTerm"
            },
            "directives": [],
            "fields": [
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Long"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "term"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "definitions"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.INPUT_OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "UpdateGuessRequest"
            },
            "directives": [],
            "fields": [
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "x"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "y"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "guess"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.INPUT_OBJECT_TYPE_DEFINITION,
            "description": {
                kind: Kind.STRING,
                "value": "Request to add or update a term. If `id` is specified, edit that term, otherwise add a new term.",
                "block": false
            },
            "name": {
                kind: Kind.NAME,
                "value": "UpdateTermRequest"
            },
            "directives": [],
            "fields": [
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "deckId"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Long"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Long"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "term"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.INPUT_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "definitions"
                    },
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "String"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            "name": {
                kind: Kind.NAME,
                "value": "User"
            },
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "changeEmailRequest"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "active"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Boolean"
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "ChangeEmailRequest"
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "selfVisible"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "connectedAccounts"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "ConnectedAccountType"
                                    }
                                }
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "selfVisible"
                            },
                            "arguments": [
                                {
                                    kind: Kind.ARGUMENT,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "admin"
                                    },
                                    "value": {
                                        kind: Kind.BOOLEAN,
                                        "value": true
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "crosswords"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Crossword"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "decks"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Deck"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "displayName"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "email"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "selfVisible"
                            },
                            "arguments": [
                                {
                                    kind: Kind.ARGUMENT,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "admin"
                                    },
                                    "value": {
                                        kind: Kind.BOOLEAN,
                                        "value": true
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "favouriteDecks"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Deck"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "followed"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Boolean"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "followedUsers"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "User"
                                    }
                                }
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "selfVisible"
                            },
                            "arguments": [
                                {
                                    kind: Kind.ARGUMENT,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "admin"
                                    },
                                    "value": {
                                        kind: Kind.BOOLEAN,
                                        "value": true
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "hardestTerms"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "count"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Int"
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Score"
                                    }
                                }
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "selfOnly"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "hasPassword"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Boolean"
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "selfOnly"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "isMe"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Boolean"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "languages"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "UserLanguage"
                                    }
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "lastReadNewsId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "selfOnly"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "name"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "profileImage"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "String"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "recentAttempts"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "count"
                            },
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Int"
                                    }
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Attempt"
                                    }
                                }
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "selfOnly"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "recentFailures"
                    },
                    "arguments": [
                        {
                            kind: Kind.INPUT_VALUE_DEFINITION,
                            "name": {
                                kind: Kind.NAME,
                                "value": "count"
                            },
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "Int"
                                }
                            },
                            "directives": []
                        }
                    ],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.LIST_TYPE,
                            "type": {
                                kind: Kind.NON_NULL_TYPE,
                                "type": {
                                    kind: Kind.NAMED_TYPE,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "Score"
                                    }
                                }
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "selfOnly"
                            },
                            "arguments": []
                        }
                    ]
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "signUpDate"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Date"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "superuser"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Boolean"
                            }
                        }
                    },
                    "directives": [
                        {
                            kind: Kind.DIRECTIVE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "selfVisible"
                            },
                            "arguments": [
                                {
                                    kind: Kind.ARGUMENT,
                                    "name": {
                                        kind: Kind.NAME,
                                        "value": "admin"
                                    },
                                    "value": {
                                        kind: Kind.BOOLEAN,
                                        "value": true
                                    }
                                }
                            ]
                        }
                    ]
                }
            ],
            "directives": [],
            "interfaces": []
        },
        {
            kind: Kind.INTERFACE_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "UserAccountRequest"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "created"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "used"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "DateTime"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "expires"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "valid"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Boolean"
                            }
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "UserAudit"
            },
            "interfaces": [
                {
                    kind: Kind.NAMED_TYPE,
                    "name": {
                        kind: Kind.NAME,
                        "value": "Audit"
                    }
                }
            ],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "id"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "created"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "type"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "AuditType"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "code"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "AuditCode"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "parameters"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.LIST_TYPE,
                        "type": {
                            kind: Kind.NON_NULL_TYPE,
                            "type": {
                                kind: Kind.NAMED_TYPE,
                                "name": {
                                    kind: Kind.NAME,
                                    "value": "String"
                                }
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "userId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "user"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "User"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "otherUserId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "Int"
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "otherUser"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NAMED_TYPE,
                        "name": {
                            kind: Kind.NAME,
                            "value": "User"
                        }
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.ENUM_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "UserAuditCode"
            },
            "directives": [],
            "values": [
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "UserFollowed"
                    },
                    "directives": []
                },
                {
                    kind: Kind.ENUM_VALUE_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "UserUnfollowed"
                    },
                    "directives": []
                }
            ]
        },
        {
            kind: Kind.OBJECT_TYPE_DEFINITION,
            "name": {
                kind: Kind.NAME,
                "value": "UserLanguage"
            },
            "interfaces": [],
            "directives": [],
            "fields": [
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "userId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "languageId"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "String"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "decks"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "crosswordsCreated"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "crosswordsCompleted"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Int"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "started"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "lastActivity"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "DateTime"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "user"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "User"
                            }
                        }
                    },
                    "directives": []
                },
                {
                    kind: Kind.FIELD_DEFINITION,
                    "name": {
                        kind: Kind.NAME,
                        "value": "language"
                    },
                    "arguments": [],
                    "type": {
                        kind: Kind.NON_NULL_TYPE,
                        "type": {
                            kind: Kind.NAMED_TYPE,
                            "name": {
                                kind: Kind.NAME,
                                "value": "Language"
                            }
                        }
                    },
                    "directives": []
                }
            ]
        }
    ]
}
