import { User } from "../entities/User";

export * from "./ActivitySchema";
export * from "./AdminSchema";
export * from "./AttemptSchema";
export * from "./AuthSchema";
export * from "./ClueSchema";
export * from "./CrosswordSchema";
export * from "./DeckSchema";
export * from "./Long";
export * from "./LanguageSchema";
export * from "./ScoreSchema";
export * from "./SearchSchema";
export * from "./UserSchema";

// Some schemas have a more sophisticated approach to resolving
// where they can resolve from either an ID or from the 
// entity model itself.
export type UserSource = number | User;
