import { Injectable } from "injection-js";
import { SchemaProvider } from "../../interfaces";
import { CrosswordService } from "../services";
import { DeckService } from "../services/decks/DeckService";

@Injectable()
export class ClueSchema extends SchemaProvider {
    public constructor() {
        super({
            Clue: {
                crossword: (clue, args, ctx) => ctx.resolve(CrosswordService).getById(clue.crosswordId).then(c => c!),
                deck: (clue, args, ctx) => ctx.resolve(DeckService).getById(clue.deckId),
                term: (clue, args, ctx) => clue.termId ? ctx.resolve(DeckService).getTermById(clue.termId) : null,
                length: clue => clue.answer.length,
            },
            Direction: {
                ACROSS: "a",
                DOWN: "d",
            },
        });
    }
}
