import { Injectable } from "injection-js";
import { getAccountProvider, IAccountProvider } from "../../decorators/AccountProvider";
import { Context } from "../../interfaces";
import { SchemaProvider } from "../../interfaces/ISchemaProvider";
import { User } from "../entities";
import { CrosswordService, UserService, SearchService } from "../services";
import { AccountService } from "../services/account";
import { ChangeEmailService } from "../services/account/ChangeEmailService";
import { RequestCodeService } from "../services/account/RequestCodeService";

@Injectable()
export class UserSchema extends SchemaProvider {
    public constructor() {
        super({
            User: {
                id: src => src instanceof User ? src.id : src,
                name: userFieldResolver(u => u.name),
                isMe: (src, args, ctx) => ctx.user ? (src instanceof User ? src.id : src) === ctx.user.id : false,
                displayName: userFieldResolver(u => u.displayName),
                signUpDate: userFieldResolver(u => u.created),
                email: userFieldResolver(u => u.email),
                lastReadNewsId: userFieldResolver(u => u.lastReadNewsId),
                changeEmailRequest: (u, {active}, ctx) => 
                    ctx.resolve(ChangeEmailService).getRequestForUser(resolveUserId(u), active ?? undefined),

                // At the moment, the front end returns random cat pictures based on a hash
                // of the user name, which is good enough for now
                profileImage: () => undefined,

                decks: async (src, args, ctx) =>
                    ctx.resolve(UserService).getDecks(resolveUserId(src)),
                favouriteDecks: async (src, args, ctx) =>
                    ctx.resolve(UserService).getFavouriteDecks(resolveUserId(src)),
                crosswords: async (src, args, ctx) =>
                    ctx.resolve(CrosswordService).getByUserId(resolveUserId(src)),
                languages: async (src, {}, ctx) =>
                    ctx.resolve(UserService).getUserLanguages(resolveUserId(src)),
                followed: async (src, args, ctx) => ctx.user
                    ? ctx.resolve(UserService).isFollowed(resolveUserId(src), ctx.user)
                    : null,
                followedUsers: async (src, args, ctx) => 
                    ctx.resolve(UserService).getFollowedUsers(resolveUserId(src)),

                connectedAccounts:
                    userFieldResolver((user, _args, ctx) => 
                        (ctx.resolve(IAccountProvider) as IAccountProvider[])
                            .filter(p => p.isConnected(user))
                            .map(p => p.accountType)
                    ),
                hasPassword: userFieldResolver(user => !!user.passwordHash),
            },
            Query: {
                me: (src, args, ctx) => ctx.user,
                user: (src, { id, name }, ctx) => {
                    if (id)
                        return ctx.resolve(UserService).getById(id);
                    if (name)
                        return ctx.resolve(UserService).getByName(name);
                    return ctx.user;
                },
                users: (src, { search }, ctx) => 
                    ctx.resolve(SearchService).searchUsers(search),
            },
            Mutation: {
                changeEmail: (src, { email }, ctx) => ctx.resolve(ChangeEmailService).create(ctx.user!, email),
                cancelChangeEmailRequest: (src, { id }, ctx) => ctx.resolve(ChangeEmailService).cancel(id, ctx.user!),
                confirmChangeEmail: (src, { code }, ctx) => ctx.resolve(ChangeEmailService).apply(code, ctx.user!),
                changePassword: (src, { currentPassword, newPassword }, ctx) => 
                    ctx.resolve(AccountService).changePassword(ctx.user!.id, currentPassword, newPassword),
                markNewsRead: (src, { id }, ctx) => ctx.resolve(UserService).markNewsRead(ctx.user!, id),
                followUser: (src, { userId, follow }, ctx) => 
                    ctx.resolve(UserService).followUser(userId, follow, ctx.user!),
                disconnectAccount: (_src, { type }, ctx) =>
                    ctx.resolve(getAccountProvider(type)).disconnectAccount(ctx.user!),
            },
            ChangeEmailRequest: {
                valid: (req, _, ctx) => ctx.resolve(RequestCodeService).isValid(req),
            },
            UserLanguage: {
                user: src => src.userId,
                language: src => src.languageId,
            },
            UserAccountRequest: {
                __resolveType(req) {
                    return req.constructor.name as "ChangeEmailRequest";
                }
            }
        });
    }
}

function resolveUserId(src: number | User) {
    if (typeof src === "number")
        return src;

    return src.id;
}

function userFieldResolver<T, TArgs>(field: (user: User, args: TArgs, ctx: Context) => T) {
    return (src: number | User, args: TArgs, ctx: Context) => {
        if (typeof src === "number")
            return ctx.resolve(UserService).getById(src).then(user => field(user, args, ctx));

        return field(src, args, ctx);
    };
}
