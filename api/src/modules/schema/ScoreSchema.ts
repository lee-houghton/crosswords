import { Injectable } from "injection-js";
import { SchemaProvider } from "../../interfaces/ISchemaProvider";
import { DeckService } from "../services";
import { ScoringService } from "../services/crosswords/ScoringService";

@Injectable()
export class ScoreSchema extends SchemaProvider {
    public constructor() {
        super({
            Score: {
                term: (userTerm, _args, ctx) => ctx.resolve(DeckService).getTermById(userTerm.termId),
            },
            Term: {
                score: (term, _args, ctx) => ctx.resolve(ScoringService).getUserScore(ctx.user!.id, term.id),
            },
            User: {
                hardestTerms: (user, { count }, ctx) => 
                    ctx.resolve(ScoringService).getUserLowestScoring(ctx.user!.id, count ?? 5),
                recentFailures: (user, { count }, ctx) => 
                    ctx.resolve(ScoringService).getUserRecentFailures(ctx.user!.id, count ?? 5),
            },
        });
    }
}
