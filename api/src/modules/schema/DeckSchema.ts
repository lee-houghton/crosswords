import { Injectable } from "injection-js";
import { SchemaProvider } from "../../interfaces/ISchemaProvider";
import { CrosswordService, DeckService } from "../services";
import { MutationCreateDeckArgs, MutationUpdateDeckArgs, MutationUpdateTermArgs } from "./types";

@Injectable()
export class DeckSchema extends SchemaProvider {
    public constructor() {
        super({
            Deck: {
                createdBy: deck => deck.userId,
                termLang: deck => deck.termLanguage,
                definitionLang: deck => deck.definitionLanguage,
                terms: (deck, args, ctx) => ctx.resolve(DeckService).getTerms(deck.id),
                crosswords: (deck, args, ctx) => ctx.resolve(CrosswordService).getByDeckId(deck.id),
            },
            Term: {
                deck: (term, _args, ctx) => ctx.resolve(DeckService).getById(term.deckId)
            },
            Query: {
                deck: (src, {id}, ctx) => ctx.resolve(DeckService).getById(id),
                term: (src, {id}, ctx) => ctx.resolve(DeckService).getTermById(id),
                topDecks: (src, { termLang, definitionLang, count }, ctx) =>
                    ctx.resolve(DeckService).topDecks(termLang, definitionLang || undefined, count),
            },
            Mutation: {
                toggleSetFavourite: (src, { id, favourite }, ctx) =>
                    ctx.resolve(DeckService).toggleDeckFavourite(id, favourite, ctx.user!),
                createDeck: (src, { request }: MutationCreateDeckArgs, ctx) =>
                    ctx.resolve(DeckService).createDeck(request, ctx.user!),
                updateDeck: (src, { request }: MutationUpdateDeckArgs, ctx) =>
                    ctx.resolve(DeckService).updateDeck(request, ctx.user!),
                updateTerm: (src, { request }: MutationUpdateTermArgs, ctx) =>
                    ctx.resolve(DeckService).updateTerm(request, ctx.user!),
            }
        });
    }
}
