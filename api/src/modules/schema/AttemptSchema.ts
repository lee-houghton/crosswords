import { SchemaProvider } from "../../interfaces";
import { CrosswordService } from "../services";
import { AttemptService } from "../services/crosswords/AttemptService";

export class AttemptSchema extends SchemaProvider {
    public constructor() {
        super({
            Attempt: {
                guesses: (src, _args, ctx) => 
                    ctx.resolve(AttemptService).getGuesses(src.id, ctx.user),
                crossword: async (src, _args, ctx) =>
                    src.crossword || await ctx.resolve(CrosswordService).getById(src.crosswordId),

                // Can't peek at your score if you haven't clicked Show Answers
                score: src => src.completed ? src.score : undefined,
            },
            Guess: {
                guess: g => g.contents,
            },
            User: {
                recentAttempts: (src, { count }, ctx) => 
                    ctx.resolve(AttemptService).recentAttempts(count, ctx.user),
            },
            Mutation: {
                saveAttempt: (src, { attempt: { crosswordId, guesses }, complete }, ctx) => 
                    ctx.resolve(AttemptService).save(crosswordId, guesses, complete, ctx.user!),
                completeAttempt: (src, { attempt }, ctx) => 
                    ctx.resolve(AttemptService).complete(attempt, ctx.user!),
            }
        });
    }
}
