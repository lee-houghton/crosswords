import { Injectable } from "injection-js";
import { SchemaProvider } from "../../interfaces/ISchemaProvider";
import { CrosswordAudit, DeckAudit, UserAudit } from "../entities";
import { AuditCode } from "../entities/AuditCode";
import { AuditType } from "../entities/AuditType";
import { AuditTypeName } from "../entities/auditTypes";
import { CrosswordService, DeckService, UserService } from "../services";
import { AuditService } from "../services/audit/AuditService";
import { enumResolver } from "./enumResolver";

@Injectable()
export class AdminSchema extends SchemaProvider {
    public constructor() {
        super({
            AuditType: enumResolver(AuditType),
            AuditCode: enumResolver(AuditCode),
            Audit: {
                __resolveType: src => src.constructor.name as AuditTypeName,
                type: src => AuditType[src.constructor.name],
                code: src => src.code,
                user: (src, _, ctx) => src.userId 
                    ? ctx.resolve(UserService).getById(src.userId) 
                    : undefined,
            },
            DeckAudit: {
                deck: (src, _, ctx) => src instanceof DeckAudit && src.deckId 
                    ? ctx.resolve(DeckService).getById(src.deckId) 
                    : undefined
            },
            CrosswordAudit: {
                crossword: (src, _, ctx) => src instanceof CrosswordAudit && src.crosswordId
                    ? ctx.resolve(CrosswordService).getById(src.crosswordId) 
                    : undefined
            },
            UserAudit: {
                otherUser: (src, _, ctx) => src instanceof UserAudit && src.otherUserId
                    ? ctx.resolve(UserService).getById(src.otherUserId) 
                    : undefined
            },
            Query: {
            }, 
            Subscription: {
                auditAdded: {
                    resolve: src => src,
                    subscribe: (_, { type }, ctx) =>
                        ctx.resolve(AuditService).asyncIterator(
                            type ? AuditType[type] as AuditTypeName : undefined,
                        )
                }
            }
        });
    }
}
