import { Injectable } from "injection-js";
import { SchemaProvider } from "../../interfaces/ISchemaProvider";
import { Indexer, SearchService } from "../services";

@Injectable()
export class SearchSchema extends SchemaProvider {
    public constructor() {
        super({
            Query: {
                searchDecksAndTerms: (src, {search}, ctx) => 
                    ctx.resolve(SearchService).searchDecksAndTerms(search),
                suggestTerms: (src, {search, termLang, excludeDeckId}, ctx) =>
                    ctx.resolve(SearchService).suggestTerms(search, termLang || undefined, excludeDeckId || undefined),
                debugSearch: (src, {index, q, body}, ctx) => 
                    ctx.resolve(SearchService).executeDebugSearch(index || undefined, q || undefined, body),
            },
            DeckSearchResult: {
                __resolveType: src => src.constructor.name as "Deck" | "Term",
            },
            Mutation: {
                reindex: (src, _args, ctx) =>
                    ctx.resolve(Indexer).indexAllDecks()
            }
        });
    }
}
