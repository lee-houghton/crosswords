const { printSchema, printIntrospectionSchema, Kind } = require("graphql");
const gql = require("graphql-tag");

module.exports = {
    /** @type {import("@graphql-codegen/plugin-helpers").CodegenPlugin["plugin"]} */
    plugin: (schema, documents, config) => {
        const definitions = [
            ...schema.getDirectives().map((d) => d.astNode),
            ...Object.values(schema.getTypeMap()).map((t) => t.astNode),
            // schema.getQueryType().astNode,
            // schema.getMutationType().astNode,
            // schema.getSubscriptionType().astNode,
        ].filter((node) => node); // Some built-ins (e.g. Int, __Type, @skip) don't have astNodes, which is fine

        const node = stripLoc({ kind: "Document", definitions });
        //const node = { ...gql(printSchema(schema)), loc: undefined };

        let json = JSON.stringify(node, undefined, 4);
        for (const [key, value] of Object.entries(Kind)) {
            json = json.replace(new RegExp(`"kind": "${value}"`, "g"), `kind: Kind.${key}`);
        }

        return (
            `import { DocumentNode, Kind } from "graphql";\n\n` +
            `export const schemaTypeDefs: DocumentNode = ${json}\n`
        );
    },
};

function stripLoc(node) {
    if (Array.isArray(node)) return node.map(stripLoc);

    if (typeof node === "object")
        return Object.fromEntries(
            Object.entries(node).map(([k, v]) => (k === "loc" ? [k, undefined] : [k, stripLoc(v)]))
        );

    return node;
}
