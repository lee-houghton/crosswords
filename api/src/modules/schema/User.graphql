# If admin = true, superusers can still access this field/perform this action
directive @selfOnly(admin: Boolean) on OBJECT | FIELD_DEFINITION # Throws if unauthorised
directive @selfVisible(admin: Boolean) on OBJECT | FIELD_DEFINITION # Returns null if unauthorised

directive @adminOnly on OBJECT | FIELD_DEFINITION # Throws if unauthorised
directive @adminVisible on OBJECT | FIELD_DEFINITION # Returns null if unauthorised

enum ConnectedAccountType {
    GOOGLE
    APPLE
    FACEBOOK
}

type User {
    id: Int!
    name: String!
    isMe: Boolean!

    displayName: String!
    signUpDate: Date!
    profileImage: String

    decks: [Deck!]!
    favouriteDecks: [Deck!]!
    crosswords: [Crossword!]!
    languages: [UserLanguage!]!

    followed: Boolean
    followedUsers: [User!]! @selfVisible(admin: true)

    superuser: Boolean! @selfVisible(admin: true)
    email: String @selfVisible(admin: true)
    changeEmailRequest(active: Boolean): ChangeEmailRequest @selfVisible
    lastReadNewsId: Int @selfOnly

    connectedAccounts: [ConnectedAccountType!]! @selfVisible(admin: true)
    hasPassword: Boolean @selfOnly
}

type UserLanguage {
    userId: Int!
    languageId: String!
    decks: Int!
    #favourites: Int! # Not implemented
    crosswordsCreated: Int!
    crosswordsCompleted: Int!

    started: DateTime!
    lastActivity: DateTime!

    user: User!
    language: Language!
}

interface UserAccountRequest {
    id: Int!
    created: DateTime!
    used: DateTime
    expires: DateTime!
    valid: Boolean!
}

type ChangeEmailRequest implements UserAccountRequest {
    id: Int!
    userId: Int!
    created: DateTime!
    used: DateTime
    cancelled: DateTime
    expires: DateTime!
    valid: Boolean!

    newEmail: String!
}

extend type Query {
    user(id: Int, name: String): User
    users(search: String!): [User!]!
}

extend type Mutation {
    changeEmail(email: String!): ChangeEmailRequest! @auth

    # Returns the User to simplify updating the cache
    cancelChangeEmailRequest(id: Int!): User! @auth
    confirmChangeEmail(code: String!): User! @auth
    changePassword(currentPassword: String!, newPassword: String!): User! @auth

    # Doesn't return anything because it isn't clear which user it would return
    followUser(userId: Int!, follow: Boolean!): Null @auth

    # setPassword(password: String!)
    # closeAccount()
    markNewsRead(id: Int!): User! @auth

    disconnectAccount(type: ConnectedAccountType!): User! @auth
}
