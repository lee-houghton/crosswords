export class QuizletError extends Error {
    public constructor(message: string, inner?: Error) {
        super(message);
    }
}
