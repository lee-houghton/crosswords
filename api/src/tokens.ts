export const taskRunnerToken = Symbol("taskRunnerToken");

export const requestChildContainerToken = Symbol("requestChildContainerToken");

export const endpointProviderToken = Symbol("endpointProviderToken");

export const redisClientToken = Symbol("redisClientToken");
