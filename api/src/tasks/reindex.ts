import { getContainer } from "../container";
import { Indexer } from "../modules/services/search/Indexer";
import { SearchService } from "../modules/services";
import { ElasticsearchClientError } from "@elastic/elasticsearch/lib/errors";

async function run() {
    const container = await getContainer();
    const indexer = container.get(Indexer);
    const search = container.get(SearchService);

    console.log("Declaring mappings");
    await search.createMappings();

    console.log("Indexing users");
    await indexer.indexAllUsers(true);

    console.log("Indexing decks");
    await indexer.indexAllDecks(true);

    console.log("Done!");
    process.exit(0);
}

run().catch(err => {
    const reason = err instanceof ElasticsearchClientError ? err["meta"]?.body?.error?.reason : undefined;
    console.error("Error!", reason || "", err.stack);
        
    process.exit(1);
});
