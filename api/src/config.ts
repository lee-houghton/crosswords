import { InjectionToken, Provider } from "injection-js";
import { required } from "./env";

export const envToken = new InjectionToken<NodeJS.ProcessEnv>("process.env");

export const allConfigTokens: Provider[] = [
    {
        provide: envToken,
        useValue: process.env,
    },
];

type Injected<T> = T extends InjectionToken<infer I> ? I : never;
type InjectedValues<TDeps> = { [D in keyof TDeps]: Injected<TDeps[D]> };

export function createConfigToken<TDeps extends any[]>(desc: string, ...deps: TDeps) {
    return <T>(factory: (...args: InjectedValues<TDeps>) => T) => {
        const token = new InjectionToken<T>(desc);

        allConfigTokens.push({
            provide: token,
            useFactory: factory,
            deps,
        });

        return token;
    };
}

/**
 * Converts http://a.b/c to http://a.b/c/
 */
function sanitizeUrl(url: string) {
    if (!url.endsWith("/")) return url + "/";
    return url;
}

export function envTokenFor(name: string) {
    return createConfigToken(`process.env.${name}`, envToken)((env) => env[name]);
}

export function requiredEnvTokenFor<T>(name: string, transform: (value: string) => T) {
    return createConfigToken(`process.env.${name}`, envToken)((env) => transform(required(env, name)));
}

export const frontEndUrlToken = requiredEnvTokenFor("FRONTEND_URL", sanitizeUrl);
export const apiUrlToken = requiredEnvTokenFor("API_URL", sanitizeUrl);
