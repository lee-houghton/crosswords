import { Injectable, Provider } from "injection-js";
import { ConnectedAccountType } from "../modules/schema/types";
import { User } from "../modules/entities";

type AccountProviderConstructor = { new(...args: any[]): IAccountProvider };

const accountProviders = new Map<ConnectedAccountType, AccountProviderConstructor>();

export function getAccountProvider(type: ConnectedAccountType) {
    const provider = accountProviders.get(type);
    if (!provider)
        throw new Error("Unknown account provider: " + type);

    return provider;
}

export abstract class IAccountProvider {
    public abstract get accountType(): ConnectedAccountType;
    public abstract isConnected(user: User): boolean;
    public abstract disconnectAccount(user: User): Promise<User>;
}

export function getAccountProviders(): Provider[] {
    return [...accountProviders.entries()].map(([type, constructor]) => ({
        provide: IAccountProvider,
        useClass: constructor,
        multi: true
    }))
}

export function AccountProvider(type: ConnectedAccountType) {
    return <T extends AccountProviderConstructor>(value: T) => {
        if (accountProviders.has(type))
            throw new Error("Multiple account providers registered for: " + type);
        accountProviders.set(type, value);
        return Injectable()(value) as T;
    }
}
