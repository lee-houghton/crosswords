import { Injectable } from "injection-js";

type ServiceType = { new(...args: any[]): any };

const allServices = new Map<ServiceType, ServiceScope>();

export function filterServices(services: any[], scope: ServiceScope) {
    return services.filter(s => allServices.get(s) === scope);
}

export enum ServiceScope {
    Singleton,
    Request
}

export function Service(scope = ServiceScope.Singleton) {
    return <T extends ServiceType>(value: T) => {
        allServices.set(value, scope);
        return Injectable()(value) as T;
    }
}
