import { PubSubEngine } from "graphql-subscriptions";
import { RedisPubSub } from "graphql-redis-subscriptions";
import { InjectionToken, Provider, ReflectiveInjector, Type } from "injection-js";
import Redis from "ioredis";
import { createClient } from "redis";
import { Connection, createConnection, getConnectionOptions } from "typeorm";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";
import { allConfigTokens } from "./config";
import { getAccountProviders } from "./decorators/AccountProvider";
import { ServiceScope, filterServices } from "./decorators/Service";
import { env, optionalInt, required } from "./env";
import { ISchemaProvider } from "./interfaces";
import * as Entities from "./modules/entities";
import * as Schemas from "./modules/schema";
import * as Services from "./modules/services";
import { IPasswordHasher, ITaskScheduler } from "./modules/services";
import { mailDomainToken, mailgunApiKeyToken } from "./modules/services/email";
import { CrosswordSvgEndpoint, StatusEndpoint } from "./modules/services/endpoints";
import { endpointProviderToken, redisClientToken, requestChildContainerToken, taskRunnerToken } from "./tokens";

// Audit has a protected constructor, but TypeORM doesn't know about that and is able to
// construct instances anyway.
//
// TypeScript consumers are only able to construct Audit instances using the factory
// methods such as DeckAudit.created(deckId, userId)
const repositoryFactory = <TEntity>(entity: { prototype: TEntity }) =>
    ({
        provide: entity,
        deps: [Connection],
        useFactory: (connection: Connection) => () => connection.getRepository(entity as any),
    } as Provider);

export type Providee<T> = T extends Type<infer R> ? R : T extends InjectionToken<infer R> ? R : any;

// Provides better Intellisense for ReflectiveInjector, and a measure of abstraction.
export class Container {
    public constructor(private readonly injector: ReflectiveInjector) {}

    public get<T>(token: T): Providee<T> {
        return this.injector.get(token);
    }

    public getAll<T>(token: T): ReadonlyArray<Providee<T>> {
        const providers = this.injector.get(token);
        if (!Array.isArray(providers)) throw new Error(`Container.getAll(${token}) did not return an array`);
        return providers;
    }
}

export async function getContainer(): Promise<Container> {
    const connectionOptions = (await getConnectionOptions()) as PostgresConnectionOptions;

    if (process.env.NODE_ENV === "development") {
        const { type, host, database, username, synchronize } = connectionOptions;
        console.log(
            `Waiting for database: ${type}://${username}:********@${host}/${database}?synchronize=${synchronize}`
        );
    }

    const connection = await createConnection({
        ...connectionOptions,
        type: "postgres",
        entities: Object.values(Entities),
    });

    if (process.env.NODE_ENV === "development") console.log(`Connected to database`);

    const redisOptions = {
        db: optionalInt(env, "CACHE_REDIS_DB"),
        host: required(env, "REDIS_HOST"),
        port: optionalInt(env, "REDIS_PORT", 6379),
        password: env.REDIS_PASSWORD,
    };

    const redisClient = await connectToRedis(redisOptions);

    const resolvedGlobalProviders = ReflectiveInjector.resolve([
        // External things
        {
            provide: redisClientToken,
            useValue: redisClient,
        },
        {
            provide: PubSubEngine,
            useValue: new RedisPubSub({
                publisher: new Redis(redisOptions),
                subscriber: new Redis(redisOptions),
            }),
        },
        {
            provide: RedisPubSub,
            useExisting: PubSubEngine,
        },
        {
            provide: Connection,
            useValue: connection,
        },
        allConfigTokens,
        { provide: mailDomainToken, useValue: process.env.MAIL_DOMAIN },
        { provide: mailgunApiKeyToken, useValue: process.env.MAILGUN_API_KEY },

        // Global services
        ...filterServices(Object.values(Services), ServiceScope.Singleton),

        // Abstract services
        // TODO: Automatically creator Providers for these based on prototype chains
        { provide: ITaskScheduler, useClass: Services.LocalTaskScheduler },
        { provide: IPasswordHasher, useClass: Services.Argon2PasswordHasher },

        // Endpoint providers
        // Since these are registered with express at startup, they need to be singletons
        {
            provide: endpointProviderToken,
            multi: true,
            useClass: CrosswordSvgEndpoint,
        },
        {
            provide: endpointProviderToken,
            multi: true,
            useClass: StatusEndpoint,
        },
        {
            provide: endpointProviderToken,
            multi: true,
            useExisting: Services.AuthService,
        },
        {
            provide: endpointProviderToken,
            multi: true,
            useExisting: Services.GoogleAuthService,
        },
        {
            provide: endpointProviderToken,
            multi: true,
            useExisting: Services.FacebookAuthService,
        },
        {
            provide: endpointProviderToken,
            multi: true,
            useExisting: Services.GraphQL,
        },

        // Account providers for external login (Google, Facebook)
        ...getAccountProviders(),

        // GraphQL Schemas
        Object.values(Schemas)
            .filter((f) => typeof f === "function" && f.prototype instanceof ISchemaProvider)
            .map((schemaProvider) => ({
                provide: ISchemaProvider,
                multi: true,
                useClass: schemaProvider,
            })),

        // Entity repositories
        connection.entityMetadatas.map((e) => repositoryFactory(e.target as Function)),

        // Task runner tokens
        //
        // These simply define which task types are handled by which classes.
        // Using `useValue` here because these are not actually _instantiated_ globally.
        // They are instantiated in a child container, where each scheduled task gets its own child container.
        {
            provide: taskRunnerToken,
            multi: true,
            useValue: Services.GenerateCrosswordService,
        },

        // Create a child container for use in
        // - A GraphQL request
        // - A scheduled task
        {
            provide: requestChildContainerToken,
            useValue: () => rootInjector.createChildFromResolved(resolvedRequestProviders),
        },

        // Logging
        // In InversifyJS it was possible to use the injection target to create scoped loggers
        // without the consumer knowing; it is probably possible with injection-js but the
        // method will likely be somewhat different.
        Services.LoggerService,
    ]);

    const resolvedRequestProviders = ReflectiveInjector.resolve([
        // Per-request services (e.g. services which use DataLoader to avoid duplicate calls)
        ...filterServices(Object.values(Services), ServiceScope.Request),
    ]);

    const rootInjector = ReflectiveInjector.fromResolvedProviders(resolvedGlobalProviders);
    return new Container(rootInjector);
}

async function connectToRedis(redisOptions: {
    db: number | undefined;
    host: string;
    port: number;
    password: string | undefined;
}) {
    try {
        const client = createClient(redisOptions).on("error", handleRedisError);
        client.connect().catch(handleRedisError);
        return client;
    } catch (error) {
        handleRedisError(error);
        return undefined;
    }
}

function handleRedisError(error: unknown) {
    console.error("[redis]", error);
}
