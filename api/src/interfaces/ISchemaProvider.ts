import { Injectable } from "injection-js";
import { Resolvers } from "../modules/schema/types";

@Injectable()
export abstract class ISchemaProvider {
    public abstract get resolvers(): Resolvers;
}

export abstract class SchemaProvider extends ISchemaProvider {
    public constructor(
        public readonly resolvers: Resolvers,
    ) {
        super();
    }
}
