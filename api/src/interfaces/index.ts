import { Container } from "../container";
import { User } from "../modules/entities/User";

export * from "./ISchemaProvider";

export interface Context {
    login(user: User): Promise<void>;
    logout(): void;
    user?: User;
    container: Container;
    resolve: Container["get"];
}
