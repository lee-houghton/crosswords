import "reflect-metadata";

import { getContainer } from "./container";
import { Http, ITaskScheduler } from "./modules/services";
import { taskRunnerToken } from "./tokens";

async function run() {
    const container = await getContainer();

    container.get(Http).start();
    container.get(ITaskScheduler).start();

    // Ensure all ITaskRunner instances are instantiated, as they register themselves with
    // the task registry
    container.getAll(taskRunnerToken);
}

function start() {
    run().catch(err => {
        // tslint:disable-next-line:no-console
        console.log(err);
        process.exit(1);
    });
}

start();
