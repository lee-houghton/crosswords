import { config } from "dotenv";

export { env } from "process";

config({ path: ".env.local" });
config();

export function required(environment: NodeJS.ProcessEnv, name: string) {
    const value = environment[name];
    if (!value) throw new Error(`Environment variable ${name} is required`);
    return value;
}

export function optionalInt(environment: NodeJS.ProcessEnv, name: string, defaultValue: number): number;
export function optionalInt(environment: NodeJS.ProcessEnv, name: string, defaultValue?: number): number | undefined;
export function optionalInt(environment: NodeJS.ProcessEnv, name: string, defaultValue?: number) {
    const rawValue = environment[name];

    if (!rawValue) return defaultValue;

    const value = Number(rawValue);
    if (isNaN(value)) throw new Error(`Environment variable ${name} (${rawValue}) should be an number`);

    if (value !== Math.floor(value)) throw new Error(`Environment variable ${name} (${rawValue}) should be an integer`);

    return value;
}

export function optionalBool(environment: NodeJS.ProcessEnv, name: string, defaultValue: boolean) {
    switch (environment[name]) {
        case undefined:
        case "":
            return defaultValue;
        case "true":
            return true;
        case "false":
            return false;
        default:
            throw new Error(`Environment variable ${name} (${environment[name]}) should be "true" or "false"`);
    }
}
