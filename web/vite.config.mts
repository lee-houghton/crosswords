/// <reference types="vitest" />
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import tsconfigPaths from "vite-tsconfig-paths";
import checker from "vite-plugin-checker";

export default defineConfig({
    // depending on your application, base can also be "/"
    base: "",
    build: {
        outDir: "build",
    },
    plugins: [checker({ typescript: { tsconfigPath: "tsconfig.app.json" } }), react(), tsconfigPaths()],
    server: {
        // this ensures that the browser opens upon server start
        open: true,
        // this sets a default port to 3000
        port: 3000,
    },
    test: {
        environment: "jsdom",
    },
});
