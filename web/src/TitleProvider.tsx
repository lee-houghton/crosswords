import React, { createContext, useState, ReactNode, useContext, useEffect } from "react";

const TitleContext = createContext<string | undefined>(undefined);
const SetTitleContext = createContext<(title: string | undefined) => void>(() => {});
TitleContext.displayName = "Title";
SetTitleContext.displayName = "SetTitle";

export function useTitle() {
    return useContext(TitleContext);
}

export function useSetTitle() {
    return useContext(SetTitleContext);
}

export function useAppTitle(title?: string) {
    const setTitle = useContext(SetTitleContext);

    useEffect(() => {
        setTitle(title);
        return () => setTitle(undefined);
    }, [title, setTitle]);
}

export function TitleProvider({ children }: { children: ReactNode }) {
    const [title, setTitle] = useState<string>();

    return <TitleContext.Provider value={title || "Crosswords"}>
        <SetTitleContext.Provider value={setTitle}>
            {children}
        </SetTitleContext.Provider>
    </TitleContext.Provider>
}
