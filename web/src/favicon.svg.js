const { execSync } = require("child_process");
const { writeFileSync } = require("fs");

const grid = [
    " x x  x x ",
    "xx x xxxxx",
    " x x xx x ",
    "xxxxxx  x ",
    " x x xxxxx",
    " x   x   x",
    "     x   x",
    "xxxxxxxx x",
];

function range(from, to) {
    const arr = [];
    for (let i = from; i <= to; i++)
        arr.push(i);
    return arr;
}

function makeFavIcon(number, border, stroke, size, filename = `favicon-n${number}-w${size}-s${stroke}-b${border}`) {
    const resolution = number * size + stroke * (number + 1) + border * 2;
    
    const svg = `<svg xmlns="http://www.w3.org/2000/svg" width="${resolution}" height="${resolution}">
        <style>.box { fill: #fff }</style>
        <rect fill="#fff" rx="${resolution / 8}" width="${resolution}" height="${resolution}"/>
        <rect fill="#000" rx="${resolution / 16}" width="${resolution - border * 2}" height="${resolution - border * 2}" x="${border}" y="${border}"/>
        ${
            range(0, number - 1).flatMap(y => range(0, number - 1).flatMap(x => (grid[y] || "")[x] === "x"
                ? `<rect class="box" width="${size}" height="${size}" x="${x * (size + stroke) + stroke + border}" y="${y * (size + stroke) + stroke + border}"/>`
                : ""
            )).join("")
        }
    </svg>`;
    
    writeFileSync(`../public/${filename}.svg`, svg, { encoding: "utf8" });
    
    execSync(`rsvg-convert ../public/${filename}.svg -o ../public/${filename}.png -w 64 -h 64`);
    execSync(`pngcrush -ow ../public/${filename}.png`);

    for (const res of [32, 128, 152, 167, 180, 192, 196, 512]) {
        execSync(`rsvg-convert ../public/${filename}.svg -o ../public/${filename}-${res}.png -w ${res} -h ${res}`);
        execSync(`pngcrush -ow ../public/${filename}-${res}.png`);
    }
}

// for (let b = 0; b < 8; b++) {
//     for (let n = 4; n < 6; n++) {
//         for (let s = 1; s < 3; s++) { 
//             const w = (64-b*2-s*(n+1))/n; 
//             if (w !== Math.trunc(w))
//                 continue;

//             makeFavIcon(n, b, s, w);
//         }
//     }
// }

makeFavIcon(5, 4, 1, 10, "favicon");
