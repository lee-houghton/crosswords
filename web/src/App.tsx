import { ApolloProvider } from "@apollo/client";
import CssBaseline from "@mui/material/CssBaseline";
import { StyledEngineProvider, Theme, ThemeProvider, adaptV4Theme, createTheme } from "@mui/material/styles";
import { SnackbarProvider } from "notistack";
import { Suspense } from "react";
import { Helmet } from "react-helmet";
import { RouterProvider } from "react-router-dom";
import { TitleProvider } from "./TitleProvider";
import { createClient } from "./client";
import { DrawerIsOpenProvider } from "./components/Drawer";
import { LoadingScreen } from "./components/LoadingScreen";
import { router } from "./components/router";

import { deepOrange as primary, deepPurple as secondary } from "@mui/material/colors";

declare module "@mui/styles/defaultTheme" {
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    interface DefaultTheme extends Theme {}
}

const fontFamily = [`"Lato"`, `"Roboto"`, `"Helvetica"`, `"Arial"`, "sans-serif"].join(",");

const muiTheme = createTheme(
    adaptV4Theme({
        palette: {
            primary: {
                main: primary[800],
                light: primary[400],
                dark: primary[900],
                contrastText: "#fff",
            },
            secondary,
        },
        typography: {
            h1: { fontFamily },
            h2: { fontFamily },
            h3: { fontFamily },
            h4: { fontFamily },
            h5: { fontFamily },
            h6: { fontFamily },
        },
    })
);

const client = createClient();

export function App() {
    return (
        <ApolloProvider client={client}>
            <CssBaseline />
            <Helmet>
                <title>Crosswords</title>
            </Helmet>
            <DrawerIsOpenProvider>
                <TitleProvider>
                    <SnackbarProvider maxSnack={1}>
                        <StyledEngineProvider injectFirst>
                            <ThemeProvider theme={muiTheme}>
                                <Suspense fallback={<LoadingScreen />}>
                                    <RouterProvider router={router} />
                                </Suspense>
                            </ThemeProvider>
                        </StyledEngineProvider>
                    </SnackbarProvider>
                </TitleProvider>
            </DrawerIsOpenProvider>
        </ApolloProvider>
    );
}
