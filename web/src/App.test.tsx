import "@testing-library/jest-dom/vitest";
import { render, screen, waitFor } from "@testing-library/react";
import { expect, test } from "vitest";
import { App } from "./App";

test("renders without crashing", () => {
    render(<App />);
    expect(screen.getByRole("heading", { level: 1 })).toBeInTheDocument();
});

test("sets page title", async () => {
    render(<App />);
    await waitFor(() => expect(document).toHaveProperty("title", "Crosswords"));
});
