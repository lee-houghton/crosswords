import { useEffect } from "react";

let rootElement: HTMLElement | undefined = undefined;

export function getRootElement() {
    if (!rootElement)
        rootElement = document.getElementById("root")!;
    return rootElement;
}

export function useApplyClass(element: HTMLElement, classes?: string) {
    useEffect(() => {
        if (!classes)
            return;

        element.classList.add(classes);
        return () => {
            element.classList.remove(classes);
        };
    }, [classes, element]);
}
