import { ApolloClient } from "@apollo/client";
import { ApolloLink, split } from "@apollo/client/link/core";
import { onError } from "@apollo/client/link/error";
import { HttpLink } from "@apollo/client/link/http";
import { GraphQLWsLink } from "@apollo/client/link/subscriptions";
import { getMainDefinition } from "@apollo/client/utilities";
import { createCache } from "./cache";
import { createClient as createWsClient } from "graphql-ws";
import { apiUrl } from "./env";

const uri = new URL("graphql", new URL(apiUrl, window.location.href));

// http:// becomes ws://, https:// becomes wss://
const wsUri = new URL(uri.href);
wsUri.protocol = uri.protocol.replace("http", "ws");

const httpLink = new HttpLink({
    uri: uri.href,
    credentials: "include",
});

const wsLink = new GraphQLWsLink(
    createWsClient({
        url: wsUri.href,
    })
);

const link = split(
    (op) => {
        const def = getMainDefinition(op.query);
        return (
            def.kind === "OperationDefinition" &&
            def.operation === "subscription"
        );
    },
    wsLink,
    httpLink
);

export const createClient = () =>
    new ApolloClient({
        link: ApolloLink.from([
            onError(({ graphQLErrors, networkError }) => {
                if (graphQLErrors)
                    graphQLErrors.forEach(({ message, locations, path }) =>
                        console.warn(
                            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
                        )
                    );
                if (networkError)
                    console.warn(`[Network error]: ${networkError}`);
            }),
            link,
        ]),
        cache: createCache(),
    });
