function memoize<T>(fn: () => T): () => T {
    let value: T | undefined;
    return () => {
        if (value)
            return value;
        value = fn();
        return value;
    };
}

export const isIos = memoize(function isIos() {
    return [
        "iPad", "iPad Simulator",
        "iPhone", "iPhone Simulator",
        "iPod", "iPod Simulator",
    ].includes(navigator.platform)
    // iPad on iOS 13 detection
    || (navigator.userAgent.includes("Mac") && "ontouchend" in document);
});
