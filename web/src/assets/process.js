const Sharp = require("sharp");
const { promises: fs } = require("fs");
const { basename, join } = require("path");

async function resize(/** @type {Sharp} */ sharp, outputFile, size, format) {
    const resized = sharp.resize({ width: size, withoutEnlargement: true });
    const converted = (format === "avif")
        ? resized.heif({ compression: "av1", quality: 70 })
        : resized[format]();

    try {
        const info = await converted.toFile(outputFile);
        console.log(`Wrote ${outputFile} (${info.width}x${info.height})`);
    } catch(err) {
        if (format === "avif")
            return console.warn(`Failed to generate AVIF output (${err.message})`);
        throw err;
    }
}

async function process(inputDir, outputDir) {
    const rawFiles = await fs.readdir(inputDir);
    for (const fileName of rawFiles) {
        const languageId = basename(fileName, ".jpg");
        const sharp = Sharp(join(inputDir, fileName));
        await Promise.all([
            resize(sharp.clone(), join(outputDir, `${languageId}.512.jpg`), 512, "jpeg"),
            resize(sharp.clone(), join(outputDir, `${languageId}.1024.jpg`), 1024, "jpeg"),
            resize(sharp.clone(), join(outputDir, `${languageId}.2048.jpg`), 2048, "jpeg"),
            resize(sharp.clone(), join(outputDir, `${languageId}.512.webp`), 512, "webp"),
            resize(sharp.clone(), join(outputDir, `${languageId}.1024.webp`), 1024, "webp"),
            resize(sharp.clone(), join(outputDir, `${languageId}.2048.webp`), 2048, "webp"),
            resize(sharp.clone(), join(outputDir, `${languageId}.512.avif`), 512, "avif"),
            resize(sharp.clone(), join(outputDir, `${languageId}.1024.avif`), 1024, "avif"),
            resize(sharp.clone(), join(outputDir, `${languageId}.2048.avif`), 2048, "avif"),
        ]);
    }
}

process(
    join(__dirname, "covers/raw"),
    join(__dirname, "covers")
).catch(err => {
    console.error("Error processing image files: " + err.message);
});
