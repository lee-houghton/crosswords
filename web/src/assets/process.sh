#!/bin/bash 

set -e 

export DOCKER_BUILDKIT=1 
docker build . -t crosswords/image-processing
docker run --rm -v /etc/passwd:/etc/passwd:ro -u $(id -u):$(id -g) -v $(pwd):/usr/src/app crosswords/image-processing node process.js
