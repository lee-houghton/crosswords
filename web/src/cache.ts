import { FieldPolicy, FieldReadFunction, InMemoryCache } from "@apollo/client/cache";
import introspectionQueryResultData from "./graphql/fragmentTypes.json";
import { Activity, PageInfo } from "./graphql/operations";

function simpleCacheRedirect(__typename: string): FieldReadFunction<any, any> {
    return (existingData, { args, toReference }) => 
        existingData || toReference({ __typename, id: args?.id });
}

export function createCache() {
    return new InMemoryCache({
        typePolicies: {
            Query: {
                fields: {
                    term: simpleCacheRedirect("Term"),
                    deck: simpleCacheRedirect("Deck"),
                    user: simpleCacheRedirect("User"),
                    language: simpleCacheRedirect("Language"),
                    crossword: simpleCacheRedirect("Crossword"),
                    recentActivity: cursorPagination<Activity, "activities">("activities", undefined),
                }
            },
            Guess: {
                keyFields: ["attemptId", "x", "y"]
            },
            Score: {
                keyFields: ["userId", "termId"]
            },
        },

        // Without this, Apollo generates warnings about fragment matching for certain interface
        // types such as Audit.
        // https://www.apollographql.com/docs/react/data/fragments/
        possibleTypes: introspectionQueryResultData.possibleTypes
    });
}

type CursorResult<T, ItemsKey extends string> = {
    [key in ItemsKey]: T[];
} & { page: PageInfo; };

function cursorPagination<T extends { cursor: string }, ItemsKey extends string>(
    itemsField: ItemsKey, 
    keyArgs: FieldPolicy<any>["keyArgs"] = false
): FieldPolicy<CursorResult<T, ItemsKey>, CursorResult<T, ItemsKey>, CursorResult<T, ItemsKey>> {
    return {
        keyArgs,
        merge(existing, incoming, { args, readField }) {
            if (!args || !existing)
                return incoming;

            let items = existing[itemsField] as T[];
            let page = existing.page;

            if (args.after) {
                const index = items.findIndex(item => readField("cursor", item) === args.after);
                if (index !== -1) {
                    items = items.slice();
                    items.splice(index + 1, 0, ...incoming[itemsField]);
                }

                const endCursor = incoming[itemsField].length >= (args.count ?? 1)
                    ? incoming.page.endCursor 
                    : null;
                
                page = { ...page, endCursor };
            } else if (args.before) {
                const index = items.findIndex(item => readField("cursor", item) === args.before);
                if (index !== -1) {
                    items.splice(index, 0, ...incoming[itemsField]);
                    items = items.slice();
                }

                const startCursor = incoming[itemsField].length >= (args.count ?? 1)
                    ? incoming.page.startCursor 
                    : null;

                page = { ...page, startCursor };
            } else {
                // No arguments, which means it's a new fetch
                // In theory, you could try to find where the new set of items 
                // overlaps with the existing items and merge into those, but...
                items = incoming[itemsField];
                page = incoming.page;
            }

            return {
                ...existing,
                [itemsField]: items,
                page
            }
        }
    };
}
