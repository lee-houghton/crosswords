import { Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";

// The theme is constant for now, but it doesn't hurt to use the standard methods.
export const useStyles = makeStyles((theme: Theme) => ({
    appBarSpacer: theme.mixins.toolbar
}));

export function AppBarSpacer() {
    const classes = useStyles();

    return <div className={classes.appBarSpacer} />;
}
