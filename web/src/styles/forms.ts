import makeStyles from '@mui/styles/makeStyles';

export const useFormStyles = makeStyles(theme => ({
    wrapper: {
        marginLeft: "auto",
        marginRight: "auto",
        maxWidth: "30em",
    },
    form: {
        display: "flex",
        flexDirection: "column",
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    }
}));
