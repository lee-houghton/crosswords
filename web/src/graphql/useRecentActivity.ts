import { RecentActivityBaseQueryVariables, useRecentActivityBaseQuery } from "./operations";

export function useRecentActivity(variables: RecentActivityBaseQueryVariables) {
    const { data, fetchMore, ...rest } = useRecentActivityBaseQuery({
        variables,
        fetchPolicy: "cache-and-network",
    });

    const cursor = data?.recentActivity.page.endCursor;

    return {
        data,
        ...rest,
        fetchMore: cursor
            ? () =>
                  fetchMore({
                      variables: { ...variables, cursor },
                      // updateQuery: (previousResult, { fetchMoreResult }) => ({
                      //     recentActivity: {
                      //         __typename: "Activities",
                      //         activities: [
                      //             ...previousResult.recentActivity.activities,
                      //             ...(fetchMoreResult?.recentActivity.activities || [])
                      //         ],
                      //         page: fetchMoreResult?.recentActivity.page || { __typename: "PageInfo" }
                      //     }
                      // })
                  })
            : undefined,
    };
}
