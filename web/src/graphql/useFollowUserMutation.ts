import gql from "graphql-tag";
import { useFollowUserBaseMutation } from "./operations";

const fragment = gql`fragment UserIsFollowed on User { followed }`;

export function useFollowUserMutation(userId: number, follow: boolean) {
    return useFollowUserBaseMutation({ 
        variables: { userId, follow },

        optimisticResponse: {
            followUser: {}
        },

        update(proxy) {
            proxy.writeFragment({
                fragment,
                id: `User:${userId}`,
                data: { 
                    __typename: "User", 
                    followed: follow 
                }
            });
        },
    });
}
