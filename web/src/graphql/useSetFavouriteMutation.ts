import { FavouriteDeckIdsBaseDocument, FavouriteDeckIdsBaseQuery, useToggleDeckFavouriteBaseMutation } from "./operations";

export function useToggleDeckFavouriteMutation(
    deckId: number,
    favourite: boolean,
) {
    return useToggleDeckFavouriteBaseMutation({ 
        variables: { 
            id: deckId,
            favourite
        },
        optimisticResponse: {
            toggleSetFavourite: {}
        },

        update(proxy) {
            const data = proxy.readQuery<FavouriteDeckIdsBaseQuery>({ query: FavouriteDeckIdsBaseDocument });

            if (!data || !data.me || !data.me.favouriteDecks)
                return console.warn("useSetFavouriteMutation: update(): no cached data to update");

            const newData: FavouriteDeckIdsBaseQuery = {
                ...data,
                me: {
                    ...data.me,
                    favouriteDecks: favourite
                        ? [...data.me.favouriteDecks, { id: deckId, __typename: "Deck" }]
                        : data.me.favouriteDecks.filter(d => d.id !== deckId)
                }
            };

            proxy.writeQuery({
                query: FavouriteDeckIdsBaseDocument,
                data: newData
            });
        },
    });
}
