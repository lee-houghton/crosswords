fragment BasicUserInfo on User {
    id
    name
    isMe
    displayName
    profileImage
    signUpDate
}

fragment MeUserInfo on User {
    ...BasicUserInfo
    lastReadNewsId
    superuser
}

fragment UserProfile on User {
    ...BasicUserInfo

    followed

    languages {
        language {
            ...LanguageInfo
        }
        decks
        crosswordsCreated
        crosswordsCompleted
        started
        lastActivity
    }

    # Lists section
    decks {
        ...DeckCardInfo
    }
}

fragment ManageAccount on User {
    # Manage Account section
    email
    changeEmailRequest(active: true) {
        id
        newEmail
    }
    connectedAccounts
    hasPassword
}

query User($name: String, $self: Boolean!) {
    user(name: $name) {
        ...UserProfile
        ...ManageAccount @include(if: $self)
    }
}

query MeBase {
    me {
        ...MeUserInfo
    }
}

query CheckUserName($username: String) {
    user(name: $username) {
        id
    }
}

fragment UserLanguage on UserLanguage {
    language {
        ...LanguageInfo
    }
    decks
    lastActivity
}

query UserLanguages {
    me {
        id
        languages {
            ...UserLanguage
        }
    }
}

mutation MarkNewsReadBase($id: Int!) {
    markNewsRead(id: $id) {
        lastReadNewsId
    }
}

mutation FollowUserBase($userId: Int!, $follow: Boolean!) {
    followUser(userId: $userId, follow: $follow)
}

mutation DisconnectAccount($type: ConnectedAccountType!) {
    disconnectAccount(type: $type) {
        id
        connectedAccounts
    }
}
