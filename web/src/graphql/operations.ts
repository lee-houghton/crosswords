import { DocumentNode } from 'graphql';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  Date: { input: any; output: any; }
  DateTime: { input: any; output: any; }
  JSON: { input: any; output: any; }
  Long: { input: any; output: any; }
  Null: { input: any; output: any; }
  Time: { input: any; output: any; }
};

export type Activities = {
  readonly __typename: 'Activities';
  readonly activities: ReadonlyArray<Activity>;
  readonly page: PageInfo;
};

export type Activity = {
  readonly __typename: 'Activity';
  readonly crosswordIds?: Maybe<ReadonlyArray<Scalars['Int']['output']>>;
  readonly cursor: Scalars['String']['output'];
  readonly date: Scalars['DateTime']['output'];
  readonly deckIds?: Maybe<ReadonlyArray<Scalars['Int']['output']>>;
  readonly decks?: Maybe<ReadonlyArray<Deck>>;
  readonly id: Scalars['Int']['output'];
  readonly parameters?: Maybe<ReadonlyArray<Scalars['String']['output']>>;
  readonly type: ActivityType;
  readonly user: User;
};

export enum ActivityType {
  CrosswordsCompleted = 'CrosswordsCompleted',
  DeckCreated = 'DeckCreated',
  DeckTermsAdded = 'DeckTermsAdded'
}

export type Attempt = {
  readonly __typename: 'Attempt';
  readonly completed: Scalars['Boolean']['output'];
  readonly crossword: Crossword;
  readonly guesses: ReadonlyArray<Guess>;
  readonly id: Scalars['Long']['output'];
  readonly lastPlayed: Scalars['Date']['output'];
  readonly percentCompleted: Scalars['Int']['output'];
  readonly score?: Maybe<Scalars['Int']['output']>;
  readonly started: Scalars['Date']['output'];
  readonly user: User;
  readonly userName: Scalars['String']['output'];
};

export type Audit = {
  readonly code: AuditCode;
  readonly created: Scalars['DateTime']['output'];
  readonly id: Scalars['Int']['output'];
  readonly parameters?: Maybe<ReadonlyArray<Scalars['String']['output']>>;
  readonly type: AuditType;
  readonly user?: Maybe<User>;
  readonly userId?: Maybe<Scalars['Int']['output']>;
};

export enum AuditCode {
  AccountActivated = 'AccountActivated',
  AccountRegistered = 'AccountRegistered',
  CrosswordCompleted = 'CrosswordCompleted',
  CrosswordCreated = 'CrosswordCreated',
  CrosswordError = 'CrosswordError',
  CrosswordGenerated = 'CrosswordGenerated',
  DeckCreated = 'DeckCreated',
  DeckDeleted = 'DeckDeleted',
  DeckTermEdited = 'DeckTermEdited',
  DeckUpdated = 'DeckUpdated',
  EmailChangeRequested = 'EmailChangeRequested',
  EmailChangeSuccess = 'EmailChangeSuccess',
  FacebookAccountLinked = 'FacebookAccountLinked',
  FacebookAccountRemoved = 'FacebookAccountRemoved',
  ForgotPassword = 'ForgotPassword',
  GoogleAccountLinked = 'GoogleAccountLinked',
  GoogleAccountRemoved = 'GoogleAccountRemoved',
  PasswordReset = 'PasswordReset',
  PasswordResetSuccess = 'PasswordResetSuccess',
  PasswordUpdated = 'PasswordUpdated',
  UserFollowed = 'UserFollowed',
  UserUnfollowed = 'UserUnfollowed'
}

export enum AuditType {
  CrosswordAudit = 'CrosswordAudit',
  DeckAudit = 'DeckAudit',
  SecurityAudit = 'SecurityAudit',
  UserAudit = 'UserAudit'
}

export type ChangeEmailRequest = UserAccountRequest & {
  readonly __typename: 'ChangeEmailRequest';
  readonly cancelled?: Maybe<Scalars['DateTime']['output']>;
  readonly created: Scalars['DateTime']['output'];
  readonly expires: Scalars['DateTime']['output'];
  readonly id: Scalars['Int']['output'];
  readonly newEmail: Scalars['String']['output'];
  readonly used?: Maybe<Scalars['DateTime']['output']>;
  readonly userId: Scalars['Int']['output'];
  readonly valid: Scalars['Boolean']['output'];
};

export type Clue = {
  readonly __typename: 'Clue';
  readonly answer?: Maybe<Scalars['String']['output']>;
  readonly clue: Scalars['String']['output'];
  readonly crossword: Crossword;
  readonly deck: Deck;
  readonly deckId: Scalars['Int']['output'];
  readonly direction: Direction;
  readonly id: Scalars['Int']['output'];
  readonly length: Scalars['Int']['output'];
  readonly lengthHint: Scalars['String']['output'];
  readonly number: Scalars['Int']['output'];
  readonly originalAnswer?: Maybe<Scalars['String']['output']>;
  readonly term?: Maybe<Term>;
  readonly termId?: Maybe<Scalars['Int']['output']>;
  readonly x: Scalars['Int']['output'];
  readonly y: Scalars['Int']['output'];
};

export enum ConnectedAccountType {
  Apple = 'APPLE',
  Facebook = 'FACEBOOK',
  Google = 'GOOGLE'
}

export type CreateDeckRequest = {
  readonly definitionLangId: Scalars['String']['input'];
  readonly description?: InputMaybe<Scalars['String']['input']>;
  readonly termLangId: Scalars['String']['input'];
  readonly terms?: InputMaybe<ReadonlyArray<UpdateDeckTerm>>;
  readonly title: Scalars['String']['input'];
};

export type Crossword = {
  readonly __typename: 'Crossword';
  readonly attempt?: Maybe<Attempt>;
  readonly available?: Maybe<Scalars['DateTime']['output']>;
  readonly clues?: Maybe<ReadonlyArray<Clue>>;
  readonly created: Scalars['DateTime']['output'];
  readonly decks: ReadonlyArray<Deck>;
  readonly description?: Maybe<Scalars['String']['output']>;
  readonly height?: Maybe<Scalars['Int']['output']>;
  readonly id: Scalars['Int']['output'];
  readonly imageUrl: Scalars['String']['output'];
  readonly language?: Maybe<Language>;
  readonly stars: Scalars['Int']['output'];
  readonly title?: Maybe<Scalars['String']['output']>;
  readonly updated: Scalars['DateTime']['output'];
  readonly user?: Maybe<User>;
  readonly width?: Maybe<Scalars['Int']['output']>;
};

export type CrosswordAudit = Audit & {
  readonly __typename: 'CrosswordAudit';
  readonly code: AuditCode;
  readonly created: Scalars['DateTime']['output'];
  readonly crossword?: Maybe<Crossword>;
  readonly crosswordId?: Maybe<Scalars['Int']['output']>;
  readonly id: Scalars['Int']['output'];
  readonly parameters?: Maybe<ReadonlyArray<Scalars['String']['output']>>;
  readonly type: AuditType;
  readonly user?: Maybe<User>;
  readonly userId?: Maybe<Scalars['Int']['output']>;
};

export enum CrosswordAuditCode {
  CrosswordCompleted = 'CrosswordCompleted',
  CrosswordCreated = 'CrosswordCreated',
  CrosswordError = 'CrosswordError',
  CrosswordGenerated = 'CrosswordGenerated'
}

export type Deck = {
  readonly __typename: 'Deck';
  readonly createdBy: User;
  readonly crosswordCount: Scalars['Int']['output'];
  readonly crosswords: ReadonlyArray<Crossword>;
  readonly definitionLang: Language;
  readonly description?: Maybe<Scalars['String']['output']>;
  readonly id: Scalars['Long']['output'];
  readonly stars: Scalars['Int']['output'];
  readonly termCount: Scalars['Int']['output'];
  readonly termLang: Language;
  readonly terms: ReadonlyArray<Term>;
  readonly title: Scalars['String']['output'];
  readonly url: Scalars['String']['output'];
};

export type DeckAudit = Audit & {
  readonly __typename: 'DeckAudit';
  readonly code: AuditCode;
  readonly created: Scalars['DateTime']['output'];
  readonly deck?: Maybe<Deck>;
  readonly deckId?: Maybe<Scalars['Int']['output']>;
  readonly id: Scalars['Int']['output'];
  readonly parameters?: Maybe<ReadonlyArray<Scalars['String']['output']>>;
  readonly type: AuditType;
  readonly user?: Maybe<User>;
  readonly userId?: Maybe<Scalars['Int']['output']>;
};

export enum DeckAuditCode {
  DeckCreated = 'DeckCreated',
  DeckDeleted = 'DeckDeleted',
  DeckTermEdited = 'DeckTermEdited',
  DeckUpdated = 'DeckUpdated'
}

export type DeckSearchResult = Deck | Term;

export enum Direction {
  Across = 'ACROSS',
  Down = 'DOWN'
}

export type Guess = {
  readonly __typename: 'Guess';
  readonly attemptId: Scalars['Long']['output'];
  readonly guess?: Maybe<Scalars['String']['output']>;
  readonly id: Scalars['Long']['output'];
  readonly x: Scalars['Int']['output'];
  readonly y: Scalars['Int']['output'];
};

export type Language = {
  readonly __typename: 'Language';
  readonly id: Scalars['String']['output'];
  readonly name: Scalars['String']['output'];
  readonly nativeName: Scalars['String']['output'];
};

export type Mutation = {
  readonly __typename: 'Mutation';
  readonly activateAccount: User;
  readonly addCrossword: Crossword;
  readonly cancelChangeEmailRequest: User;
  readonly changeEmail: ChangeEmailRequest;
  readonly changePassword: User;
  /**
   * Same as checkAnswers, but deprecated because it is better to return the Attempt rather than the Crossword.
   * @deprecated Field no longer supported
   */
  readonly completeAttempt: Crossword;
  readonly confirmChangeEmail: User;
  readonly confirmResetPassword: User;
  readonly createDeck: Deck;
  readonly disconnectAccount: User;
  readonly followUser?: Maybe<Scalars['Null']['output']>;
  readonly login: User;
  readonly logout?: Maybe<Scalars['Null']['output']>;
  readonly markNewsRead: User;
  readonly register?: Maybe<Scalars['Null']['output']>;
  readonly reindex?: Maybe<Scalars['Null']['output']>;
  readonly resetPassword?: Maybe<Scalars['Null']['output']>;
  readonly saveAttempt: Attempt;
  readonly toggleSetFavourite?: Maybe<Scalars['Null']['output']>;
  readonly updateDeck: Deck;
  readonly updateTerm: Term;
};


export type MutationActivateAccountArgs = {
  activationCode: Scalars['String']['input'];
  displayName: Scalars['String']['input'];
  password: Scalars['String']['input'];
};


export type MutationAddCrosswordArgs = {
  deckIds?: InputMaybe<ReadonlyArray<Scalars['Long']['input']>>;
  description?: InputMaybe<Scalars['String']['input']>;
  height?: InputMaybe<Scalars['Int']['input']>;
  languageId?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  width?: InputMaybe<Scalars['Int']['input']>;
};


export type MutationCancelChangeEmailRequestArgs = {
  id: Scalars['Int']['input'];
};


export type MutationChangeEmailArgs = {
  email: Scalars['String']['input'];
};


export type MutationChangePasswordArgs = {
  currentPassword: Scalars['String']['input'];
  newPassword: Scalars['String']['input'];
};


export type MutationCompleteAttemptArgs = {
  attempt: UpdateAttemptRequest;
};


export type MutationConfirmChangeEmailArgs = {
  code: Scalars['String']['input'];
};


export type MutationConfirmResetPasswordArgs = {
  code: Scalars['String']['input'];
  password: Scalars['String']['input'];
};


export type MutationCreateDeckArgs = {
  request: CreateDeckRequest;
};


export type MutationDisconnectAccountArgs = {
  type: ConnectedAccountType;
};


export type MutationFollowUserArgs = {
  follow: Scalars['Boolean']['input'];
  userId: Scalars['Int']['input'];
};


export type MutationLoginArgs = {
  password: Scalars['String']['input'];
  username: Scalars['String']['input'];
};


export type MutationMarkNewsReadArgs = {
  id: Scalars['Int']['input'];
};


export type MutationRegisterArgs = {
  email: Scalars['String']['input'];
  username: Scalars['String']['input'];
};


export type MutationResetPasswordArgs = {
  email: Scalars['String']['input'];
};


export type MutationSaveAttemptArgs = {
  attempt: UpdateAttemptRequest;
  complete?: InputMaybe<Scalars['Boolean']['input']>;
};


export type MutationToggleSetFavouriteArgs = {
  favourite: Scalars['Boolean']['input'];
  id: Scalars['Long']['input'];
};


export type MutationUpdateDeckArgs = {
  request: UpdateDeckRequest;
};


export type MutationUpdateTermArgs = {
  request: UpdateTermRequest;
};

export type PageInfo = {
  readonly __typename: 'PageInfo';
  readonly endCursor?: Maybe<Scalars['String']['output']>;
  readonly startCursor?: Maybe<Scalars['String']['output']>;
};

export type Query = {
  readonly __typename: 'Query';
  readonly crossword?: Maybe<Crossword>;
  readonly debugSearch: Scalars['JSON']['output'];
  readonly deck: Deck;
  readonly language?: Maybe<Language>;
  readonly languages: ReadonlyArray<Language>;
  readonly me?: Maybe<User>;
  readonly recentActivity: Activities;
  readonly searchDecksAndTerms: ReadonlyArray<DeckSearchResult>;
  readonly session?: Maybe<Session>;
  /** Given part of a term, find other terms with similar text. */
  readonly suggestTerms: ReadonlyArray<Term>;
  readonly term: Term;
  /** Returns the most popular decks for a particular language, optionally filtered by definition language. */
  readonly topDecks: ReadonlyArray<Deck>;
  readonly user?: Maybe<User>;
  readonly users: ReadonlyArray<User>;
};


export type QueryCrosswordArgs = {
  id: Scalars['Long']['input'];
};


export type QueryDebugSearchArgs = {
  body?: InputMaybe<Scalars['JSON']['input']>;
  index?: InputMaybe<Scalars['String']['input']>;
  q?: InputMaybe<Scalars['String']['input']>;
};


export type QueryDeckArgs = {
  id: Scalars['Long']['input'];
};


export type QueryLanguageArgs = {
  id: Scalars['String']['input'];
};


export type QueryRecentActivityArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  count?: InputMaybe<Scalars['Int']['input']>;
};


export type QuerySearchDecksAndTermsArgs = {
  search: Scalars['String']['input'];
};


export type QuerySuggestTermsArgs = {
  excludeDeckId?: InputMaybe<Scalars['Int']['input']>;
  search: Scalars['String']['input'];
  termLang?: InputMaybe<Scalars['String']['input']>;
};


export type QueryTermArgs = {
  id: Scalars['Long']['input'];
};


export type QueryTopDecksArgs = {
  count?: InputMaybe<Scalars['Int']['input']>;
  definitionLang?: InputMaybe<Scalars['String']['input']>;
  termLang: Scalars['String']['input'];
};


export type QueryUserArgs = {
  id?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};


export type QueryUsersArgs = {
  search: Scalars['String']['input'];
};

export type Score = {
  readonly __typename: 'Score';
  readonly attempts: Scalars['Int']['output'];
  readonly lastPlayed: Scalars['Date']['output'];
  readonly lastResultSuccess: Scalars['Boolean']['output'];
  readonly score: Scalars['Float']['output'];
  readonly successes: Scalars['Int']['output'];
  readonly term: Term;
  readonly termId: Scalars['Int']['output'];
  readonly userId: Scalars['Int']['output'];
};

export type SecurityAudit = Audit & {
  readonly __typename: 'SecurityAudit';
  readonly code: AuditCode;
  readonly created: Scalars['DateTime']['output'];
  readonly id: Scalars['Int']['output'];
  readonly parameters?: Maybe<ReadonlyArray<Scalars['String']['output']>>;
  readonly type: AuditType;
  readonly user?: Maybe<User>;
  readonly userId?: Maybe<Scalars['Int']['output']>;
};

export enum SecurityAuditCode {
  AccountActivated = 'AccountActivated',
  AccountRegistered = 'AccountRegistered',
  EmailChangeRequested = 'EmailChangeRequested',
  EmailChangeSuccess = 'EmailChangeSuccess',
  FacebookAccountLinked = 'FacebookAccountLinked',
  FacebookAccountRemoved = 'FacebookAccountRemoved',
  ForgotPassword = 'ForgotPassword',
  GoogleAccountLinked = 'GoogleAccountLinked',
  GoogleAccountRemoved = 'GoogleAccountRemoved',
  PasswordReset = 'PasswordReset',
  PasswordResetSuccess = 'PasswordResetSuccess',
  PasswordUpdated = 'PasswordUpdated'
}

export type Session = {
  readonly __typename: 'Session';
  readonly expires: Scalars['Date']['output'];
};

export type Subscription = {
  readonly __typename: 'Subscription';
  readonly auditAdded: Audit;
};


export type SubscriptionAuditAddedArgs = {
  type?: InputMaybe<AuditType>;
};

export type Term = {
  readonly __typename: 'Term';
  readonly deck?: Maybe<Deck>;
  readonly definitions: ReadonlyArray<Scalars['String']['output']>;
  readonly example?: Maybe<Scalars['String']['output']>;
  readonly id: Scalars['Long']['output'];
  readonly order: Scalars['Int']['output'];
  readonly pronunciation: Scalars['String']['output'];
  readonly score?: Maybe<Score>;
  readonly term: Scalars['String']['output'];
};

export type UpdateAttemptRequest = {
  readonly crosswordId: Scalars['Long']['input'];
  readonly guesses: ReadonlyArray<UpdateGuessRequest>;
};

export type UpdateDeckRequest = {
  readonly definitionLangId?: InputMaybe<Scalars['String']['input']>;
  readonly description?: InputMaybe<Scalars['String']['input']>;
  readonly id: Scalars['Long']['input'];
  readonly termLangId?: InputMaybe<Scalars['String']['input']>;
  readonly terms?: InputMaybe<ReadonlyArray<UpdateDeckTerm>>;
  readonly title?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateDeckTerm = {
  readonly definitions: ReadonlyArray<Scalars['String']['input']>;
  readonly id?: InputMaybe<Scalars['Long']['input']>;
  readonly term: Scalars['String']['input'];
};

export type UpdateGuessRequest = {
  readonly guess: Scalars['String']['input'];
  readonly x: Scalars['Int']['input'];
  readonly y: Scalars['Int']['input'];
};

/** Request to add or update a term. If `id` is specified, edit that term, otherwise add a new term. */
export type UpdateTermRequest = {
  readonly deckId: Scalars['Long']['input'];
  readonly definitions: ReadonlyArray<Scalars['String']['input']>;
  readonly id?: InputMaybe<Scalars['Long']['input']>;
  readonly term: Scalars['String']['input'];
};

export type User = {
  readonly __typename: 'User';
  readonly changeEmailRequest?: Maybe<ChangeEmailRequest>;
  readonly connectedAccounts: ReadonlyArray<ConnectedAccountType>;
  readonly crosswords: ReadonlyArray<Crossword>;
  readonly decks: ReadonlyArray<Deck>;
  readonly displayName: Scalars['String']['output'];
  readonly email?: Maybe<Scalars['String']['output']>;
  readonly favouriteDecks: ReadonlyArray<Deck>;
  readonly followed?: Maybe<Scalars['Boolean']['output']>;
  readonly followedUsers: ReadonlyArray<User>;
  readonly hardestTerms: ReadonlyArray<Score>;
  readonly hasPassword?: Maybe<Scalars['Boolean']['output']>;
  readonly id: Scalars['Int']['output'];
  readonly isMe: Scalars['Boolean']['output'];
  readonly languages: ReadonlyArray<UserLanguage>;
  readonly lastReadNewsId?: Maybe<Scalars['Int']['output']>;
  readonly name: Scalars['String']['output'];
  readonly profileImage?: Maybe<Scalars['String']['output']>;
  readonly recentAttempts: ReadonlyArray<Attempt>;
  readonly recentFailures: ReadonlyArray<Score>;
  readonly signUpDate: Scalars['Date']['output'];
  readonly superuser: Scalars['Boolean']['output'];
};


export type UserChangeEmailRequestArgs = {
  active?: InputMaybe<Scalars['Boolean']['input']>;
};


export type UserHardestTermsArgs = {
  count?: InputMaybe<Scalars['Int']['input']>;
};


export type UserRecentAttemptsArgs = {
  count: Scalars['Int']['input'];
};


export type UserRecentFailuresArgs = {
  count?: InputMaybe<Scalars['Int']['input']>;
};

export type UserAccountRequest = {
  readonly created: Scalars['DateTime']['output'];
  readonly expires: Scalars['DateTime']['output'];
  readonly id: Scalars['Int']['output'];
  readonly used?: Maybe<Scalars['DateTime']['output']>;
  readonly valid: Scalars['Boolean']['output'];
};

export type UserAudit = Audit & {
  readonly __typename: 'UserAudit';
  readonly code: AuditCode;
  readonly created: Scalars['DateTime']['output'];
  readonly id: Scalars['Int']['output'];
  readonly otherUser?: Maybe<User>;
  readonly otherUserId?: Maybe<Scalars['Int']['output']>;
  readonly parameters?: Maybe<ReadonlyArray<Scalars['String']['output']>>;
  readonly type: AuditType;
  readonly user?: Maybe<User>;
  readonly userId?: Maybe<Scalars['Int']['output']>;
};

export enum UserAuditCode {
  UserFollowed = 'UserFollowed',
  UserUnfollowed = 'UserUnfollowed'
}

export type UserLanguage = {
  readonly __typename: 'UserLanguage';
  readonly crosswordsCompleted: Scalars['Int']['output'];
  readonly crosswordsCreated: Scalars['Int']['output'];
  readonly decks: Scalars['Int']['output'];
  readonly language: Language;
  readonly languageId: Scalars['String']['output'];
  readonly lastActivity: Scalars['DateTime']['output'];
  readonly started: Scalars['DateTime']['output'];
  readonly user: User;
  readonly userId: Scalars['Int']['output'];
};

export type ChangeEmailBaseMutationVariables = Exact<{
  email: Scalars['String']['input'];
}>;


export type ChangeEmailBaseMutation = { readonly changeEmail: { readonly __typename: 'ChangeEmailRequest', readonly id: number, readonly userId: number, readonly newEmail: string } };

export type ConfirmChangeEmailMutationVariables = Exact<{
  code: Scalars['String']['input'];
}>;


export type ConfirmChangeEmailMutation = { readonly user: { readonly __typename: 'User', readonly id: number, readonly email?: string | null, readonly changeEmailRequest?: { readonly __typename: 'ChangeEmailRequest', readonly id: number, readonly newEmail: string } | null } };

export type CancelChangeEmailMutationVariables = Exact<{
  id: Scalars['Int']['input'];
}>;


export type CancelChangeEmailMutation = { readonly cancelChangeEmailRequest: { readonly __typename: 'User', readonly id: number, readonly changeEmailRequest?: { readonly __typename: 'ChangeEmailRequest', readonly id: number, readonly newEmail: string } | null } };

export type ChangePasswordMutationVariables = Exact<{
  currentPassword: Scalars['String']['input'];
  newPassword: Scalars['String']['input'];
}>;


export type ChangePasswordMutation = { readonly changePassword: { readonly __typename: 'User', readonly id: number, readonly hasPassword?: boolean | null } };

export type ConfirmResetPasswordBaseMutationVariables = Exact<{
  code: Scalars['String']['input'];
  password: Scalars['String']['input'];
}>;


export type ConfirmResetPasswordBaseMutation = { readonly confirmResetPassword: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly isMe: boolean, readonly displayName: string, readonly profileImage?: string | null, readonly signUpDate: any } };

export type RegisterMutationVariables = Exact<{
  username: Scalars['String']['input'];
  email: Scalars['String']['input'];
}>;


export type RegisterMutation = { readonly register?: any | null };

export type ResetPasswordMutationVariables = Exact<{
  email: Scalars['String']['input'];
}>;


export type ResetPasswordMutation = { readonly resetPassword?: any | null };

type AuditAdded_CrosswordAudit_Fragment = { readonly __typename: 'CrosswordAudit', readonly id: number, readonly type: AuditType, readonly code: AuditCode, readonly created: any, readonly parameters?: ReadonlyArray<string> | null, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null } | null };

type AuditAdded_DeckAudit_Fragment = { readonly __typename: 'DeckAudit', readonly id: number, readonly type: AuditType, readonly code: AuditCode, readonly created: any, readonly parameters?: ReadonlyArray<string> | null, readonly deck?: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string } } | null, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null } | null };

type AuditAdded_SecurityAudit_Fragment = { readonly __typename: 'SecurityAudit', readonly id: number, readonly type: AuditType, readonly code: AuditCode, readonly created: any, readonly parameters?: ReadonlyArray<string> | null, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null } | null };

type AuditAdded_UserAudit_Fragment = { readonly __typename: 'UserAudit', readonly id: number, readonly type: AuditType, readonly code: AuditCode, readonly created: any, readonly parameters?: ReadonlyArray<string> | null, readonly otherUser?: { readonly __typename: 'User', readonly name: string, readonly displayName: string, readonly profileImage?: string | null } | null, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null } | null };

export type AuditAddedFragment = AuditAdded_CrosswordAudit_Fragment | AuditAdded_DeckAudit_Fragment | AuditAdded_SecurityAudit_Fragment | AuditAdded_UserAudit_Fragment;

export type AuditAddedSubscriptionVariables = Exact<{ [key: string]: never; }>;


export type AuditAddedSubscription = { readonly auditAdded: { readonly __typename: 'CrosswordAudit', readonly id: number, readonly type: AuditType, readonly code: AuditCode, readonly created: any, readonly parameters?: ReadonlyArray<string> | null, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null } | null } | { readonly __typename: 'DeckAudit', readonly id: number, readonly type: AuditType, readonly code: AuditCode, readonly created: any, readonly parameters?: ReadonlyArray<string> | null, readonly deck?: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string } } | null, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null } | null } | { readonly __typename: 'SecurityAudit', readonly id: number, readonly type: AuditType, readonly code: AuditCode, readonly created: any, readonly parameters?: ReadonlyArray<string> | null, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null } | null } | { readonly __typename: 'UserAudit', readonly id: number, readonly type: AuditType, readonly code: AuditCode, readonly created: any, readonly parameters?: ReadonlyArray<string> | null, readonly otherUser?: { readonly __typename: 'User', readonly name: string, readonly displayName: string, readonly profileImage?: string | null } | null, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null } | null } };

export type LoginBaseMutationVariables = Exact<{
  username: Scalars['String']['input'];
  password: Scalars['String']['input'];
}>;


export type LoginBaseMutation = { readonly login: { readonly __typename: 'User', readonly lastReadNewsId?: number | null, readonly superuser: boolean, readonly id: number, readonly name: string, readonly isMe: boolean, readonly displayName: string, readonly profileImage?: string | null, readonly signUpDate: any } };

export type LogoutBaseMutationVariables = Exact<{ [key: string]: never; }>;


export type LogoutBaseMutation = { readonly logout?: any | null };

export type ActivateMutationVariables = Exact<{
  activationCode: Scalars['String']['input'];
  password: Scalars['String']['input'];
  displayName: Scalars['String']['input'];
}>;


export type ActivateMutation = { readonly activateAccount: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly isMe: boolean, readonly displayName: string, readonly profileImage?: string | null, readonly signUpDate: any } };

export type CrosswordCardInfoFragment = { readonly __typename: 'Crossword', readonly id: number, readonly title?: string | null, readonly description?: string | null, readonly created: any, readonly width?: number | null, readonly height?: number | null, readonly imageUrl: string, readonly stars: number, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string } | null, readonly language?: { readonly __typename: 'Language', readonly id: string } | null, readonly attempt?: { readonly __typename: 'Attempt', readonly id: any, readonly completed: boolean, readonly percentCompleted: number } | null };

export type ClueDetailFragment = { readonly __typename: 'Clue', readonly id: number, readonly number: number, readonly direction: Direction, readonly x: number, readonly y: number, readonly length: number, readonly clue: string, readonly lengthHint: string, readonly answer?: string | null, readonly originalAnswer?: string | null, readonly deckId: number, readonly termId?: number | null };

export type GuessInfoFragment = { readonly __typename: 'Guess', readonly attemptId: any, readonly x: number, readonly y: number, readonly guess?: string | null };

export type CrosswordDetailFragment = { readonly __typename: 'Crossword', readonly id: number, readonly title?: string | null, readonly description?: string | null, readonly stars: number, readonly created: any, readonly available?: any | null, readonly width?: number | null, readonly height?: number | null, readonly imageUrl: string, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null } | null, readonly clues?: ReadonlyArray<{ readonly __typename: 'Clue', readonly id: number, readonly number: number, readonly direction: Direction, readonly x: number, readonly y: number, readonly length: number, readonly clue: string, readonly lengthHint: string, readonly answer?: string | null, readonly originalAnswer?: string | null, readonly deckId: number, readonly termId?: number | null }> | null, readonly decks: ReadonlyArray<{ readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } }>, readonly attempt?: { readonly __typename: 'Attempt', readonly id: any, readonly started: any, readonly lastPlayed: any, readonly completed: boolean, readonly percentCompleted: number, readonly score?: number | null, readonly guesses: ReadonlyArray<{ readonly __typename: 'Guess', readonly attemptId: any, readonly x: number, readonly y: number, readonly guess?: string | null }> } | null };

export type CrosswordDetailQueryVariables = Exact<{
  id: Scalars['Long']['input'];
}>;


export type CrosswordDetailQuery = { readonly crossword?: { readonly __typename: 'Crossword', readonly id: number, readonly title?: string | null, readonly description?: string | null, readonly stars: number, readonly created: any, readonly available?: any | null, readonly width?: number | null, readonly height?: number | null, readonly imageUrl: string, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null } | null, readonly clues?: ReadonlyArray<{ readonly __typename: 'Clue', readonly id: number, readonly number: number, readonly direction: Direction, readonly x: number, readonly y: number, readonly length: number, readonly clue: string, readonly lengthHint: string, readonly answer?: string | null, readonly originalAnswer?: string | null, readonly deckId: number, readonly termId?: number | null }> | null, readonly decks: ReadonlyArray<{ readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } }>, readonly attempt?: { readonly __typename: 'Attempt', readonly id: any, readonly started: any, readonly lastPlayed: any, readonly completed: boolean, readonly percentCompleted: number, readonly score?: number | null, readonly guesses: ReadonlyArray<{ readonly __typename: 'Guess', readonly attemptId: any, readonly x: number, readonly y: number, readonly guess?: string | null }> } | null } | null };

export type AddCrosswordBaseMutationVariables = Exact<{
  deckIds?: InputMaybe<ReadonlyArray<Scalars['Long']['input']> | Scalars['Long']['input']>;
  languageId?: InputMaybe<Scalars['String']['input']>;
  width?: InputMaybe<Scalars['Int']['input']>;
  height?: InputMaybe<Scalars['Int']['input']>;
}>;


export type AddCrosswordBaseMutation = { readonly addCrossword: { readonly __typename: 'Crossword', readonly id: number, readonly title?: string | null, readonly description?: string | null, readonly created: any, readonly width?: number | null, readonly height?: number | null, readonly imageUrl: string, readonly stars: number, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string } | null, readonly language?: { readonly __typename: 'Language', readonly id: string } | null, readonly attempt?: { readonly __typename: 'Attempt', readonly id: any, readonly completed: boolean, readonly percentCompleted: number } | null } };

export type SaveAttemptBaseMutationVariables = Exact<{
  attempt: UpdateAttemptRequest;
  complete?: InputMaybe<Scalars['Boolean']['input']>;
}>;


export type SaveAttemptBaseMutation = { readonly saveAttempt: { readonly __typename: 'Attempt', readonly id: any, readonly completed: boolean, readonly percentCompleted: number, readonly score?: number | null } };

export type CompleteAttemptMutationVariables = Exact<{
  attempt: UpdateAttemptRequest;
}>;


export type CompleteAttemptMutation = { readonly completeAttempt: { readonly __typename: 'Crossword', readonly id: number, readonly clues?: ReadonlyArray<{ readonly __typename: 'Clue', readonly id: number, readonly number: number, readonly direction: Direction, readonly x: number, readonly y: number, readonly length: number, readonly clue: string, readonly lengthHint: string, readonly answer?: string | null, readonly originalAnswer?: string | null, readonly deckId: number, readonly termId?: number | null }> | null, readonly attempt?: { readonly __typename: 'Attempt', readonly id: any, readonly completed: boolean, readonly percentCompleted: number, readonly score?: number | null, readonly guesses: ReadonlyArray<{ readonly __typename: 'Guess', readonly attemptId: any, readonly x: number, readonly y: number, readonly guess?: string | null }> } | null } };

export type DeckCardInfoFragment = { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } };

export type TermInfoFragment = { readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string>, readonly example?: string | null, readonly deck?: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string } } | null };

export type TermDetailFragment = { readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string>, readonly example?: string | null, readonly order: number, readonly score?: { readonly __typename: 'Score', readonly userId: number, readonly termId: number, readonly attempts: number, readonly successes: number, readonly score: number } | null };

export type DeckDetailFragment = { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly terms: ReadonlyArray<{ readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string>, readonly example?: string | null, readonly order: number, readonly score?: { readonly __typename: 'Score', readonly userId: number, readonly termId: number, readonly attempts: number, readonly successes: number, readonly score: number } | null }>, readonly crosswords: ReadonlyArray<{ readonly __typename: 'Crossword', readonly id: number, readonly title?: string | null, readonly description?: string | null, readonly created: any, readonly width?: number | null, readonly height?: number | null, readonly imageUrl: string, readonly stars: number, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string } | null, readonly language?: { readonly __typename: 'Language', readonly id: string } | null, readonly attempt?: { readonly __typename: 'Attempt', readonly id: any, readonly completed: boolean, readonly percentCompleted: number } | null }>, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } };

export type DeckDetailQueryVariables = Exact<{
  id: Scalars['Long']['input'];
}>;


export type DeckDetailQuery = { readonly deck: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly terms: ReadonlyArray<{ readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string>, readonly example?: string | null, readonly order: number, readonly score?: { readonly __typename: 'Score', readonly userId: number, readonly termId: number, readonly attempts: number, readonly successes: number, readonly score: number } | null }>, readonly crosswords: ReadonlyArray<{ readonly __typename: 'Crossword', readonly id: number, readonly title?: string | null, readonly description?: string | null, readonly created: any, readonly width?: number | null, readonly height?: number | null, readonly imageUrl: string, readonly stars: number, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string } | null, readonly language?: { readonly __typename: 'Language', readonly id: string } | null, readonly attempt?: { readonly __typename: 'Attempt', readonly id: any, readonly completed: boolean, readonly percentCompleted: number } | null }>, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } } };

export type FavouriteDeckIdsBaseQueryVariables = Exact<{ [key: string]: never; }>;


export type FavouriteDeckIdsBaseQuery = { readonly me?: { readonly __typename: 'User', readonly id: number, readonly favouriteDecks: ReadonlyArray<{ readonly __typename: 'Deck', readonly id: any }> } | null };

export type TermInfoQueryVariables = Exact<{
  id: Scalars['Long']['input'];
}>;


export type TermInfoQuery = { readonly term: { readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string>, readonly example?: string | null, readonly deck?: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string } } | null } };

export type TermSuggestionFragment = { readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string>, readonly deck?: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string } } | null };

export type SuggestTermsQueryVariables = Exact<{
  search: Scalars['String']['input'];
  termLang?: InputMaybe<Scalars['String']['input']>;
  excludeDeckId?: InputMaybe<Scalars['Int']['input']>;
}>;


export type SuggestTermsQuery = { readonly suggestTerms: ReadonlyArray<{ readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string>, readonly deck?: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string } } | null }> };

export type CreateDeckMutationVariables = Exact<{
  request: CreateDeckRequest;
}>;


export type CreateDeckMutation = { readonly createDeck: { readonly __typename: 'Deck', readonly id: any, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly decks: ReadonlyArray<{ readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } }> } } };

export type ToggleDeckFavouriteBaseMutationVariables = Exact<{
  id: Scalars['Long']['input'];
  favourite: Scalars['Boolean']['input'];
}>;


export type ToggleDeckFavouriteBaseMutation = { readonly toggleSetFavourite?: any | null };

export type UpdateDeckMutationVariables = Exact<{
  request: UpdateDeckRequest;
}>;


export type UpdateDeckMutation = { readonly updateDeck: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly terms: ReadonlyArray<{ readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string>, readonly example?: string | null, readonly order: number, readonly score?: { readonly __typename: 'Score', readonly userId: number, readonly termId: number, readonly attempts: number, readonly successes: number, readonly score: number } | null }>, readonly crosswords: ReadonlyArray<{ readonly __typename: 'Crossword', readonly id: number, readonly title?: string | null, readonly description?: string | null, readonly created: any, readonly width?: number | null, readonly height?: number | null, readonly imageUrl: string, readonly stars: number, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string } | null, readonly language?: { readonly __typename: 'Language', readonly id: string } | null, readonly attempt?: { readonly __typename: 'Attempt', readonly id: any, readonly completed: boolean, readonly percentCompleted: number } | null }>, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } } };

export type UpdateTermBaseMutationVariables = Exact<{
  request: UpdateTermRequest;
}>;


export type UpdateTermBaseMutation = { readonly updateTerm: { readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string>, readonly example?: string | null, readonly order: number, readonly score?: { readonly __typename: 'Score', readonly userId: number, readonly termId: number, readonly attempts: number, readonly successes: number, readonly score: number } | null } };

export type AttemptCardInfoFragment = { readonly __typename: 'Attempt', readonly id: any, readonly lastPlayed: any, readonly crossword: { readonly __typename: 'Crossword', readonly id: number, readonly title?: string | null, readonly description?: string | null, readonly created: any, readonly width?: number | null, readonly height?: number | null, readonly imageUrl: string, readonly stars: number, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string } | null, readonly language?: { readonly __typename: 'Language', readonly id: string } | null, readonly attempt?: { readonly __typename: 'Attempt', readonly id: any, readonly completed: boolean, readonly percentCompleted: number } | null } };

export type UserLanguageStatsFragment = { readonly __typename: 'UserLanguage', readonly lastActivity: any, readonly crosswordsCreated: number, readonly language: { readonly __typename: 'Language', readonly id: string, readonly name: string } };

export type HomeQueryVariables = Exact<{ [key: string]: never; }>;


export type HomeQuery = { readonly me?: { readonly __typename: 'User', readonly id: number, readonly decks: ReadonlyArray<{ readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } }>, readonly recentAttempts: ReadonlyArray<{ readonly __typename: 'Attempt', readonly id: any, readonly lastPlayed: any, readonly crossword: { readonly __typename: 'Crossword', readonly id: number, readonly title?: string | null, readonly description?: string | null, readonly created: any, readonly width?: number | null, readonly height?: number | null, readonly imageUrl: string, readonly stars: number, readonly user?: { readonly __typename: 'User', readonly id: number, readonly name: string } | null, readonly language?: { readonly __typename: 'Language', readonly id: string } | null, readonly attempt?: { readonly __typename: 'Attempt', readonly id: any, readonly completed: boolean, readonly percentCompleted: number } | null } }>, readonly languages: ReadonlyArray<{ readonly __typename: 'UserLanguage', readonly lastActivity: any, readonly crosswordsCreated: number, readonly language: { readonly __typename: 'Language', readonly id: string, readonly name: string } }> } | null };

export type TopDeckFragment = { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly terms: ReadonlyArray<{ readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string> }>, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } };

export type TopDecksQueryVariables = Exact<{
  termLang: Scalars['String']['input'];
}>;


export type TopDecksQuery = { readonly topDecks: ReadonlyArray<{ readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly terms: ReadonlyArray<{ readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string> }>, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } }> };

export type TermsToStudyQueryVariables = Exact<{ [key: string]: never; }>;


export type TermsToStudyQuery = { readonly me?: { readonly __typename: 'User', readonly id: number, readonly hardestTerms: ReadonlyArray<{ readonly __typename: 'Score', readonly userId: number, readonly termId: number, readonly score: number, readonly successes: number, readonly attempts: number, readonly lastPlayed: any, readonly lastResultSuccess: boolean, readonly term: { readonly __typename: 'Term', readonly id: any, readonly definitions: ReadonlyArray<string>, readonly deck?: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string } } | null } }>, readonly recentFailures: ReadonlyArray<{ readonly __typename: 'Score', readonly userId: number, readonly termId: number, readonly score: number, readonly successes: number, readonly attempts: number, readonly lastPlayed: any, readonly lastResultSuccess: boolean, readonly term: { readonly __typename: 'Term', readonly id: any, readonly definitions: ReadonlyArray<string>, readonly deck?: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string } } | null } }> } | null };

export type ActivityInfoFragment = { readonly __typename: 'Activity', readonly id: number, readonly date: any, readonly type: ActivityType, readonly crosswordIds?: ReadonlyArray<number> | null, readonly parameters?: ReadonlyArray<string> | null, readonly user: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly isMe: boolean, readonly displayName: string, readonly profileImage?: string | null, readonly signUpDate: any }, readonly decks?: ReadonlyArray<{ readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string } }> | null };

export type RecentActivityBaseQueryVariables = Exact<{
  count: Scalars['Int']['input'];
  cursor?: InputMaybe<Scalars['String']['input']>;
}>;


export type RecentActivityBaseQuery = { readonly recentActivity: { readonly __typename: 'Activities', readonly activities: ReadonlyArray<{ readonly __typename: 'Activity', readonly cursor: string, readonly id: number, readonly date: any, readonly type: ActivityType, readonly crosswordIds?: ReadonlyArray<number> | null, readonly parameters?: ReadonlyArray<string> | null, readonly user: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly isMe: boolean, readonly displayName: string, readonly profileImage?: string | null, readonly signUpDate: any }, readonly decks?: ReadonlyArray<{ readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string } }> | null }>, readonly page: { readonly __typename: 'PageInfo', readonly endCursor?: string | null } } };

export type LanguageInfoFragment = { readonly __typename: 'Language', readonly id: string, readonly name: string };

export type LanguagesQueryVariables = Exact<{ [key: string]: never; }>;


export type LanguagesQuery = { readonly languages: ReadonlyArray<{ readonly __typename: 'Language', readonly id: string, readonly name: string }> };

export type LanguageInfoQueryVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type LanguageInfoQuery = { readonly language?: { readonly __typename: 'Language', readonly id: string, readonly name: string } | null };

export type ScoreInfoFragment = { readonly __typename: 'Score', readonly userId: number, readonly termId: number, readonly score: number, readonly successes: number, readonly attempts: number, readonly lastPlayed: any, readonly lastResultSuccess: boolean, readonly term: { readonly __typename: 'Term', readonly id: any, readonly definitions: ReadonlyArray<string>, readonly deck?: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string } } | null } };

type SearchResult_Deck_Fragment = { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } };

type SearchResult_Term_Fragment = { readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string>, readonly example?: string | null, readonly deck?: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string } } | null };

export type SearchResultFragment = SearchResult_Deck_Fragment | SearchResult_Term_Fragment;

export type GlobalSearchQueryVariables = Exact<{
  search: Scalars['String']['input'];
}>;


export type GlobalSearchQuery = { readonly results: ReadonlyArray<{ readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } } | { readonly __typename: 'Term', readonly id: any, readonly term: string, readonly definitions: ReadonlyArray<string>, readonly example?: string | null, readonly deck?: { readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termLang: { readonly __typename: 'Language', readonly id: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string } } | null }> };

export type BasicUserInfoFragment = { readonly __typename: 'User', readonly id: number, readonly name: string, readonly isMe: boolean, readonly displayName: string, readonly profileImage?: string | null, readonly signUpDate: any };

export type MeUserInfoFragment = { readonly __typename: 'User', readonly lastReadNewsId?: number | null, readonly superuser: boolean, readonly id: number, readonly name: string, readonly isMe: boolean, readonly displayName: string, readonly profileImage?: string | null, readonly signUpDate: any };

export type UserProfileFragment = { readonly __typename: 'User', readonly followed?: boolean | null, readonly id: number, readonly name: string, readonly isMe: boolean, readonly displayName: string, readonly profileImage?: string | null, readonly signUpDate: any, readonly languages: ReadonlyArray<{ readonly __typename: 'UserLanguage', readonly decks: number, readonly crosswordsCreated: number, readonly crosswordsCompleted: number, readonly started: any, readonly lastActivity: any, readonly language: { readonly __typename: 'Language', readonly id: string, readonly name: string } }>, readonly decks: ReadonlyArray<{ readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } }> };

export type ManageAccountFragment = { readonly __typename: 'User', readonly email?: string | null, readonly connectedAccounts: ReadonlyArray<ConnectedAccountType>, readonly hasPassword?: boolean | null, readonly changeEmailRequest?: { readonly __typename: 'ChangeEmailRequest', readonly id: number, readonly newEmail: string } | null };

export type UserQueryVariables = Exact<{
  name?: InputMaybe<Scalars['String']['input']>;
  self: Scalars['Boolean']['input'];
}>;


export type UserQuery = { readonly user?: { readonly __typename: 'User', readonly followed?: boolean | null, readonly email?: string | null, readonly connectedAccounts: ReadonlyArray<ConnectedAccountType>, readonly hasPassword?: boolean | null, readonly id: number, readonly name: string, readonly isMe: boolean, readonly displayName: string, readonly profileImage?: string | null, readonly signUpDate: any, readonly languages: ReadonlyArray<{ readonly __typename: 'UserLanguage', readonly decks: number, readonly crosswordsCreated: number, readonly crosswordsCompleted: number, readonly started: any, readonly lastActivity: any, readonly language: { readonly __typename: 'Language', readonly id: string, readonly name: string } }>, readonly decks: ReadonlyArray<{ readonly __typename: 'Deck', readonly id: any, readonly title: string, readonly termCount: number, readonly description?: string | null, readonly createdBy: { readonly __typename: 'User', readonly id: number, readonly name: string, readonly displayName: string, readonly profileImage?: string | null }, readonly termLang: { readonly __typename: 'Language', readonly id: string, readonly name: string }, readonly definitionLang: { readonly __typename: 'Language', readonly id: string, readonly name: string } }>, readonly changeEmailRequest?: { readonly __typename: 'ChangeEmailRequest', readonly id: number, readonly newEmail: string } | null } | null };

export type MeBaseQueryVariables = Exact<{ [key: string]: never; }>;


export type MeBaseQuery = { readonly me?: { readonly __typename: 'User', readonly lastReadNewsId?: number | null, readonly superuser: boolean, readonly id: number, readonly name: string, readonly isMe: boolean, readonly displayName: string, readonly profileImage?: string | null, readonly signUpDate: any } | null };

export type CheckUserNameQueryVariables = Exact<{
  username?: InputMaybe<Scalars['String']['input']>;
}>;


export type CheckUserNameQuery = { readonly user?: { readonly __typename: 'User', readonly id: number } | null };

export type UserLanguageFragment = { readonly __typename: 'UserLanguage', readonly decks: number, readonly lastActivity: any, readonly language: { readonly __typename: 'Language', readonly id: string, readonly name: string } };

export type UserLanguagesQueryVariables = Exact<{ [key: string]: never; }>;


export type UserLanguagesQuery = { readonly me?: { readonly __typename: 'User', readonly id: number, readonly languages: ReadonlyArray<{ readonly __typename: 'UserLanguage', readonly decks: number, readonly lastActivity: any, readonly language: { readonly __typename: 'Language', readonly id: string, readonly name: string } }> } | null };

export type MarkNewsReadBaseMutationVariables = Exact<{
  id: Scalars['Int']['input'];
}>;


export type MarkNewsReadBaseMutation = { readonly markNewsRead: { readonly __typename: 'User', readonly lastReadNewsId?: number | null } };

export type FollowUserBaseMutationVariables = Exact<{
  userId: Scalars['Int']['input'];
  follow: Scalars['Boolean']['input'];
}>;


export type FollowUserBaseMutation = { readonly followUser?: any | null };

export type DisconnectAccountMutationVariables = Exact<{
  type: ConnectedAccountType;
}>;


export type DisconnectAccountMutation = { readonly disconnectAccount: { readonly __typename: 'User', readonly id: number, readonly connectedAccounts: ReadonlyArray<ConnectedAccountType> } };

export const AuditAddedFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"AuditAdded"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Audit"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"created"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"parameters"}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DeckAudit"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deck"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UserAudit"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"otherUser"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}}]}}]}}]} as unknown as DocumentNode;
export const ClueDetailFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ClueDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Clue"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"number"}},{"kind":"Field","name":{"kind":"Name","value":"direction"}},{"kind":"Field","name":{"kind":"Name","value":"x"}},{"kind":"Field","name":{"kind":"Name","value":"y"}},{"kind":"Field","name":{"kind":"Name","value":"length"}},{"kind":"Field","name":{"kind":"Name","value":"clue"}},{"kind":"Field","name":{"kind":"Name","value":"lengthHint"}},{"kind":"Field","name":{"kind":"Name","value":"answer"}},{"kind":"Field","name":{"kind":"Name","value":"originalAnswer"}},{"kind":"Field","name":{"kind":"Name","value":"deckId"}},{"kind":"Field","name":{"kind":"Name","value":"termId"}}]}}]} as unknown as DocumentNode;
export const LanguageInfoFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;
export const DeckCardInfoFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;
export const GuessInfoFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"GuessInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Guess"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"attemptId"}},{"kind":"Field","name":{"kind":"Name","value":"x"}},{"kind":"Field","name":{"kind":"Name","value":"y"}},{"kind":"Field","name":{"kind":"Name","value":"guess"}}]}}]} as unknown as DocumentNode;
export const CrosswordDetailFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"CrosswordDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Crossword"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"stars"}},{"kind":"Field","name":{"kind":"Name","value":"created"}},{"kind":"Field","name":{"kind":"Name","value":"available"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"imageUrl"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"clues"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ClueDetail"}}]}},{"kind":"Field","name":{"kind":"Name","value":"decks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"attempt"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"started"}},{"kind":"Field","name":{"kind":"Name","value":"lastPlayed"}},{"kind":"Field","name":{"kind":"Name","value":"completed"}},{"kind":"Field","name":{"kind":"Name","value":"percentCompleted"}},{"kind":"Field","name":{"kind":"Name","value":"guesses"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"GuessInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"score"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ClueDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Clue"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"number"}},{"kind":"Field","name":{"kind":"Name","value":"direction"}},{"kind":"Field","name":{"kind":"Name","value":"x"}},{"kind":"Field","name":{"kind":"Name","value":"y"}},{"kind":"Field","name":{"kind":"Name","value":"length"}},{"kind":"Field","name":{"kind":"Name","value":"clue"}},{"kind":"Field","name":{"kind":"Name","value":"lengthHint"}},{"kind":"Field","name":{"kind":"Name","value":"answer"}},{"kind":"Field","name":{"kind":"Name","value":"originalAnswer"}},{"kind":"Field","name":{"kind":"Name","value":"deckId"}},{"kind":"Field","name":{"kind":"Name","value":"termId"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"GuessInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Guess"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"attemptId"}},{"kind":"Field","name":{"kind":"Name","value":"x"}},{"kind":"Field","name":{"kind":"Name","value":"y"}},{"kind":"Field","name":{"kind":"Name","value":"guess"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;
export const TermDetailFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TermDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"example"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"score"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userId"}},{"kind":"Field","name":{"kind":"Name","value":"termId"}},{"kind":"Field","name":{"kind":"Name","value":"attempts"}},{"kind":"Field","name":{"kind":"Name","value":"successes"}},{"kind":"Field","name":{"kind":"Name","value":"score"}}]}}]}}]} as unknown as DocumentNode;
export const CrosswordCardInfoFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"CrosswordCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Crossword"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"created"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"imageUrl"}},{"kind":"Field","name":{"kind":"Name","value":"stars"}},{"kind":"Field","name":{"kind":"Name","value":"attempt"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"completed"}},{"kind":"Field","name":{"kind":"Name","value":"percentCompleted"}}]}}]}}]} as unknown as DocumentNode;
export const DeckDetailFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}},{"kind":"Field","name":{"kind":"Name","value":"terms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"TermDetail"}}]}},{"kind":"Field","name":{"kind":"Name","value":"crosswords"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"CrosswordCardInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"CrosswordCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Crossword"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"created"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"imageUrl"}},{"kind":"Field","name":{"kind":"Name","value":"stars"}},{"kind":"Field","name":{"kind":"Name","value":"attempt"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"completed"}},{"kind":"Field","name":{"kind":"Name","value":"percentCompleted"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TermDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"example"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"score"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userId"}},{"kind":"Field","name":{"kind":"Name","value":"termId"}},{"kind":"Field","name":{"kind":"Name","value":"attempts"}},{"kind":"Field","name":{"kind":"Name","value":"successes"}},{"kind":"Field","name":{"kind":"Name","value":"score"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;
export const TermSuggestionFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TermSuggestion"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"deck"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]} as unknown as DocumentNode;
export const AttemptCardInfoFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"AttemptCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Attempt"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"lastPlayed"}},{"kind":"Field","name":{"kind":"Name","value":"crossword"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"CrosswordCardInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"CrosswordCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Crossword"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"created"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"imageUrl"}},{"kind":"Field","name":{"kind":"Name","value":"stars"}},{"kind":"Field","name":{"kind":"Name","value":"attempt"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"completed"}},{"kind":"Field","name":{"kind":"Name","value":"percentCompleted"}}]}}]}}]} as unknown as DocumentNode;
export const UserLanguageStatsFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"UserLanguageStats"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UserLanguage"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"lastActivity"}},{"kind":"Field","name":{"kind":"Name","value":"crosswordsCreated"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;
export const TopDeckFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TopDeck"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}},{"kind":"Field","name":{"kind":"Name","value":"terms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;
export const BasicUserInfoFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"BasicUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isMe"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}},{"kind":"Field","name":{"kind":"Name","value":"signUpDate"}}]}}]} as unknown as DocumentNode;
export const ActivityInfoFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ActivityInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Activity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"BasicUserInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"decks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"crosswordIds"}},{"kind":"Field","name":{"kind":"Name","value":"parameters"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"BasicUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isMe"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}},{"kind":"Field","name":{"kind":"Name","value":"signUpDate"}}]}}]} as unknown as DocumentNode;
export const ScoreInfoFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ScoreInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Score"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userId"}},{"kind":"Field","name":{"kind":"Name","value":"termId"}},{"kind":"Field","name":{"kind":"Name","value":"score"}},{"kind":"Field","name":{"kind":"Name","value":"successes"}},{"kind":"Field","name":{"kind":"Name","value":"attempts"}},{"kind":"Field","name":{"kind":"Name","value":"lastPlayed"}},{"kind":"Field","name":{"kind":"Name","value":"lastResultSuccess"}},{"kind":"Field","name":{"kind":"Name","value":"term"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"deck"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]}}]} as unknown as DocumentNode;
export const TermInfoFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TermInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"example"}},{"kind":"Field","name":{"kind":"Name","value":"deck"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]} as unknown as DocumentNode;
export const SearchResultFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"SearchResult"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DeckSearchResult"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"TermInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TermInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"example"}},{"kind":"Field","name":{"kind":"Name","value":"deck"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;
export const MeUserInfoFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"MeUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"BasicUserInfo"}},{"kind":"Field","name":{"kind":"Name","value":"lastReadNewsId"}},{"kind":"Field","name":{"kind":"Name","value":"superuser"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"BasicUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isMe"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}},{"kind":"Field","name":{"kind":"Name","value":"signUpDate"}}]}}]} as unknown as DocumentNode;
export const UserProfileFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"UserProfile"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"BasicUserInfo"}},{"kind":"Field","name":{"kind":"Name","value":"followed"}},{"kind":"Field","name":{"kind":"Name","value":"languages"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"decks"}},{"kind":"Field","name":{"kind":"Name","value":"crosswordsCreated"}},{"kind":"Field","name":{"kind":"Name","value":"crosswordsCompleted"}},{"kind":"Field","name":{"kind":"Name","value":"started"}},{"kind":"Field","name":{"kind":"Name","value":"lastActivity"}}]}},{"kind":"Field","name":{"kind":"Name","value":"decks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"BasicUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isMe"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}},{"kind":"Field","name":{"kind":"Name","value":"signUpDate"}}]}}]} as unknown as DocumentNode;
export const ManageAccountFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ManageAccount"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"changeEmailRequest"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"active"},"value":{"kind":"BooleanValue","value":true}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"newEmail"}}]}},{"kind":"Field","name":{"kind":"Name","value":"connectedAccounts"}},{"kind":"Field","name":{"kind":"Name","value":"hasPassword"}}]}}]} as unknown as DocumentNode;
export const UserLanguageFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"UserLanguage"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UserLanguage"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"decks"}},{"kind":"Field","name":{"kind":"Name","value":"lastActivity"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;
export const ChangeEmailBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"ChangeEmailBase"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"email"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"changeEmail"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"email"},"value":{"kind":"Variable","name":{"kind":"Name","value":"email"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"userId"}},{"kind":"Field","name":{"kind":"Name","value":"newEmail"}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useChangeEmailBaseMutation__
 *
 * To run a mutation, you first call `useChangeEmailBaseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeEmailBaseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeEmailBaseMutation, { data, loading, error }] = useChangeEmailBaseMutation({
 *   variables: {
 *      email: // value for 'email'
 *   },
 * });
 */
export function useChangeEmailBaseMutation(baseOptions?: Apollo.MutationHookOptions<ChangeEmailBaseMutation, ChangeEmailBaseMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeEmailBaseMutation, ChangeEmailBaseMutationVariables>(ChangeEmailBaseDocument, options);
      }
export type ChangeEmailBaseMutationHookResult = ReturnType<typeof useChangeEmailBaseMutation>;
export type ChangeEmailBaseMutationResult = Apollo.MutationResult<ChangeEmailBaseMutation>;
export type ChangeEmailBaseMutationOptions = Apollo.BaseMutationOptions<ChangeEmailBaseMutation, ChangeEmailBaseMutationVariables>;
export const ConfirmChangeEmailDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"ConfirmChangeEmail"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"code"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"user"},"name":{"kind":"Name","value":"confirmChangeEmail"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"code"},"value":{"kind":"Variable","name":{"kind":"Name","value":"code"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"changeEmailRequest"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"active"},"value":{"kind":"BooleanValue","value":true}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"newEmail"}}]}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useConfirmChangeEmailMutation__
 *
 * To run a mutation, you first call `useConfirmChangeEmailMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useConfirmChangeEmailMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [confirmChangeEmailMutation, { data, loading, error }] = useConfirmChangeEmailMutation({
 *   variables: {
 *      code: // value for 'code'
 *   },
 * });
 */
export function useConfirmChangeEmailMutation(baseOptions?: Apollo.MutationHookOptions<ConfirmChangeEmailMutation, ConfirmChangeEmailMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ConfirmChangeEmailMutation, ConfirmChangeEmailMutationVariables>(ConfirmChangeEmailDocument, options);
      }
export type ConfirmChangeEmailMutationHookResult = ReturnType<typeof useConfirmChangeEmailMutation>;
export type ConfirmChangeEmailMutationResult = Apollo.MutationResult<ConfirmChangeEmailMutation>;
export type ConfirmChangeEmailMutationOptions = Apollo.BaseMutationOptions<ConfirmChangeEmailMutation, ConfirmChangeEmailMutationVariables>;
export const CancelChangeEmailDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CancelChangeEmail"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"cancelChangeEmailRequest"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"changeEmailRequest"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"active"},"value":{"kind":"BooleanValue","value":true}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"newEmail"}}]}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useCancelChangeEmailMutation__
 *
 * To run a mutation, you first call `useCancelChangeEmailMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCancelChangeEmailMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [cancelChangeEmailMutation, { data, loading, error }] = useCancelChangeEmailMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useCancelChangeEmailMutation(baseOptions?: Apollo.MutationHookOptions<CancelChangeEmailMutation, CancelChangeEmailMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CancelChangeEmailMutation, CancelChangeEmailMutationVariables>(CancelChangeEmailDocument, options);
      }
export type CancelChangeEmailMutationHookResult = ReturnType<typeof useCancelChangeEmailMutation>;
export type CancelChangeEmailMutationResult = Apollo.MutationResult<CancelChangeEmailMutation>;
export type CancelChangeEmailMutationOptions = Apollo.BaseMutationOptions<CancelChangeEmailMutation, CancelChangeEmailMutationVariables>;
export const ChangePasswordDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"ChangePassword"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"currentPassword"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"newPassword"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"changePassword"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"currentPassword"},"value":{"kind":"Variable","name":{"kind":"Name","value":"currentPassword"}}},{"kind":"Argument","name":{"kind":"Name","value":"newPassword"},"value":{"kind":"Variable","name":{"kind":"Name","value":"newPassword"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"hasPassword"}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useChangePasswordMutation__
 *
 * To run a mutation, you first call `useChangePasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangePasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changePasswordMutation, { data, loading, error }] = useChangePasswordMutation({
 *   variables: {
 *      currentPassword: // value for 'currentPassword'
 *      newPassword: // value for 'newPassword'
 *   },
 * });
 */
export function useChangePasswordMutation(baseOptions?: Apollo.MutationHookOptions<ChangePasswordMutation, ChangePasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangePasswordMutation, ChangePasswordMutationVariables>(ChangePasswordDocument, options);
      }
export type ChangePasswordMutationHookResult = ReturnType<typeof useChangePasswordMutation>;
export type ChangePasswordMutationResult = Apollo.MutationResult<ChangePasswordMutation>;
export type ChangePasswordMutationOptions = Apollo.BaseMutationOptions<ChangePasswordMutation, ChangePasswordMutationVariables>;
export const ConfirmResetPasswordBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"ConfirmResetPasswordBase"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"code"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"password"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"confirmResetPassword"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"code"},"value":{"kind":"Variable","name":{"kind":"Name","value":"code"}}},{"kind":"Argument","name":{"kind":"Name","value":"password"},"value":{"kind":"Variable","name":{"kind":"Name","value":"password"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"BasicUserInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"BasicUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isMe"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}},{"kind":"Field","name":{"kind":"Name","value":"signUpDate"}}]}}]} as unknown as DocumentNode;

/**
 * __useConfirmResetPasswordBaseMutation__
 *
 * To run a mutation, you first call `useConfirmResetPasswordBaseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useConfirmResetPasswordBaseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [confirmResetPasswordBaseMutation, { data, loading, error }] = useConfirmResetPasswordBaseMutation({
 *   variables: {
 *      code: // value for 'code'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useConfirmResetPasswordBaseMutation(baseOptions?: Apollo.MutationHookOptions<ConfirmResetPasswordBaseMutation, ConfirmResetPasswordBaseMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ConfirmResetPasswordBaseMutation, ConfirmResetPasswordBaseMutationVariables>(ConfirmResetPasswordBaseDocument, options);
      }
export type ConfirmResetPasswordBaseMutationHookResult = ReturnType<typeof useConfirmResetPasswordBaseMutation>;
export type ConfirmResetPasswordBaseMutationResult = Apollo.MutationResult<ConfirmResetPasswordBaseMutation>;
export type ConfirmResetPasswordBaseMutationOptions = Apollo.BaseMutationOptions<ConfirmResetPasswordBaseMutation, ConfirmResetPasswordBaseMutationVariables>;
export const RegisterDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"Register"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"username"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"email"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"register"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"username"},"value":{"kind":"Variable","name":{"kind":"Name","value":"username"}}},{"kind":"Argument","name":{"kind":"Name","value":"email"},"value":{"kind":"Variable","name":{"kind":"Name","value":"email"}}}]}]}}]} as unknown as DocumentNode;

/**
 * __useRegisterMutation__
 *
 * To run a mutation, you first call `useRegisterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerMutation, { data, loading, error }] = useRegisterMutation({
 *   variables: {
 *      username: // value for 'username'
 *      email: // value for 'email'
 *   },
 * });
 */
export function useRegisterMutation(baseOptions?: Apollo.MutationHookOptions<RegisterMutation, RegisterMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterMutation, RegisterMutationVariables>(RegisterDocument, options);
      }
export type RegisterMutationHookResult = ReturnType<typeof useRegisterMutation>;
export type RegisterMutationResult = Apollo.MutationResult<RegisterMutation>;
export type RegisterMutationOptions = Apollo.BaseMutationOptions<RegisterMutation, RegisterMutationVariables>;
export const ResetPasswordDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"ResetPassword"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"email"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"resetPassword"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"email"},"value":{"kind":"Variable","name":{"kind":"Name","value":"email"}}}]}]}}]} as unknown as DocumentNode;

/**
 * __useResetPasswordMutation__
 *
 * To run a mutation, you first call `useResetPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useResetPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [resetPasswordMutation, { data, loading, error }] = useResetPasswordMutation({
 *   variables: {
 *      email: // value for 'email'
 *   },
 * });
 */
export function useResetPasswordMutation(baseOptions?: Apollo.MutationHookOptions<ResetPasswordMutation, ResetPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ResetPasswordMutation, ResetPasswordMutationVariables>(ResetPasswordDocument, options);
      }
export type ResetPasswordMutationHookResult = ReturnType<typeof useResetPasswordMutation>;
export type ResetPasswordMutationResult = Apollo.MutationResult<ResetPasswordMutation>;
export type ResetPasswordMutationOptions = Apollo.BaseMutationOptions<ResetPasswordMutation, ResetPasswordMutationVariables>;
export const AuditAddedDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"subscription","name":{"kind":"Name","value":"AuditAdded"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"auditAdded"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"AuditAdded"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"AuditAdded"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Audit"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"code"}},{"kind":"Field","name":{"kind":"Name","value":"created"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"parameters"}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DeckAudit"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deck"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UserAudit"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"otherUser"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useAuditAddedSubscription__
 *
 * To run a query within a React component, call `useAuditAddedSubscription` and pass it any options that fit your needs.
 * When your component renders, `useAuditAddedSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAuditAddedSubscription({
 *   variables: {
 *   },
 * });
 */
export function useAuditAddedSubscription(baseOptions?: Apollo.SubscriptionHookOptions<AuditAddedSubscription, AuditAddedSubscriptionVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useSubscription<AuditAddedSubscription, AuditAddedSubscriptionVariables>(AuditAddedDocument, options);
      }
export type AuditAddedSubscriptionHookResult = ReturnType<typeof useAuditAddedSubscription>;
export type AuditAddedSubscriptionResult = Apollo.SubscriptionResult<AuditAddedSubscription>;
export const LoginBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"LoginBase"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"username"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"password"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"login"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"username"},"value":{"kind":"Variable","name":{"kind":"Name","value":"username"}}},{"kind":"Argument","name":{"kind":"Name","value":"password"},"value":{"kind":"Variable","name":{"kind":"Name","value":"password"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MeUserInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"BasicUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isMe"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}},{"kind":"Field","name":{"kind":"Name","value":"signUpDate"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"MeUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"BasicUserInfo"}},{"kind":"Field","name":{"kind":"Name","value":"lastReadNewsId"}},{"kind":"Field","name":{"kind":"Name","value":"superuser"}}]}}]} as unknown as DocumentNode;

/**
 * __useLoginBaseMutation__
 *
 * To run a mutation, you first call `useLoginBaseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginBaseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginBaseMutation, { data, loading, error }] = useLoginBaseMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginBaseMutation(baseOptions?: Apollo.MutationHookOptions<LoginBaseMutation, LoginBaseMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LoginBaseMutation, LoginBaseMutationVariables>(LoginBaseDocument, options);
      }
export type LoginBaseMutationHookResult = ReturnType<typeof useLoginBaseMutation>;
export type LoginBaseMutationResult = Apollo.MutationResult<LoginBaseMutation>;
export type LoginBaseMutationOptions = Apollo.BaseMutationOptions<LoginBaseMutation, LoginBaseMutationVariables>;
export const LogoutBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"LogoutBase"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"logout"}}]}}]} as unknown as DocumentNode;

/**
 * __useLogoutBaseMutation__
 *
 * To run a mutation, you first call `useLogoutBaseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLogoutBaseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [logoutBaseMutation, { data, loading, error }] = useLogoutBaseMutation({
 *   variables: {
 *   },
 * });
 */
export function useLogoutBaseMutation(baseOptions?: Apollo.MutationHookOptions<LogoutBaseMutation, LogoutBaseMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LogoutBaseMutation, LogoutBaseMutationVariables>(LogoutBaseDocument, options);
      }
export type LogoutBaseMutationHookResult = ReturnType<typeof useLogoutBaseMutation>;
export type LogoutBaseMutationResult = Apollo.MutationResult<LogoutBaseMutation>;
export type LogoutBaseMutationOptions = Apollo.BaseMutationOptions<LogoutBaseMutation, LogoutBaseMutationVariables>;
export const ActivateDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"Activate"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"activationCode"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"password"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"displayName"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"activateAccount"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"activationCode"},"value":{"kind":"Variable","name":{"kind":"Name","value":"activationCode"}}},{"kind":"Argument","name":{"kind":"Name","value":"password"},"value":{"kind":"Variable","name":{"kind":"Name","value":"password"}}},{"kind":"Argument","name":{"kind":"Name","value":"displayName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"displayName"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"BasicUserInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"BasicUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isMe"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}},{"kind":"Field","name":{"kind":"Name","value":"signUpDate"}}]}}]} as unknown as DocumentNode;

/**
 * __useActivateMutation__
 *
 * To run a mutation, you first call `useActivateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useActivateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [activateMutation, { data, loading, error }] = useActivateMutation({
 *   variables: {
 *      activationCode: // value for 'activationCode'
 *      password: // value for 'password'
 *      displayName: // value for 'displayName'
 *   },
 * });
 */
export function useActivateMutation(baseOptions?: Apollo.MutationHookOptions<ActivateMutation, ActivateMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ActivateMutation, ActivateMutationVariables>(ActivateDocument, options);
      }
export type ActivateMutationHookResult = ReturnType<typeof useActivateMutation>;
export type ActivateMutationResult = Apollo.MutationResult<ActivateMutation>;
export type ActivateMutationOptions = Apollo.BaseMutationOptions<ActivateMutation, ActivateMutationVariables>;
export const CrosswordDetailDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"CrosswordDetail"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Long"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"crossword"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"CrosswordDetail"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ClueDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Clue"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"number"}},{"kind":"Field","name":{"kind":"Name","value":"direction"}},{"kind":"Field","name":{"kind":"Name","value":"x"}},{"kind":"Field","name":{"kind":"Name","value":"y"}},{"kind":"Field","name":{"kind":"Name","value":"length"}},{"kind":"Field","name":{"kind":"Name","value":"clue"}},{"kind":"Field","name":{"kind":"Name","value":"lengthHint"}},{"kind":"Field","name":{"kind":"Name","value":"answer"}},{"kind":"Field","name":{"kind":"Name","value":"originalAnswer"}},{"kind":"Field","name":{"kind":"Name","value":"deckId"}},{"kind":"Field","name":{"kind":"Name","value":"termId"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"GuessInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Guess"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"attemptId"}},{"kind":"Field","name":{"kind":"Name","value":"x"}},{"kind":"Field","name":{"kind":"Name","value":"y"}},{"kind":"Field","name":{"kind":"Name","value":"guess"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"CrosswordDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Crossword"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"stars"}},{"kind":"Field","name":{"kind":"Name","value":"created"}},{"kind":"Field","name":{"kind":"Name","value":"available"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"imageUrl"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"clues"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ClueDetail"}}]}},{"kind":"Field","name":{"kind":"Name","value":"decks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"attempt"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"started"}},{"kind":"Field","name":{"kind":"Name","value":"lastPlayed"}},{"kind":"Field","name":{"kind":"Name","value":"completed"}},{"kind":"Field","name":{"kind":"Name","value":"percentCompleted"}},{"kind":"Field","name":{"kind":"Name","value":"guesses"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"GuessInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"score"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;

/**
 * __useCrosswordDetailQuery__
 *
 * To run a query within a React component, call `useCrosswordDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `useCrosswordDetailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCrosswordDetailQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useCrosswordDetailQuery(baseOptions: Apollo.QueryHookOptions<CrosswordDetailQuery, CrosswordDetailQueryVariables> & ({ variables: CrosswordDetailQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CrosswordDetailQuery, CrosswordDetailQueryVariables>(CrosswordDetailDocument, options);
      }
export function useCrosswordDetailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CrosswordDetailQuery, CrosswordDetailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CrosswordDetailQuery, CrosswordDetailQueryVariables>(CrosswordDetailDocument, options);
        }
export function useCrosswordDetailSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<CrosswordDetailQuery, CrosswordDetailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<CrosswordDetailQuery, CrosswordDetailQueryVariables>(CrosswordDetailDocument, options);
        }
export type CrosswordDetailQueryHookResult = ReturnType<typeof useCrosswordDetailQuery>;
export type CrosswordDetailLazyQueryHookResult = ReturnType<typeof useCrosswordDetailLazyQuery>;
export type CrosswordDetailSuspenseQueryHookResult = ReturnType<typeof useCrosswordDetailSuspenseQuery>;
export type CrosswordDetailQueryResult = Apollo.QueryResult<CrosswordDetailQuery, CrosswordDetailQueryVariables>;
export const AddCrosswordBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"AddCrosswordBase"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"deckIds"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Long"}}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"languageId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"width"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"height"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"addCrossword"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"deckIds"},"value":{"kind":"Variable","name":{"kind":"Name","value":"deckIds"}}},{"kind":"Argument","name":{"kind":"Name","value":"languageId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"languageId"}}},{"kind":"Argument","name":{"kind":"Name","value":"width"},"value":{"kind":"Variable","name":{"kind":"Name","value":"width"}}},{"kind":"Argument","name":{"kind":"Name","value":"height"},"value":{"kind":"Variable","name":{"kind":"Name","value":"height"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"CrosswordCardInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"CrosswordCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Crossword"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"created"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"imageUrl"}},{"kind":"Field","name":{"kind":"Name","value":"stars"}},{"kind":"Field","name":{"kind":"Name","value":"attempt"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"completed"}},{"kind":"Field","name":{"kind":"Name","value":"percentCompleted"}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useAddCrosswordBaseMutation__
 *
 * To run a mutation, you first call `useAddCrosswordBaseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddCrosswordBaseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addCrosswordBaseMutation, { data, loading, error }] = useAddCrosswordBaseMutation({
 *   variables: {
 *      deckIds: // value for 'deckIds'
 *      languageId: // value for 'languageId'
 *      width: // value for 'width'
 *      height: // value for 'height'
 *   },
 * });
 */
export function useAddCrosswordBaseMutation(baseOptions?: Apollo.MutationHookOptions<AddCrosswordBaseMutation, AddCrosswordBaseMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddCrosswordBaseMutation, AddCrosswordBaseMutationVariables>(AddCrosswordBaseDocument, options);
      }
export type AddCrosswordBaseMutationHookResult = ReturnType<typeof useAddCrosswordBaseMutation>;
export type AddCrosswordBaseMutationResult = Apollo.MutationResult<AddCrosswordBaseMutation>;
export type AddCrosswordBaseMutationOptions = Apollo.BaseMutationOptions<AddCrosswordBaseMutation, AddCrosswordBaseMutationVariables>;
export const SaveAttemptBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"SaveAttemptBase"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"attempt"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"UpdateAttemptRequest"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"complete"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"saveAttempt"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"attempt"},"value":{"kind":"Variable","name":{"kind":"Name","value":"attempt"}}},{"kind":"Argument","name":{"kind":"Name","value":"complete"},"value":{"kind":"Variable","name":{"kind":"Name","value":"complete"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"completed"}},{"kind":"Field","name":{"kind":"Name","value":"percentCompleted"}},{"kind":"Field","name":{"kind":"Name","value":"score"}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useSaveAttemptBaseMutation__
 *
 * To run a mutation, you first call `useSaveAttemptBaseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSaveAttemptBaseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [saveAttemptBaseMutation, { data, loading, error }] = useSaveAttemptBaseMutation({
 *   variables: {
 *      attempt: // value for 'attempt'
 *      complete: // value for 'complete'
 *   },
 * });
 */
export function useSaveAttemptBaseMutation(baseOptions?: Apollo.MutationHookOptions<SaveAttemptBaseMutation, SaveAttemptBaseMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SaveAttemptBaseMutation, SaveAttemptBaseMutationVariables>(SaveAttemptBaseDocument, options);
      }
export type SaveAttemptBaseMutationHookResult = ReturnType<typeof useSaveAttemptBaseMutation>;
export type SaveAttemptBaseMutationResult = Apollo.MutationResult<SaveAttemptBaseMutation>;
export type SaveAttemptBaseMutationOptions = Apollo.BaseMutationOptions<SaveAttemptBaseMutation, SaveAttemptBaseMutationVariables>;
export const CompleteAttemptDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CompleteAttempt"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"attempt"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"UpdateAttemptRequest"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"completeAttempt"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"attempt"},"value":{"kind":"Variable","name":{"kind":"Name","value":"attempt"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"clues"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ClueDetail"}}]}},{"kind":"Field","name":{"kind":"Name","value":"attempt"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"completed"}},{"kind":"Field","name":{"kind":"Name","value":"percentCompleted"}},{"kind":"Field","name":{"kind":"Name","value":"score"}},{"kind":"Field","name":{"kind":"Name","value":"guesses"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"GuessInfo"}}]}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ClueDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Clue"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"number"}},{"kind":"Field","name":{"kind":"Name","value":"direction"}},{"kind":"Field","name":{"kind":"Name","value":"x"}},{"kind":"Field","name":{"kind":"Name","value":"y"}},{"kind":"Field","name":{"kind":"Name","value":"length"}},{"kind":"Field","name":{"kind":"Name","value":"clue"}},{"kind":"Field","name":{"kind":"Name","value":"lengthHint"}},{"kind":"Field","name":{"kind":"Name","value":"answer"}},{"kind":"Field","name":{"kind":"Name","value":"originalAnswer"}},{"kind":"Field","name":{"kind":"Name","value":"deckId"}},{"kind":"Field","name":{"kind":"Name","value":"termId"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"GuessInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Guess"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"attemptId"}},{"kind":"Field","name":{"kind":"Name","value":"x"}},{"kind":"Field","name":{"kind":"Name","value":"y"}},{"kind":"Field","name":{"kind":"Name","value":"guess"}}]}}]} as unknown as DocumentNode;

/**
 * __useCompleteAttemptMutation__
 *
 * To run a mutation, you first call `useCompleteAttemptMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCompleteAttemptMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [completeAttemptMutation, { data, loading, error }] = useCompleteAttemptMutation({
 *   variables: {
 *      attempt: // value for 'attempt'
 *   },
 * });
 */
export function useCompleteAttemptMutation(baseOptions?: Apollo.MutationHookOptions<CompleteAttemptMutation, CompleteAttemptMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CompleteAttemptMutation, CompleteAttemptMutationVariables>(CompleteAttemptDocument, options);
      }
export type CompleteAttemptMutationHookResult = ReturnType<typeof useCompleteAttemptMutation>;
export type CompleteAttemptMutationResult = Apollo.MutationResult<CompleteAttemptMutation>;
export type CompleteAttemptMutationOptions = Apollo.BaseMutationOptions<CompleteAttemptMutation, CompleteAttemptMutationVariables>;
export const DeckDetailDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"DeckDetail"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Long"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deck"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckDetail"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"CrosswordCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Crossword"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"created"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"imageUrl"}},{"kind":"Field","name":{"kind":"Name","value":"stars"}},{"kind":"Field","name":{"kind":"Name","value":"attempt"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"completed"}},{"kind":"Field","name":{"kind":"Name","value":"percentCompleted"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TermDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"example"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"score"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userId"}},{"kind":"Field","name":{"kind":"Name","value":"termId"}},{"kind":"Field","name":{"kind":"Name","value":"attempts"}},{"kind":"Field","name":{"kind":"Name","value":"successes"}},{"kind":"Field","name":{"kind":"Name","value":"score"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}},{"kind":"Field","name":{"kind":"Name","value":"terms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"TermDetail"}}]}},{"kind":"Field","name":{"kind":"Name","value":"crosswords"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"CrosswordCardInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;

/**
 * __useDeckDetailQuery__
 *
 * To run a query within a React component, call `useDeckDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `useDeckDetailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDeckDetailQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeckDetailQuery(baseOptions: Apollo.QueryHookOptions<DeckDetailQuery, DeckDetailQueryVariables> & ({ variables: DeckDetailQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<DeckDetailQuery, DeckDetailQueryVariables>(DeckDetailDocument, options);
      }
export function useDeckDetailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<DeckDetailQuery, DeckDetailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<DeckDetailQuery, DeckDetailQueryVariables>(DeckDetailDocument, options);
        }
export function useDeckDetailSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<DeckDetailQuery, DeckDetailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<DeckDetailQuery, DeckDetailQueryVariables>(DeckDetailDocument, options);
        }
export type DeckDetailQueryHookResult = ReturnType<typeof useDeckDetailQuery>;
export type DeckDetailLazyQueryHookResult = ReturnType<typeof useDeckDetailLazyQuery>;
export type DeckDetailSuspenseQueryHookResult = ReturnType<typeof useDeckDetailSuspenseQuery>;
export type DeckDetailQueryResult = Apollo.QueryResult<DeckDetailQuery, DeckDetailQueryVariables>;
export const FavouriteDeckIdsBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"FavouriteDeckIdsBase"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"me"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"favouriteDecks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useFavouriteDeckIdsBaseQuery__
 *
 * To run a query within a React component, call `useFavouriteDeckIdsBaseQuery` and pass it any options that fit your needs.
 * When your component renders, `useFavouriteDeckIdsBaseQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFavouriteDeckIdsBaseQuery({
 *   variables: {
 *   },
 * });
 */
export function useFavouriteDeckIdsBaseQuery(baseOptions?: Apollo.QueryHookOptions<FavouriteDeckIdsBaseQuery, FavouriteDeckIdsBaseQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FavouriteDeckIdsBaseQuery, FavouriteDeckIdsBaseQueryVariables>(FavouriteDeckIdsBaseDocument, options);
      }
export function useFavouriteDeckIdsBaseLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FavouriteDeckIdsBaseQuery, FavouriteDeckIdsBaseQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FavouriteDeckIdsBaseQuery, FavouriteDeckIdsBaseQueryVariables>(FavouriteDeckIdsBaseDocument, options);
        }
export function useFavouriteDeckIdsBaseSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<FavouriteDeckIdsBaseQuery, FavouriteDeckIdsBaseQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<FavouriteDeckIdsBaseQuery, FavouriteDeckIdsBaseQueryVariables>(FavouriteDeckIdsBaseDocument, options);
        }
export type FavouriteDeckIdsBaseQueryHookResult = ReturnType<typeof useFavouriteDeckIdsBaseQuery>;
export type FavouriteDeckIdsBaseLazyQueryHookResult = ReturnType<typeof useFavouriteDeckIdsBaseLazyQuery>;
export type FavouriteDeckIdsBaseSuspenseQueryHookResult = ReturnType<typeof useFavouriteDeckIdsBaseSuspenseQuery>;
export type FavouriteDeckIdsBaseQueryResult = Apollo.QueryResult<FavouriteDeckIdsBaseQuery, FavouriteDeckIdsBaseQueryVariables>;
export const TermInfoDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"TermInfo"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Long"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"term"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"TermInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TermInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"example"}},{"kind":"Field","name":{"kind":"Name","value":"deck"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useTermInfoQuery__
 *
 * To run a query within a React component, call `useTermInfoQuery` and pass it any options that fit your needs.
 * When your component renders, `useTermInfoQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTermInfoQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useTermInfoQuery(baseOptions: Apollo.QueryHookOptions<TermInfoQuery, TermInfoQueryVariables> & ({ variables: TermInfoQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TermInfoQuery, TermInfoQueryVariables>(TermInfoDocument, options);
      }
export function useTermInfoLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TermInfoQuery, TermInfoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TermInfoQuery, TermInfoQueryVariables>(TermInfoDocument, options);
        }
export function useTermInfoSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<TermInfoQuery, TermInfoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<TermInfoQuery, TermInfoQueryVariables>(TermInfoDocument, options);
        }
export type TermInfoQueryHookResult = ReturnType<typeof useTermInfoQuery>;
export type TermInfoLazyQueryHookResult = ReturnType<typeof useTermInfoLazyQuery>;
export type TermInfoSuspenseQueryHookResult = ReturnType<typeof useTermInfoSuspenseQuery>;
export type TermInfoQueryResult = Apollo.QueryResult<TermInfoQuery, TermInfoQueryVariables>;
export const SuggestTermsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"SuggestTerms"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"search"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"termLang"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"excludeDeckId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"suggestTerms"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"search"},"value":{"kind":"Variable","name":{"kind":"Name","value":"search"}}},{"kind":"Argument","name":{"kind":"Name","value":"termLang"},"value":{"kind":"Variable","name":{"kind":"Name","value":"termLang"}}},{"kind":"Argument","name":{"kind":"Name","value":"excludeDeckId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"excludeDeckId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"TermSuggestion"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TermSuggestion"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"deck"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useSuggestTermsQuery__
 *
 * To run a query within a React component, call `useSuggestTermsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSuggestTermsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSuggestTermsQuery({
 *   variables: {
 *      search: // value for 'search'
 *      termLang: // value for 'termLang'
 *      excludeDeckId: // value for 'excludeDeckId'
 *   },
 * });
 */
export function useSuggestTermsQuery(baseOptions: Apollo.QueryHookOptions<SuggestTermsQuery, SuggestTermsQueryVariables> & ({ variables: SuggestTermsQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SuggestTermsQuery, SuggestTermsQueryVariables>(SuggestTermsDocument, options);
      }
export function useSuggestTermsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SuggestTermsQuery, SuggestTermsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SuggestTermsQuery, SuggestTermsQueryVariables>(SuggestTermsDocument, options);
        }
export function useSuggestTermsSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<SuggestTermsQuery, SuggestTermsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<SuggestTermsQuery, SuggestTermsQueryVariables>(SuggestTermsDocument, options);
        }
export type SuggestTermsQueryHookResult = ReturnType<typeof useSuggestTermsQuery>;
export type SuggestTermsLazyQueryHookResult = ReturnType<typeof useSuggestTermsLazyQuery>;
export type SuggestTermsSuspenseQueryHookResult = ReturnType<typeof useSuggestTermsSuspenseQuery>;
export type SuggestTermsQueryResult = Apollo.QueryResult<SuggestTermsQuery, SuggestTermsQueryVariables>;
export const CreateDeckDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateDeck"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"request"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"CreateDeckRequest"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createDeck"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"request"},"value":{"kind":"Variable","name":{"kind":"Name","value":"request"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"decks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}}]}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;

/**
 * __useCreateDeckMutation__
 *
 * To run a mutation, you first call `useCreateDeckMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateDeckMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createDeckMutation, { data, loading, error }] = useCreateDeckMutation({
 *   variables: {
 *      request: // value for 'request'
 *   },
 * });
 */
export function useCreateDeckMutation(baseOptions?: Apollo.MutationHookOptions<CreateDeckMutation, CreateDeckMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateDeckMutation, CreateDeckMutationVariables>(CreateDeckDocument, options);
      }
export type CreateDeckMutationHookResult = ReturnType<typeof useCreateDeckMutation>;
export type CreateDeckMutationResult = Apollo.MutationResult<CreateDeckMutation>;
export type CreateDeckMutationOptions = Apollo.BaseMutationOptions<CreateDeckMutation, CreateDeckMutationVariables>;
export const ToggleDeckFavouriteBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"ToggleDeckFavouriteBase"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Long"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"favourite"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"toggleSetFavourite"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"favourite"},"value":{"kind":"Variable","name":{"kind":"Name","value":"favourite"}}}]}]}}]} as unknown as DocumentNode;

/**
 * __useToggleDeckFavouriteBaseMutation__
 *
 * To run a mutation, you first call `useToggleDeckFavouriteBaseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useToggleDeckFavouriteBaseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [toggleDeckFavouriteBaseMutation, { data, loading, error }] = useToggleDeckFavouriteBaseMutation({
 *   variables: {
 *      id: // value for 'id'
 *      favourite: // value for 'favourite'
 *   },
 * });
 */
export function useToggleDeckFavouriteBaseMutation(baseOptions?: Apollo.MutationHookOptions<ToggleDeckFavouriteBaseMutation, ToggleDeckFavouriteBaseMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ToggleDeckFavouriteBaseMutation, ToggleDeckFavouriteBaseMutationVariables>(ToggleDeckFavouriteBaseDocument, options);
      }
export type ToggleDeckFavouriteBaseMutationHookResult = ReturnType<typeof useToggleDeckFavouriteBaseMutation>;
export type ToggleDeckFavouriteBaseMutationResult = Apollo.MutationResult<ToggleDeckFavouriteBaseMutation>;
export type ToggleDeckFavouriteBaseMutationOptions = Apollo.BaseMutationOptions<ToggleDeckFavouriteBaseMutation, ToggleDeckFavouriteBaseMutationVariables>;
export const UpdateDeckDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateDeck"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"request"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"UpdateDeckRequest"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateDeck"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"request"},"value":{"kind":"Variable","name":{"kind":"Name","value":"request"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckDetail"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"CrosswordCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Crossword"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"created"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"imageUrl"}},{"kind":"Field","name":{"kind":"Name","value":"stars"}},{"kind":"Field","name":{"kind":"Name","value":"attempt"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"completed"}},{"kind":"Field","name":{"kind":"Name","value":"percentCompleted"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TermDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"example"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"score"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userId"}},{"kind":"Field","name":{"kind":"Name","value":"termId"}},{"kind":"Field","name":{"kind":"Name","value":"attempts"}},{"kind":"Field","name":{"kind":"Name","value":"successes"}},{"kind":"Field","name":{"kind":"Name","value":"score"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}},{"kind":"Field","name":{"kind":"Name","value":"terms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"TermDetail"}}]}},{"kind":"Field","name":{"kind":"Name","value":"crosswords"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"CrosswordCardInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;

/**
 * __useUpdateDeckMutation__
 *
 * To run a mutation, you first call `useUpdateDeckMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateDeckMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateDeckMutation, { data, loading, error }] = useUpdateDeckMutation({
 *   variables: {
 *      request: // value for 'request'
 *   },
 * });
 */
export function useUpdateDeckMutation(baseOptions?: Apollo.MutationHookOptions<UpdateDeckMutation, UpdateDeckMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateDeckMutation, UpdateDeckMutationVariables>(UpdateDeckDocument, options);
      }
export type UpdateDeckMutationHookResult = ReturnType<typeof useUpdateDeckMutation>;
export type UpdateDeckMutationResult = Apollo.MutationResult<UpdateDeckMutation>;
export type UpdateDeckMutationOptions = Apollo.BaseMutationOptions<UpdateDeckMutation, UpdateDeckMutationVariables>;
export const UpdateTermBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateTermBase"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"request"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"UpdateTermRequest"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateTerm"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"request"},"value":{"kind":"Variable","name":{"kind":"Name","value":"request"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"TermDetail"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TermDetail"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"example"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"score"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userId"}},{"kind":"Field","name":{"kind":"Name","value":"termId"}},{"kind":"Field","name":{"kind":"Name","value":"attempts"}},{"kind":"Field","name":{"kind":"Name","value":"successes"}},{"kind":"Field","name":{"kind":"Name","value":"score"}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useUpdateTermBaseMutation__
 *
 * To run a mutation, you first call `useUpdateTermBaseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateTermBaseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateTermBaseMutation, { data, loading, error }] = useUpdateTermBaseMutation({
 *   variables: {
 *      request: // value for 'request'
 *   },
 * });
 */
export function useUpdateTermBaseMutation(baseOptions?: Apollo.MutationHookOptions<UpdateTermBaseMutation, UpdateTermBaseMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateTermBaseMutation, UpdateTermBaseMutationVariables>(UpdateTermBaseDocument, options);
      }
export type UpdateTermBaseMutationHookResult = ReturnType<typeof useUpdateTermBaseMutation>;
export type UpdateTermBaseMutationResult = Apollo.MutationResult<UpdateTermBaseMutation>;
export type UpdateTermBaseMutationOptions = Apollo.BaseMutationOptions<UpdateTermBaseMutation, UpdateTermBaseMutationVariables>;
export const HomeDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"Home"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"me"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"decks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"recentAttempts"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"count"},"value":{"kind":"IntValue","value":"6"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"AttemptCardInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"languages"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"UserLanguageStats"}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"CrosswordCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Crossword"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"created"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"imageUrl"}},{"kind":"Field","name":{"kind":"Name","value":"stars"}},{"kind":"Field","name":{"kind":"Name","value":"attempt"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"completed"}},{"kind":"Field","name":{"kind":"Name","value":"percentCompleted"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"AttemptCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Attempt"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"lastPlayed"}},{"kind":"Field","name":{"kind":"Name","value":"crossword"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"CrosswordCardInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"UserLanguageStats"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UserLanguage"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"lastActivity"}},{"kind":"Field","name":{"kind":"Name","value":"crosswordsCreated"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;

/**
 * __useHomeQuery__
 *
 * To run a query within a React component, call `useHomeQuery` and pass it any options that fit your needs.
 * When your component renders, `useHomeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useHomeQuery({
 *   variables: {
 *   },
 * });
 */
export function useHomeQuery(baseOptions?: Apollo.QueryHookOptions<HomeQuery, HomeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<HomeQuery, HomeQueryVariables>(HomeDocument, options);
      }
export function useHomeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<HomeQuery, HomeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<HomeQuery, HomeQueryVariables>(HomeDocument, options);
        }
export function useHomeSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<HomeQuery, HomeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<HomeQuery, HomeQueryVariables>(HomeDocument, options);
        }
export type HomeQueryHookResult = ReturnType<typeof useHomeQuery>;
export type HomeLazyQueryHookResult = ReturnType<typeof useHomeLazyQuery>;
export type HomeSuspenseQueryHookResult = ReturnType<typeof useHomeSuspenseQuery>;
export type HomeQueryResult = Apollo.QueryResult<HomeQuery, HomeQueryVariables>;
export const TopDecksDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"TopDecks"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"termLang"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"topDecks"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"termLang"},"value":{"kind":"Variable","name":{"kind":"Name","value":"termLang"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"TopDeck"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TopDeck"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}},{"kind":"Field","name":{"kind":"Name","value":"terms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;

/**
 * __useTopDecksQuery__
 *
 * To run a query within a React component, call `useTopDecksQuery` and pass it any options that fit your needs.
 * When your component renders, `useTopDecksQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTopDecksQuery({
 *   variables: {
 *      termLang: // value for 'termLang'
 *   },
 * });
 */
export function useTopDecksQuery(baseOptions: Apollo.QueryHookOptions<TopDecksQuery, TopDecksQueryVariables> & ({ variables: TopDecksQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TopDecksQuery, TopDecksQueryVariables>(TopDecksDocument, options);
      }
export function useTopDecksLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TopDecksQuery, TopDecksQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TopDecksQuery, TopDecksQueryVariables>(TopDecksDocument, options);
        }
export function useTopDecksSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<TopDecksQuery, TopDecksQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<TopDecksQuery, TopDecksQueryVariables>(TopDecksDocument, options);
        }
export type TopDecksQueryHookResult = ReturnType<typeof useTopDecksQuery>;
export type TopDecksLazyQueryHookResult = ReturnType<typeof useTopDecksLazyQuery>;
export type TopDecksSuspenseQueryHookResult = ReturnType<typeof useTopDecksSuspenseQuery>;
export type TopDecksQueryResult = Apollo.QueryResult<TopDecksQuery, TopDecksQueryVariables>;
export const TermsToStudyDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"TermsToStudy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"me"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"hardestTerms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ScoreInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"recentFailures"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ScoreInfo"}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ScoreInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Score"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userId"}},{"kind":"Field","name":{"kind":"Name","value":"termId"}},{"kind":"Field","name":{"kind":"Name","value":"score"}},{"kind":"Field","name":{"kind":"Name","value":"successes"}},{"kind":"Field","name":{"kind":"Name","value":"attempts"}},{"kind":"Field","name":{"kind":"Name","value":"lastPlayed"}},{"kind":"Field","name":{"kind":"Name","value":"lastResultSuccess"}},{"kind":"Field","name":{"kind":"Name","value":"term"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"deck"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useTermsToStudyQuery__
 *
 * To run a query within a React component, call `useTermsToStudyQuery` and pass it any options that fit your needs.
 * When your component renders, `useTermsToStudyQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTermsToStudyQuery({
 *   variables: {
 *   },
 * });
 */
export function useTermsToStudyQuery(baseOptions?: Apollo.QueryHookOptions<TermsToStudyQuery, TermsToStudyQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TermsToStudyQuery, TermsToStudyQueryVariables>(TermsToStudyDocument, options);
      }
export function useTermsToStudyLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TermsToStudyQuery, TermsToStudyQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TermsToStudyQuery, TermsToStudyQueryVariables>(TermsToStudyDocument, options);
        }
export function useTermsToStudySuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<TermsToStudyQuery, TermsToStudyQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<TermsToStudyQuery, TermsToStudyQueryVariables>(TermsToStudyDocument, options);
        }
export type TermsToStudyQueryHookResult = ReturnType<typeof useTermsToStudyQuery>;
export type TermsToStudyLazyQueryHookResult = ReturnType<typeof useTermsToStudyLazyQuery>;
export type TermsToStudySuspenseQueryHookResult = ReturnType<typeof useTermsToStudySuspenseQuery>;
export type TermsToStudyQueryResult = Apollo.QueryResult<TermsToStudyQuery, TermsToStudyQueryVariables>;
export const RecentActivityBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"RecentActivityBase"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"count"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"cursor"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"recentActivity"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"count"},"value":{"kind":"Variable","name":{"kind":"Name","value":"count"}}},{"kind":"Argument","name":{"kind":"Name","value":"after"},"value":{"kind":"Variable","name":{"kind":"Name","value":"cursor"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"activities"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ActivityInfo"}},{"kind":"Field","name":{"kind":"Name","value":"cursor"}}]}},{"kind":"Field","name":{"kind":"Name","value":"page"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"endCursor"}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ActivityInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Activity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"BasicUserInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"decks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"crosswordIds"}},{"kind":"Field","name":{"kind":"Name","value":"parameters"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"BasicUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isMe"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}},{"kind":"Field","name":{"kind":"Name","value":"signUpDate"}}]}}]} as unknown as DocumentNode;

/**
 * __useRecentActivityBaseQuery__
 *
 * To run a query within a React component, call `useRecentActivityBaseQuery` and pass it any options that fit your needs.
 * When your component renders, `useRecentActivityBaseQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRecentActivityBaseQuery({
 *   variables: {
 *      count: // value for 'count'
 *      cursor: // value for 'cursor'
 *   },
 * });
 */
export function useRecentActivityBaseQuery(baseOptions: Apollo.QueryHookOptions<RecentActivityBaseQuery, RecentActivityBaseQueryVariables> & ({ variables: RecentActivityBaseQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<RecentActivityBaseQuery, RecentActivityBaseQueryVariables>(RecentActivityBaseDocument, options);
      }
export function useRecentActivityBaseLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<RecentActivityBaseQuery, RecentActivityBaseQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<RecentActivityBaseQuery, RecentActivityBaseQueryVariables>(RecentActivityBaseDocument, options);
        }
export function useRecentActivityBaseSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<RecentActivityBaseQuery, RecentActivityBaseQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<RecentActivityBaseQuery, RecentActivityBaseQueryVariables>(RecentActivityBaseDocument, options);
        }
export type RecentActivityBaseQueryHookResult = ReturnType<typeof useRecentActivityBaseQuery>;
export type RecentActivityBaseLazyQueryHookResult = ReturnType<typeof useRecentActivityBaseLazyQuery>;
export type RecentActivityBaseSuspenseQueryHookResult = ReturnType<typeof useRecentActivityBaseSuspenseQuery>;
export type RecentActivityBaseQueryResult = Apollo.QueryResult<RecentActivityBaseQuery, RecentActivityBaseQueryVariables>;
export const LanguagesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"Languages"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"languages"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useLanguagesQuery__
 *
 * To run a query within a React component, call `useLanguagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useLanguagesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useLanguagesQuery({
 *   variables: {
 *   },
 * });
 */
export function useLanguagesQuery(baseOptions?: Apollo.QueryHookOptions<LanguagesQuery, LanguagesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<LanguagesQuery, LanguagesQueryVariables>(LanguagesDocument, options);
      }
export function useLanguagesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<LanguagesQuery, LanguagesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<LanguagesQuery, LanguagesQueryVariables>(LanguagesDocument, options);
        }
export function useLanguagesSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<LanguagesQuery, LanguagesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<LanguagesQuery, LanguagesQueryVariables>(LanguagesDocument, options);
        }
export type LanguagesQueryHookResult = ReturnType<typeof useLanguagesQuery>;
export type LanguagesLazyQueryHookResult = ReturnType<typeof useLanguagesLazyQuery>;
export type LanguagesSuspenseQueryHookResult = ReturnType<typeof useLanguagesSuspenseQuery>;
export type LanguagesQueryResult = Apollo.QueryResult<LanguagesQuery, LanguagesQueryVariables>;
export const LanguageInfoDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"LanguageInfo"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"language"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]} as unknown as DocumentNode;

/**
 * __useLanguageInfoQuery__
 *
 * To run a query within a React component, call `useLanguageInfoQuery` and pass it any options that fit your needs.
 * When your component renders, `useLanguageInfoQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useLanguageInfoQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useLanguageInfoQuery(baseOptions: Apollo.QueryHookOptions<LanguageInfoQuery, LanguageInfoQueryVariables> & ({ variables: LanguageInfoQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<LanguageInfoQuery, LanguageInfoQueryVariables>(LanguageInfoDocument, options);
      }
export function useLanguageInfoLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<LanguageInfoQuery, LanguageInfoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<LanguageInfoQuery, LanguageInfoQueryVariables>(LanguageInfoDocument, options);
        }
export function useLanguageInfoSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<LanguageInfoQuery, LanguageInfoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<LanguageInfoQuery, LanguageInfoQueryVariables>(LanguageInfoDocument, options);
        }
export type LanguageInfoQueryHookResult = ReturnType<typeof useLanguageInfoQuery>;
export type LanguageInfoLazyQueryHookResult = ReturnType<typeof useLanguageInfoLazyQuery>;
export type LanguageInfoSuspenseQueryHookResult = ReturnType<typeof useLanguageInfoSuspenseQuery>;
export type LanguageInfoQueryResult = Apollo.QueryResult<LanguageInfoQuery, LanguageInfoQueryVariables>;
export const GlobalSearchDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GlobalSearch"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"search"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"results"},"name":{"kind":"Name","value":"searchDecksAndTerms"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"search"},"value":{"kind":"Variable","name":{"kind":"Name","value":"search"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"SearchResult"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"TermInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"term"}},{"kind":"Field","name":{"kind":"Name","value":"definitions"}},{"kind":"Field","name":{"kind":"Name","value":"example"}},{"kind":"Field","name":{"kind":"Name","value":"deck"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"SearchResult"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"DeckSearchResult"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}}]}},{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Term"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"TermInfo"}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useGlobalSearchQuery__
 *
 * To run a query within a React component, call `useGlobalSearchQuery` and pass it any options that fit your needs.
 * When your component renders, `useGlobalSearchQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGlobalSearchQuery({
 *   variables: {
 *      search: // value for 'search'
 *   },
 * });
 */
export function useGlobalSearchQuery(baseOptions: Apollo.QueryHookOptions<GlobalSearchQuery, GlobalSearchQueryVariables> & ({ variables: GlobalSearchQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GlobalSearchQuery, GlobalSearchQueryVariables>(GlobalSearchDocument, options);
      }
export function useGlobalSearchLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GlobalSearchQuery, GlobalSearchQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GlobalSearchQuery, GlobalSearchQueryVariables>(GlobalSearchDocument, options);
        }
export function useGlobalSearchSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<GlobalSearchQuery, GlobalSearchQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<GlobalSearchQuery, GlobalSearchQueryVariables>(GlobalSearchDocument, options);
        }
export type GlobalSearchQueryHookResult = ReturnType<typeof useGlobalSearchQuery>;
export type GlobalSearchLazyQueryHookResult = ReturnType<typeof useGlobalSearchLazyQuery>;
export type GlobalSearchSuspenseQueryHookResult = ReturnType<typeof useGlobalSearchSuspenseQuery>;
export type GlobalSearchQueryResult = Apollo.QueryResult<GlobalSearchQuery, GlobalSearchQueryVariables>;
export const UserDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"User"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"name"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"self"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"user"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"name"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"UserProfile"}},{"kind":"FragmentSpread","name":{"kind":"Name","value":"ManageAccount"},"directives":[{"kind":"Directive","name":{"kind":"Name","value":"include"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"if"},"value":{"kind":"Variable","name":{"kind":"Name","value":"self"}}}]}]}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"DeckCardInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Deck"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"termCount"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"createdBy"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}}]}},{"kind":"Field","name":{"kind":"Name","value":"termLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"definitionLang"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"BasicUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isMe"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}},{"kind":"Field","name":{"kind":"Name","value":"signUpDate"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"UserProfile"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"BasicUserInfo"}},{"kind":"Field","name":{"kind":"Name","value":"followed"}},{"kind":"Field","name":{"kind":"Name","value":"languages"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"decks"}},{"kind":"Field","name":{"kind":"Name","value":"crosswordsCreated"}},{"kind":"Field","name":{"kind":"Name","value":"crosswordsCompleted"}},{"kind":"Field","name":{"kind":"Name","value":"started"}},{"kind":"Field","name":{"kind":"Name","value":"lastActivity"}}]}},{"kind":"Field","name":{"kind":"Name","value":"decks"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"DeckCardInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ManageAccount"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"changeEmailRequest"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"active"},"value":{"kind":"BooleanValue","value":true}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"newEmail"}}]}},{"kind":"Field","name":{"kind":"Name","value":"connectedAccounts"}},{"kind":"Field","name":{"kind":"Name","value":"hasPassword"}}]}}]} as unknown as DocumentNode;

/**
 * __useUserQuery__
 *
 * To run a query within a React component, call `useUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserQuery({
 *   variables: {
 *      name: // value for 'name'
 *      self: // value for 'self'
 *   },
 * });
 */
export function useUserQuery(baseOptions: Apollo.QueryHookOptions<UserQuery, UserQueryVariables> & ({ variables: UserQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<UserQuery, UserQueryVariables>(UserDocument, options);
      }
export function useUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UserQuery, UserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<UserQuery, UserQueryVariables>(UserDocument, options);
        }
export function useUserSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<UserQuery, UserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<UserQuery, UserQueryVariables>(UserDocument, options);
        }
export type UserQueryHookResult = ReturnType<typeof useUserQuery>;
export type UserLazyQueryHookResult = ReturnType<typeof useUserLazyQuery>;
export type UserSuspenseQueryHookResult = ReturnType<typeof useUserSuspenseQuery>;
export type UserQueryResult = Apollo.QueryResult<UserQuery, UserQueryVariables>;
export const MeBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"MeBase"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"me"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MeUserInfo"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"BasicUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isMe"}},{"kind":"Field","name":{"kind":"Name","value":"displayName"}},{"kind":"Field","name":{"kind":"Name","value":"profileImage"}},{"kind":"Field","name":{"kind":"Name","value":"signUpDate"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"MeUserInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"BasicUserInfo"}},{"kind":"Field","name":{"kind":"Name","value":"lastReadNewsId"}},{"kind":"Field","name":{"kind":"Name","value":"superuser"}}]}}]} as unknown as DocumentNode;

/**
 * __useMeBaseQuery__
 *
 * To run a query within a React component, call `useMeBaseQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeBaseQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeBaseQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeBaseQuery(baseOptions?: Apollo.QueryHookOptions<MeBaseQuery, MeBaseQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MeBaseQuery, MeBaseQueryVariables>(MeBaseDocument, options);
      }
export function useMeBaseLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MeBaseQuery, MeBaseQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MeBaseQuery, MeBaseQueryVariables>(MeBaseDocument, options);
        }
export function useMeBaseSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<MeBaseQuery, MeBaseQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<MeBaseQuery, MeBaseQueryVariables>(MeBaseDocument, options);
        }
export type MeBaseQueryHookResult = ReturnType<typeof useMeBaseQuery>;
export type MeBaseLazyQueryHookResult = ReturnType<typeof useMeBaseLazyQuery>;
export type MeBaseSuspenseQueryHookResult = ReturnType<typeof useMeBaseSuspenseQuery>;
export type MeBaseQueryResult = Apollo.QueryResult<MeBaseQuery, MeBaseQueryVariables>;
export const CheckUserNameDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"CheckUserName"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"username"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"user"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"username"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useCheckUserNameQuery__
 *
 * To run a query within a React component, call `useCheckUserNameQuery` and pass it any options that fit your needs.
 * When your component renders, `useCheckUserNameQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCheckUserNameQuery({
 *   variables: {
 *      username: // value for 'username'
 *   },
 * });
 */
export function useCheckUserNameQuery(baseOptions?: Apollo.QueryHookOptions<CheckUserNameQuery, CheckUserNameQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CheckUserNameQuery, CheckUserNameQueryVariables>(CheckUserNameDocument, options);
      }
export function useCheckUserNameLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CheckUserNameQuery, CheckUserNameQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CheckUserNameQuery, CheckUserNameQueryVariables>(CheckUserNameDocument, options);
        }
export function useCheckUserNameSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<CheckUserNameQuery, CheckUserNameQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<CheckUserNameQuery, CheckUserNameQueryVariables>(CheckUserNameDocument, options);
        }
export type CheckUserNameQueryHookResult = ReturnType<typeof useCheckUserNameQuery>;
export type CheckUserNameLazyQueryHookResult = ReturnType<typeof useCheckUserNameLazyQuery>;
export type CheckUserNameSuspenseQueryHookResult = ReturnType<typeof useCheckUserNameSuspenseQuery>;
export type CheckUserNameQueryResult = Apollo.QueryResult<CheckUserNameQuery, CheckUserNameQueryVariables>;
export const UserLanguagesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"UserLanguages"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"me"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"languages"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"UserLanguage"}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"LanguageInfo"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Language"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"UserLanguage"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UserLanguage"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"language"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"LanguageInfo"}}]}},{"kind":"Field","name":{"kind":"Name","value":"decks"}},{"kind":"Field","name":{"kind":"Name","value":"lastActivity"}}]}}]} as unknown as DocumentNode;

/**
 * __useUserLanguagesQuery__
 *
 * To run a query within a React component, call `useUserLanguagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserLanguagesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserLanguagesQuery({
 *   variables: {
 *   },
 * });
 */
export function useUserLanguagesQuery(baseOptions?: Apollo.QueryHookOptions<UserLanguagesQuery, UserLanguagesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<UserLanguagesQuery, UserLanguagesQueryVariables>(UserLanguagesDocument, options);
      }
export function useUserLanguagesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UserLanguagesQuery, UserLanguagesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<UserLanguagesQuery, UserLanguagesQueryVariables>(UserLanguagesDocument, options);
        }
export function useUserLanguagesSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<UserLanguagesQuery, UserLanguagesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<UserLanguagesQuery, UserLanguagesQueryVariables>(UserLanguagesDocument, options);
        }
export type UserLanguagesQueryHookResult = ReturnType<typeof useUserLanguagesQuery>;
export type UserLanguagesLazyQueryHookResult = ReturnType<typeof useUserLanguagesLazyQuery>;
export type UserLanguagesSuspenseQueryHookResult = ReturnType<typeof useUserLanguagesSuspenseQuery>;
export type UserLanguagesQueryResult = Apollo.QueryResult<UserLanguagesQuery, UserLanguagesQueryVariables>;
export const MarkNewsReadBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"MarkNewsReadBase"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"markNewsRead"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"lastReadNewsId"}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useMarkNewsReadBaseMutation__
 *
 * To run a mutation, you first call `useMarkNewsReadBaseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useMarkNewsReadBaseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [markNewsReadBaseMutation, { data, loading, error }] = useMarkNewsReadBaseMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useMarkNewsReadBaseMutation(baseOptions?: Apollo.MutationHookOptions<MarkNewsReadBaseMutation, MarkNewsReadBaseMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<MarkNewsReadBaseMutation, MarkNewsReadBaseMutationVariables>(MarkNewsReadBaseDocument, options);
      }
export type MarkNewsReadBaseMutationHookResult = ReturnType<typeof useMarkNewsReadBaseMutation>;
export type MarkNewsReadBaseMutationResult = Apollo.MutationResult<MarkNewsReadBaseMutation>;
export type MarkNewsReadBaseMutationOptions = Apollo.BaseMutationOptions<MarkNewsReadBaseMutation, MarkNewsReadBaseMutationVariables>;
export const FollowUserBaseDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"FollowUserBase"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"userId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"follow"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"followUser"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"userId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"userId"}}},{"kind":"Argument","name":{"kind":"Name","value":"follow"},"value":{"kind":"Variable","name":{"kind":"Name","value":"follow"}}}]}]}}]} as unknown as DocumentNode;

/**
 * __useFollowUserBaseMutation__
 *
 * To run a mutation, you first call `useFollowUserBaseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useFollowUserBaseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [followUserBaseMutation, { data, loading, error }] = useFollowUserBaseMutation({
 *   variables: {
 *      userId: // value for 'userId'
 *      follow: // value for 'follow'
 *   },
 * });
 */
export function useFollowUserBaseMutation(baseOptions?: Apollo.MutationHookOptions<FollowUserBaseMutation, FollowUserBaseMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<FollowUserBaseMutation, FollowUserBaseMutationVariables>(FollowUserBaseDocument, options);
      }
export type FollowUserBaseMutationHookResult = ReturnType<typeof useFollowUserBaseMutation>;
export type FollowUserBaseMutationResult = Apollo.MutationResult<FollowUserBaseMutation>;
export type FollowUserBaseMutationOptions = Apollo.BaseMutationOptions<FollowUserBaseMutation, FollowUserBaseMutationVariables>;
export const DisconnectAccountDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"DisconnectAccount"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"type"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ConnectedAccountType"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"disconnectAccount"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"type"},"value":{"kind":"Variable","name":{"kind":"Name","value":"type"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"connectedAccounts"}}]}}]}}]} as unknown as DocumentNode;

/**
 * __useDisconnectAccountMutation__
 *
 * To run a mutation, you first call `useDisconnectAccountMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDisconnectAccountMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [disconnectAccountMutation, { data, loading, error }] = useDisconnectAccountMutation({
 *   variables: {
 *      type: // value for 'type'
 *   },
 * });
 */
export function useDisconnectAccountMutation(baseOptions?: Apollo.MutationHookOptions<DisconnectAccountMutation, DisconnectAccountMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DisconnectAccountMutation, DisconnectAccountMutationVariables>(DisconnectAccountDocument, options);
      }
export type DisconnectAccountMutationHookResult = ReturnType<typeof useDisconnectAccountMutation>;
export type DisconnectAccountMutationResult = Apollo.MutationResult<DisconnectAccountMutation>;
export type DisconnectAccountMutationOptions = Apollo.BaseMutationOptions<DisconnectAccountMutation, DisconnectAccountMutationVariables>;