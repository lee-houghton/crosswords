import { useSnackbar } from "notistack";
import { useNavigate } from "react-router";
import {
    AddCrosswordBaseMutationOptions,
    AddCrosswordBaseMutationVariables,
    DeckDetailDocument,
    DeckDetailQuery,
    DeckDetailQueryVariables,
    useAddCrosswordBaseMutation,
} from "./operations";

type AddCrosswordMutationOptions = Pick<AddCrosswordBaseMutationOptions, "onCompleted" | "onError">;

export function useAddCrosswordMutation(options?: AddCrosswordMutationOptions) {
    const navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();

    const [call, state] = useAddCrosswordBaseMutation({
        onCompleted(data) {
            options?.onCompleted?.(data);
            navigate(`/crosswords/${data.addCrossword.id}`);
        },
        onError(err) {
            options?.onError?.(err);
            enqueueSnackbar(err.message, { variant: "error" });
        },
    });

    return [
        (variables: AddCrosswordBaseMutationVariables) =>
            call({
                variables,
                update(proxy, result) {
                    if (!variables.deckIds) return;

                    // TODO There must be a better way to do this
                    for (const id of variables.deckIds) {
                        try {
                            const data = proxy.readQuery<DeckDetailQuery, DeckDetailQueryVariables>({
                                query: DeckDetailDocument,
                                variables: { id },
                            });
                            if (data && data.deck) {
                                proxy.writeQuery<DeckDetailQuery, DeckDetailQueryVariables>({
                                    query: DeckDetailDocument,
                                    variables: { id },
                                    data: {
                                        deck: {
                                            ...data.deck,
                                            crosswords: [...data.deck.crosswords, result.data!.addCrossword],
                                        },
                                    },
                                });
                            }
                        } catch (err) {
                            console.warn(err);
                        }
                    }
                },
            }),
        state,
    ] as const;
}
