import { useApolloClient } from "@apollo/client";
import { useConfirmResetPasswordBaseMutation, ConfirmResetPasswordBaseMutationVariables } from "./operations";

export function useConfirmResetPasswordMutation(variables?: ConfirmResetPasswordBaseMutationVariables) {
    const client = useApolloClient();

    return useConfirmResetPasswordBaseMutation({
        variables,
        update() {
            // Confirming a reset password logs you in as the user, so the user may have changed
            client.resetStore();
        },
    });
}
