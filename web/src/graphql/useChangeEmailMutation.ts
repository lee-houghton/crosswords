import gql from "graphql-tag";
import { useChangeEmailBaseMutation } from "./operations";

export function useChangeEmailMutation() {
    return useChangeEmailBaseMutation({ 
        update(proxy, result) {
            // The user now has a change email request, but the mutation only returns the 
            // request, not the User, but it's simple enough to patch it in client-side.
            proxy.writeFragment({
                id: `User:${result.data?.changeEmail.userId}`,
                fragment: gql`
                    fragment UserChangeEmailRequest on User { 
                        changeEmailRequest(active: true) { 
                            id newEmail 
                        } 
                    }
                `,
                data: {
                    __typename: "User", 
                    changeEmailRequest: result.data?.changeEmail
                }
            })
        }
    });
}
