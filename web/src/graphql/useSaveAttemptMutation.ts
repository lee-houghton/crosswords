import gql from "graphql-tag";
import { CrosswordDetailFragment, SaveAttemptBaseMutationVariables, useSaveAttemptBaseMutation } from "./operations";

const fragment = gql`
    fragment CrosswordAttempt on Crossword {
        attempt {
            id 
            completed 
            percentCompleted
            guesses {
                attemptId x y guess
            }
        }
    }
`;

export function useSaveAttemptMutation(crosswordId: number, refetch = false) {
    const [call, state] = useSaveAttemptBaseMutation();

    return [
        (variables: SaveAttemptBaseMutationVariables) => call({
            variables,
            update(proxy, result) {
                // Apollo should automatically update the attempt itself, now just update the crossword
                // to ensure it is linked to the crossword in the Apollo cache (which will be necessary
                // the first time the attempt is saved).

                const attempt = result.data?.saveAttempt;
                if (!attempt)
                    return;

                proxy.writeFragment({
                    fragment,
                    id: `Crossword:${crosswordId}`,
                    data: { 
                        __typename: "Crossword", 
                        attempt: {
                            ...attempt,
                            guesses: variables.attempt.guesses.map(g => ({
                                __typename: "Guess",
                                attemptId: attempt.id,
                                x: g.x,
                                y: g.y,
                                guess: g.guess
                            }))
                        } as Partial<CrosswordDetailFragment["attempt"]>
                    }
                });
            }
        }),
        state
    ] as const;
}
