import { useApolloClient } from "@apollo/client";
import { useLoginBaseMutation } from "./operations";


export function useLoginMutation() {
    const client = useApolloClient();

    return useLoginBaseMutation({
        update: (proxy, result) => {
            // Resets the Apollo cache, and updates the UI
            if (result.data)
                client.resetStore();
        },
        
        // This is handled by useMutation
        onError: () => {}
    });
}
