import { useMeBaseQuery } from "./operations";

export const useMeQuery = (returnPartialData = false) => 
    useMeBaseQuery({ fetchPolicy: "cache-and-network", returnPartialData });
