import { DeckDetailDocument, DeckDetailQuery, DeckDetailQueryVariables, useUpdateTermBaseMutation } from "./operations";

export function useUpdateTermMutation(termId?: number, deckId?: number) {
    return useUpdateTermBaseMutation({
        update: (proxy, result) => {
            if (termId)
                return; // Already-existing terms will be handled fine by default cache logic 

            if (!result.data?.updateTerm)
                return;
            
            const data = proxy.readQuery<DeckDetailQuery, DeckDetailQueryVariables>({ 
                query: DeckDetailDocument, 
                variables: { id: deckId },
            });

            if (!data?.deck)
                return;

            proxy.writeQuery<DeckDetailQuery, DeckDetailQueryVariables>({
                query: DeckDetailDocument,
                variables: { id: deckId },
                data: {
                    deck: {
                        ...data.deck,
                        termCount: data.deck.termCount + 1,
                        terms: [
                            ...data.deck.terms,
                            result.data.updateTerm
                        ]
                    },
                }
            });
        }
    });
}
