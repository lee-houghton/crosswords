// Queries, and mutations which don't need any cache updates
export { 
    Direction,
    useActivateMutation,
    useAuditAddedSubscription,
    useCancelChangeEmailMutation, 
    useChangePasswordMutation,
    useCheckUserNameQuery,
    useCompleteAttemptMutation,
    useConfirmChangeEmailMutation,
    useCreateDeckMutation,
    useCrosswordDetailQuery,
    useDeckDetailQuery,
    useDisconnectAccountMutation,
    useHomeQuery,
    useLanguageInfoQuery,
    useLanguagesQuery,
    useRegisterMutation,
    useResetPasswordMutation,
    useTermInfoQuery,
    useTermsToStudyQuery,
    useUpdateDeckMutation,
    useUserLanguagesQuery,
    useUserQuery,
} from "./operations";

export type {
    AuditAddedFragment,
    BasicUserInfoFragment,
    ChangeEmailRequest,
    ClueDetailFragment,
    CrosswordCardInfoFragment,
    CrosswordDetailFragment,
    DeckCardInfoFragment,
    DeckDetailFragment,
    LanguageInfoFragment,
    MeUserInfoFragment,
    ScoreInfoFragment,
    TermInfoFragment,
    TermsToStudyQuery,
    UpdateAttemptRequest,
    UpdateGuessRequest,
    UserLanguageFragment,
    UserProfileFragment,
} from "./operations";

// Queries 
// 
// It's rare that these need to be wrapped. Occasionally it's easier to use queries
// that have been wrapped with useMemo or something similar, or for convenience.
export * from "./useFavouriteDeckIds";
export * from "./useIsSuperuser";
export * from "./useMeQuery";

export * from "./useAddCrosswordMutation";
export * from "./useChangeEmailMutation";
export * from "./useConfirmResetPasswordMutation";
export * from "./useFollowUserMutation";
export * from "./useLoginMutation";
export * from "./useLogoutMutation";
export * from "./useMarkNewsReadMutation";
export * from "./useRecentActivity";
export * from "./useSaveAttemptMutation";
export * from "./useSetFavouriteMutation";
export * from "./useUpdateTermMutation";
