import { useMemo } from "react";
import { useFavouriteDeckIdsBaseQuery } from "./operations";

export const useFavouriteDeckIdsQuery = () => {
    const query = useFavouriteDeckIdsBaseQuery({ 
        partialRefetch: true,
    });

    const favouriteDecks = query.data?.me?.favouriteDecks;

    return useMemo(
        () => new Set(favouriteDecks?.map(d => d.id)),
        [favouriteDecks]
    );
}
