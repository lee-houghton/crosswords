import { useMeQuery } from "./useMeQuery";

export const useIsSuperuser = () => useMeQuery().data?.me?.superuser;
