import { MeBaseDocument, useMarkNewsReadBaseMutation, MeBaseQuery } from "./operations";

export function useMarkNewsReadMutation() {
    return useMarkNewsReadBaseMutation({
        // This is handled by useMutation
        onError: () => {},

        optimisticResponse: vars => ({
            __typename: "Mutation",
            markNewsRead: {
                __typename: "User",
                lastReadNewsId: vars.id
            }
        }),

        // Would be good to use optimistic mutations here, but it's not quite as simple as you'd think.
        // Optimistic mutations have to return the whole set of data which would include the user ID,
        // which we don't have here.
        // https://github.com/apollographql/react-apollo/issues/2063
        update(proxy, result) {
            const me = proxy.readQuery<MeBaseQuery>({ query: MeBaseDocument });
            if (!me?.me || !result.data)
                return;

            proxy.writeQuery<MeBaseQuery>({
                query: MeBaseDocument,
                data: {
                    me: {
                        ...me.me,
                        lastReadNewsId: result.data.markNewsRead.lastReadNewsId || null
                    }
                }
            })
        }
    });
}
