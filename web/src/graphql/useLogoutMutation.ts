import { useApolloClient } from "@apollo/client";
import { useLogoutBaseMutation } from "./operations";

export function useLogoutMutation() {
    const client = useApolloClient();

    return useLogoutBaseMutation({
        update() {
            // Resets the Apollo cache, and updates the UI
            client.resetStore();
        }
    });
}
