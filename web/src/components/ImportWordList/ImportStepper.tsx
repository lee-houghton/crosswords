import { Step, StepLabel, Stepper } from "@mui/material";
import { Theme } from "@mui/material/styles";
import makeStyles from '@mui/styles/makeStyles';
import React from "react";
import { EditTerms } from "./EditTerms";
import { ImportState, useImportState } from "./useImportWordListState";
import { UploadFile } from "./UploadFile";
import { ReviewAndSubmit } from "./ReviewAndSubmit";

const useImportStepperStyles = makeStyles((theme: Theme) => ({
    stepper: {
        marginBottom: theme.spacing(2),
    }
}));

export function ImportStepper() {
    const classes = useImportStepperStyles();
    const [state] = useImportState();
    const step = getStep(state);

    return <>
        <Stepper activeStep={step} className={classes.stepper}>
            <Step>
                <StepLabel>Upload file</StepLabel>
            </Step>
            <Step>
                <StepLabel>Edit terms</StepLabel>
            </Step>
            <Step>
                <StepLabel>Review and save</StepLabel>
            </Step>
        </Stepper>
        {step === 0 && <UploadFile />}
        {step === 1 && <EditTerms />}
        {step === 2 && <ReviewAndSubmit />}
    </>;
}

export function getStep(state: ImportState) {
    switch (state.type) {
        case "empty":
        case "loading":
        case "error":
        default:
            return 0;
        case "loaded":
            return 1;
        case "review":
            return 2;
    }
}
