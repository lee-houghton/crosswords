import { Typography } from "@mui/material";
import React from "react";
import { ScreenWrapper } from "../ScreenWrapper";
import { ImportStepper } from "./ImportStepper";
import { ImportStateProvider } from "./useImportWordListState";

export function ImportWordList() {
    return <ImportStateProvider>
        <ScreenWrapper>
            <Typography component="h2" variant="h4" align="center" gutterBottom>
                Import word list
            </Typography>
            <ImportStepper/>
        </ScreenWrapper>
    </ImportStateProvider>;
}

