import { createReducerContext } from "react-use";

interface ImportTerm {
    id: number;
    term: string;
    definition: string;
}

export type ImportEditState = { 
    type: "loaded"; 
    terms: ImportTerm[];
    title: string;
    description: string;
};

export type ImportReviewState = {
    type: "review"; 
    terms: ImportTerm[];
    title: string;
    description: string;
    termLanguage?: string;
    definitionLanguage?: string;
}

export type ImportState 
    = { type: "empty" }
    | { type: "loading" }
    | { type: "error"; error: Error }
    | ImportEditState
    | ImportReviewState
    ;

type EditAction
    = { type: "DeleteTerm"; id: number }
    | { type: "SwapTermAndDefiniton" | "SortAsc" }
    ;

type ReviewAction = 
    | { type: "EditMetadata"; fields: Partial<Pick<ImportReviewState, "title" | "description" | "termLanguage" | "definitionLanguage">> }
    ;

export type ImportAction
    = { type: "Loading" | "Clear" | "DismissError" }
    | { type: "LoadTerms"; terms: ImportTerm[]; title: string; description: string }
    | { type: "Error"; error: Error }
    | { type: "Next" }
    | EditAction
    | ReviewAction;

function reviewReducer(state: ImportState, action: ReviewAction): ImportState {
    if (state.type !== "review")
        return state;

    switch(action.type) {
        case "EditMetadata":
            return { ...state, ...action.fields };
    }
}

function editReducer(state: ImportState, action: EditAction): ImportState {
    if (state.type !== "loaded")
        return state;

    switch(action.type) {
        case "DeleteTerm": 
            return { ...state, terms: state.terms.filter(t => t.id !== action.id) };

        case "SortAsc":
            const collator = new Intl.Collator();
            return { 
                ...state, 
                terms: state.terms.slice()
                    .sort((t1, t2) => 
                        collator.compare(t1.term, t2.term)
                        || collator.compare(t1.definition, t2.definition)
                    )
            };
        case "SwapTermAndDefiniton":
            return { 
                ...state, 
                terms: state.terms.map(t => ({ id: t.id, term: t.definition, definition: t.term }))
            };
    }
}

function importReducer(state: ImportState, action: ImportAction): ImportState {
    switch(action.type) {
        case "Next":
            if (state.type === "loaded")
                return { ...state, type: "review" };
            return state;

        case "DismissError":
            return state.type === "error" ? { type: "empty" } : state;
        case "Clear":
            return { type: "empty" };
        case "Loading":
            return { type: "loading" };
        case "LoadTerms":
            return { ...action, type: "loaded" };
        case "Error": 
            return { type: "error", error: action.error };
        
        case "EditMetadata":
            return reviewReducer(state, action);

        default:
            return editReducer(state, action);
    }
}

export const [useImportState, ImportStateProvider] = 
    createReducerContext(importReducer, { type: "empty" });
