import { Button, TextField } from "@mui/material";
import { Theme } from "@mui/material/styles";
import makeStyles from '@mui/styles/makeStyles';
import React from "react";
import { useNavigate } from "react-router";
import { useCreateDeckMutation } from "../../graphql";
import { Alert } from "../Alert";
import { LanguageSelector } from "../LanguageSelector";
import { useImportState } from "./useImportWordListState";

const useStyles = makeStyles((theme: Theme) => ({
    form: {
        display: "grid",
        gap: theme.spacing(2), // JSS doesn't seem to convert numbers to px for `gap`
        alignContent: "center",
        maxWidth: "30em",
        marginLeft: "auto",
        marginRight: "auto",
    },
}));

export function ReviewAndSubmit() {
    const classes = useStyles();
    const navigate = useNavigate();

    const [state, dispatch] = useImportState();
    const [save, { loading: saving, error }] = useCreateDeckMutation();

    if (state.type !== "review") return null;

    const canSubmit = Boolean(state.termLanguage && state.definitionLanguage && !saving);

    const submit = async (e: React.FormEvent) => {
        try {
            e.preventDefault();

            if (!canSubmit || state.type !== "review" || !state.termLanguage || !state.definitionLanguage) return;

            const { data } = await save({
                variables: {
                    request: {
                        title: state.title,
                        description: state.description,
                        termLangId: state.termLanguage,
                        definitionLangId: state.definitionLanguage,
                        terms: state.terms.map((t) => ({
                            term: t.term,
                            definitions: [t.definition],
                        })),
                    },
                },
            });
            if (data) navigate(`/lists/${data.createDeck.id}`);
        } catch (err) {
            // Handled by the `error` variable
        }
    };

    return (
        <form className={classes.form} onSubmit={submit}>
            <TextField
                value={state.title}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="title"
                name="title"
                label="Title"
                autoFocus
                onChange={(e) => dispatch({ type: "EditMetadata", fields: { title: e.target.value } })}
                helperText={"This is the name that will show in most places including lists."}
            />

            <TextField
                value={state.description}
                variant="outlined"
                margin="normal"
                fullWidth
                id="description"
                name="description"
                label="Description"
                helperText={
                    "Give some more context about where the list -- for example, where the terms come from or who the list might be useful to."
                }
                onChange={(e) => dispatch({ type: "EditMetadata", fields: { description: e.target.value } })}
            />

            <LanguageSelector
                id="term-language-id"
                placeholder="Term language"
                helpText="The language you are trying to learn"
                languageId={state.termLanguage}
                onChange={(termLanguage) => dispatch({ type: "EditMetadata", fields: { termLanguage } })}
            />

            <LanguageSelector
                id="definition-language-id"
                placeholder="Definition language"
                helpText="The language that you already know"
                languageId={state.definitionLanguage}
                onChange={(definitionLanguage) => dispatch({ type: "EditMetadata", fields: { definitionLanguage } })}
            />

            <Alert error={error} />

            <Button disabled={!canSubmit} type="submit" variant="contained" color="primary">
                Create
            </Button>
        </form>
    );
}
