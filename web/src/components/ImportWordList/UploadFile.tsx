import { Backdrop, Button, IconButton, Typography } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import { Cancel, CloudUpload } from "@mui/icons-material";
import { Alert, AlertTitle } from "@mui/material";
import React, { useRef } from "react";
import { useDrop } from "react-use";
import { parseUpload } from "./parseFile";
import { useImportState } from "./useImportWordListState";

const useStyles = makeStyles(theme => ({
    controls: {
        display: "grid",
        justifyContent: "center",
        gap: theme.spacing(2),
    },
    backdrop: {
        pointerEvents: "none",
        zIndex: theme.zIndex.modal,
    },
    alert: {
        marginTop: theme.spacing(2),
    },
}));

export function UploadFile() {
    const ref = useRef<HTMLInputElement>(null);
    const [state, dispatch] = useImportState();
    const classes = useStyles();

    const openFile = async (file: File) => {
        try {
            dispatch(await parseUpload(file));
        } catch (error) {
            dispatch({ type: "Error", error: error instanceof Error ? error : new Error(`${error}`) });
        }
    };

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0];
        if (file) openFile(file);

        e.target.files = null;
    };

    const dropState = useDrop({
        onFiles: files => files.length > 0 && openFile(files[0]),
    });

    const dismiss = () => dispatch({ type: "DismissError" });

    return (
        <section className={classes.controls} aria-label="Import controls">
            <input hidden type="file" ref={ref} name="file" accept="application/vnd.anki" onChange={handleChange} />

            <Button
                id="upload"
                variant="contained"
                color="primary"
                onClick={() => ref.current?.click()}
                startIcon={<CloudUpload />}
                children="Upload file"
            />

            {state.type === "error" && (
                <Alert
                    className={classes.alert}
                    severity="error"
                    children={state.error.message}
                    action={
                        <IconButton onClick={dismiss} size="large">
                            <Cancel />
                        </IconButton>
                    }
                />
            )}

            <Alert severity="info">
                <AlertTitle>Supported file types</AlertTitle>
                <ul>
                    <li>Anki exports (.apkg)</li>
                    <li>CSV file (.csv, 1st column: term, 2nd column: definition)</li>
                </ul>
            </Alert>

            <Backdrop open={dropState.over} className={classes.backdrop}>
                <Typography color="textSecondary" variant="h2">
                    Drop file here
                </Typography>
            </Backdrop>
        </section>
    );
}
