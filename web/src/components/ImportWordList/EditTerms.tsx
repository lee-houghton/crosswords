import { Button } from "@mui/material";
import { Theme } from "@mui/material/styles";
import makeStyles from "@mui/styles/makeStyles";
import DataTable, { MUIDataTableColumnDef, MUIDataTableOptions } from "mui-datatables";
import React, { useCallback } from "react";
import { useImportState } from "./useImportWordListState";

const cols: MUIDataTableColumnDef[] = [
    { name: "term", label: "Term" },
    { name: "definition", label: "Definition" },
];

const useStyles = makeStyles((theme: Theme) => ({
    buttons: {
        display: "grid",
        placeItems: "center",
        gap: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    table: {
        marginBottom: theme.spacing(2),
    },
}));

export function EditTerms() {
    const classes = useStyles();
    const [state, dispatch] = useImportState();

    const options: MUIDataTableOptions = {
        filter: false,
        onRowsDelete: useCallback(() => false as const, []),
    };

    if (state.type !== "loaded") return null;

    return (
        <div>
            <div className={classes.buttons}>
                <Button
                    variant="contained"
                    children="Swap terms/definitions"
                    onClick={() => dispatch({ type: "SwapTermAndDefiniton" })}
                />
            </div>
            <div className={classes.table}>
                <DataTable title="Entries" columns={cols} data={state.terms} options={options} />
            </div>
            <div className={classes.buttons}>
                <Button
                    variant="contained"
                    color="primary"
                    children="Next step"
                    onClick={() => dispatch({ type: "Next" })}
                />
            </div>
        </div>
    );
}
