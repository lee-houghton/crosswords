import JSZip from "jszip";
import Papa from "papaparse";
import { ImportAction } from "./useImportWordListState";

export async function parseUpload(file: File): Promise<ImportAction> {
    const name = file.name.toLowerCase();

    if (name.endsWith(".csv")) return await parseCsv(file);
    else if (name.endsWith(".apkg")) return await parseAnki(file);

    throw new TypeError(
        "Unknown file type. Please see the list of allowed file types and ensure the extension matches."
    );
}

/**
 * Imports a .csv file into a word list. It is assumed that the first column is the term and the second
 * column is the definition. Although, if this is wrong, it's possible to swap the terms/definitions
 * when reviewing the parsed content.
 */
async function parseCsv(file: File) {
    return new Promise<ImportAction>((resolve, reject) => {
        Papa.parse<string[]>(file, {
            worker: true,
            header: false,
            complete(results) {
                if (results.errors.length > 0 && results.data.length === 0) {
                    return reject(new Error("CSV error:" + results.errors[0].message));
                }

                if (results.data.length === 0) {
                    return reject(new Error("No data found in CSV file"));
                }

                // Handle headers from uploads from Crossy Words itself.
                //
                // As for other apps, well, sorry.
                // If the headers get included you can just delete them in the next
                // step of the wizard.
                if (results.data[0][0] === "Term" && results.data[0][1] === "Definition") {
                    results.data.splice(0, 1);
                }

                resolve({
                    type: "LoadTerms",
                    terms: results.data.map(([term = "", definition = ""], index) => ({
                        id: index + 1,
                        term,
                        definition,
                    })),
                    title: file.name,
                    description: "",
                });
            },
        });
    });
}

/**
 * Imports a word list from an Anki file.
 *
 * This is kind of complicated because an Anki export is an SQLite database embedded within a .zip archive
 * (which is usually named .apkg).
 *
 * We use sql.js to query the SQLite file in a web worker. This requires some setup in the webpack config
 * which you can see in config-overrides.js. The query does make several assumptions about the structure
 * of the SQLite database but these generally seem to hold up, in practice.
 */
async function parseAnki(file: File) {
    return new Promise<ImportAction>(async (resolve, reject) => {
        const zip = await JSZip.loadAsync(await file.arrayBuffer());
        const collection = await zip.file("collection.anki2")?.async("uint8array");
        if (!collection) return reject(new Error("Cannot find collection.anki2 (is this an Anki file?)"));

        const worker = new Worker("/static/js/worker.sql-wasm.js", { name: "SqlJS" });
        worker.onmessage = (event) => {
            if (event.data.error) return reject(new Error(event.data.error));

            console.log("Database opened");
            worker.onmessage = (event) => {
                if (event.data.error) return reject(new Error(event.data.error));

                const [{ values }, decks] = event.data.results as {
                    columns: string[];
                    values: (string | number)[][];
                }[];
                if (values.length === 0) return reject(new Error("Upload contains no terms"));

                const terms = values.map((row, idx) => {
                    const [definition, term] = `${row[0]}`.split("\u001f", 2);
                    return { id: idx + 1, term, definition };
                });

                const did = values[0]?.[1] as number | undefined;
                const deck = did && decks.values[0] ? JSON.parse(decks.values[0][0] as string)[did] : undefined;

                resolve({
                    type: "LoadTerms",
                    terms,
                    title: deck?.name || "",
                    description: deck?.desc || "",
                } as ImportAction);
            };

            worker.postMessage({
                id: 2,
                action: "exec",
                sql: `
                    SELECT n.flds, c.did
                    FROM cards c
                    JOIN notes n ON c.nid = n.id
                    WHERE c.ord = 0
                    ORDER BY flds;

                    SELECT decks from col;
                `,
            });
        };

        worker.postMessage({ id: 1, action: "open", buffer: collection });
    });
}
