import { IconButton } from "@mui/material";
import { Link, LinkProps } from "react-router-dom";
import { IconButtonProps } from "@mui/material/IconButton";
import React, { forwardRef } from "react";

const ForwardedLink = forwardRef((props, ref) => 
    <Link innerRef={ref as any} {...props as any} />);

export function LinkIconButton(props: Omit<IconButtonProps, keyof LinkProps> & LinkProps) {
    return (
        <IconButton
            disableTouchRipple
            component={ForwardedLink}
            {...props as any}
            size="large" />
    );
}
