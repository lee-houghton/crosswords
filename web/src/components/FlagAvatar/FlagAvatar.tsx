import { Avatar } from "@mui/material";
import { AvatarProps } from "@mui/material/Avatar";
import makeStyles from '@mui/styles/makeStyles';
import { List as ListIcon } from "@mui/icons-material";
import clsx from "clsx";
import React from "react";
import { flagIdMap } from "../FlagIcon";

// For normal <FlagIcon>, we use the flags from `svg-country-flags` with correct aspect ratios.
//
// For avatars, we need the 1x1 flags from `flag-icon-css`, because the ones with
// technically correct aspect ratios could look weird.
//
// (Well, still, the UK flag could look weird when compressed down from 2:1 to 1:1, as could the Qatar
// flag which has a native aspect ratio of 2.55:1...)
const flags = import.meta.glob("../../../../node_modules/flag-icon-css/flags/1x1/*.svg", {
    eager: true,
    import: "default",
});
const keys = new Set<string>(Object.keys(flags));

const useStyles = makeStyles({
    small: {
        width: 24,
        height: 24,
    },
    faded: {
        opacity: 0.66,
    },
    square: {
        transform: "scaleX(1.4)",
    },
});

interface Props {
    lang?: string;
    variant?: "square" | "rounded";
    small?: boolean;
    faded?: boolean;
}

export function flagImage(lang: string | undefined) {
    if (!lang) return undefined;

    const country = flagIdMap[lang] || lang;

    const key = `./${country}.svg`;
    if (keys.has(key)) return flags[key] as string;

    return undefined;
}

export function FlagAvatar(props: Props) {
    const src = flagImage(props.lang);
    const classes = useStyles();

    const avatarProps: AvatarProps = {
        variant: props.variant,
        className: clsx(
            props.small && classes.small,
            props.faded && classes.faded,
            props.variant === "square" && classes.square
        ),
    };

    if (src) return <Avatar {...avatarProps} src={src} />;

    return (
        <Avatar>
            <ListIcon />
        </Avatar>
    );
}
