import makeStyles from "@mui/styles/makeStyles";
import { Alert, AlertColor, Color } from "@mui/material";
import React, { createElement, ReactNode } from "react";
import { KawaiiMood, KawaiiProps, Planet } from "react-kawaii";

export interface EmptyStateProps extends KawaiiProps {
    // Kawaii props
    kawaii?: React.ComponentType<KawaiiProps>;
    mood?: KawaiiMood;
    size?: number;
    color?: string;

    // Alert props
    action?: ReactNode;
    severity?: AlertColor;
    children?: ReactNode;
}

const useStyles = makeStyles((theme) => ({
    empty: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
        textAlign: "center", // Centers the kawaii character
    },
    alert: {
        maxWidth: "fit-content",
        marginLeft: "auto",
        marginRight: "auto",
    },
}));

export function EmptyState(props: EmptyStateProps) {
    const classes = useStyles();

    return (
        <div className={classes.empty}>
            {createElement(props.kawaii || Planet, {
                mood: props.mood || "blissful",
                size: props.size || 150,
                color: props.color || "lightblue",
            })}

            <Alert className={classes.alert} severity={props.severity || "info"} action={props.action}>
                {props.children}
            </Alert>
        </div>
    );
}
