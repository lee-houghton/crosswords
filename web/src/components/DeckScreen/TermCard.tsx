import { Avatar, IconButton, ListItem, ListItemAvatar, ListItemProps, ListItemSecondaryAction, ListItemText } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import { Edit } from "@mui/icons-material";
import ListIcon from "@mui/icons-material/List";
import clsx from "clsx";
import React, { forwardRef, useCallback, useState } from "react";
import { TermDetailFragment } from "../../graphql/operations";
import { EditTermDialog } from "../EditTermDialog";

const useClasses = makeStyles(theme => ({
    container: {
        "&:focus, &:hover": {
            background: theme.palette.action.selected
        }
    },
    good: { 
        backgroundColor: theme.palette.success.main,
        color: theme.palette.success.contrastText,
    },
    ok: {
        backgroundColor: theme.palette.warning.main,
        color: theme.palette.warning.contrastText,
    },
    bad: {
        backgroundColor: theme.palette.error.main,
        color: theme.palette.error.contrastText,
    },
    action: {
        // Default is 16px margin on the right, but then it just doesn't line up
        right: 0,
    }
}));

function listIconStyle(term: TermDetailFragment) {
    if (!term.score || term.score.attempts === 0)
        return undefined;

    if (term.score.score > 50)
        return "good";
    else if (term.score.score < -50)
        return "bad";
    else
        return "ok";
}


interface TermCardProps extends Omit<ListItemProps, "button" | "onClick" | "onFocus"> {
    tabIndex?: number;
    deckId: number;
    term: TermDetailFragment;

    // KeyboardList will always supply these, but they're not _necessary_
    onClick?: React.MouseEventHandler<HTMLDivElement>;
    onFocus?: React.FocusEventHandler<HTMLDivElement>;
}

export const TermCard = forwardRef(function TermCard({ deckId, term, tabIndex, onClick, onFocus, ...props }: TermCardProps, ref) {
    const classes = useClasses();
    const [editing, setEditing] = useState(false);
    const stopEditing = useCallback(() => setEditing(false), []);
    const avatarStyleName = listIconStyle(term);
    const scoreTitle = term.score
        ? `${term.score.successes} out of ${term.score.attempts} (Score: ${Math.round(term.score.score)})`
        : undefined;

    const onKeyDown = (e: React.KeyboardEvent) => {
        if (e.key === "Enter") {
            setEditing(true);
            e.preventDefault();
        }
    };

    const className = clsx(props.className);
    const containerProps = { tabIndex, onClick, className: classes.container, onKeyDown, onFocus };

    return <>
        {/* We need innerRef here since that is where the props go, KeyboardList needs access
            to the element which has the tabindex attribute. */}
        <ListItem disableGutters ref={ref as any}
            ContainerProps={containerProps}
            {...props} className={className}
            aria-haspopup="dialog"
        >
            <ListItemAvatar>
                <Avatar title={scoreTitle} className={avatarStyleName ? classes[avatarStyleName] : undefined}>
                    <ListIcon />
                </Avatar>
            </ListItemAvatar>

            <ListItemText primary={term.term} secondary={term.definitions[0]} />

            <ListItemSecondaryAction className={classes.action}>
                {/* The Enter key allows editing the term, another tab stop is not necessary */}
                <IconButton
                    tabIndex={-1}
                    aria-label="Edit"
                    title="Edit"
                    onClick={() => setEditing(true)}
                    size="large">
                    <Edit/>
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>

        {editing && <EditTermDialog 
            termId={term.id}
            deckId={deckId}
            onClose={stopEditing}
            open={true}
        />}
    </>;
});
