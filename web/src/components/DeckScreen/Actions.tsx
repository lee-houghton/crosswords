import { Add, Edit } from "@mui/icons-material";
import { SpeedDial, SpeedDialAction, SpeedDialIcon } from '@mui/material';
import React, { memo, useCallback, useState } from "react";
import { EditTermDialog } from "../EditTermDialog";
import { useFabStyles } from "../useFabStyles";
import { SpeedDialActionLink } from "./SpeedDialActionLink";

interface ActionsProps {
    deckId: number;

    /** Called when the Add Term dialog is dismissed */
    onDialogClosed(): void;
}

export const Actions = memo(function Actions({ deckId, onDialogClosed }: ActionsProps) {
    const classes = useFabStyles();
    const [fabOpen, setFabOpen] = useState(false);
    const [adding, setAdding] = useState(false);

    const handleClose = useCallback(() => {
        setAdding(false);
        onDialogClosed();
    }, [onDialogClosed]);

    return <>
        <EditTermDialog
            deckId={deckId}
            open={adding}
            onClose={handleClose}
            termId={null} />

        <SpeedDial
            ariaLabel="Actions"
            className={classes.fab}
            icon={<SpeedDialIcon icon={<Add/>} />}
            onClose={() => setFabOpen(false)}
            onOpen={() => setFabOpen(true)}
            open={fabOpen}
        >
            <SpeedDialActionLink
                to={`/lists/${deckId}/edit`}
                icon={<Edit />}
                tooltipTitle={"Edit"}
                tooltipOpen />
            <SpeedDialAction
                icon={<Add />}
                tooltipTitle={"Add entry"}
                tooltipOpen
                onClick={() => {
                    setFabOpen(false);
                    setAdding(true);
                }} />
        </SpeedDial>
    </>;
});
