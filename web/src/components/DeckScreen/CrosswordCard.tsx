import { Box, Card, CardActions, CardHeader, IconButton, LinearProgress } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import { FavoriteBorderRounded, Share } from "@mui/icons-material";
import clsx from "clsx";
import React from "react";
import { Link } from "react-router-dom";
import { apiUrl } from "../../env";
import { CrosswordCardInfoFragment } from "../../graphql";
import { FlagIcon } from "../FlagIcon";
import { CardActionAreaLink } from "../Links";

const useStyles = makeStyles(theme => ({
    card: {
        position: "relative",
        paddingTop: "100px",
    },
    cardSmall: {
        position: "relative",
        width: theme.spacing(12),
        height: theme.spacing(12),
    },
    full: {
        position: "absolute",
        top: 0, right: 0, bottom: 0, left: 0,
    },
    background: (props: Props) => ({
        backgroundImage: `url(${apiUrl}${props.crossword.imageUrl}?background=true)`,
        backgroundSize: "cover",
        transform: `rotate(${[5, -5, 9, -9, 12, -12][(props.crossword.id % 6)]}deg) scale(1.2)`,
        filter: "blur(3px)",
    }),
    backdrop: {
        background: "rgba(255, 255, 255, 0.6)",
        position: "relative",
    },
    media: {
        backgroundSize: "contain",
        height: "8em",
        zIndex: 1,
    },
    done: {
        color: theme.palette.primary.dark,
    },
    completion: {
        position: "absolute",
        left: 0, right: 0, bottom: 0,
    },
    flag: {
        position: "absolute",
        right: theme.spacing(1),
        bottom: theme.spacing(1.5),
        height: "1.5em",
        boxShadow: "0 0 1em 0.5em rgba(255, 255, 255, 0.5)",
    }
}));

interface Props {
    className?: string;
    crossword: CrosswordCardInfoFragment;
    small?: boolean;
}

export function CrosswordCard(props: Props) {
    const classes = useStyles(props);
    const { crossword, small } = props;
    const created = new Date(crossword.created).toLocaleDateString();

    return (
        <Card className={clsx(props.className, small ? classes.cardSmall : classes.card)}>
            <Box className={clsx(classes.full, classes.background)}/>
            {small && crossword.language?.id && <FlagIcon className={classes.flag} lang={crossword.language.id} />}

            {/* Tne link covers the above non-interactive elements */}
            <Link className={classes.full} to={`/crosswords/${crossword.id}`}/>

            {!small && <CardActionAreaLink className={classes.backdrop} to={`/crosswords/${crossword.id}`}>
                <CardHeader title={crossword.title || "Untitled"} subheader={
                    `Created on ${created}${crossword.user ? ` by ${crossword.user.name}` : ""}`
                }/>
            </CardActionAreaLink>}

            {!small && <CardActions className={classes.backdrop}>
                <IconButton size="large">
                    <FavoriteBorderRounded/>
                </IconButton>
                <IconButton size="large">
                    <Share/>
                </IconButton>
            </CardActions>}
            
            {crossword.attempt && <LinearProgress variant="determinate" color="secondary"
                className={classes.completion}
                value={crossword.attempt.percentCompleted} 
            />}
        </Card>
    );
}

