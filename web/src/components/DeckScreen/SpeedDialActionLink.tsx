import makeStyles from "@mui/styles/makeStyles";
import { SpeedDialAction, SpeedDialActionProps } from "@mui/material";
import { Link } from "react-router-dom";

const useStyles = makeStyles(theme => ({
    link: {
        color: theme.palette.text.primary,
    },
}));

export interface SpeedDialActionLinkProps extends SpeedDialActionProps {
    to: string;
}

export function SpeedDialActionLink({ to, ...props }: SpeedDialActionLinkProps) {
    const classes = useStyles();

    return (
        <SpeedDialAction
            role="link"
            {...props}
            // The action itself can have focus so no need to make the link focusable too
            icon={
                <Link className={classes.link} tabIndex={-1} to={to}>
                    {props.icon}
                </Link>
            }
        />
    );
}
