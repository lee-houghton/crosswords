import { List, ListProps } from "@mui/material";
import React, {
    HTMLAttributes,
    ReactHTMLElement,
    cloneElement,
    forwardRef,
    isValidElement,
    useEffect,
    useRef,
} from "react";

interface KeyboardListProps extends Omit<ListProps, "id"> {
    id: string;
}
/**
 * Implements a keyboard-accessible list using the "roving tabindex" strategy.
 * https://www.w3.org/TR/wai-aria-practices/#kbd_roving_tabindex
 *
 * This allows tabbing to and from the list while still maintaining the focused
 * element within the list. Navigation inside the list is done with up/down arrows
 * and also the home/end keys.
 * https://www.w3.org/TR/wai-aria-practices/#kbd_generalnav
 */
export const KeyboardList = forwardRef(function KeyboardList(props: KeyboardListProps, ref) {
    const refs = useRef<(HTMLElement | null)[]>([]);
    const index = useRef(0);
    const count = React.Children.count(props.children);

    useEffect(() => {
        refs.current.length = count;
    }, [count]);

    const onKeyDown = (e: React.KeyboardEvent<HTMLElement>) => {
        const element = e.target;
        if (!(element instanceof HTMLElement)) return;

        const oldItem = refs.current[index.current];
        let handled = true;

        switch (e.key) {
            case "ArrowUp":
                index.current = Math.max(0, index.current - 1);
                break;
            case "ArrowDown":
                index.current = Math.min((index.current = index.current + 1), count - 1);
                break;
            case "Home":
                index.current = 0;
                break;
            case "End":
                index.current = count - 1;
                break;

            // TODO: Typeahead support
            default:
                handled = false;
                break;
        }

        if (handled) e.preventDefault();

        const item = refs.current[index.current];
        if (item) {
            item.tabIndex = 0;
            item.focus();
        }

        if (oldItem && oldItem !== item) {
            oldItem.tabIndex = -1;
        }
    };

    const children =
        index === undefined
            ? props.children
            : React.Children.map(props.children, (c, i) =>
                  isValidElement(c)
                      ? cloneElement(c, {
                            ref: (e: HTMLElement | null) => {
                                refs.current[i] = e;
                            },
                            id: props.id + "-" + i,
                            tabIndex: i === index.current ? 0 : -1,
                            onFocus: (e: React.FocusEvent) => {
                                const item = e.currentTarget;
                                const oldItem = refs.current[index.current];
                                if (item !== oldItem) {
                                    if (item instanceof HTMLElement) {
                                        console.log("onFocus", e.currentTarget, refs.current.indexOf(item));
                                        item.tabIndex = 0;
                                        index.current = refs.current.indexOf(item);
                                    }
                                    if (oldItem instanceof HTMLElement) oldItem.tabIndex = -1;
                                }
                            },
                            // selected: i === index.current, // Child must be a ListItem
                        } as any)
                      : c
              );

    return <List ref={ref as any} onKeyDown={onKeyDown} {...props} children={children} />;
});
