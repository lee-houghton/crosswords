import { Box, CircularProgress } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import React, { useCallback, useRef } from "react";
import { useParams } from "react-router-dom";
import { useDeckDetailQuery } from "../../graphql";
import { Alert } from "../Alert";
import { DeckCard } from "../DeckCard";
import { DownloadDeckButton } from "../DownloadDeckButton";
import { LinkButton } from "../Links";
import { ScreenWrapper } from "../ScreenWrapper";
import { Title } from "../Title";
import { Actions } from "./Actions";
import { EmptyState } from "./EmptyState";
import { KeyboardList } from "./KeyboardList";
import { TermCard } from "./TermCard";

const useStyles = makeStyles(theme => ({
    list: {
        // Avoids the FAB covering up important parts of the page
        marginBottom: theme.spacing(7),
    },
}));

export function DeckScreen() {
    const classes = useStyles();
    const list = useRef<HTMLUListElement>(null);
    const params = useParams<{ id: string }>();
    const deckId = params.id ? parseInt(params.id, 10) : -1;
    const { data, loading, error } = useDeckDetailQuery({ variables: { id: deckId } });

    const deck = data?.deck;

    // By default, after a dialog is closed, the previously-focused element is refocused.
    // This isn't ideal, as it means the FAB speed dial is reopened to focus the Add term
    // action.
    //
    // After the add term dialog is closed, we intentionally focus the list.
    // However, this must be done after a small delay since the dialog will focus the Add
    // button again.
    //
    // TODO: Ensure this doesn't give a confusing experience for screen readers.
    const onDialogClosed = useCallback(() => {
        if (!(list.current instanceof HTMLElement)) return;

        // Focusing the list itself doesn't work, we need to find the actual item that
        // can be tabbed to.
        const item = list.current.querySelector("[tabindex='0']");
        if (item instanceof HTMLElement) setTimeout(() => item.focus(), 50);
    }, []);

    return (
        <ScreenWrapper>
            <Title>{deck?.title}</Title>
            {loading && (
                <Box textAlign="center">
                    <CircularProgress />
                </Box>
            )}
            {error && <Alert error={error} />}
            {deck && (
                <div>
                    <DeckCard
                        title
                        deck={deck}
                        link={false}
                        avatar={true}
                        buttons={deck && <DownloadDeckButton deck={deck} />}
                    />

                    {deck.terms && deck.terms.length > 0 && (
                        <KeyboardList
                            className={classes.list}
                            ref={list}
                            id="deck-terms"
                            aria-label={`Entries (${deck.terms.length})`}
                        >
                            {deck.terms.map(term => (
                                <TermCard key={term.id} deckId={deckId} term={term} />
                            ))}
                        </KeyboardList>
                    )}

                    {deck.terms?.length === 0 && (
                        <EmptyState action={<LinkButton to={`/lists/${deckId}/edit`}>Add some entries</LinkButton>}>
                            This word list is empty.
                        </EmptyState>
                    )}

                    <Actions deckId={deckId} onDialogClosed={onDialogClosed} />
                </div>
            )}
        </ScreenWrapper>
    );
}
