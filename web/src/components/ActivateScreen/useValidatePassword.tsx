import { useState, useEffect } from "react";

/**
 * Lazy load the zxcvbn library, only loading it when it is actually required,
 * to avoid zxcvbn's substantial startup delay.
 */
function useLazyZxcvbn(needed = false) {
    // Ideally this would just be: useState<typeof import("zxcvbn")[]>()
    // However, useState values can't be a function, since it's treated as a reducer
    const [zxcvbn, setZxcvbn] = useState<typeof import("zxcvbn")[]>();
    const shouldImport = needed && !zxcvbn;

    useEffect(() => {
        if (shouldImport)
            import("zxcvbn").then(exports => setZxcvbn([exports.default]));
    }, [shouldImport]);

    return zxcvbn?.[0];
}

export function useValidatePassword(password: string | undefined) {
    // useMemo() is absolutely necessary here - without it, each re-render
    // will call import() again, which will return a new Promise, which will
    // force a re-render due to the promise being a dependency of useEffect.
    //
    // Unfortunately for technical reasons we can't have something like:
    //  const zxcvbn = useAwaitImport("zxcvbn");
    // Because webpack wouldn't be able to handle it.
    const zxcvbn = useLazyZxcvbn(!!password);

    if (!zxcvbn)
        return undefined;

    if (!password)
        return undefined;

    return zxcvbn(password);
}

export function useValidPasswordWithConfirm(password: string, confirmPassword: string) {
    const passwordResult = useValidatePassword(password);
    const passwordMessage = passwordResult && (
        passwordResult.feedback.warning
        || ["", "", "", "This is a good password!", "Congratulations! You are an excellent chooser of passwords!"][passwordResult.score]
    );
    const passwordError = Boolean(passwordResult && passwordResult.feedback.warning);
    const confirmPasswordError = Boolean(confirmPassword) && confirmPassword !== password;
    const confirmPasswordMessage = confirmPasswordError ? "Passwords do not match" : undefined;
    return {
        passwordMessage,
        passwordError,
        confirmPasswordError,
        confirmPasswordMessage,
    };
}
