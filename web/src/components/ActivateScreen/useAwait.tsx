import { useState, useEffect } from "react";

export type AwaitResult<T> = { state: "pending" } | { state: "resolved", value: T } | { state: "rejected", error: any };

export function useAwait<T>(promise: Promise<T>): AwaitResult<T> {
    const [value, setValue] = useState<AwaitResult<T>>({ state: "pending" });
    useEffect(() => {
        promise.catch(error => setValue({ state: "rejected", error }));
        promise.then(result => setValue({ state: "resolved", value: result }));
    }, [promise]);
    return value;
}
