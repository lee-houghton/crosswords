import { Button, Link as MuiLink, TextField, Typography } from "@mui/material";
import React, { ChangeEvent, useCallback, useState } from "react";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { useActivateMutation } from "../../graphql";
import { useFormStyles } from "../../styles/forms";
import { Alert } from "../Alert";
import { ScreenWrapper } from "../ScreenWrapper";
import { useValidatePassword } from "./useValidatePassword";

export function ActivateScreen() {
    const { activationCode } = useParams<{ activationCode: string }>();
    const formClasses = useFormStyles();
    const [displayName, setDisplayName] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const onDisplayNameChange = useCallback((e: ChangeEvent<HTMLInputElement>) => setDisplayName(e.target.value), []);
    const onPasswordChange = useCallback((e: ChangeEvent<HTMLInputElement>) => setPassword(e.target.value), []);
    const onConfirmPasswordChange = useCallback(
        (e: ChangeEvent<HTMLInputElement>) => setConfirmPassword(e.target.value),
        []
    );

    // Display name checking
    // (Essentially just checks that the name isn't only whitespace)
    const nameError = Boolean(displayName) && !displayName.trim();
    const nameMessage = nameError ? "You must enter a name" : undefined;

    // Password checking
    const passwordResult = useValidatePassword(password);
    const passwordMessage =
        passwordResult &&
        (passwordResult.feedback.warning ||
            ["", "", "", "This is a good password!", "Congratulations! You are an excellent chooser of passwords!"][
                passwordResult.score
            ]);
    const passwordError = Boolean(passwordResult && passwordResult.feedback.warning);
    const confirmPasswordError = Boolean(confirmPassword) && confirmPassword !== password;
    const confirmPasswordMessage = confirmPasswordError ? "Passwords do not match" : undefined;

    // Form submission
    const [activate, { data: activated, error: activateError, loading: activating }] = useActivateMutation();
    const canSubmit = !(activating || nameError || passwordError || confirmPasswordError);
    const onSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        if (activationCode && canSubmit) activate({ variables: { activationCode, password, displayName } });
    };

    if (activated) {
        return (
            <ScreenWrapper>
                <p>
                    Success! You can now{" "}
                    <MuiLink component={Link} to="/login">
                        login
                    </MuiLink>{" "}
                    as {activated.activateAccount.name}. Well, actually, you're already logged in.
                </p>
            </ScreenWrapper>
        );
    }

    return (
        <ScreenWrapper>
            <div className={formClasses.wrapper}>
                <Typography component="h1" variant="h5">
                    Activate your account
                </Typography>

                <p>Thanks for registering! Just a few more details&hellip;</p>

                <form className={formClasses.form} onSubmit={onSubmit}>
                    <TextField
                        value={displayName}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="displayName"
                        name="displayName"
                        label="Display name"
                        autoFocus
                        error={Boolean(nameError)}
                        helperText={nameMessage || "This is the name that other users will see."}
                        onChange={onDisplayNameChange}
                    />

                    <TextField
                        type="password"
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="password"
                        label="Password"
                        name="password"
                        error={passwordError}
                        onChange={onPasswordChange}
                        helperText={
                            passwordMessage || "Use a strong password that you haven't used on any other websites."
                        }
                    />

                    <TextField
                        type="password"
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="confirmPassword"
                        label="Confirm Password"
                        name="confirmPassword"
                        error={confirmPasswordError}
                        onChange={onConfirmPasswordChange}
                        helperText={confirmPasswordMessage}
                    />

                    {activateError && <Alert error={activateError} />}

                    <Button
                        type="submit"
                        disabled={!canSubmit}
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={formClasses.submit}
                    >
                        Activate
                    </Button>
                </form>
            </div>
        </ScreenWrapper>
    );
}
