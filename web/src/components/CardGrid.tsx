import makeStyles from '@mui/styles/makeStyles';
import React, { ReactNode } from "react";

const useStyles = makeStyles(theme => ({
    grid: {
        display: "grid",
        gridTemplateColumns: "repeat(12, 1fr)",
        gridGap: theme.spacing(2),
        padding: 0,
        margin: 0,
        alignItems: "stretch",
    },
    item: {
        listStyleType: "none",
        padding: 0,
        margin: 0,
        [theme.breakpoints.only("xs")]: { gridColumnEnd: "span 12" },
        [theme.breakpoints.only("sm")]: { gridColumnEnd: "span 6" },
        [theme.breakpoints.only("md")]: { gridColumnEnd: "span 4" },
        [theme.breakpoints.only("lg")]: { gridColumnEnd: "span 3" },
        "& > *": {
            height: "100%"
        }
    },
    placeholderItem: {
        listStyleType: "none",
        padding: 0,
        margin: 0,
        [theme.breakpoints.only("xs")]: { gridColumnEnd: "span 12" },
        [theme.breakpoints.up("sm")]: { gridColumnEnd: "span 6" },
        "& > *": {
            height: "100%"
        }
    },
}));

interface CardGridProps {
    children?: ReactNode;
}

interface CardGridItemProps {
    children?: ReactNode;
}

export function CardGrid(props: CardGridProps) {
    const classes = useStyles();
    return (
        <div className={classes.grid}>
            {props.children}
        </div>
    );
}

export function CardGridItem(props: CardGridItemProps) {
    const classes = useStyles();
    return (
        <div className={classes.item}>
            {props.children}
        </div>
    );
}
