import { Grid, SnackbarContent } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import InfoIcon from "@mui/icons-material/Info";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import React from "react";
import { blue, green } from "@mui/material/colors";

const useStyles = makeStyles(theme => ({
    wrapper: {
        textAlign: "center",
    },
    snackbarInfo: {
        backgroundColor: blue[500],
        color: "white",
    },
    snackbarSuccess: {
        backgroundColor: green[500],
        color: "white",
    },
    message: {
        display: "flex",
        alignItems: "center",
    }
}));

interface Props {
    success?: boolean;
    className?: string;
    message: string;
}

export function Info(props: Props) {
    const classes = useStyles();

    if (!props.message)
        return <div/>;

    return (
        <Grid className={props.className} container alignContent="center" justifyContent="center">
            <Grid item xs={12} lg={8} className={classes.wrapper}>
                <SnackbarContent className={props.success ? classes.snackbarSuccess : classes.snackbarInfo} message={
                    <span className={classes.message} >
                        {props.success ? <CheckCircleIcon/> : <InfoIcon/>}
                        {props.message}
                    </span>
                }/>
            </Grid>
        </Grid>
    );
};
