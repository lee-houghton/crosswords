import { IconButton } from "@mui/material";
import { CloudDownload } from "@mui/icons-material";
import React from "react";
import { DeckDetailFragment } from "../graphql";

interface DownloadDeckButtonProps {
    deck: DeckDetailFragment;
}

export function DownloadDeckButton({ deck }: DownloadDeckButtonProps) {
    return (
        <IconButton
            disabled={deck.terms.length === 0}
            onClick={() => download(deck)}
            aria-label="Download"
            title="Download"
            size="large">
            <CloudDownload/>
        </IconButton>
    );
}

async function download(deck: DeckDetailFragment) {
    const { downloadWordList } = (await import("./downloadWordList"));
    downloadWordList(deck);
}
