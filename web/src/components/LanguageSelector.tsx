import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import React, { ChangeEvent, useEffect, useMemo, memo } from "react";
import { FlagIcon } from "./FlagIcon";
import makeStyles from "@mui/styles/makeStyles";
import InputLabel from "@mui/material/InputLabel";
import FormHelperText from "@mui/material/FormHelperText";
import { useLanguagesQuery } from "../graphql";

interface Props {
    id: string;
    className?: string;
    languageId?: string;
    onChange?: (languageId?: string) => void;
    required?: boolean;
    disabled?: boolean;
    placeholder?: string;
    helpText?: string;
}

const useStyles = makeStyles(theme => ({
    option: {
        appearance: "none",
        webkitAppearance: "none",
        lineHeight: "150%",
        padding: "9px 0",
    },
    margin: {
        margin: theme.spacing(1),
    },
    flag: {
        // Make room for the dropdown arrow
        marginRight: "1em",
    },
}));

export const LanguageSelector = memo(function LanguageSelector(props: Props) {
    const { data, loading } = useLanguagesQuery();
    const classes = useStyles();

    const handleChange = props.onChange
        ? (e: SelectChangeEvent) => props.onChange!(e.target.value !== "NONE" ? (e.target.value as string) : undefined)
        : undefined;

    const languages = data?.languages;
    const items = useMemo(
        () =>
            languages?.map(lang => (
                <option className={classes.option} key={lang.id} value={lang.id}>
                    {lang.name}
                </option>
            )),
        [classes.option, languages]
    );

    return (
        <FormControl variant="outlined" className={props.className}>
            {props.placeholder && (
                <InputLabel shrink={props.languageId !== undefined} htmlFor={props.id}>
                    {props.placeholder}
                </InputLabel>
            )}

            <Select<string>
                native
                endAdornment={props.languageId && <FlagIcon muted className={classes.flag} lang={props.languageId} />}
                id={props.id}
                value={props.languageId || "NONE"}
                disabled={props.disabled || loading}
                placeholder={props.placeholder}
                onChange={handleChange}
                label={props.placeholder} // Used only for layout
            >
                <option value="NONE" />
                {items}
            </Select>

            {props.helpText && <FormHelperText>{props.helpText}</FormHelperText>}
        </FormControl>
    );
});
