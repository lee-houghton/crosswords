import React, { useEffect, useReducer } from "react";
import { CrosswordPuzzle } from "../CrosswordScreen/CrosswordPuzzle";
import { PuzzleAction, PuzzleCell, PuzzleState, GridCell } from "../CrosswordScreen/reducer";
import { LoadingMessage } from "./LoadingMessage";
import { CrosswordDetailFragment } from "../../graphql";

const DEFAULT_INTERVAL = 500;
const CLUE_COUNT = 15;
// const CHANGES_PER_ITERATION = 7;

export interface CrosswordLoadingProps {
    width: number;
    height: number;
}

interface Clue {
    x: number;
    y: number;
    direction: "across" | "down";
    length: number;
}

interface AnimationState {
    crossword: CrosswordDetailFragment;
    width: number;
    height: number;
    clues: Clue[];
}

function fakeDispatch(action: PuzzleAction) {}

function generate<T>(count: number, generator: (i: number) => T) {
    const items = [];
    for (let i = 0; i < count; i++)
        items.push(generator(i));
    return items;
}

function fakeCrossword(width: number, height: number): CrosswordDetailFragment {
    return {
        __typename: "Crossword",
        width,
        height,
        attempt : null,
        available: true,
        clues: null,
        created: new Date(),
        decks: [],
        description: "Fake crossword for the loading animation",
        id: 1,
        imageUrl: "http://crossword.image/1",
        stars: 0,
        title: "Fake crossword",
        user: {
            __typename: "User",
            id: 42,
            name: "hackerman420",
            displayName: "Mr Hacker Man",
            profileImage: "http://user.image/hackerman420",
        },
    };
}

function getInitialState({ width, height }: CrosswordLoadingProps): AnimationState {
    return reducer({
        crossword: fakeCrossword(width, height),
        width,
        height,
        clues: [],
    });
};

function contains(clue: Clue, x: number, y: number) {
    return clue.direction === "across"
        ? (clue.y === y && x >= clue.x && x < clue.x + clue.length)
        : (clue.x === x && y >= clue.y && y < clue.y + clue.length);
}

function getPuzzleState(state: AnimationState): PuzzleState {
    const rows = generate(state.height, y => generate(state.width, x => ({
        x, y,
        filled: state.clues.some(c => contains(c, x, y)),
    } as PuzzleCell)));

    return {
        crossword: state.crossword,
        filledIn: false,
        modified: false,
        composing: false,
        rows,
        grid: rows.map(r => 
            r.map(cell => 
                cell.filled ? { cell, active: false, focused: false, guess: undefined } as GridCell : undefined))
    };
};

function reducer({ width, height, clues: oldClues, ...rest }: AnimationState): AnimationState {
    const clues: Clue[] = [];
    const numClues = Math.floor(CLUE_COUNT * (width / 14) ** 1.5);

    for (let i = 0; i < 10; i++) {
        while (clues.length < numClues) {
            const clue = generateClue(width, height, clues);
            if (!clue)
                break;

            clues.push(clue);
        }
    }

    return { width, height, clues, ...rest };
}

function overlapsX(clue1: Clue, clue2: Clue) {
    return (clue1.y >= clue2.y - 1 && clue1.y <= clue2.y + 1)
        && clue1.x < clue2.x + clue2.length 
        && clue2.x < clue1.x + clue1.length;
}

function overlapsY(clue1: Clue, clue2: Clue) {
    return (clue1.x >= clue2.x - 1 && clue1.x <= clue2.x + 1)
        && clue1.y < clue2.y + clue2.length 
        && clue2.y < clue1.y + clue1.length;
}

function crosses(across: Clue, down: Clue) {
    if (down.x < across.x || down.x >= across.x + across.length)
        return false;
    
    if (across.y < down.y || across.y >= down.y + down.length)
        return false;

    return true;
}

function generateClue(width: number, height: number, existing: Clue[]): Clue | undefined {
    let n = 100;

    while (n --> 0) {
        const length = Math.floor(3 + (Math.random() + Math.random()) * 6);
        const x = Math.floor(Math.random() * (width - length + 1));
        const y = Math.floor(Math.random() * (height - length + 1));
        const direction = Math.random() > 0.5 ? "across" : "down";
        const clue: Clue = { x, y, direction, length };

        // When adding the first clue, there's not going to be anything to attach to
        if (existing.length === 0)
            return clue;

        if (direction === "across") {
            if (existing.some(c => c.direction === "across" && overlapsX(clue, c)))
                continue;
            if (existing.some(c => c.direction === "down" && crosses(clue, c)))
                return clue;
        } else {
            if (existing.some(c => c.direction === "down" && overlapsY(clue, c)))
                continue;
            if (existing.some(c => c.direction === "across" && crosses(c, clue)))
                return clue;
        }
    }

    return undefined;
}

export function CrosswordLoading(props: CrosswordLoadingProps) {
    const [state, nextAnimState] = useReducer(reducer, props, getInitialState);

    useEffect(() => {
        const timeout = setInterval(nextAnimState, DEFAULT_INTERVAL);
        return () => clearTimeout(timeout);
    }, []);

    return <CrosswordPuzzle 
        readOnly 
        dispatch={fakeDispatch} 
        state={getPuzzleState(state)} 
        showAnswers={false}
        children={<LoadingMessage/>}
    />
}
