import React from "react";
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
    background: {
        backgroundColor: "rgba(0, 0, 0, 0.5)",
        position: "absolute",
        left: "50%", 
        top: "50%",
        transform: "translate(-50%, -50%)",
        borderRadius: "8px",
        padding: "20px",
    },
    text: {
        color: "white",
        fontSize: "2em",
        textShadow: "2px 2px 4px rgba(1, 1, 1, 0.2)",
    },
});

export function LoadingMessage() {
    const classes = useStyles();
    
    return <div className={classes.background}>
        <span className={classes.text}>
            Generating crossword...
        </span>
    </div>
}
