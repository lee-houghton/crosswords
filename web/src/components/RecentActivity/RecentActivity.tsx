import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Alert from '@mui/material/Alert';
import Skeleton from '@mui/material/Skeleton';
import { addDays, addMonths, format, startOfDay, startOfMonth, startOfWeek } from "date-fns";
import React, { memo } from "react";
import { Planet } from "react-kawaii";
import { Link } from "react-router-dom";
import { useRecentActivity } from "../../graphql";
import { ActivityInfoFragment } from "../../graphql/operations";
import { ActivityGroup } from "./ActivityGroup";
import { ListSubheader } from "@mui/material";

const skeleton = <Card>
    <CardContent>
        <ListSubheader>
            Recent activity
        </ListSubheader>
        <Skeleton variant="text"/>
        <Skeleton variant="text"/>
        <ListSubheader>
            This week
        </ListSubheader>
        <Skeleton variant="text" width={70}/>
        <Skeleton variant="text"/>
        <Skeleton variant="text"/>
        <Skeleton variant="text"/>
        <Skeleton variant="text"/>
    </CardContent>
</Card>;

export const RecentActivity = memo(function RecentActivity({ count = 10 }: { count?: number }) {
    const { data, error, loading, fetchMore, refetch } = useRecentActivity({ count });
    const activities = data?.recentActivity?.activities;

    if (error)
        return <Alert severity="error" action={<Button onClick={() => refetch({ count })}>Retry</Button>}>
            {error.message}
        </Alert>;

    if (!activities || (loading && activities.length === 0))
        return skeleton;

    if (activities.length === 0)
        return <Card>
            <CardContent>
                <Box textAlign="center">
                    <Planet size={160} color="lightblue" mood="excited"/>
                    <Alert severity="info" style={{maxWidth: "fit-content", marginLeft: "auto", marginRight:"auto"}}>
                        This is where you'd see recent activity, if you had any. <Link to="/lists/add">Create some word lists</Link> and do some crosswords to get started!{" "}
                        Or follow someone else if you know someone else who uses this app.
                    </Alert>
                </Box>
            </CardContent>
        </Card>;

    const groups = groupByDate(data?.recentActivity.activities || []);

    return <Card>
        <CardContent>
            {groups.map(group => 
                <ActivityGroup key={group.type} type={group.type} activities={group.items}/>
            )}
        </CardContent>
        {data?.recentActivity.page.endCursor && <CardActions>
            <Button onClick={fetchMore}>Load More</Button>
        </CardActions>}
    </Card>;
});

function getDateCategory(activity: ActivityInfoFragment) {
    const now = new Date();
    const date = new Date(activity.date).getTime();

    if (date >= startOfDay(now).getTime())
        return "Today";

    if (date >= startOfWeek(now, { weekStartsOn: 0 }).getTime())
        return "This Week";

    if (date >= addDays(startOfWeek(now), -7).getTime())
        return "Last Week";

    if (date >= startOfMonth(now).getTime())
        return "This Month";

    if (date >= addMonths(startOfMonth(now), -1).getTime())
        return "Last Month";

    return format(date, "MM yyyy");
}

function groupByDate(activities: readonly ActivityInfoFragment[]) {
    let last: string | undefined = undefined;
    const groups: { type: string, items: ActivityInfoFragment[] }[] = [];
    let group: ActivityInfoFragment[] = [];

    for (const activity of activities) {
        const type = getDateCategory(activity);
        if (type !== last) {
            group = [];
            groups.push({ type: groups.length === 0 ? "Recent Activity" : type, items: group });
            last = type;
        }
        group.push(activity);
    }

    return groups;
}
