import React from "react";
import { UserAvatar } from "../Home/UserAvatar";
import makeStyles from '@mui/styles/makeStyles';
import { Link } from "react-router-dom";

interface UserInlineProps {
    user: {
        name: string;
        displayName: string;
        profileImage?: string | null;
    };
}

const useStyles = makeStyles(theme => ({
    link: {
        fontWeight: "bold",
        color: theme.palette.text.primary,
    },
}));

export function UserInline({ user }: UserInlineProps) {
    const classes = useStyles();

    return <Link className={classes.link} to={`/users/${encodeURIComponent(user.name)}`}>
        <UserAvatar inline tiny
            name={user.displayName}
            profileImage={user.profileImage || undefined} />
        {user.displayName}
    </Link>;
}
