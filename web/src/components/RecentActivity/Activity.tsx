import ListItemText from "@mui/material/ListItemText";
import makeStyles from '@mui/styles/makeStyles';
import { formatDistance } from "date-fns";
import React, { ReactNode } from "react";
import { ActivityInfoFragment, ActivityType } from "../../graphql/operations";
import { DeckChip } from "../DeckChip";
import { UserInline } from "./UserInline";

const encouragement = [
    "Good job!",
    "Hooray!",
    "Nice!",
    "Awesome!",
    "Keep it up!",
    "Sweet!"
];

const useStyles = makeStyles(theme => ({
    timestamp: {
        color: theme.palette.text.secondary
    }
}));

interface ActivityGroup {
    className?: string;
    activity: ActivityInfoFragment;
}

export function Activity({ className, activity }: ActivityGroup) {
    const classes = useStyles();
    const user = <UserInline user={activity.user}/>;
    const decks = activity.decks?.map(deck => <DeckChip size="small" key={deck.id} deck={deck}/>);
    let content: ReactNode = <>{user} did something!</>;

    switch(activity.type) {
        case ActivityType.DeckCreated:
            content = <>{user} created {pluralise(activity.parameters?.[0], "list", "lists")}: {decks}</>;
            break;
        case ActivityType.DeckTermsAdded:
            content = <>{user} added {pluralise(activity.parameters?.[0], "term", "terms")} to: {decks}</>;
            break;
        case ActivityType.CrosswordsCompleted:
            content = <>{user} completed {pluralise(activity.crosswordIds?.length, "crossword", "crosswords")}
            . {encouragement[activity.id % encouragement.length]}</>;
            break;
    }

    const date = new Date(activity.date);

    return <ListItemText className={className}>
        {content} <span className={classes.timestamp} title={date.toLocaleString()}>
            {formatDistance(date, new Date(), { addSuffix: true })}
        </span>
    </ListItemText>
}

function pluralise(count: number | string | undefined, singular: string, plural: string) {
    if (count === "0" || count === 0 || count === undefined)
        return "some " + plural;
    if (count === 1 || count === "1")
        return "a " + singular;
    return `${count} ${plural}`;
}
