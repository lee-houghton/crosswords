import React from "react";
import { ActivityInfoFragment } from "../../graphql/operations";
import ListSubheader from "@mui/material/ListSubheader";
import { Activity } from "./Activity";
import makeStyles from '@mui/styles/makeStyles';
import ListItem from "@mui/material/ListItem";

export interface ActivityGroupProps {
    type: string;
    activities: readonly ActivityInfoFragment[];
}

const useStyles = makeStyles(theme => ({
    activity: {
        paddingTop: theme.spacing(0.25),
        paddingBottom: theme.spacing(0.25),
    }
}));

export function ActivityGroup({ type, activities }: ActivityGroupProps) {
    const classes = useStyles();

    return <>
        <ListSubheader>
            {type}
        </ListSubheader>

        {activities.map(activity => <ListItem key={activity.id} className={classes.activity}>
            <Activity activity={activity}/>
        </ListItem>)}
    </>;
}
