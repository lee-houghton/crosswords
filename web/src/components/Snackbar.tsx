import { Button, IconButton, Snackbar as MuiSnackbar, SnackbarContent } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import CloseIcon from "@mui/icons-material/Close";
import ErrorIcon from "@mui/icons-material/Error";
import React, { useCallback } from "react";
import clsx from "clsx";

export interface SnackbarProps {
    message: string;
    error?: Error;

    open: boolean;
    setOpen: (open: boolean) => void;
    undo?: () => void;
}

const anchorOrigin = { vertical: "bottom", horizontal: "left" } as const;

const useStyles = makeStyles(theme => ({
    close: {
        padding: theme.spacing(0.5),
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    message: {
        display: "flex",
        alignItems: "center",
    },
}));

export function Snackbar({ message, error, open, setOpen, undo }: SnackbarProps) {
    const handleClose = useCallback(() => {
        setOpen(false);
    }, [setOpen]);
    const handleUndo = useCallback(() => {
        setOpen(false);
        if (undo)
            undo();
    }, [setOpen, undo]);
    const classes = useStyles();

    const undoAction = (
        <Button 
            key="undo" 
            color="secondary" 
            size="small"
            onClick={handleUndo}>
            UNDO
        </Button>
    );
    const closeAction = (
        <IconButton
            key="close"
            aria-label="close"
            color="inherit"
            className={classes.close}
            onClick={handleClose}
            size="large">
            <CloseIcon />
        </IconButton>
    );

    const actions = undo ? [undoAction, closeAction] : [closeAction];

    return (
        <MuiSnackbar
            anchorOrigin={anchorOrigin}
            open={open}
            autoHideDuration={6000}
            onClose={handleClose}
        >
            <SnackbarContent
                className={clsx(error && classes.error)}
                aria-describedby="snackbar-message"
                message={
                    <span id="snackbar-message" className={classes.message}>
                        {error && <ErrorIcon/>}
                        {error ? error.message : message}
                    </span>
                }
                action={actions
            }/>
        </MuiSnackbar>
    );
}
