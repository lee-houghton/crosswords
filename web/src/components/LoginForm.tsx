import { Button, Link as MuiLink, TextField, Typography } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import React, { useCallback, useRef } from "react";
import { Link as RouterLink } from "react-router-dom";
import { useLoginMutation } from "../graphql";
import { Alert } from "./Alert";
import { LoginButtons } from "./LogInButtons";

const useStyles = makeStyles((theme) => ({
    login: {
        marginLeft: "auto",
        marginRight: "auto",
        maxWidth: "30em",
    },
    input: {
        gridColumn: "span 3",
    },
    alert: {
        gridColumn: "span 3",
    },
    form: {
        display: "grid",
        flexDirection: "column",
        gridAutoFlow: "row",
        gridTemplateColumns: "1fr auto 1fr",
    },
    submit: {
        gridColumn: 2,
        alignSelf: "center",
        margin: theme.spacing(2, 0),
        minWidth: "13em",
    },
    signIn: {
        gridColumn: "span 3",
    },
    forgot: {
        gridColumn: "span 3",
        marginTop: theme.spacing(2),
        textAlign: "center",
    },
}));

export function LoginForm() {
    const classes = useStyles();
    const [login, { loading: loggingIn, error }] = useLoginMutation();
    const usernameRef = useRef<HTMLInputElement>();
    const passwordRef = useRef<HTMLInputElement>();

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
        e.preventDefault();

        if (loggingIn) return;

        login({
            variables: {
                username: usernameRef.current!.value,
                password: passwordRef.current!.value,
            },
        });
    };

    return (
        <div className={classes.login}>
            <Typography component="h1" variant="h5">
                Sign in
            </Typography>

            <Typography variant="body2" color="textSecondary">
                Don't have an account?{" "}
                <MuiLink component={RouterLink} to="register">
                    Sign up!
                </MuiLink>
            </Typography>

            <form className={classes.form} onSubmit={handleSubmit}>
                <TextField
                    className={classes.input}
                    inputRef={usernameRef}
                    variant="filled"
                    margin="normal"
                    required
                    fullWidth
                    id="username"
                    label="User name"
                    name="username"
                    autoFocus
                />
                <TextField
                    className={classes.input}
                    inputRef={passwordRef}
                    variant="filled"
                    margin="normal"
                    required
                    fullWidth
                    type="password"
                    id="password"
                    label="Password"
                    name="password"
                />
                <Alert className={classes.alert} error={error} />
                <Button
                    type="submit"
                    disabled={loggingIn}
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                    Sign In
                </Button>
                <LoginButtons className={classes.signIn} />
                <MuiLink
                    color="textSecondary"
                    variant="body1"
                    className={classes.forgot}
                    component={RouterLink}
                    to="/forgot-password"
                    children="Forgot password?"
                />
            </form>
        </div>
    );
}
