import { Avatar } from "@mui/material";
import Chip, { ChipProps } from "@mui/material/Chip";
import React from "react";
import { useHref } from "react-router";
import { flagImage } from "./FlagAvatar";

export type DeckChipProps = {
    deck: {
        id: number;
        termLang: { id: string };
        title: string;
        description?: string | null;
    };
} & ChipProps<"a">;

export function DeckChip({ deck, ...props }: DeckChipProps) {
    // It would be easier to use <Link/> but that causes weird problems due to <Chip>
    // not forwarding the {to} prop correctly.
    const href = useHref("/lists/" + deck.id);

    return (
        <Chip
            key={deck.id}
            clickable
            component={"a"}
            href={href}
            avatar={<Avatar src={flagImage(deck.termLang.id)} />}
            label={deck.title}
            title={deck.description || undefined}
            {...props}
        />
    );
}
