import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import CheckIcon from "@mui/icons-material/Check";
import SaveIcon from "@mui/icons-material/Save";
import Alert from "@mui/material/Alert";
import React, { useState } from "react";
import { useParams } from "react-router";
import { useFormStyles } from "../../styles/forms";
import { ActionButton } from "../ActionButton";
import { useValidPasswordWithConfirm } from "../ActivateScreen/useValidatePassword";
import { LinkButton } from "../Links";
import { ScreenWrapper } from "../ScreenWrapper";
import { useConfirmResetPasswordMutation } from "../../graphql";

export function ConfirmResetPasswordScreen() {
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const formClasses = useFormStyles();
    const val = useValidPasswordWithConfirm(password, confirmPassword);
    const { code } = useParams<{ code: string }>();

    const [confirm, { called: confirmed, error: confirmError, loading: confirming }] =
        useConfirmResetPasswordMutation();
    const canSubmit = !(confirming || val.passwordError || val.confirmPasswordError);
    const onSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        if (code && canSubmit) confirm({ variables: { code, password } }).catch(_err => undefined); // This is handled by the hooks rather than the Promise rejection
    };

    return (
        <ScreenWrapper>
            <div className={formClasses.wrapper}>
                <Typography component="h1" variant="h5">
                    Reset your password
                </Typography>

                <p>To reset your password, enter your new password below.</p>

                <form className={formClasses.form} onSubmit={onSubmit}>
                    <TextField
                        type="password"
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="password"
                        label="Password"
                        name="password"
                        onChange={e => setPassword(e.target.value)}
                        error={val.passwordError}
                        helperText={
                            val.passwordMessage || "Use a strong password that you haven't used on any other websites."
                        }
                    />

                    <TextField
                        type="password"
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="confirmPassword"
                        label="Confirm Password"
                        name="confirmPassword"
                        onChange={e => setConfirmPassword(e.target.value)}
                        error={val.confirmPasswordError}
                        helperText={val.confirmPasswordMessage}
                    />

                    {confirmError && <Alert severity="error" children={confirmError.message} />}

                    {confirmed && !confirmError && (
                        <Alert severity="success" action={<LinkButton to="/">Home</LinkButton>}>
                            Your password has been changed.
                        </Alert>
                    )}

                    <ActionButton
                        fullWidth
                        icon={<SaveIcon />}
                        children="Set new password"
                        loading={confirming}
                        loadingText="Setting password\u2026"
                        error={confirmError}
                        errorText="Retry"
                        done={confirmed}
                        doneText="Password updated"
                        doneIcon={<CheckIcon />}
                        type="submit"
                        disabled={!canSubmit}
                        variant="contained"
                        color="primary"
                        className={formClasses.submit}
                    />
                </form>
            </div>
        </ScreenWrapper>
    );
}
