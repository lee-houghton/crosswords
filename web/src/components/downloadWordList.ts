import { saveAs } from "file-saver";
import { DeckDetailFragment } from "../graphql";

export function downloadWordList(deck: DeckDetailFragment) {
    const lines = ["Term,Definition\n"];

    const terms = deck.terms.toSorted((t1, t2) => t2.order - t1.order);
    for (const term of terms) {
        const definition = term.definitions.join("; ");
        lines.push(`${escapeCsv(term.term)},${escapeCsv(definition)}\n`);
    }

    // The Blob constructor correctly converts the UTF16 DOMString to UTF-8.
    // If you open the CSV file in Excel you will likely see gobbledygook, which is what you deserve for using Excel.
    const blob = new Blob(lines, { type: "text/csv;charset=utf8", endings: "native" });
    saveAs(blob, `${deck.title}.csv`);
}

function escapeCsv(value: string): string {
    // If the item contains a comma, double-quote, or newline, enclose it in double quotes and escape any double quotes inside it
    if (/[",\n]/.test(value)) {
        return `"${value.replace(/"/g, '""')}"`;
    }
    return value;
}
