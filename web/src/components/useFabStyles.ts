import makeStyles from '@mui/styles/makeStyles';

export const useFabStyles = makeStyles(theme => ({
    fab: {
        position: "fixed",
        bottom: theme.spacing(2),
        right: theme.spacing(2),
        zIndex: 1,
    }
}));
