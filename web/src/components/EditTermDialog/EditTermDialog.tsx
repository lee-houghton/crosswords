import { 
    Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, 
    FormControl, Hidden, IconButton, Input, InputAdornment, InputLabel, Tooltip 
} from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import SwapVert from "@mui/icons-material/SwapVert";
import React, { memo, useEffect, useRef, useState } from "react";
import { useTermInfoQuery, useUpdateTermMutation } from "../../graphql";
import { Alert } from "../Alert";
import { FlagIcon } from "../FlagIcon";
import { Title } from "../Title";

const useStyles = makeStyles(theme => ({
    form: {
        display: "flex",
        alignItems: "center",
    },
    inputs: {
        flex: 1,
        marginRight: theme.spacing(1),
    },
    term: {
        marginBottom: theme.spacing(2),
        [theme.breakpoints.up("md")]: {
            marginBottom: theme.spacing(4),
        }
    },
    wrapper: {
      margin: theme.spacing(1),
      position: "relative",
    },
    buttonProgress: {
        position: "absolute",
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
}));

interface EditTermDialogProps {
    deckId?: number;
    termId: number | null;
    open: boolean;
    onClose(): void;
}

// Stops onClick/onKeyDown from being propagated to the parent component (e.g. TermCard) which
// might handle these in a way that breaks EditTermDialog
const discard = (e: React.SyntheticEvent) => {
    e.stopPropagation();
};

export const EditTermDialog = memo(
    function EditTermDialog({ deckId, termId, open, onClose }: EditTermDialogProps) {
        const classes = useStyles();
        const termRef = useRef<HTMLInputElement | null>(null);
        const [term, setTerm] = useState("");
        const [definition, setDefinition] = useState("");

        const { data, loading, refetch, error: fetchError } = useTermInfoQuery({
            variables: { id: termId },
            onCompleted(data) {
                setTerm(data.term.term);
                setDefinition(data.term.definitions[0]);
            },
            skip: !termId
        });

        const deck = data?.term?.deck;
        const termLanguageId = deck?.termLang?.id;
        const definitionLanguageId = deck?.definitionLang?.id;
        
        const swap = () => {
            setTerm(definition);
            setDefinition(term);
        };
        
        const [save, { loading: saving, error: saveError }] = useUpdateTermMutation(termId ?? undefined, deckId);
        const submit = (e: React.FormEvent) => {
            e.preventDefault();

            save({
                variables: {
                    request: {
                        deckId,
                        id: termId, 
                        term, 
                        definitions: [definition],
                    }
                },
            }).then(() => {
                onClose();
            }, err => console.error(err))
        };

        const error = saveError || fetchError;

        useEffect(() => {
            // Reset the state when the dialog closes so that the next "Add term" dialog
            // won't have the values from the last dialog.
            if (!open) {
                setTerm("");
                setDefinition("");
            }
        }, [open]);

        const title = termId ? "Edit term" : "Add term";

        // Once loading has finished (and therefore the term field is focusable),
        // focus the term field. This is necessary because autoFocus doesn't work initially
        // since the text field is disabled while the term is loading.
        useEffect(() => {
            if (!loading && termId && termRef.current && document.activeElement !== termRef.current) {
                termRef.current.focus();
                termRef.current.select();
            }
        }, [loading, termId]);


        const handleKeyDown = (e: React.KeyboardEvent) => {
            e.stopPropagation();

            if (e.key === "Escape")
                onClose();
        }

        return (
            <Dialog open={open} onClose={onClose} aria-labelledby="edit-term-title" onClick={discard} onKeyDown={handleKeyDown}>
                <Title>{title}</Title>

                <form onSubmit={submit}>
                    <DialogTitle id="edit-term-title">{title}</DialogTitle>

                    {error && <Alert error={error} retry={() => refetch()}/>}

                    <DialogContent>
                        <Hidden mdDown>
                            <DialogContentText>
                                From word list: <strong>
                                    {data?.term?.deck?.title}
                                </strong>
                            </DialogContentText>
                        </Hidden>

                        <div className={classes.form}>
                            <div className={classes.inputs}>
                                <FormControl fullWidth className={classes.term}>
                                    <InputLabel>Term</InputLabel>
                                    <Input
                                        autoFocus
                                        // inputProps={{tabIndex:0}}
                                        inputRef={termRef}
                                        id="edit-term-term"
                                        autoComplete="crossword-term"
                                        fullWidth
                                        disabled={loading || saving}
                                        value={term}
                                        onChange={e => setTerm(e.target.value)}
                                        endAdornment={termLanguageId && 
                                            <InputAdornment position="end">
                                                <FlagIcon muted lang={termLanguageId}/>
                                            </InputAdornment>
                                        }
                                    />
                                </FormControl>

                                <FormControl fullWidth>
                                    <InputLabel>Definition</InputLabel>
                                    <Input
                                        id="edit-term-definition"
                                        autoComplete="crossword-definition"
                                        fullWidth
                                        disabled={loading || saving}
                                        value={definition}
                                        onChange={e => setDefinition(e.target.value)}
                                        endAdornment={definitionLanguageId && 
                                            <InputAdornment position="end">
                                                <FlagIcon muted lang={definitionLanguageId}/>
                                            </InputAdornment>
                                        }
                                    />
                                </FormControl>
                            </div>
                            
                            <Tooltip arrow title="Swap term and definition" aria-label="Swap term and definition">
                                <IconButton onClick={swap} size="large">
                                    <SwapVert/>
                                </IconButton>
                            </Tooltip>
                        </div>
                    </DialogContent>

                    <DialogActions>
                        <Button onClick={onClose}>
                            Cancel
                        </Button>
                        <div className={classes.wrapper}>
                            <Button type="submit" disabled={loading || saving} color="primary">
                                {saving && <CircularProgress size={24} className={classes.buttonProgress} />}
                                Save
                            </Button>
                        </div>
                    </DialogActions>
                </form>
            </Dialog>
        );
    }
);