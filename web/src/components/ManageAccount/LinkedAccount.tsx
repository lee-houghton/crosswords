import { Button, ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import Alert from '@mui/material/Alert';
import { ReactNode } from "react";
import { apiUrl } from "../../env";
import { ConnectedAccountType, ManageAccountFragment, useDisconnectAccountMutation } from "../../graphql/operations";
import { ActionButton } from "../ActionButton";

const useStyles = makeStyles((theme) => ({
    alert: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
    },
}));

interface LinkedAccountProps {
    type: ConnectedAccountType;
    user: ManageAccountFragment;
    icon: ReactNode;
}

export function LinkedAccount({ type, user, icon }: LinkedAccountProps) {
    const classes = useStyles();
    const [unlink, { error }] = useDisconnectAccountMutation({
        variables: { type },
        onError: () => {},
    });

    const connected = user.connectedAccounts.includes(type);
    const url = new URL(`connect/${type.toLowerCase()}`, apiUrl).href;

    return (
        <>
            {error && (
                <Alert className={classes.alert} severity="error">
                    {error.message}
                </Alert>
            )}
            <ListItem>
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText primary={titleCase(type)} secondary={connected ? "Connected" : "Not connected"} />
                <ListItemSecondaryAction>
                    {connected ? (
                        <ActionButton color="primary" onClick={unlink as any}>
                            Remove
                        </ActionButton>
                    ) : (
                        <Button component="a" href={url}>
                            Link
                        </Button>
                    )}
                </ListItemSecondaryAction>
            </ListItem>
        </>
    );
}

function titleCase(str: string) {
    return str[0].toUpperCase() + str.substring(1).toLowerCase();
}
