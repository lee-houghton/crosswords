import { Button, LinearProgress, Typography } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import React, { useCallback, useEffect } from "react";
import { useNavigate } from "react-router";
import { useConfirmChangeEmailMutation } from "../../graphql";
import { Alert } from "../Alert";
import { Info } from "../Info";

const useStyles = makeStyles((theme) => ({
    submit: {
        marginTop: theme.spacing(1),
        minWidth: "20em",
    },
    alert: {
        marginBottom: theme.spacing(2),
    },
}));

interface Props {
    code?: string;
}

export function ConfirmEmail({ code }: Props) {
    const [confirm, { loading: confirming, called: confirmed, error: confirmError }] = useConfirmChangeEmailMutation();
    const navigate = useNavigate();
    const classes = useStyles();

    useEffect(() => {
        if (confirmed && !confirmError && code) navigate("/profile/manage/email");
    }, [confirmed, confirmError, navigate, code]);

    const onConfirm = useCallback(() => {
        if (code) confirm({ variables: { code } });
    }, [confirm, code]);

    const done = confirmed && !confirmError;

    return (
        <>
            {code && (
                <Button
                    onClick={onConfirm}
                    className={classes.submit}
                    disabled={confirming || done}
                    variant="contained"
                    color="primary"
                >
                    Confirm new email address
                </Button>
            )}

            {confirming && (
                <>
                    <LinearProgress variant="indeterminate" />
                    <Typography align="center" color="textSecondary">
                        Confirming change of email address...
                    </Typography>
                </>
            )}

            {confirmed && !confirmError && (
                <Info success className={classes.alert} message={"Your email address has been changed."} />
            )}

            {confirmError && <Alert className={classes.alert} error={confirmError} />}
        </>
    );
}
