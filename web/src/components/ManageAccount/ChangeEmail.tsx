import { Button, TextField, Typography } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import { Save as SaveIcon } from "@mui/icons-material";
import Skeleton from '@mui/material/Skeleton';
import React, { ChangeEvent, FormEvent, useCallback, useState } from "react";
import { useCancelChangeEmailMutation } from "../../graphql";
import { useChangeEmailMutation } from "../../graphql";
import { Alert } from "../Alert";
import { Info } from "../Info";
import { ConfirmEmail } from "./ConfirmEmail";
import { ChangeEmailRequest } from "../../graphql";

const useStyles = makeStyles(theme => ({
    paragraph: {
        marginBottom: theme.spacing(1),
    },
    newEmail: {
        marginBottom: theme.spacing(1)
    },
    skeleton: {
        display: "inline-block",
        position: "relative",
        width: "10em",
        bottom: -2,
        margin: 0,
    },
    form: {
        display: "flex",
        flexDirection: "column",
        width: "100%",
    },
    alert: {
        marginTop: theme.spacing(1),
    },
    alertBoth: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    submit: {
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: theme.spacing(1),
        minWidth: "20em",
    },
}));

interface Props {
    code?: string;
    email?: string | null;
    changeEmailRequest?: Pick<ChangeEmailRequest, "id" | "newEmail">;
}

export function ChangeEmail({ code, email, changeEmailRequest }: Props) {
    const classes = useStyles();

    const requestId = changeEmailRequest && changeEmailRequest.id;
    const [changeEmail, { loading: changing, error: changeError }] = useChangeEmailMutation();
    const [cancel, { loading: cancelling, called: cancelled, error: cancelError }] = useCancelChangeEmailMutation();
    const [newEmail, setNewEmail] = useState<string>("");
    const onEmailChange = useCallback((e: ChangeEvent<HTMLInputElement>) => setNewEmail(e.target.value), [setNewEmail]);
    const onSubmit = (e: FormEvent) => {
        e.preventDefault();
        changeEmail({ variables: { email: newEmail } });
    };

    const onCancel = useCallback(() => {
        if (requestId)
            cancel({ variables: { id: requestId } });
    }, [cancel, requestId]);

    return <form className={classes.form} onSubmit={onSubmit}>
        <Typography component="p" className={classes.paragraph} color="textSecondary">
            Current email address:
            {" "}
            {email === undefined
                ? <Skeleton className={classes.skeleton} variant="text" />
                : <b>{email || "(No email)"}</b>
            }
        </Typography>
        
        {changeEmailRequest && <>
            <Typography component="p" className={classes.paragraph} color="textSecondary">
                Pending email address change:
                {" "}
                <b>{changeEmailRequest.newEmail}</b>
            </Typography>

            {cancelError && <Alert className={classes.alert} error={cancelError}/>}
            
            <Button onClick={onCancel} className={classes.submit} disabled={cancelling} variant="contained">
                Cancel
            </Button>
        </>}

        {cancelled && !changeEmailRequest && <Info className={classes.alertBoth} message={"Request cancelled."}/>}

        <ConfirmEmail code={code}/>

        {!changeEmailRequest && <> 
            <TextField 
                className={classes.newEmail}
                disabled={email === undefined}
                name="email" 
                label="New email address" 
                placeholder={email || undefined}
                variant="outlined"
                required
                type="email"
                onChange={onEmailChange}
            />

            <Typography component="p" color="textSecondary">
                A verificiation email will be sent to your new address.
            </Typography>

            {changeError && <Alert className={classes.alert} error={changeError}/>}

            <Button 
                className={classes.submit} 
                disabled={email === undefined || !newEmail || changing} 
                type="submit" 
                variant="contained" 
                color="primary"
            >
                <SaveIcon/> Save
            </Button>
        </>}
    </form>;
}

