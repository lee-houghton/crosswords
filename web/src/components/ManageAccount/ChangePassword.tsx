import makeStyles from '@mui/styles/makeStyles';
import TextField from "@mui/material/TextField";
import React, { useState, useCallback } from "react";
import { Alert } from "../Alert";
import { ActionButton } from "../ActionButton";
import SaveIcon from "@mui/icons-material/Save";
import CloseIcon from "@mui/icons-material/Close";
import { useValidatePassword } from "../ActivateScreen/useValidatePassword";
import Snackbar from "@mui/material/Snackbar";
import IconButton from "@mui/material/IconButton";
import Check from "@mui/icons-material/Check";
import { useChangePasswordMutation } from "../../graphql";

const useStyles = makeStyles(theme => ({
    form: {
        display: "flex",
        flexDirection: "column",
        width: "100%",
    },
    alert: {
        marginTop: theme.spacing(1),
    },
    submit: {
        marginTop: theme.spacing(1),
        minWidth: "20em",
    },
    submitWrapper: {
        textAlign: "center",
    },
    input: {
        marginBottom: theme.spacing(2),
    }
}));

export interface ChangePasswordProps {
    hasCurrentPassword?: boolean;
}

export function ChangePassword(props: ChangePasswordProps) {
    const classes = useStyles();
    const [currentPassword, setCurrentPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [changePassword, { loading, error, called }] = useChangePasswordMutation();
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const closeSnackbar = useCallback(() => setSnackbarOpen(false), []);

    // Password checking
    const passwordResult = useValidatePassword(newPassword);
    const passwordMessage = passwordResult && (
        passwordResult.feedback.warning
        || passwordResult.feedback.suggestions[0]
        || ["", "", "", "This is a good password!", "Congratulations! You are an excellent chooser of passwords!"][passwordResult.score]
    );
    const passwordError = Boolean(passwordResult && passwordResult.feedback.warning);
    const confirmPasswordError = Boolean(confirmPassword) && confirmPassword !== newPassword;
    const confirmPasswordMessage = confirmPasswordError ? "Passwords do not match" : undefined;

    const canSubmit = Boolean(
        !loading && 
        (currentPassword || !props.hasCurrentPassword) && 
        newPassword && 
        confirmPassword === newPassword
    );

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        if (!canSubmit)
            return;

        changePassword({ 
            variables: { 
                currentPassword, 
                newPassword 
            },
        }).then(() => {
            setSnackbarOpen(true);
            setCurrentPassword("");
            setNewPassword("");
            setConfirmPassword("");
        }, () => {
            // Nothing to do, alert is shown anyway
        });
    };

    return <form className={classes.form} onSubmit={handleSubmit}>
        {props.hasCurrentPassword && <TextField required
            className={classes.input}
            type="password"
            name="current-password"
            autoComplete="password"
            label="Current password" 
            variant="outlined"
            onChange={e => setCurrentPassword(e.target.value)}
        />}

        <TextField required
            className={classes.input}
            type="password"
            name="new-password"
            autoComplete="new-password"
            label="New password" 
            variant="outlined"
            onChange={e => setNewPassword(e.target.value)}
            error={passwordError}
            helperText={passwordMessage || "Use a strong password that you haven't used on any other websites."}
        />

        <TextField required
            className={classes.input}
            type="password"
            name="confirm-password"
            autoComplete="new-password"
            label="Confirm new password" 
            variant="outlined"
            onChange={e => setConfirmPassword(e.target.value)}
            error={confirmPasswordError}
            helperText={confirmPasswordMessage}
        />

        {error && <Alert className={classes.alert} error={error}/>}

        <div className={classes.submitWrapper}>
            <ActionButton 
                icon={<SaveIcon/>} children="Change password"
                className={classes.submit} 
                loading={loading} loadingText="Changing\u2026"
                done={called} doneText="Changed" doneIcon={<Check/>}
                error={error} errorText="Retry"
                disabled={!canSubmit} 
                type="submit" variant="contained" color="primary"
            />
        </div>

        <Snackbar
            open={snackbarOpen}
            autoHideDuration={6000}
            onClose={closeSnackbar}
            message="Your password has been changed!"
            action={
                <IconButton size="small" aria-label="close" color="inherit" onClick={closeSnackbar}>
                    <CloseIcon fontSize="small" />
                </IconButton>
            }
        />
    </form>
}
