import { List } from "@mui/material";
import React from "react";
import { ConnectedAccountType, ManageAccountFragment } from "../../graphql/operations";
import { FacebookLogoIcon } from "../../icons/FacebookLogo";
import { GoogleLogoIcon } from "../../icons/GoogleLogo";
import { LinkedAccount } from "./LinkedAccount";

interface LinkedAccountsProps {
    user: ManageAccountFragment;
}

export function LinkedAccounts({ user }: LinkedAccountsProps) {
    return <div>
        <List>
            <LinkedAccount user={user} type={ConnectedAccountType.Google} icon={<GoogleLogoIcon/>}/>
            <LinkedAccount user={user} type={ConnectedAccountType.Facebook} icon={<FacebookLogoIcon/>}/>
        </List> 
    </div>;
}
