import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import makeStyles from "@mui/styles/makeStyles";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Alert from "@mui/material/Alert";
import Skeleton from "@mui/material/Skeleton";
import { useCallback } from "react";
import { Planet } from "react-kawaii";
import { useNavigate, useOutletContext, useParams } from "react-router";
import { UserProfileFragment } from "../../graphql";
import { ManageAccountFragment } from "../../graphql/operations";
import { ChangeEmail } from "./ChangeEmail";
import { ChangePassword } from "./ChangePassword";
import { LinkedAccounts } from "./LinkedAccounts";

const useStyles = makeStyles(theme => ({
    heading: {
        flexShrink: 0,
        [theme.breakpoints.up("sm")]: {
            flexBasis: "33.3%",
        },
    },
    panels: {
        marginTop: theme.spacing(2),
    },
    subheading: {
        color: theme.palette.text.secondary,
        [theme.breakpoints.down("sm")]: {
            display: "none",
        },
    },
    details: {
        flexDirection: "column",
    },
    linked: {
        display: "block",
        padding: 0, // LinkedAccounts has its own padding
    },
    paragraph: {
        marginBottom: "1em",
    },
    closeAccount: {
        alignSelf: "center",
        minWidth: "20em",
    },
}));

const accordionSlotProps = { transition: { mountOnEnter: true } };

type ManageTab = "email" | "activity" | "accounts" | "password" | "close-account";

export function ManageAccount() {
    const me = useOutletContext<(UserProfileFragment & ManageAccountFragment) | null>();
    const { tab, code } = useParams<{ tab: ManageTab; code: string }>();
    const classes = useStyles();

    const navigate = useNavigate();
    const goto = useCallback(
        (tab?: string, open = true) => navigate(tab && open ? `/profile/manage/${tab}` : "/profile/manage"),
        [navigate]
    );

    return (
        <div>
            <Typography variant="h5" component="h2">
                Manage your account
            </Typography>

            <div className={classes.panels}>
                <Accordion
                    slotProps={accordionSlotProps}
                    expanded={tab === "email"}
                    onChange={(e, ex) => goto("email", ex)}
                >
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Change your email address</Typography>
                        <Typography className={classes.subheading}>
                            {me ? me.email : <Skeleton width={80} />}
                        </Typography>
                    </AccordionSummary>
                    <AccordionDetails className={classes.details}>
                        <ChangeEmail
                            email={me?.email}
                            changeEmailRequest={me?.changeEmailRequest || undefined}
                            code={code || undefined}
                        />
                    </AccordionDetails>
                </Accordion>

                <Accordion
                    slotProps={accordionSlotProps}
                    expanded={tab === "password"}
                    onChange={(e, ex) => goto("password", ex)}
                >
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Change your password</Typography>
                        <Typography className={classes.subheading}>********</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <ChangePassword hasCurrentPassword={me?.hasPassword ?? false} />
                    </AccordionDetails>
                </Accordion>

                <Accordion
                    slotProps={accordionSlotProps}
                    expanded={tab === "accounts"}
                    onChange={(e, ex) => goto("accounts", ex)}
                >
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Linked Accounts</Typography>
                    </AccordionSummary>
                    <AccordionDetails className={classes.linked}>{me && <LinkedAccounts user={me} />}</AccordionDetails>
                </Accordion>

                <Accordion
                    slotProps={accordionSlotProps}
                    expanded={tab === "activity"}
                    onChange={(e, ex) => goto("activity", ex)}
                >
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography>Account activity</Typography>
                    </AccordionSummary>
                    <AccordionDetails className={classes.details}>
                        <Box textAlign="center">
                            <Planet size={160} color="lightblue" mood="excited" />
                            <Alert
                                severity="info"
                                style={{ maxWidth: "fit-content", marginLeft: "auto", marginRight: "auto" }}
                            >
                                Sorry, this isn't implemented yet. Come back soon to see important account activity!
                            </Alert>
                        </Box>
                    </AccordionDetails>
                </Accordion>

                <Accordion
                    slotProps={accordionSlotProps}
                    expanded={tab === "close-account"}
                    onChange={(e, ex) => goto("close-account", ex)}
                >
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography>Close your account</Typography>
                    </AccordionSummary>
                    <AccordionDetails className={classes.details}>
                        <Typography className={classes.paragraph} component="p" color="textSecondary">
                            All your vocabulary lists, crosswords, and study history will be deleted.
                        </Typography>
                        <Typography className={classes.paragraph} component="p" color="textSecondary">
                            If another user has copied your vocabulary, their copy will not be deleted.
                        </Typography>
                        <Button className={classes.closeAccount} color="primary" variant="contained">
                            Close account
                        </Button>
                    </AccordionDetails>
                </Accordion>
            </div>
        </div>
    );
}
