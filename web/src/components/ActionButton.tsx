import Button, { ButtonProps } from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import makeStyles from '@mui/styles/makeStyles';
import clsx from "clsx";
import React from "react";

const useStyles = makeStyles(theme => ({
    wrapper: {
      margin: theme.spacing(1),
      position: "relative",
    },
    buttonProgress: {
        position: "absolute",
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
    buttonError: {
      backgroundColor: theme.palette.error.main,
      color: "white", // theme.palette.error.contrastText,
      "&:hover": {
        backgroundColor: theme.palette.error.dark,
      },
    },
    buttonSuccess: {
      backgroundColor: theme.palette.success.main,
      color: "white", // theme.palette.success.contrastText,
      "&:hover": {
        backgroundColor: theme.palette.success.dark,
      },
    },
}));

export interface ActionButtonProps extends ButtonProps {
    loading?: boolean;
    done?: boolean;
    error?: Error;
    className?: string;

    icon?: React.ReactNode;
    text?: React.ReactNode;
    loadingText?: React.ReactNode;
    doneText?: React.ReactNode;
    doneIcon?: React.ReactNode;
    errorText?: React.ReactNode;
}

export function ActionButton(props: ActionButtonProps) {
    const { 
        icon, text, children,
        loading, done, error, 
        loadingText, doneText, errorText, 
        doneIcon,
        className, color,
        ...buttonProps 
    } = props;

    const classes = useStyles();
    
    const buttonClassName = clsx(
        done && (error ? classes.buttonError : classes.buttonSuccess)
    );

    let displayedText = children;
    if (loading)
        displayedText = loadingText || displayedText;
    else if (error)
        displayedText = errorText || displayedText;
    else if (done)
        displayedText = doneText || displayedText;

    return <>
        <div className={className}>
            <Button {...buttonProps} className={buttonClassName}>
                {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
                {(done && !error && !loading && doneIcon) || icon} {displayedText}
            </Button>
        </div>
    </>;
}
