import Chip, { ChipProps } from "@mui/material/Chip";
import React from "react";
import { useHref } from "react-router";
import { UserAvatar } from "./Home/UserAvatar";

export interface UserChipProps extends ChipProps<"a"> {
    user: {
        name: string;
        displayName: string;
        profileImage?: string | null;
    };
}

export function UserChip({ user, ...props }: UserChipProps) {
    const href = useHref("/users/" + user.name);

    return (
        <Chip
            {...props}
            clickable
            component={"a"}
            href={href}
            avatar={<UserAvatar name={user.displayName} profileImage={user.profileImage || undefined} />}
            label={user.displayName}
        />
    );
}
