import React from "react";
import { ScreenWrapper } from "../ScreenWrapper";
import { useFormStyles } from "../../styles/forms";
import { Typography, TextField, Button } from "@mui/material";
import { Alert as MuiAlert } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { useCallback, useState } from "react";
import { useCreateDeckMutation } from "../../graphql";
import { Alert } from "../Alert";
import { useNavigate, useParams } from "react-router";
import { LanguageSelector } from "../LanguageSelector";
import { Link } from "react-router-dom";

function useTextFieldState(defaultValue: string, validate?: (value: string) => string | undefined) {
    const [value, setValue] = useState(defaultValue);
    const onTextChange = useCallback<React.ChangeEventHandler<HTMLInputElement>>(
        (e) => setValue(e.target.value),
        [setValue]
    );
    const error = validate ? validate(value) : undefined;
    return [value, setValue, onTextChange, error] as const;
}

const useStyles = makeStyles((theme) => ({
    language: {
        margin: theme.spacing(3, 0),
    },
}));

export function AddDeckScreen() {
    const navigate = useNavigate();
    const formClasses = useFormStyles();
    const classes = useStyles();
    const { lang } = useParams<{ lang?: string }>();

    const [title, , onTitleTextChange, titleError] = useTextFieldState("", (value) => {
        if (value && !value.trim()) return "Please enter a title";
        return undefined;
    });

    const [description, , onDescriptionTextChange] = useTextFieldState("");
    const [termLangId, setTermLangId] = useState<string | undefined>(lang);
    const [definitionLangId, setDefinitionLangId] = useState<string | undefined>(undefined);

    const [createDeck, { data: created, loading: creating, error: createError }] = useCreateDeckMutation();
    const canSubmit = Boolean(title) && !titleError && !creating && termLangId && definitionLangId;
    const onSubmit = useCallback<React.FormEventHandler>(
        (e) => {
            e.preventDefault();

            // Keep TypeScript happy
            if (!termLangId || !definitionLangId) return;

            createDeck({
                variables: {
                    request: {
                        title,
                        description,
                        termLangId,
                        definitionLangId,
                    },
                },
            });
        },
        [createDeck, title, description, termLangId, definitionLangId]
    );

    const createdId = created && created.createDeck.id;
    React.useEffect(() => {
        if (createdId) navigate(`/lists/${createdId}/edit`);
    }, [navigate, createdId]);

    return (
        <ScreenWrapper>
            <div className={formClasses.wrapper}>
                <Typography component="h1" variant="h5">
                    Create a list
                </Typography>

                <Typography paragraph variant="body1">
                    A list is a set of related terms which can be used to generate crosswords.
                </Typography>

                <Typography paragraph variant="body1">
                    These could be any topic that can be structured in a question/answer format - foreign languages,
                    trivia, studying for class&hellip;
                </Typography>

                <MuiAlert severity="info">
                    Want to import from a file?{" "}
                    <Link to="/lists/import">
                        <b>Got it covered!</b>
                    </Link>
                </MuiAlert>

                <form className={formClasses.form} onSubmit={onSubmit}>
                    <TextField
                        value={title}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="title"
                        name="title"
                        label="Title"
                        autoFocus
                        onChange={onTitleTextChange}
                        error={!!titleError}
                        helperText={titleError || "This is the name that will show in most places including lists."}
                    />

                    <TextField
                        value={description}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        id="description"
                        name="description"
                        label="Description"
                        helperText={
                            "Give some more context about where the list -- for example, where the terms come from or who the list might be useful to."
                        }
                        onChange={onDescriptionTextChange}
                    />

                    <LanguageSelector
                        id="term-language-id"
                        placeholder="Term language"
                        helpText="The language you are trying to learn"
                        className={classes.language}
                        languageId={termLangId}
                        onChange={setTermLangId}
                    />

                    <LanguageSelector
                        id="definition-language-id"
                        placeholder="Definition language"
                        helpText="The language that you already know"
                        className={classes.language}
                        languageId={definitionLangId}
                        onChange={setDefinitionLangId}
                    />

                    {createError && <Alert error={createError} />}

                    <Button
                        type="submit"
                        disabled={!canSubmit}
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={formClasses.submit}
                    >
                        Create
                    </Button>
                </form>
            </div>
        </ScreenWrapper>
    );
}
