import React from "react";
import { Helmet } from "react-helmet";

export function Title({children}: { children?: string }) {
    return <Helmet>
        <title>{children ? children + " - Crosswords" : "Crosswords"}</title>
    </Helmet>;
}
