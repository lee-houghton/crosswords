import { Button, Grid, Typography } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import AddIcon from "@mui/icons-material/Add";
import React, { useCallback } from "react";
import { Dispatch, Term } from "./reducer";
import { TermEditor } from "./TermEditor";

const useStyles = makeStyles(theme => ({
    buttons: {
        textAlign: "center",
    },
}));

interface TermListEditorProps {
    dispatch: Dispatch;
    deckId: number;
    terms: Term[];
    termLang: string | undefined;
    definitionLang: string | undefined;
    autoFocusIndex?: number;
}

export function TermListEditor(props: TermListEditorProps) {
    const {dispatch, terms, termLang, definitionLang} = props;
    const onAdd = useCallback(() => dispatch({ type: "ADD_TERM" }), [dispatch]);
    const classes = useStyles();

    return <Grid container spacing={1}>
        <Grid item xs={12}>
            <Typography component="h2" variant="h6">
                Flashcards
            </Typography>
        </Grid>
        {terms.map((term, i) => 
            <TermEditor key={term.id} 
                deckId={props.deckId}
                dispatch={dispatch}
                term={term} 
                termLang={termLang} 
                definitionLang={definitionLang}
                nextTerm={i === terms.length - 1 ? onAdd : undefined}
                autoFocus={i === props.autoFocusIndex}
            />
        )}
        <Grid item xs={12} className={classes.buttons}>
            <Button 
                color="primary" 
                onClick={onAdd} 
                variant="contained"
                startIcon={<AddIcon/>}
            >
                Add
            </Button>
        </Grid>
    </Grid>;
}
