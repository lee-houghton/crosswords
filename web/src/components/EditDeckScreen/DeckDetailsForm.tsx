import { Grid, Paper, TextField } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import React, { ChangeEvent, memo, useCallback } from "react";
import { Alert } from "../Alert";
import { LanguageSelector } from "../LanguageSelector";
import { IconTextField } from "./IconTextField";
import { Dispatch } from "./reducer";
import { SaveButton } from "./SaveButton";

const useStyles = makeStyles(theme => ({
    form: {
        padding: theme.spacing(2),
    },
    buttons: {
        textAlign: "right",
    },
}));

const useTitleFieldClasses = makeStyles({
    root: {
        // Same as Card title
        fontSize: "1.5rem",
        fontWeight: 400,
    },
});

interface Props {
    dispatch: Dispatch;

    title: string;
    description: string;
    termLang: string | undefined;
    definitionLang: string | undefined;

    canSave: boolean;
    saveError?: Error;
    onSave: () => void;
}

export const DeckDetailsForm = memo(
    ({ dispatch, termLang, definitionLang, title, description, canSave, saveError, onSave }: Props) => {
        const classes = useStyles();
        const titleClasses = useTitleFieldClasses();

        const onTitleChange = useCallback(
            (e: ChangeEvent<HTMLInputElement>) => dispatch({ type: "SET_TITLE", title: e.target.value }),
            [dispatch]
        );
        const onDescriptionChange = useCallback(
            (e: ChangeEvent<HTMLInputElement>) => dispatch({ type: "SET_DESCRIPTION", description: e.target.value }),
            [dispatch]
        );
        const onTermLangChange = useCallback(
            (languageId?: string) => dispatch({ type: "SET_TERM_LANG", languageId }),
            [dispatch]
        );
        const onDefinitionLangChange = useCallback(
            (languageId?: string) => dispatch({ type: "SET_DEFINITION_LANG", languageId }),
            [dispatch]
        );

        return (
            <Paper className={classes.form}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            id="title"
                            label="Name"
                            aria-label="Name"
                            required
                            fullWidth
                            margin="none"
                            InputProps={{ classes: titleClasses, disableUnderline: true }}
                            value={title}
                            onChange={onTitleChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <IconTextField
                            multiline
                            fullWidth
                            rows={2}
                            maxRows={4}
                            label="Description"
                            value={description}
                            onChange={onDescriptionChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <LanguageSelector
                            id="term-language"
                            placeholder="Term language"
                            helpText="The language you are trying to learn"
                            languageId={termLang}
                            onChange={onTermLangChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <LanguageSelector
                            id="definition-language"
                            placeholder="Definition language"
                            helpText="The language that you already know"
                            languageId={definitionLang}
                            onChange={onDefinitionLangChange}
                        />
                    </Grid>
                </Grid>

                {saveError && <Alert error={saveError} />}

                <div className={classes.buttons}>
                    <SaveButton enabled={canSave} onClick={onSave} />
                </div>
            </Paper>
        );
    }
);
