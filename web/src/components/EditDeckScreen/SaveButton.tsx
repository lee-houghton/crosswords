import Button, { ButtonProps } from "@mui/material/Button";
import makeStyles from '@mui/styles/makeStyles';
import { Save as SaveIcon } from "@mui/icons-material";
import React from "react";

const useStyles = makeStyles(theme => ({
    save: {
        margin: theme.spacing(3, 0, 2),
        minWidth: "10em",
    },
}));

interface Props {
    onClick: ButtonProps["onClick"]
    enabled: boolean;
}

export function SaveButton(props: Props) {
    const classes = useStyles();

    return (
        <Button disabled={!props.enabled} variant="contained" color="primary" fullWidth
            className={classes.save}
            startIcon={<SaveIcon/>}
            onClick={props.onClick}>
            Save
        </Button>
    );
}
