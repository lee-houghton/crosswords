import { Fab, Grid } from "@mui/material";
import { Add, Save } from "@mui/icons-material";
import { useCallback, useRef } from "react";
import { useParams } from "react-router";
import { useUpdateDeckMutation } from "../../graphql";
import { Alert } from "../Alert";
import { LoadingScreen } from "../LoadingScreen";
import { ScreenWrapper } from "../ScreenWrapper";
import { Title } from "../Title";
import { useFabStyles } from "../useFabStyles";
import { DeckDetailsForm } from "./DeckDetailsForm";
import { EditDeckStateProvider, useEditDeck } from "./reducer";
import { TermListEditor } from "./TermListEditor";
import { unstable_usePrompt } from "react-router-dom";

export function EditDeckScreen() {
    const deckId = parseInt(useParams<{ id: string }>().id!, 10);

    return (
        <ScreenWrapper>
            <EditDeckStateProvider deckId={deckId}>
                <View />
            </EditDeckStateProvider>
        </ScreenWrapper>
    );
}

function View() {
    const [state, dispatch] = useEditDeck();
    const { loading, error, canSave, title, description, terms, termLang, definitionLang } = state;
    const fabClasses = useFabStyles();

    // Reduces unnecessary re-renders of the details form by making onSave not change when
    // the contents of the form changes.
    const stateRef = useRef(state);
    stateRef.current = state;

    // TODO: Consider using useBlocker or persist unsaved state to sessionStorage as docs suggest
    unstable_usePrompt({
        message: "You have unsaved changes to this word list. Are you sure you want to navigate away?",
        when: state.modified,
    });

    const [updateDeck, { loading: saving, error: saveError }] = useUpdateDeckMutation();
    const onSave = useCallback(async () => {
        updateDeck({
            variables: {
                request: {
                    id: stateRef.current.deckId,
                    title: stateRef.current.title,
                    description: stateRef.current.description,
                    termLangId: stateRef.current.termLang,
                    definitionLangId: stateRef.current.definitionLang,
                    terms: stateRef.current.terms.map((term) => ({
                        // While editing, unsaved terms have a negative ID
                        id: term.id > 0 ? term.id : undefined,
                        term: term.term,
                        definitions: term.definitions,
                    })),
                },
            },
        });
        dispatch({ type: "SAVED" });
    }, [updateDeck, stateRef, dispatch]);
    const onAdd = useCallback(() => dispatch({ type: "ADD_TERM" }), [dispatch]);

    if (error) return <Alert error={error} />;

    if (loading) return <LoadingScreen />;

    return (
        <Grid container spacing={2}>
            <Title>{title}</Title>
            <Grid item xs={12}>
                <DeckDetailsForm
                    dispatch={dispatch}
                    title={title}
                    termLang={termLang}
                    definitionLang={definitionLang}
                    description={description}
                    canSave={canSave && !saving}
                    onSave={onSave}
                    saveError={saveError}
                />
            </Grid>
            <Grid item xs={12}>
                <TermListEditor
                    deckId={state.deckId}
                    dispatch={dispatch}
                    terms={terms}
                    termLang={termLang}
                    definitionLang={definitionLang}
                    autoFocusIndex={state.autoFocusIndex}
                />
            </Grid>
            <Fab disabled={saving} className={fabClasses.fab} color="primary" onClick={state.modified ? onSave : onAdd}>
                {state.modified ? <Save /> : <Add />}
            </Fab>
        </Grid>
    );
}
