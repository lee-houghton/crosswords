import { List, ListItem, ListItemText } from "@mui/material";
import React, { memo } from "react";
import { TermSuggestionFragment } from "../../graphql/operations";
import { Dispatch } from "./reducer";

interface SuggestionListProps {
    /** The ID of the term the suggestions are for (to allow accepting the suggestion) */
    termId: number;

    dispatch: Dispatch;

    terms: readonly TermSuggestionFragment[];
}

export const SuggestionList = memo(
    function SuggestionList({ dispatch, termId, terms }: SuggestionListProps) {
        return <List>
            {terms.map(term => 
                <Suggestion key={term.id} termId={termId} dispatch={dispatch} term={term} />
            )}
        </List>;
    }
);

interface SuggestionProps {
    termId: number;
    dispatch: Dispatch;
    term: TermSuggestionFragment;
}

function Suggestion({ termId, dispatch, term }: SuggestionProps) {
    const accept = () => {
        dispatch({
            type: "UPDATE_TERM",
            id: termId,
            update: {
                term: term.term,
                definitions: term.definitions,
            }
        });
    };

    return <ListItem dense button onClick={accept}>
        <ListItemText
            primary={`${term.term} • ${term.definitions.join(", ")}`}
            secondary={term.deck?.title} 
        />
    </ListItem>;
}
