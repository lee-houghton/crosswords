import React, { useRef, ReactNode, useState, useEffect } from "react";
import makeStyles from '@mui/styles/makeStyles';
import clsx from "clsx";

export interface SwipeActionsProps {
    children: ReactNode;
    left?: ReactNode;
    right?: ReactNode;
}

const MIN_SWIPE_DELTA = 4;
const SWIPE_ACTIONS_SIZE = 180;
const CLOSE_TIMEOUT = 2000;

const useStyles = makeStyles(theme => ({
    container: {
        overflow: "hidden",
        position: "relative",
    },
    content: {
        transform: `translate3d(${SWIPE_ACTIONS_SIZE}px, 0, 0)`,
        transition: `transform ${theme.transitions.duration.standard}ms ${theme.transitions.easing.easeInOut}`,
        overflow: "hidden",
        zIndex: 1,
    },
    swiping: {
        // While swiping, remove the transition delay
        transition: "none",
    },
    closed: {
        transform: `translate3d(0, 0, 0)`,
    },
    openLeft: {
        transform: `translate3d(${SWIPE_ACTIONS_SIZE}px, 0, 0)`,
    },
    openRight: {
        transform: `translate3d(${-SWIPE_ACTIONS_SIZE}px, 0, 0)`,
    },
    actionLeft: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        width: SWIPE_ACTIONS_SIZE,
    },
    actionRight: {
        position: "absolute",
        top: 0,
        bottom: 0,
        right: 0,
        width: SWIPE_ACTIONS_SIZE,
    }
}))

export function SwipeActions(props: SwipeActionsProps) {
    const classes = useStyles();
    const contentRef = useRef<HTMLDivElement>(null);
    const startX = useRef<number>();
    const direction = useRef(0);
    const swipeDelta = useRef(0);
    const [delta, setDelta] = useState(0);
    const [swiping, setSwiping] = useState(false);
    const [open, setOpen] = useState(0);
    const closeTimeout = useRef<number>();

    const onTouchStart = (e: React.TouchEvent) => {
        if (closeTimeout.current)
            clearTimeout(closeTimeout.current);

        setSwiping(true);
        if (startX.current === undefined)
            startX.current = e.touches[0].pageX;
    };

    const onTouchMove = (e: React.TouchEvent) => {
        swipeDelta.current = e.touches[0].pageX - startX.current!;
        const finalDelta = delta + swipeDelta.current;

        if (direction.current === 0 || open) {
            if (swipeDelta.current > MIN_SWIPE_DELTA && props.left)
                direction.current = 1;
            else if (swipeDelta.current < -MIN_SWIPE_DELTA && props.right)
                direction.current = -1;
        }

        if (direction.current !== 0 || open) {
            const dir = direction.current || open;

            e.preventDefault();
            const dx = dir > 0 
                ? Math.min(SWIPE_ACTIONS_SIZE, Math.max(0, finalDelta))
                : Math.max(-SWIPE_ACTIONS_SIZE, Math.min(0, finalDelta));
            contentRef.current!.style.transform = `translate3d(${dx}px, 0, 0)`;
        }
    };

    const onTouchEnd = (e: React.TouchEvent) => {
        if (e.touches.length > 0)
            return;

        const finalDelta = delta + swipeDelta.current;
        setSwiping(false);
        contentRef.current!.style.removeProperty("transform");

        if (finalDelta * direction.current > SWIPE_ACTIONS_SIZE * 0.5) {
            setDelta(SWIPE_ACTIONS_SIZE * direction.current);
            setOpen(direction.current);
            closeTimeout.current = window.setTimeout(close, CLOSE_TIMEOUT);
        } else {
            setDelta(0);
            setOpen(0);
        }

        startX.current = undefined;
        direction.current = 0;
    };
    
    const className = clsx(
        classes.content, 
        open === 1 && classes.openLeft, 
        open === 0 && classes.closed, 
        open === -1 && classes.openRight, 
        swiping && classes.swiping
    );

    const close = () => {
        if (closeTimeout.current)
            clearTimeout(closeTimeout.current);
        setDelta(0);
        setOpen(0);
    };

    useEffect(() => {
        const handleClick = close;
        if (open)
            document.addEventListener("click", handleClick, true);

        return () => {
            document.removeEventListener("click", handleClick, true);
        };
    }, [open]);

    return <div className={classes.container} onTouchStart={onTouchStart} onTouchMove={onTouchMove} onTouchEnd={onTouchEnd}>
        {props.left && <div className={classes.actionLeft} onClickCapture={close}>
            {props.left}
        </div>}
        {props.right && <div className={classes.actionRight}>
            {props.right}
        </div>}
        <div className={className} ref={contentRef}>
            {props.children}
        </div>
    </div>;
}
