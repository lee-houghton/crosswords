
import React, { createContext, ReactNode, useContext, useEffect, useReducer } from "react";
import { useDeckDetailQuery } from "../../graphql";
import { DeckDetailFragment } from "../../graphql";

export interface Term {
    readonly id: number;
    readonly term: string;
    readonly definitions: ReadonlyArray<string>;
}

export interface State {
    deckId: number;
    loading: boolean;
    error?: Error;
    title: string;
    description: string;
    modified: boolean;
    canSave: boolean;
    terms: Term[];
    nextId: number;
    termLang?: string;
    definitionLang?: string;

    /** 
     * The index of the last term which was added.
     * @remarks This allows us to autoFocus new terms after adding them. 
     */
    autoFocusIndex?: number;
}

function initialState(deckId: number): State {
    return {
        deckId,
        loading: true,
        title: "",
        description: "",
        modified: false,
        canSave: false,
        terms: [],
        nextId: -1,
    };
}

export type Action
    = { type: "LOAD_DECK", deck: DeckDetailFragment }
    | { type: "LOAD_ERROR", error: Error }
    | { type: "SET_TITLE", title: string }
    | { type: "SET_DESCRIPTION", description: string }
    | { type: "ADD_TERM" }
    | { type: "ADD_TERM_AFTER", id: number }
    | { type: "ADD_TERM_BEFORE", id: number }
    | { type: "UPDATE_TERM", id: number, update: Partial<Term> }
    | { type: "DELETE_TERM", id: number }
    | { type: "SET_TERM_LANG" | "SET_DEFINITION_LANG"; languageId: string | undefined }
    | { type: "SAVED" };

export type Dispatch = (action: Action) => void;

export interface EditDeckReducerProps { 
    state: State;
    dispatch: Dispatch;
}

function addTerm(state: State, index: number) {
    if (index === -1)
        index = state.terms.length - 1;

    const newTerm: Term = { 
        id: state.nextId, 
        term: "", 
        definitions: [""] 
    };

    const terms = [...state.terms];
    terms.splice(index, 0, newTerm);

    return { 
        ...state,
        modified: true,
        terms,
        nextId: state.nextId - 1, 
        autoFocusIndex: index,
    };
}

export function reducer(state: State, action: Action): State {
    switch (action.type) {
        case "LOAD_DECK":
            return {
                ...state,
                loading: false,
                error: undefined,
                title: action.deck.title,
                description: action.deck.description || "",
                canSave: true,
                termLang: action.deck.termLang.id,
                definitionLang: action.deck.definitionLang.id,
                terms: action.deck.terms.slice().sort((t1, t2) => t1.order - t2.order),
                autoFocusIndex: undefined,
            };
        case "LOAD_ERROR": 
            return {
                ...state,
                error: action.error,
                loading: false,
                canSave: false,
            };
        case "SET_TITLE":
            return { ...state, title: action.title, modified: true };
        case "SET_TERM_LANG":
            return { ...state, termLang: action.languageId, modified: true };
        case "SET_DEFINITION_LANG":
            return { ...state, definitionLang: action.languageId, modified: true };
        case "SET_DESCRIPTION":
            return { ...state, description: action.description, modified: true };
        case "ADD_TERM":
            return addTerm(state, state.terms.length);
        case "ADD_TERM_AFTER":
            return addTerm(state, state.terms.findIndex(t => t.id === action.id) + 1);
        case "ADD_TERM_BEFORE":
            return addTerm(state, state.terms.findIndex(t => t.id === action.id));
        case "UPDATE_TERM":
            return {
                ...state,
                modified: true,
                terms: state.terms.map(t => t.id !== action.id ? t : {
                    ...t,
                    ...action.update,
                }),
            };
        case "DELETE_TERM":
            return {
                ...state,
                modified: true,
                terms: state.terms.filter(t => t.id !== action.id),
                autoFocusIndex: undefined,
            };
        case "SAVED":
            return { ...state, modified: false };
    }
}

const StateContext = createContext<[State, Dispatch]>(undefined!);

export function EditDeckStateProvider(props: { deckId: number, children: ReactNode }) {
    const { data, error } = useDeckDetailQuery({ variables: { id: props.deckId } });
    const state = useReducer(reducer, initialState(props.deckId));
    const [, dispatch] = state;

    const deck = data && data.deck;

    useEffect(() => {
        if (deck) 
            dispatch({ type: "LOAD_DECK", deck });
        else if (error)
            dispatch({ type: "LOAD_ERROR", error });
    }, [dispatch, deck, error]);

    return <StateContext.Provider value={state}>
        {props.children}
    </StateContext.Provider>
}

export function useEditDeck() {
    return useContext(StateContext);
}
