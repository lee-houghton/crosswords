import { InputBaseProps } from "@mui/material/InputBase";
import React, { KeyboardEvent, useCallback } from "react";
import { useLanguageInfoQuery } from "../../graphql";
import { FlagIcon } from "../FlagIcon";
import { IconTextField } from "./IconTextField";

interface LanguageTextFieldProps extends InputBaseProps {
    language?: string;
    showFlag?: boolean;

    /** 
     * Called when the "tab" key is pressed.
     * 
     * @remarks If this props is set, the default action is always prevented.
     */
    onTab?: () => void;
}

export function LanguageTextField({language, showFlag, onTab, ...props}: LanguageTextFieldProps) {
    const { data: languageInfo } = useLanguageInfoQuery({ 
        variables: { id: language! }, 
        fetchPolicy: "cache-first",
        skip: !language
    });

    const onKeyDown = useCallback((e: KeyboardEvent<HTMLInputElement>) => {
        if (e.key === "Tab" && onTab && !e.getModifierState("Shift")) {
            e.preventDefault();
            onTab();
        }
    }, [onTab]);

    return (
        <IconTextField 
            label={languageInfo?.language?.name}
            {...props}
            onKeyDown={onTab && onKeyDown}
            icon={
                <FlagIcon 
                    muted
                    hidden={!showFlag}
                    lang={language}
                />
            }
        />
    );
}
