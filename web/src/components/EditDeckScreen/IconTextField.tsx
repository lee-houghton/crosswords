import { InputBase, Paper } from "@mui/material";
import { InputBaseProps } from "@mui/material/InputBase";
import makeStyles from '@mui/styles/makeStyles';
import clsx from "clsx";
import React, { ReactElement } from "react";

interface IconTextFieldProps extends InputBaseProps {
    className?: string;
    label?: string;
    icon?: ReactElement;
}

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        padding: "2px 4px",
        alignItems: "center",
        width: "100%",
        background: "#eee",
    },
    input: {
        marginRight: theme.spacing(1),
        flex: 1,
    },
}));

export function IconTextField(props: IconTextFieldProps) {
    const classes = useStyles();
    return (
        <Paper elevation={0} className={clsx(props.className, classes.root)}>
            <InputBase 
                placeholder={props.label}
                aria-label={props.label}
                {...props}
                className={classes.input}
            />
            {props.icon}
        </Paper>
    );
}