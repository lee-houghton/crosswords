import { Paper, Popper, PopperProps } from "@mui/material";
import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { PopoverOrigin } from "@mui/material/Popover";
import makeStyles from "@mui/styles/makeStyles";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import React, { ChangeEvent, memo, MouseEvent, useCallback, useEffect, useRef, useState } from "react";
import { useSuggestTermsQuery } from "../../graphql/operations";
import { LanguageTextField } from "./LanguageTextField";
import { Dispatch, Term } from "./reducer";
import { SuggestionList } from "./SuggestionList";
import { SwipeActions } from "./SwipeActions";

const useStyles = makeStyles(theme => ({
    paper: {
        background: theme.palette.background.paper,
        paddingTop: theme.spacing(2),
        paddingRight: 16 - 6, // theme.spacing(2) - 6,
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        display: "flex",
        // Show vertically on small screens, horizontally on larger screens
        [theme.breakpoints.down("md")]: {
            flexDirection: "column",
        },
    },
    swipeAction: {
        width: "100%",
        height: "100%",
    },
    term: {
        [theme.breakpoints.up("md")]: {
            flex: 1,
            marginRight: theme.spacing(1),
        },
        [theme.breakpoints.down("md")]: {
            marginBottom: theme.spacing(1),
        },
    },
    definition: {
        [theme.breakpoints.up("md")]: {
            flex: 1,
        },
    },
    // On large screens show a "..." kebab menu to the right.
    // TODO: On small screens, some other method of allowing secondary actions will be used. Either
    // 1. Buttons such as delete/move/swap term+definition
    // 2. Swipe left/right to reveal some buttons
    more: {
        padding: 6,
        [theme.breakpoints.down("md")]: {
            display: "none",
        },
    },
    popover: {
        marginTop: theme.spacing(1),
        zIndex: 10,
        width: "30ch",
    },
}));

interface TermEditorProps {
    dispatch: Dispatch;
    term: Term;
    termLang: string | undefined;
    definitionLang: string | undefined;

    /** Used to provide better suggestions */
    deckId: number;

    /**
     * A callback to be called when pressing Tab to go to the next term.
     * This is used to create a new term when pressing Tab on the last term in the list.
     * @remarks This overrides the default Tab key behaviour on the text field.
     */
    nextTerm?: () => void;

    /**
     * If true, autoFocus the term field.
     * @remarks This is set to true after adding a new term.
     */
    autoFocus?: boolean;
}

const popperModifiers: PopperProps["modifiers"] = [
    {
        name: "flip",
        enabled: false,
    },
    {
        name: "preventOverflow",
        enabled: false,
    },
];

export const TermEditor = memo((props: TermEditorProps) => {
    const { dispatch, term } = props;
    const termRef = useRef(null);
    const [showSuggestions, setShowSuggestions] = useState(true);

    // If either text field is focused, show little flag adornments
    const [focused, setFocused] = useState(false);
    const [suggestionsFocused, setSuggestionsFocused] = useState(false);

    const onTermChange = useCallback(
        (e: ChangeEvent<HTMLInputElement>) =>
            dispatch({ type: "UPDATE_TERM", id: term.id, update: { term: e.target.value } }),
        [term, dispatch]
    );
    const onDefinitionChange = useCallback(
        (e: ChangeEvent<HTMLInputElement>) =>
            dispatch({ type: "UPDATE_TERM", id: term.id, update: { definitions: [e.target.value] } }),
        [term, dispatch]
    );
    const classes = useStyles();

    const handleDelete = () => {
        dispatch({ type: "DELETE_TERM", id: term.id });
    };

    function handleKeyDown(e: React.KeyboardEvent) {
        if (e.key === "Escape") {
            setShowSuggestions(false);
            e.preventDefault();
        }
    }

    // const close = useCallback(() => setShowSuggestions(false), []);
    const shouldFetchSuggestions = (focused || suggestionsFocused) && term.term && !term.definitions?.[0];

    const suggest = useSuggestTermsQuery({
        variables: { search: term.term, termLang: props.termLang, excludeDeckId: props.deckId },
        skip: !shouldFetchSuggestions,
    });

    // When term changes, reset `showSuggestions` to true
    useEffect(() => {
        setShowSuggestions(true);
        setSuggestionsFocused(false);
    }, [term.term]);

    return (
        <Grid item xs={12}>
            <SwipeActions
                // left={" "}
                right={
                    <Button
                        className={classes.swipeAction}
                        color="secondary"
                        variant="contained"
                        onClick={handleDelete}
                    >
                        Delete
                    </Button>
                }
            >
                <div
                    className={classes.paper}
                    onFocus={useCallback(() => setFocused(true), [])}
                    onBlur={useCallback(() => setFocused(false), [])}
                >
                    <div ref={termRef} className={classes.term}>
                        <LanguageTextField
                            className={classes.term}
                            language={props.termLang}
                            value={term.term}
                            showFlag={focused}
                            onChange={onTermChange}
                            autoFocus={props.autoFocus}
                        />
                        {showSuggestions && suggest.data?.suggestTerms?.length ? (
                            <Popper
                                id="term-suggestions"
                                open={!!suggest.data?.suggestTerms.length}
                                anchorEl={termRef.current}
                                onKeyDown={handleKeyDown}
                                className={classes.popover}
                                placement="bottom-start"
                                modifiers={popperModifiers}
                                // Prevent the suggestions from being closed while the user is interacting with them.
                                onMouseDown={() => setSuggestionsFocused(true)}
                                onMouseUp={() => setSuggestionsFocused(false)}
                                onTouchStart={() => setSuggestionsFocused(true)}
                                onTouchEnd={e => setSuggestionsFocused(e.touches.length > 0)}
                            >
                                <Paper elevation={4}>
                                    <SuggestionList
                                        termId={term.id}
                                        dispatch={dispatch}
                                        terms={suggest.data?.suggestTerms}
                                    />
                                </Paper>
                            </Popper>
                        ) : null}
                    </div>

                    {/* TODO Show all definitions */}
                    <LanguageTextField
                        className={classes.definition}
                        language={props.definitionLang}
                        value={term.definitions[0]}
                        showFlag={focused}
                        onChange={onDefinitionChange}
                        onTab={props.nextTerm}
                    />

                    <ActionsButton dispatch={dispatch} className={classes.more} term={term} />
                </div>
            </SwipeActions>
        </Grid>
    );
});

interface ActionsButtonProps {
    className?: string;
    dispatch: Dispatch;
    term: Term;
}

const menuTransformOrigin: PopoverOrigin = { horizontal: "right", vertical: "top" };

function ActionsButton({ className, term, dispatch }: ActionsButtonProps) {
    const [anchorEl, setAnchorEl] = useState<Element | null>(null);
    const onClick = (e: MouseEvent) => setAnchorEl(e.currentTarget as Element);
    const onClose = () => setAnchorEl(null);

    const onDelete = useCallback(() => {
        dispatch({ type: "DELETE_TERM", id: term.id });
        setAnchorEl(null);
    }, [dispatch, term.id, setAnchorEl]);

    const addAbove = useCallback(() => {
        dispatch({ type: "ADD_TERM_BEFORE", id: term.id });
        setAnchorEl(null);
    }, [dispatch, term.id, setAnchorEl]);

    const addBelow = useCallback(() => {
        dispatch({ type: "ADD_TERM_AFTER", id: term.id });
        setAnchorEl(null);
    }, [dispatch, term.id, setAnchorEl]);

    return (
        <>
            <IconButton className={className} onClick={onClick} size="large">
                <MoreVertIcon />
            </IconButton>
            <Menu
                anchorEl={anchorEl}
                transformOrigin={menuTransformOrigin}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={onClose}
            >
                <MenuItem onClick={onClose}>Move up</MenuItem>
                <MenuItem onClick={onClose}>Move down</MenuItem>
                <MenuItem onClick={addAbove}>Add term above</MenuItem>
                <MenuItem onClick={addBelow}>Add term below</MenuItem>
                <Divider />
                <MenuItem onClick={onDelete}>Delete</MenuItem>
            </Menu>
        </>
    );
}
