import React from "react";
import makeStyles from '@mui/styles/makeStyles';
import { LogInWithGoogle as SignInWithGoogle } from "./SignInWithGoogle";
import { LogInWithFacebook as SignInWithFacebook } from "./SignInWithFacebook";
import clsx from "clsx";
import { LinkButton } from "../Links";

const useStyles = makeStyles(theme => ({
    signUp: {
        textAlign: "center",
        alignItems: "center",
    },
    signUpButton: {
        width: "13em",
        height: 40, // Match the Google/Facebook buttons
    },
    signIn: {
        textAlign: "center",
        "& > *": {
            margin: theme.spacing(1),
            verticalAlign: "middle", // Some set this, some don't - make them consistent
        }
    },
}));

export interface LoginButtonsProps {
    className?: string;
    showSignUp?: boolean;
}

export function LoginButtons({ showSignUp, className }: LoginButtonsProps) {
    const classes = useStyles();

    return <div className={clsx(className, classes.signIn)}>
        {showSignUp && <LinkButton className={classes.signUpButton} to="/register" variant="contained" color="primary">
            Sign up or log in
        </LinkButton>}
        <SignInWithGoogle/>
        <SignInWithFacebook/>
    </div>;
}
