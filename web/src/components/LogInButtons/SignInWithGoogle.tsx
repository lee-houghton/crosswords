import makeStyles from '@mui/styles/makeStyles';
import React from "react";
import { apiUrl } from "../../env";
import { GoogleLogoIcon } from "../../icons/GoogleLogo";

const theme = "light" as string;

const useStyles = makeStyles({
    button: {
        backgroundColor: theme === "dark" ? "rgb(66, 133, 244)" : "#fff",
        display: "inline-flex",
        alignItems: "center",
        justifyItems: "center",
        color: theme === "dark" ? "#fff" : "rgba(0, 0, 0, .54)",
        boxShadow: "0 2px 2px 0 rgba(0, 0, 0, .24), 0 0 1px 0 rgba(0, 0, 0, .24)",
        padding: 8,
        borderRadius: 2,
        border: "1px solid transparent",

        height: 40,
        fontWeight: 500,
        fontFamily: "Roboto, sans-serif",
        "&:hover": {
            opacity: 0.9,
        },
        "&:active": {
            backgroundColor: theme === "dark" ? "#3367D6" : "#eee",
            color: theme === "dark" ? "#fff" : "rgba(0, 0, 0, .54)",
            opacity: 1,
        },
    },
    logo: {
        width: 18,
        height: 18,
        marginRight: 15, // The recommended gap is 24dp, dp are at 160dpi whereas CSS pixels are 96dpi.
        // 24dp * (96dpi / 160dpi) = 14.4px
    },
});

export function LogInWithGoogle() {
    const classes = useStyles();
    const { href } = new URL("auth/google", apiUrl);

    return (
        <a className={classes.button} href={href}>
            <GoogleLogoIcon className={classes.logo} />
            Sign in with Google
        </a>
    );
}
