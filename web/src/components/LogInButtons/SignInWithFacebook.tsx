import makeStyles from '@mui/styles/makeStyles';
import React from "react";
import { apiUrl } from "../../env";
import { FacebookLogoIcon } from "../../icons/FacebookLogo";

const useStyles = makeStyles({
    button: {
        border: "1px solid #1877F2",
        color: "#1877F2",

        display: "inline-flex",
        alignItems: "center",
        justifyItems: "center",
        boxShadow: "0 2px 2px 0 rgba(0, 0, 0, .24), 0 0 1px 0 rgba(0, 0, 0, .24)",
        padding: 8,
        borderRadius: 2,

        height: 40,
        fontWeight: 500,
        fontFamily: "Roboto, sans-serif",
        "&:hover": {
            opacity: 0.9,
        },
        "&:active": {
            backgroundColor: "#eee",
            color: "rgba(0, 0, 0, .54)",
            opacity: 1,
        },
    },
    logo: {
        width: 18,
        height: 18,
        marginRight: 15,
    },
});

export function LogInWithFacebook() {
    const classes = useStyles();
    const { href } = new URL("auth/facebook", apiUrl);

    return (
        <a className={classes.button} href={href}>
            <FacebookLogoIcon className={classes.logo} />
            Sign in with Facebook
        </a>
    );
}
