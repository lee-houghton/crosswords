import { Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableFooter from "@mui/material/TableFooter";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Alert from '@mui/material/Alert';
import { formatRelative } from "date-fns";
import React, { useState } from "react";
import { AuditAddedFragment, useAuditAddedSubscription } from "../../graphql";
import { AuditCode } from "../../graphql/operations";
import { DeckChip } from "../DeckChip";
import { ScreenWrapper } from "../ScreenWrapper";
import { UserChip } from "../UserChip";

export function AdminScreen() {
    const [records, setRecords] = useState<AuditAddedFragment[]>([]);
    const { error } = useAuditAddedSubscription({
        onSubscriptionData: ({subscriptionData: { data }}) => {
            if (data)
                setRecords(r => [...r, data.auditAdded]);
        }
    });

    const now = new Date();

    return <ScreenWrapper>
        {error && <Alert severity="error">{error.message}</Alert>}
        {<Table>
            <TableHead>
                <TableRow>
                    <TableCell>When</TableCell>
                    <TableCell>Who</TableCell>
                    <TableCell>Action</TableCell>
                    <TableCell>What</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {records.map(audit => 
                    <TableRow key={audit.id}>
                        <TableCell>
                            <Typography variant="body1">
                                {formatRelative(new Date(audit.created), now)}
                            </Typography>
                        </TableCell>
                        <TableCell>
                            {audit.user && <UserChip user={audit.user}/>}
                        </TableCell>
                        <TableCell>
                            <Typography variant="body1">
                                {audit.code}
                            </Typography>
                        </TableCell>
                        <TableCell>
                            {audit.__typename === "DeckAudit" && audit.deck && <DeckChip deck={audit.deck}/>}
                            {audit.__typename === "UserAudit" && audit.otherUser && <UserChip user={audit.otherUser}/>}
                            {audit.code === AuditCode.GoogleAccountLinked && audit.parameters?.[0]}
                        </TableCell>
                    </TableRow>
                )}
            </TableBody>
            <TableFooter>
                <TableRow>
                    <TableCell colSpan={4}>
                        <Box textAlign="center">
                            <Typography variant="body1">
                                Waiting for more events...
                            </Typography>
                        </Box>
                    </TableCell>
                </TableRow>
            </TableFooter>
        </Table>}
    </ScreenWrapper>;
}
