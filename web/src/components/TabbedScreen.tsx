import Tabs from "@mui/material/Tabs";
import { useTheme } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";
import { ReactNode } from "react";
import { useLocation, useNavigate } from "react-router";
import { ScreenWrapper } from "./ScreenWrapper";
import { TabLink } from "./TabLink";

export interface TabbedScreenProps {
    children?: ReactNode;
    visible?: boolean;
    baseUrl: string;
    tabs: {
        url: string;
        exact?: boolean;
        title: string;
        content?: ReactNode;
    }[];
}

export function TabbedScreen({ tabs, children, visible, baseUrl }: TabbedScreenProps) {
    const theme = useTheme();
    const isLarge = useMediaQuery(theme.breakpoints.up("sm"));
    const navigate = useNavigate();
    const { pathname } = useLocation();

    // Almost like useState, except the state is stored in the URL...
    let index = tabs.findIndex((t) =>
        t.exact ? baseUrl + "/" + t.url === pathname : pathname.startsWith(baseUrl + "/" + t.url)
    );

    if (index < 0) index = 0;

    const tabStrip = (
        <Tabs
            centered={isLarge}
            variant={isLarge ? "standard" : "fullWidth"}
            value={index}
            indicatorColor="primary"
            textColor="primary"
        >
            {tabs.map((tab) => (
                <TabLink key={tab.url} to={tab.url} label={tab.title} />
            ))}
        </Tabs>
    );

    return (
        <ScreenWrapper tabStrip={tabStrip} topPadding={false}>
            {visible !== false && children}
        </ScreenWrapper>
    );
}
