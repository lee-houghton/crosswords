import makeStyles from '@mui/styles/makeStyles';
import clsx from "clsx";
import React from "react";

const flags = import.meta.glob("../../../../node_modules/svg-country-flags/svg/", { eager: true, import: "default" });
const keys = new Set<string>(Object.keys(flags));

const useStyles = makeStyles({
    flag: {
        height: "1em",
        boxShadow: "0px 0px 8px 0px rgba(0, 0, 0, 0.25)",
    },
    space: {
        marginRight: "1ex",
    },
    muted: {
        opacity: 0.5,
    },
});

interface Props {
    className?: string;

    /** The ISO-639-1 language ID, e.g. "fr". */
    lang?: string;

    /** An image (or other element) to show in case the flag is not found. */
    fallback?: React.ReactElement;

    /** Dims the flag to avoid creating too much of a distracting look. */
    muted?: boolean;

    /** If true, adds a small space after the flag (e.g. for if the flag is followed by text) */
    space?: boolean;

    // Standard CSS props (should spread ...props instead?)
    title?: string;
    hidden?: boolean;
}

export function FlagIcon(props: Props) {
    const classes = useStyles();
    const key = `./${flagIdFromLang(props.lang)}.svg`;

    if (keys.has(key)) {
        return (
            <img
                alt=""
                hidden={props.hidden}
                className={clsx(
                    props.className,
                    classes.flag,
                    props.muted && classes.muted,
                    props.space && classes.space
                )}
                src={flags[key] as string | undefined}
                title={props.title}
            />
        );
    }

    return props.fallback || null;
}

export const flagIdMap: Record<string, string> = {
    en: "gb",
    ja: "jp",
    zh: "cn",
};

function flagIdFromLang(lang?: string) {
    return (lang && flagIdMap[lang]) || lang;
}
