import type { ReactNode } from "react";
import { CircularProgress, Grid, Typography } from "@mui/material";

export interface LoadingGateProps {
    children: ReactNode;
    loading: boolean;
    loadingText?: string;
    error?: Error;
}

export function LoadingGate(props: LoadingGateProps) {
    const { loadingText = "Loading\u2026" } = props;

    if (props.loading) {
        return (
            <Grid container alignContent="center" justifyContent="center">
                <Grid item xs={12} lg={8}>
                    <CircularProgress title={loadingText} />
                </Grid>
            </Grid>
        );
    }

    if (props.error) {
        return (
            <Grid container alignContent="center" justifyContent="center">
                <Grid item xs={12} lg={8}>
                    <Typography component="h2">{props.error.message}</Typography>
                </Grid>
            </Grid>
        );
    }

    return <div>{props.children}</div>;
}
