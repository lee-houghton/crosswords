import Button from "@mui/material/Button";
import Hidden from "@mui/material/Hidden";
import makeStyles from "@mui/styles/makeStyles";
import clsx from "clsx";
import React, { useEffect, useMemo, useState } from "react";
import { useParams } from "react-router";
import { ClueDetailFragment, Direction, useCrosswordDetailQuery, useSaveAttemptMutation } from "../../graphql";
import { useAppTitle } from "../../TitleProvider";
import { getRootElement, useApplyClass } from "../../useApplyClass";
import { Alert } from "../Alert";
import { CrosswordLoading } from "../CrosswordLoading";
import { LoadingScreen } from "../LoadingScreen";
import { ScreenWrapper } from "../ScreenWrapper";
import { ClueLegend } from "./ClueLegend";
import { CrosswordCompleted } from "./CrosswordCompleted";
import { CrosswordPuzzle } from "./CrosswordPuzzle";
import { CurrentClue } from "./CurrentClue";
import { DeckList } from "./DeckList";
import { getUpdateAttemptRequest, GridRows, usePuzzleState } from "./reducer";

const useStyles = makeStyles(theme => ({
    screen: {
        display: "grid",
        margin: "0 auto",
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        gridTemplate: `
            "completed clues" auto
            "puzzle clues" 1fr
            "clue clues" auto
            "buttons clues" auto
            "decks clues" auto
            / 1fr auto
        `,
        [theme.breakpoints.down("md")]: {
            paddingTop: theme.spacing(1),
            paddingBottom: theme.spacing(1),
        },
    },
    playing: {
        [theme.breakpoints.down("md")]: {
            height: "100%",
            marginBottom: theme.spacing(0),
        },
        [theme.breakpoints.only("sm")]: {
            gridTemplate: `
                "completed" auto
                "puzzle" 1fr
                "clue" auto
                "buttons" auto
                "decks" auto
                / 1fr
            `,
        },
        // When playing, move the "Hint" button inline, almost
        // Otherwise, show it below the current clue
        [theme.breakpoints.only("xs")]: {
            gridTemplate: `
                ". completed completed ." auto
                ". puzzle puzzle ." minmax(auto, max-content)
                ". . . ." 1fr
                ". clue buttons ." auto
                ". decks decks ." auto
                / 1fr min-content auto 1fr
            `,
        },
    },
    puzzleContainer: {
        gridArea: "puzzle",
        justifySelf: "center",
        [theme.breakpoints.down("md")]: {
            overflow: "auto",
        },
    },
    completed: {
        padding: theme.spacing(0, 1),
        gridArea: "completed",
        justifyContent: "center",
        textAlign: "center",
        fontWeight: "bold",
        "@media print": { display: "none" },
    },
    clue: {
        gridArea: "clue",
        textAlign: "center",
        alignSelf: "center",
        "@media print": { display: "none" },
        marginTop: theme.spacing(1),
    },
    buttons: {
        gridArea: "buttons",
        alignSelf: "center",
        textAlign: "center",
        "@media print": { display: "none" },
    },
    clues: {
        gridArea: "clues",
        [theme.breakpoints.down("md")]: {
            display: "none",
        },
    },
    deckList: {
        gridArea: "decks",
        "@media print": { display: "none" },
    },
    fixedHeightInMobile: {
        [theme.breakpoints.down("md")]: {
            height: "100%",
        },
    },
    hideHeaderInMobile: {
        [theme.breakpoints.down("md")]: {
            "& #header": {
                display: "none",
            },
        },
    },
    showAnswers: {
        margin: theme.spacing(1, 0, 0, 1),
    },
}));

export function CrosswordScreen() {
    const classes = useStyles();
    const params = useParams<{ id: string }>();
    const id = params.id ? parseInt(params.id, 10) : -1;

    const [showAnswers, setShowAnswers] = useState(false);
    const { data, loading, error, startPolling, stopPolling } = useCrosswordDetailQuery({ variables: { id } });

    const [state, dispatch] = usePuzzleState(id, data?.crossword || undefined);

    const [save, { data: saved, loading: completing }] = useSaveAttemptMutation(id);
    const complete = async () => {
        await save({ complete: true, attempt: getUpdateAttemptRequest(id, state) });
        setShowAnswers(true);
    };

    const canShowAnswers = state.crossword?.available && !showAnswers;

    useAppTitle(
        state.clue && !showAnswers
            ? `${state.clue.number} ${state.clue.direction.toLowerCase()}: ${state.clue.clue} (${
                  state.clue.lengthHint
              })`
            : undefined
    );

    const onKey = (e: React.KeyboardEvent<HTMLDivElement>) => {
        if (e.key === "Enter" && e.getModifierState("Control")) {
            e.preventDefault();
            e.stopPropagation();
            if (!showAnswers) complete();
        }
    };

    const currentGuess = useMemo(() => getGuess(state.grid, state.clue), [state.grid, state.clue]);

    const mobileStyles = data?.crossword && !showAnswers;
    useApplyClass(document.body.parentElement!, mobileStyles ? classes.fixedHeightInMobile : undefined);
    useApplyClass(document.body, mobileStyles ? classes.fixedHeightInMobile : undefined);
    useApplyClass(getRootElement(), mobileStyles ? classes.fixedHeightInMobile : undefined);
    useApplyClass(document.body, mobileStyles ? classes.hideHeaderInMobile : undefined);

    // If the crossword ID changes reset the showAnswers state.
    useEffect(() => {
        setShowAnswers(false);
    }, [id]);

    if (!data || loading || !state.rows) return <LoadingScreen />;

    if (error || !data.crossword) return <Alert error={error || new Error("Crossword not found")} />;

    const { available, clues } = data.crossword;

    if (!available) startPolling(2000);
    else stopPolling();

    const { width, height } = state.crossword;
    const attempt = saved?.saveAttempt || state.crossword.attempt;

    // I believe you are allowed to use setState in the body of a render function, as long as
    // you're careful to avoid infinite loops. In this case, it negates its own condition,
    // so it should be fine.
    if (state.crossword.attempt?.completed && !showAnswers) setShowAnswers(true);

    return (
        <ScreenWrapper
            topPadding={false}
            className={clsx(classes.screen, !showAnswers && classes.playing)}
            margins="responsive"
        >
            {showAnswers && width && attempt && typeof attempt.score === "number" && (
                <CrosswordCompleted
                    className={classes.completed}
                    crossword={state.crossword}
                    score={attempt.score}
                    cols={width}
                />
            )}

            <div className={classes.puzzleContainer} onKeyDownCapture={onKey}>
                {available ? (
                    <CrosswordPuzzle
                        state={state}
                        dispatch={dispatch}
                        showAnswers={showAnswers}
                        readOnly={showAnswers}
                    />
                ) : (
                    <CrosswordLoading width={width || 14} height={height || 14} />
                )}
            </div>

            {state.clue && (
                <CurrentClue
                    className={classes.clue}
                    completed={showAnswers}
                    clue={state.clue}
                    guess={currentGuess}
                    showAnswer={showAnswers}
                    decks={state.crossword.decks}
                />
            )}

            <div className={classes.buttons}>
                {canShowAnswers && (
                    <Button
                        className={classes.showAnswers}
                        variant={state.filledIn ? "contained" : "contained"}
                        color={state.filledIn ? "primary" : undefined}
                        disabled={completing}
                        onClick={complete as any}
                    >
                        <Hidden smDown>Show answers</Hidden>
                        <Hidden smUp>Answers</Hidden>
                    </Button>
                )}
            </div>

            {/* Show which word lists a crossword was created from */}
            {data.crossword && showAnswers && <DeckList className={classes.deckList} crossword={data.crossword} />}

            {clues && <ClueLegend className={classes.clues} clues={clues} dispatch={dispatch} clue={state.clue} />}
        </ScreenWrapper>
    );
}

function getGuess(grid?: GridRows, clue?: ClueDetailFragment) {
    if (!grid || !clue) return undefined;

    let guess = "";
    for (let i = 0; i < clue.length; i++)
        guess +=
            grid[clue.y + (clue.direction === Direction.Down ? i : 0)][
                clue.x + (clue.direction === Direction.Across ? i : 0)
            ]?.guess || " ";

    return guess.trimRight();
}
