import Button, { ButtonProps } from "@mui/material/Button";
import React, { useState } from "react";
import { EditTermDialog } from "../EditTermDialog";

export interface EditTermButtonProps extends ButtonProps {
    deckId?: number;
    termId: number;
}

export function EditTermButton({ deckId, termId, children, ...buttonProps }: EditTermButtonProps) {
    const [open, setOpen] = useState(false);

    return <>
        <Button color="primary" size="small" variant="outlined" {...buttonProps} onClick={() => setOpen(true)}>
            {children}
        </Button>
        <EditTermDialog
            deckId={deckId}
            termId={termId}
            open={open}
            onClose={() => setOpen(false)}
        />
    </>;
}
