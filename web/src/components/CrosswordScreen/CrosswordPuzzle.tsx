import { Theme } from "@mui/material/styles";
import makeStyles from '@mui/styles/makeStyles';
import React, { ReactNode } from "react";
import { PuzzleGrid } from "./PuzzleGrid";
import { PuzzleInput } from "./PuzzleInput";
import { PuzzleAction, PuzzleState } from "./reducer";

const useStyles = makeStyles<Theme, { width: number }>(theme => ({
    puzzle: {
        flexDirection: "column",
        alignItems: "center",
        position: "relative",
        marginLeft: "auto",
        marginRight: "auto",
        padding: 1, // Ensure the cell outlines are visible
    },
    row: {
        display: "flex",
        flexDirection: "row",
    },
    cell: {
        // Ensure that on small screens, the puzzle isn't larger than the screen width
        maxWidth: props => `calc(${100/props.width}vw - 1px)`,
        maxHeight: props => `calc(${100/props.width}vw - 1px)`,
    },
}));

const useInputStyles = makeStyles<Theme, { x: number; y: number; width: number; height: number }>({
    input: {
        // Position the input over the correct grid cell
        position: "absolute",
        left: (({x, width}) => `${100 * (x/width)}%`), 
        width: (({width}) => `calc(100% / ${width} - 1px)`), 
        top: (({y, height}) => `${100 * (y/height)}%`), 
        height: (({height}) => `calc(100% / ${height} - 1px)`), 

        textAlign: "center", verticalAlign: "middle",
        textTransform: "uppercase",
        background: "lemonchiffon",
        backgroundClip: "padding-box", // Required for Firefox to show the cell borders
        fontSize: "1.25em",
        
        // Remove outline, even when focused
        borderWidth: 0,
        outline: "none",
        "&:focus": {
            outline: "none",
        },
        "@media print": {
            display: "none"
        }
    },
})

interface CrosswordPuzzleProps {
    state: PuzzleState;
    showAnswers: boolean;
    dispatch: React.Dispatch<PuzzleAction>;
    readOnly: boolean;
    children?: ReactNode;
}

export function CrosswordPuzzle({ children, state, dispatch, readOnly, showAnswers }: CrosswordPuzzleProps) {
    const { grid, cell } = state;
    const classes = useStyles({ width: state.crossword.width! }); // We know the crossword has been generated and has a width
    const {input} = useInputStyles({ 
        x: cell?.cell.x || 0, 
        y: cell?.cell.y || 0, 
        width: state.crossword.width || 1, 
        height: state.crossword.height || 1 
    });

    return (
        <div className={classes.puzzle}>
            <PuzzleGrid
                className={classes.row}
                cellClassName={classes.cell}
                showAnswers={showAnswers}
                grid={grid}
                dispatch={dispatch}
            >
                {cell && !readOnly && <PuzzleInput
                    x={cell.cell.x}
                    y={cell.cell.y}
                    className={input}
                    dispatch={dispatch}
                    value={cell.guess || ""}
                />}
            </PuzzleGrid>
            {children}
        </div>
    );
}

