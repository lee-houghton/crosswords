import clsx from "clsx";
import React, { memo, useCallback } from "react";
import { GridCell, PuzzleAction } from "./reducer";
import { useCellStyles } from "./styles";

export interface CellProps {
    cell: GridCell;
    dispatch: React.Dispatch<PuzzleAction>;
    showAnswer: boolean;

    /** Allow applying a class name to the Cell component to restrict its size (for example). */
    cellClassName?: string;
}

export const Cell = memo(function Cell(props: CellProps) {
    const { cell, dispatch } = props;
    const classes = useCellStyles();
    
    const onClick = useCallback((e: React.MouseEvent<HTMLInputElement>) => {
        e.preventDefault();
        dispatch({ type: "SelectCell", cell: cell.cell })
    }, [cell, dispatch]);

    const highlightClass = cellStyle(classes, cell, props.showAnswer);
    const className = clsx(classes.cell, props.cellClassName, highlightClass);

    return <div className={className} onClick={onClick}>
        {props.showAnswer ? (cell.cell.contents || cell.guess) : cell.guess}
        {cell.cell.hint && <span className={classes.hint}>
            {cell.cell.hint}
        </span>}
    </div>;
});

const collator = Intl.Collator(undefined, { sensitivity: "accent" });

function cellStyle(classes: ReturnType<typeof useCellStyles>, cell: GridCell, showAnswer: boolean) {
    if (showAnswer && cell.cell.contents && collator.compare(cell.cell.contents, cell.guess || "") !== 0)
        return classes.wrong;
    if (cell.active)
        return classes.active;
    return classes.empty;
}
