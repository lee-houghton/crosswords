import React, { memo } from "react";
import makeStyles from '@mui/styles/makeStyles';

interface SpacedGuessProps {
    guess?: string;
    answer: string;
    hint: string;
}

const useStyles = makeStyles({
    guess: {
        whiteSpace: "pre-wrap",
    }
});

export const SpacedGuess = memo(function SpacedGuess({ guess, answer, hint }: SpacedGuessProps) {
    const classes = useStyles();

    if (!guess)
        return null;

    interface Part {
        text: string;
        underline: boolean;
    }

    let parts: Part[] = [];
    let part: Part | undefined;
    let i = 0;

    for (const chars of hint.split(", ")) {
        const word = guess.substr(i, parseInt(chars)).toLowerCase();
        const originalWord = answer.substr(i, word.length).toLowerCase();

        // Add a space between words
        if (parts.length > 0) {
            if (part?.underline === false) {
                part.text += " ";
            }
            else {
                parts.push({ text: " ", underline: false });
                part = undefined;
            }
        }

        for (let j = 0; j < word.length; j++) {
            const underline = word[j] !== originalWord[j];

            // If the underline of this letter is different from the last,
            // start a new span.
            if (underline !== part?.underline) {
                part = { text: "", underline };
                parts.push(part);
            }

            part.text += word[j];
        }

        i += word.length;
    }

    return <span className={classes.guess}>
        {parts.map((part, i) => part.underline
            ? <u key={i}>{part.text}</u>
            : <span key={i}>{part.text}</span>
        )}
    </span>;
});
