import makeStyles from '@mui/styles/makeStyles';
import ListItem from "@mui/material/ListItem";
import clsx from "clsx";
import React, { Dispatch, useCallback } from "react";
import { PuzzleAction } from "./reducer";
import { ClueDetailFragment } from "../../graphql";

const useClueStyles = makeStyles({
    clue: {
        cursor: "pointer",
    },
    active: {
        "@media screen": {
            fontWeight: "bold",
        }
    }
});

interface ClueListItemProps {
    clue: ClueDetailFragment;
    active: boolean;
    dispatch: Dispatch<PuzzleAction>;
}

export function ClueListItem({ clue, active, dispatch }: ClueListItemProps) {
    const classes = useClueStyles();
    const onClick = useCallback(() => dispatch({ type: "SelectClue", clue }), [clue, dispatch]);

    return (
        <ListItem className={clsx(classes.clue, active && classes.active)} onClick={onClick}>
            {clue.number}. {clue.clue} ({clue.lengthHint})
        </ListItem>
    );
}
