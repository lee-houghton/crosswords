import React, { useLayoutEffect, useRef } from "react";
import { PuzzleCell } from "./reducer";

interface AutoFocusInputProps extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    cell?: PuzzleCell;
    focused?: boolean;
}

export function AutoFocusInput({ cell, focused, ...props }: AutoFocusInputProps) {
    const ref = useRef<HTMLInputElement>(null);
    
    // useLayoutEffect(() => {
    //     if (focused && ref.current && ref.current !== document.activeElement) {
    //         ref.current.focus();

    //         // On Android, this doesn't work without setTimeout. Why? Who knows.
    //         setTimeout(() => {
    //             if (ref.current) {
    //                 ref.current.selectionStart = 0;
    //                 ref.current.selectionEnd = ref.current.value.length;
    //             }
    //         }, 0);
    //     }
    // }, [focused]);

    // When the value changes, re-select the text
    // Because most likely the input has moved now
    useLayoutEffect(() => {
        if (ref.current) {
            console.log("focusing with value", ref.current.value)

            ref.current.focus();
            // ref.current.selectionStart = 0;
            // ref.current.selectionEnd = ref.current.value.length;
            // ref.current.selectionDirection = "forward";
            ref.current.setSelectionRange(0, ref.current.value.length, "forward");
        }
    }, [cell]);

    console.log("render with value", props.value);

    if (ref.current) {
        ref.current.value = props.value as string;
        ref.current.focus();
        ref.current.setSelectionRange(0, ref.current.value.length, "forward");
    }

    return <input ref={ref} {...props} />;
}
