import React, { Dispatch, memo } from "react";
import { GridCell, PuzzleAction } from "./reducer";
import { Cell } from "./Cell";
import { EmptyCell } from "./EmptyCell";

interface PuzzleRowProps {
    className: string;
    showAnswers: boolean;
    cells: (GridCell | undefined)[];
    cellClassName?: string;
    dispatch: Dispatch<PuzzleAction>;
}

export const PuzzleRow = memo(function PuzzleRow(props: PuzzleRowProps) {
    return <div className={props.className}>{
        props.cells.map((cell, x) => 
            cell 
                ? <Cell key={x} 
                    cell={cell}
                    cellClassName={props.cellClassName}
                    dispatch={props.dispatch}
                    showAnswer={props.showAnswers}
                />
                : <EmptyCell key={x} cellClassName={props.cellClassName}/>
        )
    }</div>
});
