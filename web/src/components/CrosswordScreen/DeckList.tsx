import Box from "@mui/material/Box";
import makeStyles from '@mui/styles/makeStyles';
import React, { memo } from "react";
import { DeckChip } from "../DeckChip";
import { UserChip } from "../UserChip";
import { CrosswordDetailFragment } from "../../graphql";

const useChipStyles = makeStyles(theme => ({
    chip: {
        margin: theme.spacing(0.5)
    },
}));

interface DeckListProps {
    className?: string;
    crossword: CrosswordDetailFragment;
}

export const DeckList = memo(function DeckList({ crossword, className }: DeckListProps) {
    const classes = useChipStyles();

    return <Box className={className} mt={1} textAlign="center">
        {crossword.decks.map(deck =>
            <DeckChip className={classes.chip} key={deck.id} deck={deck}/>
        )}
        {crossword.user && <UserChip user={crossword.user}/>}
    </Box>;
});
