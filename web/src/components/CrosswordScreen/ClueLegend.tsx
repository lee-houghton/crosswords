import Box from "@mui/material/Box";
import List from "@mui/material/List";
import ListSubheader from "@mui/material/ListSubheader";
import { Dispatch, memo } from "react";
import { ClueDetailFragment, Direction } from "../../graphql";
import { ClueListItem } from "./ClueListItem";
import { PuzzleAction } from "./reducer";

interface ClueLegendProps {
    className?: string;
    clue?: ClueDetailFragment;
    clues: readonly ClueDetailFragment[];
    dispatch: Dispatch<PuzzleAction>;
}

export const ClueLegend = memo(function ClueLegend(props: ClueLegendProps) {
    return (
        <Box
            className={props.className}
            display="flex"
            flexWrap="wrap"
            flexDirection="row"
            justifyContent="center"
            justifyItems="center"
        >
            <ClueList
                dispatch={props.dispatch}
                header="Across"
                clue={props.clue}
                clues={props.clues}
                direction={Direction.Across}
            />

            <ClueList
                dispatch={props.dispatch}
                header="Down"
                clue={props.clue}
                clues={props.clues}
                direction={Direction.Down}
            />
        </Box>
    );
});

function ClueList({
    dispatch,
    header,
    clue,
    clues,
    direction,
}: ClueLegendProps & { header: string; direction: Direction }) {
    return (
        <Box mx={1} minWidth="15em">
            <List>
                <ListSubheader>{header}</ListSubheader>
                {clues.map(
                    c =>
                        c.direction === direction && (
                            <ClueListItem key={c.id} clue={c} active={c === clue} dispatch={dispatch} />
                        )
                )}
            </List>
        </Box>
    );
}
