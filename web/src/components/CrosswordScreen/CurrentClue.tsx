import Typography from "@mui/material/Typography";
import React, { memo } from "react";
import { ClueDetailFragment, DeckCardInfoFragment, useMeQuery } from "../../graphql";
import { EditTermButton } from "./EditTermButton";
import { SpacedGuess } from "./SpacedGuess";

interface CurrentClueProps {
    className?: string;
    completed: boolean;
    clue: ClueDetailFragment;
    guess?: string;
    decks: readonly DeckCardInfoFragment[];
    showAnswer: boolean;
}

const collator = new Intl.Collator(undefined, { sensitivity: "accent" })

export const CurrentClue = memo(function CurrentClue({ className, completed, clue, decks, guess, showAnswer }: CurrentClueProps) {
    const deck = decks.find(d => d.id === clue.deckId);
    const { data: me } = useMeQuery();
    const editable = completed && deck && me?.me?.id === deck.createdBy.id;
    
    const wrong = guess && clue.answer && collator.compare(guess, clue.answer) !== 0;

    return <div className={className}>
        <Typography variant="subtitle1" component="p">
            {clue.number} {clue.direction.toLowerCase()}: {clue.clue}{" "}
            <Typography component="span" color="textSecondary">({clue.lengthHint})</Typography>{" "}
            {editable && clue.termId && <EditTermButton 
                deckId={clue.deckId}
                termId={clue.termId}
                children="Edit"
            />}
        </Typography>
        
        {showAnswer && wrong && <Typography variant="subtitle1" component="p" color="error">
            Your answer: <strong>
                <SpacedGuess guess={guess} answer={clue.answer!} hint={clue.lengthHint}/>
            </strong>
        </Typography>}

        {showAnswer && clue.answer && <Typography variant="subtitle1" component="p" color="secondary">
            {wrong ? "Correct answer" : "Answer"}: <strong>{clue.originalAnswer}</strong>
        </Typography>}
    </div>;
});
