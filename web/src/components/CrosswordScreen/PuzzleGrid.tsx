import React, { ReactNode, Dispatch, memo } from "react";
import { GridCell, PuzzleAction } from "./reducer";
import { PuzzleRow } from "./PuzzleRow";

interface PuzzleGridProps {
    children?: ReactNode;
    showAnswers: boolean;
    className: string;
    grid: (GridCell | undefined)[][];
    cellClassName?: string;
    dispatch: Dispatch<PuzzleAction>;
}

export const PuzzleGrid = memo(function PuzzleGrid(props: PuzzleGridProps) {
    return <>
        {props.grid.map((row, i) => <PuzzleRow
            key={i}
            showAnswers={props.showAnswers}
            className={props.className}
            cellClassName={props.cellClassName}
            cells={row}
            dispatch={props.dispatch}
        />)}
        {props.children}
    </>;
});
