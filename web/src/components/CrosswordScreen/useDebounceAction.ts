import { useState, useRef, useEffect, MutableRefObject } from "react";

function makeDebounced<T, TArgs extends any[]>(
    ref: MutableRefObject<{ debounced?: (...args: TArgs) => void, timeout?: number }>, 
    delay: number,
    setValue: (value: T) => void,
    action: (...args: TArgs) => T
) {
    return (...args: TArgs) => {
        if (ref.current.timeout)
            clearTimeout(ref.current.timeout);

        ref.current.timeout = window.setTimeout(() => {
            clearTimeout(ref.current.timeout);
            ref.current.timeout = undefined;
            setValue(action(...args));
        }, delay);
    };
};

export function useDebounceAction<T, TArgs extends any[]>(action: (...args: TArgs) => T, delay: number) {
    const [result, setResult] = useState<T | undefined>(undefined);
    const ref = useRef<{ debounced?: (...args: TArgs) => void, timeout?: number, value?: T }>({});

    // Set on first call only, otherwise it introduces new versions of the 
    // function which causes equality checks to fail
    if (!ref.current.debounced)
        ref.current.debounced = makeDebounced(ref, delay, setResult, action);

    // Reset the timer if the action/delay changes
    useEffect(() => {
        if (ref.current.timeout)
            clearTimeout(ref.current.timeout);
        ref.current.debounced = makeDebounced(ref, delay, setResult, action);
    }, [action, delay]);
    
    return [ref.current.debounced!, result] as const;
}
