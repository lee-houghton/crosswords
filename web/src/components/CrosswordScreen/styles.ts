import makeStyles from '@mui/styles/makeStyles';
import { red } from "@mui/material/colors";

export const useCellStyles = makeStyles(theme => ({
    cell: {
        // Centre the text/input
        display: "flex",
        justifyContent: "center",
        alignItems: "center",

        width: "2rem",
        height: "2rem",
        fontSize: "1.25em",
        [theme.breakpoints.only("xs")]: { fontSize: "1em" },
        textTransform: "uppercase",
        outline: "thin solid black",
        position: "relative", // Provide a stacking context within which to absolutely position the hint
        userSelect: "none",
        cursor: "default",
        backgroundClip: "padding-box", // Required for Firefox to show the cell borders

        // Don't print the cell contents
        // (Allows printing crosswords after showing the answers, e.g. for a friend)
        "@media print": {
            color: "transparent",
        }
    },
    active: {
        "@media screen": {
            backgroundColor: "lightgray",
        },
        "@media print": {
            backgroundColor: "white",
        }
    },
    void: {
        backgroundColor: "black !important",
    },
    empty: {
        backgroundColor: "white",
    },
    hint: {
        position: "absolute",
        fontSize: "0.5em",
        top: "2px", left: "2px",
        zIndex: 1,
        pointerEvents: "none",
        userSelect: "none",
        color: "black", // Override the inherited transparent colour in print styles
    },
    wrong: {
        "@media screen": {
            background: red[400],
            color: "white",
        }
    },
}));
