import React, { Dispatch, memo, useLayoutEffect, useRef, useEffect } from "react";
import { PuzzleAction } from "./reducer";

export interface PuzzleInputProps {
    x: number;
    y: number;
    className?: string;
    dispatch: Dispatch<PuzzleAction>;
    value: string;
}

// Android seems to treat all typing as composition (Gboard at least) which
// is very problematic for us.
const enableIME = !navigator.userAgent.includes("Android");

/**
 * Renders an <input> which allows editing the contents of the focussed cell.
 *
 * This is complicated because it needs to handle IME events such as
 * - compositionstart (when the user starts typing a complex character like é or 田中)
 * - compositionupdate (the user is still typing a complex characeter)
 * - compositionend (when the user finishes typing it)
 * - input (when the value of the input changes)
 * - keydown (mostly used for navigating between cells)
 *
 * The hard part is that browsers do not raise these events in the same order.
 * https://github.com/w3c/uievents/issues/202
 *
 * The other hard part is that in Chrome the input event is never even fired sometimes.
 * To work around this, we keep track of the last `input` event. Then, when
 * a compositionend event is received, we just check if the last input matches the
 * compositionend event's data property. If they don't match, then handleTextChange()
 * is called as it's assumed that there are no more input events coming.
 *
 * Mobile browsers often handle autocorrect and suggestions as IME events,
 * which may need extra work to handle without causing strange issues.
 */
export const PuzzleInput = memo(function PuzzleInput(props: PuzzleInputProps) {
    const { dispatch, className, value, x, y } = props;

    const composing = useRef(false);
    const lastInput = useRef<string>();
    const inputRef = useRef<HTMLInputElement>(null);
    const timeoutRef = useRef<number>();

    // Extracted out from onInput because it is also used by endComposition
    function handleTextChange() {
        if (!inputRef.current) return;

        // On Android, backspaces seem to manifest simply as an `input` event,
        // the actual backspace key event is lost.
        const newValue = inputRef.current.value.trim();

        if (value && !newValue) return dispatch({ type: "Backspace" });
        else if (newValue)
            // TODO: Loop through the unicode characters if there are multiple?
            dispatch({ type: "EnterGuess", x, y, text: newValue });

        // Help out the Android keyboard, hopefully
        // inputRef.current.value = "";
    }

    function startComposition(e: React.CompositionEvent<HTMLInputElement>) {
        if (!enableIME) return;

        if (import.meta.env.DEV) console.log("%c%s", "color: green", e.type, e.data, inputRef.current?.value);

        composing.current = true;
        lastInput.current = undefined;
    }

    function endComposition(e: React.CompositionEvent<HTMLInputElement>) {
        if (!enableIME) return;

        if (import.meta.env.DEV)
            console.log("%c%s", "color: green", e.type, e.data, inputRef.current?.value, lastInput.current);

        composing.current = false;

        // If this is the case, then it's likely there will not be another input event raised.
        // (i.e. Chrome or WebKit)
        //
        // However, we have to wait before handling the input event - otherwise, weird things happen.
        // For example, if the input gets modified while processing this event, it can cause a blank
        // input event which blanks out the input.
        //
        // TODO: Now Chrome has changed its behaviour in a way I don't understand.
        // Thanks Google!
        // if (lastInput.current === e.data)
        if (navigator.userAgent.includes("Chrome/")) setTimeout(handleTextChange, 0);
    }

    const onInput = (e: React.FormEvent<HTMLInputElement>) => {
        const { inputType, isComposing, data } = e.nativeEvent as InputEvent;

        if (import.meta.env.DEV)
            console.log(
                "%cinput, type = %s, isComposing = %s, data = %s",
                "color: rebeccapurple",
                inputType,
                isComposing,
                data
            );

        lastInput.current = data || undefined;

        // Don't dispatch EnterGuess during composition.
        // Instead, do that when composition ends.
        if (enableIME && (isComposing || composing.current)) return;

        e.preventDefault();
        e.stopPropagation();

        handleTextChange();
    };

    const onKey = (e: React.KeyboardEvent) => {
        if (import.meta.env.DEV) console.log("%conkeydown", "color: green", e.key);

        // Allow "Alt-Left" for back button and other such things
        if (e.getModifierState("Alt") || e.getModifierState("Control")) return;

        const shift = e.getModifierState("Shift");

        // Don't handle normal key events during IME composition
        // Oddly, e.nativeEvent.isComposing is never true here (in Chrome, at least)
        if (!composing.current && e.key && e.key.length === 1) {
            e.preventDefault();
            e.stopPropagation();
            dispatch({ type: "EnterGuess", x, y, text: e.key });
        }

        switch (e.key) {
            case "Tab":
            case "Enter":
                dispatch({ type: "Tab", shift });
                e.preventDefault();
                return false;

            case "Backspace":
            case "Delete":
            case "ArrowLeft":
            case "ArrowRight":
            case "ArrowUp":
            case "ArrowDown":
                e.preventDefault();
                e.stopPropagation();
                dispatch({ type: e.key });
                return false;

            // Android sends Unidentified key events for most keypresses, unfortunately...
            // case "Unidentified":
            //e.preventDefault();
            //return false;
            // return;
        }
        return true;
    };

    // Ensure that the text entry field has the text selected to allow easy typing.
    useLayoutEffect(() => {
        if (inputRef.current) {
            const input = inputRef.current;
            if (timeoutRef.current) clearTimeout(timeoutRef.current);

            const apply = () => {
                if (input !== document.activeElement) input.focus();
                if (value) input.select();
            };

            // Apply the changes, but also after a timeout because Android is special
            apply();
            timeoutRef.current = window.setTimeout(apply, 0);
        }
    });

    // We can't use react's controlled inputs because it messes with IME composition events,
    // so the best we can get is using `useEffect` to update the value when the props change.
    //
    // This should also avoid the case where a `Saved` action fires, causing the PuzzleInput
    // to re-render and replacing the current IME text with the stored state.
    //
    // X, Y are included because the input needs to be cleared when moving to the next square.
    useEffect(() => {
        if (inputRef.current && inputRef.current.value !== value) inputRef.current.value = value;

        composing.current = false;
    }, [x, y, value]);

    return (
        <input
            ref={inputRef}
            className={className}
            onInput={onInput}
            onKeyDown={onKey}
            onCompositionStart={startComposition}
            onCompositionUpdate={startComposition}
            onCompositionEnd={endComposition}
            // https://www.codementor.io/@leonardofaria/disabling-autofill-in-chrome-zec47xcui
            autoComplete="crossword-cell"
            autoCorrect="off"
            autoCapitalize="off"
            spellCheck={false}
        />
    );
});
