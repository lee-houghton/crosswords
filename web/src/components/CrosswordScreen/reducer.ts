import { useEffect, useReducer } from "react";
import {
    ClueDetailFragment,
    CrosswordDetailFragment,
    Direction,
    UpdateAttemptRequest,
    UpdateGuessRequest,
    useMeQuery,
    useSaveAttemptMutation,
} from "../../graphql";
import { useDebounceAction } from "./useDebounceAction";

export interface PuzzleCell {
    x: number;
    y: number;
    filled: boolean;
    contents?: string;
    hint?: string;

    across?: ClueDetailFragment;
    down?: ClueDetailFragment;
}

export interface GridCell {
    cell: PuzzleCell;
    active: boolean;
    guess?: string;
}

export type GridRows = (GridCell | undefined)[][];

export interface PuzzleState {
    crossword: CrosswordDetailFragment;
    rows: PuzzleCell[][];
    grid: GridRows;

    filledIn: boolean;
    modified: boolean;

    // Current selection
    clue?: ClueDetailFragment;
    cell?: GridCell;
    composing: boolean;
}

function initialState(
    crossword: CrosswordDetailFragment | undefined,
    copyGuessesFrom?: GridRows
): Readonly<PuzzleState> {
    // Needed only because you can't useReducer after an early return out of the component's function
    if (!crossword) return {} as any;

    if (!crossword.clues) throw new Error("Crossword is not generated yet");

    // Try to find an across clue, then just any clue
    const clue = crossword.clues.find((c) => c.direction === "ACROSS") || crossword.clues[0];

    const { rows, grid } = compileGrid(crossword, clue, copyGuessesFrom);

    // This is actually expected if it's just been generated - in this case, the hook will
    // keep polling until it is generated
    // if (!clue)
    //     throw new Error("Crossword has no clues");

    return {
        crossword,
        rows,
        grid,
        filledIn: isFilledIn(grid),
        modified: false,
        clue,
        cell: clue && grid[clue.y]?.[clue.x],
        composing: false,
    };
}

function isFilledIn(grid: GridRows) {
    return grid.every((r) => r.every((c) => c?.guess || !c?.cell.filled));
}

export type PuzzleAction =
    | { type: "Backspace" | "Delete" | "ArrowLeft" | "ArrowUp" | "ArrowDown" | "ArrowRight" }
    | { type: "Tab"; shift: boolean }
    | { type: "StartComposition"; text: string }
    | { type: "UpdateComposition"; text: string }
    | { type: "EnterGuess"; x: number; y: number; text: string }
    | { type: "SelectCell" | "FocusCell"; cell: PuzzleCell }
    | { type: "SelectClue"; clue: ClueDetailFragment }
    | { type: "Reset"; crossword?: CrosswordDetailFragment; resetGuesses: boolean }
    | { type: "Save" | "Saved" }
    | { type: "SaveError"; error: Error };

/**
 * Given a state, find the "next" cell in the current clue (if it exists).
 *
 * This is only within a single clue, jumping to the next/previous clue is handled by the Tab or Shift+Tab.
 *
 * @param state The current state
 * @returns The "next" GridCell
 */
function nextCell(state: PuzzleState, backwards = false): GridCell | undefined {
    const { clue, cell } = state;

    if (!clue || !cell) return undefined;

    const dx = clue.direction === "ACROSS" ? 1 : 0;
    const dy = 1 - dx;
    const sign = backwards ? -1 : 1;
    return state.grid[cell.cell.y + sign * dy]?.[cell.cell.x + sign * dx]; // If it doesn't exist, we don't move
}

function updateGrid(grid: GridRows, update: (cell: GridCell) => GridCell) {
    let gridModified = false;

    for (let y = 0; y < grid.length; y++) {
        let row = grid[y];
        let rowModified = false;

        for (let x = 0; x < grid.length; x++) {
            const cell = row[x];
            if (!cell) continue;

            const newCell = update(cell);
            if (newCell === cell) continue;

            if (!rowModified) row = row.slice();

            rowModified = true;
            row[x] = newCell;
        }

        if (rowModified) {
            if (!gridModified) grid = grid.slice();

            gridModified = true;
            grid[y] = row;
        }
    }

    return grid;
}

function moveAndUpdate(state: PuzzleState, newGuess: string | undefined, newCell: GridCell, composing = false) {
    const grid = updateGrid(state.grid, (cell) => {
        const guess = cell === state.cell ? newGuess ?? cell.guess : cell.guess;
        if (guess === cell.guess) return cell;

        return { ...cell, guess };
    });

    if (grid === state.grid && newCell === state.cell && state.composing !== composing) return state;

    let filledIn = state.filledIn;
    if (newGuess === "") filledIn = false;
    else filledIn = isFilledIn(grid);

    return {
        ...state,
        filledIn,
        grid,
        modified: state.modified || newGuess !== undefined,
        cell: grid[newCell.cell.y][newCell.cell.x],
        composing,
    };
}

function changeClue(state: PuzzleState, clue: ClueDetailFragment, newCell: GridCell) {
    if (clue === state.clue && newCell === state.cell) return state;

    const grid = updateGrid(state.grid, (cell) => {
        const active = cell.cell.across === clue || cell.cell.down === clue;
        const same = active === cell.active;
        if (same) return cell;

        return { ...cell, active };
    });

    return {
        ...state,
        grid,
        clue,
        cell: grid[newCell.cell.y][newCell.cell.x],
        composing: false,
    };
}

function arrowKey(state: PuzzleState, key: string): PuzzleState {
    const { clue, cell } = state;

    if (!clue || !cell) return state;

    const { direction } = clue;

    if (key === "Delete" || key === "Backspace") {
        const newCell = nextCell(state, key === "Backspace");
        if (!newCell && !cell.guess && !state.composing) return state; // Optimisation

        return moveAndUpdate(state, "", newCell || cell);
    }

    // Changing direction on the current square
    // TODO: Is it more intuitive to also move one square in the indicated direction?
    if ((key === "ArrowLeft" || key === "ArrowRight") && direction === "DOWN" && cell.cell.across)
        return changeClue(state, cell.cell.across, cell);

    if ((key === "ArrowUp" || key === "ArrowDown") && direction === "ACROSS" && cell.cell.down)
        return changeClue(state, cell.cell.down, cell);

    switch (key) {
        case "ArrowLeft":
            return moveAndUpdate(state, undefined, state.grid[cell.cell.y][cell.cell.x - 1] || cell);

        case "ArrowRight":
            return moveAndUpdate(state, undefined, state.grid[cell.cell.y][cell.cell.x + 1] || cell);

        case "ArrowUp":
            return moveAndUpdate(state, undefined, state.grid[cell.cell.y - 1]?.[cell.cell.x] || cell);

        case "ArrowDown":
            return moveAndUpdate(state, undefined, state.grid[cell.cell.y + 1]?.[cell.cell.x] || cell);
    }

    return state;
}

function prevClue(state: PuzzleState) {
    const {
        clue,
        crossword: { clues },
    } = state;
    if (!clue || !clues) return clue;

    const sameDirection = clues
        .filter((c) => c.direction === clue.direction && c.number < clue.number)
        .sort((c1, c2) => c1.number - c2.number);
    if (sameDirection.length > 0) return sameDirection[sameDirection.length - 1];

    const otherDirection = clues.filter((c) => c.direction !== clue.direction).sort((c1, c2) => c1.number - c2.number);
    return otherDirection[otherDirection.length - 1] || clue;
}

function nextClue(state: PuzzleState) {
    const {
        clue,
        crossword: { clues },
    } = state;
    if (!clue || !clues) return clue;

    const sameDirection = clues
        .filter((c) => c.direction === clue.direction && c.number > clue.number)
        .sort((c1, c2) => c1.number - c2.number);
    if (sameDirection.length > 0) return sameDirection[0];

    const otherDirection = clues.filter((c) => c.direction !== clue.direction).sort((c1, c2) => c1.number - c2.number);
    return otherDirection[0] || clue;
}

function puzzleReducer(state: PuzzleState, action: PuzzleAction): PuzzleState {
    if (action.type === "Reset") return initialState(action.crossword, action.resetGuesses ? undefined : state.grid);

    if (!state.clue || !state.cell) return state;

    if (import.meta.env.DEV)
        console.log("%c%s", "color: goldenrod", state.cell?.cell.x, state.cell?.cell.y, action.type, action);

    switch (action.type) {
        case "Backspace":
        case "Delete":
        case "ArrowLeft":
        case "ArrowRight":
        case "ArrowDown":
        case "ArrowUp":
            return arrowKey(state, action.type);

        case "StartComposition":
            return moveAndUpdate(state, action.text, state.cell, true);

        case "UpdateComposition":
            return moveAndUpdate(state, action.text, state.cell, true);

        case "Tab": {
            const clue = action.shift ? prevClue(state) : nextClue(state);
            const cell = clue && state.grid[clue.y][clue.x];
            if (!clue || !cell) return state;
            return changeClue(state, clue, cell);
        }

        case "EnterGuess":
            // Guard against duplicate input events by tracking the X and Y when
            // the text was entered.
            if (action.x !== state.cell.cell.x || action.y !== state.cell.cell.y) return state;

            for (const char of action.text) state = moveAndUpdate(state, char, nextCell(state) || state.cell!);

            return state;

        // In this case, we want to keep the current direction if possible.
        case "FocusCell": {
            const { cell } = action;
            const gridCell = state.grid[cell.y][cell.x];
            if (!gridCell) return state;

            // After focusing a new cell, we have to update state.clue.
            //
            // We also need to check that the current direction is still viable,
            // if moving to a cell that doesn't support the current direction then
            // we need to use the other one.
            let direction = state.clue.direction;
            if (direction === "DOWN" && !cell.down) direction = Direction.Across;
            else if (direction === "ACROSS" && !cell.across) direction = Direction.Down;

            const clue = direction === "DOWN" ? cell.down : cell.across;
            if (!clue) return state; // Not possible

            return changeClue(state, clue, gridCell);
        }

        // Handles specifically when the user clicks on a cell, which generally means
        // they clicked on the current cell and want to switch directions.
        case "SelectCell": {
            const altDirection = state.clue.direction === "ACROSS" ? "DOWN" : "ACROSS";
            const { cell } = action;
            const gridCell = state.grid[cell.y][cell.x];
            if (!gridCell) return state;

            const clue =
                gridCell.cell === state.cell.cell
                    ? // Find a clue perpendicular to the current one at the same cell
                      altDirection === "ACROSS"
                        ? cell.across
                        : cell.down
                    : // Try to find a clue in the same direction, otherwise a clue in the
                      // the perpendicular direction
                      (state.clue.direction === "ACROSS" && cell.across) ||
                      (state.clue.direction === "DOWN" && cell.down) ||
                      cell.across ||
                      cell.down;

            if (!clue) return state; // TODO warn/throw?
            return changeClue(state, clue, gridCell);
        }

        case "SelectClue": {
            const cell = state.grid[action.clue.y][action.clue.x]!;
            if (!cell) return state; // TODO warn/throw?
            return changeClue(state, action.clue, cell);
        }

        case "Saved":
            return {
                ...state,
                modified: false,
            };
    }

    return state;
}

export function usePuzzleState(crosswordId: number, crossword: CrosswordDetailFragment | undefined) {
    const reducerValue = useReducer(puzzleReducer, crossword, initialState);
    const [state, dispatch] = reducerValue;
    const [save, { called: saved, loading: saving, error: saveError }] = useSaveAttemptMutation(crosswordId);
    const [saveDebounced] = useDebounceAction((attempt: UpdateAttemptRequest) => {
        dispatch({ type: "Save" });
        save({ attempt });
    }, 2000);

    // useReducer() doesn't reset when the `initialState` changes
    // so we need to dispatch a Reset action with the new initial state.
    //
    // Also reset to blank if the ID changes without unmounting (e.g. ID in the URL changes)
    const shouldResetGuesses = state.crossword?.id !== crossword?.id;
    const shouldResetCrossword =
        shouldResetGuesses ||
        crossword?.attempt?.completed ||
        !!state.crossword?.attempt?.completed !== !!crossword?.attempt?.completed ||
        state.crossword?.available !== crossword?.available;

    // console.log("shouldResetCrossword =", shouldResetCrossword, state.crossword, "->", crossword);

    useEffect(() => {
        if (shouldResetCrossword) dispatch({ type: "Reset", crossword, resetGuesses: shouldResetGuesses });
    }, [shouldResetCrossword, shouldResetGuesses, crossword, dispatch]);

    useEffect(() => {
        if (!saving && saved) {
            if (saveError) dispatch({ type: "SaveError", error: saveError });
            else dispatch({ type: "Saved" });
        }
    }, [dispatch, saving, saved, saveError]);

    const isLoggedIn = Boolean(useMeQuery().data?.me);

    useEffect(() => {
        if (state.modified && isLoggedIn) saveDebounced(getUpdateAttemptRequest(crosswordId, state));
    }, [saveDebounced, state, crosswordId, isLoggedIn]);

    return reducerValue;
}

export function getUpdateAttemptRequest(crosswordId: number, state: PuzzleState): UpdateAttemptRequest {
    const guesses: UpdateGuessRequest[] = [];

    for (const row of state.grid)
        for (const cell of row)
            if (cell && cell.guess) guesses.push({ x: cell.cell.x, y: cell.cell.y, guess: cell.guess });

    return {
        crosswordId,
        guesses,
    };
}

function compileGrid(
    crossword: CrosswordDetailFragment,
    activeClue: ClueDetailFragment,
    copyGuessesFrom: GridRows | undefined
) {
    if (!crossword.width || !crossword.height || !crossword.clues) throw new Error("Crossword is not generated yet");

    const rows: PuzzleCell[][] = [];

    for (let y = 0; y < crossword.height; y++) {
        const row = [] as PuzzleCell[];

        for (let x = 0; x < crossword.width; x++) row.push({ x, y, filled: false });

        rows.push(row);
    }

    for (const clue of crossword.clues) {
        const dx = clue.direction === "ACROSS" ? 1 : 0;
        const dy = 1 - dx;

        for (let i = 0; i < clue.length; i++) {
            const cell = rows[clue.y + dy * i][clue.x + dx * i];

            cell.filled = true;
            if (clue.answer) cell.contents = clue.answer[i];
            if (clue.direction === "ACROSS") cell.across = clue;
            else cell.down = clue;

            if (i === 0) cell.hint = `${clue.number}`;
        }
    }

    const grid: GridRows = rows.map((r) =>
        r.map((cell) =>
            cell.filled
                ? {
                      cell,
                      active: cell.across === activeClue || cell.down === activeClue,
                      focused: cell.x === activeClue.x && cell.y === activeClue.y,
                      guess: copyGuessesFrom?.[cell.y]?.[cell.x]?.guess,
                  }
                : undefined
        )
    );

    if (crossword.attempt) {
        for (const { x, y, guess } of crossword.attempt.guesses) {
            const cell = grid[y][x];
            if (cell && guess) cell.guess = guess;
        }
    }

    return { rows, grid };
}
