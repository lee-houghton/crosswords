import clsx from "clsx";
import React, { memo } from "react";
import { useCellStyles } from "./styles";

export interface EmptyCellProps {
    cellClassName?: string;
}

export const EmptyCell = memo(function EmptyCell(props: EmptyCellProps) {
    const classes = useCellStyles();
    const className = clsx(classes.cell, classes.void, props.cellClassName);

    return <div className={className}/>;
});
