import { Button } from "@mui/material";
import { Theme } from "@mui/material/styles";
import makeStyles from '@mui/styles/makeStyles';
import Alert from '@mui/material/Alert';
import clsx from "clsx";
import React from "react";
import { useNavigate } from "react-router";
import { CrosswordDetailFragment, useAddCrosswordMutation } from "../../graphql";

const useStyles = makeStyles<Theme, CrosswordCompletedProps>((theme) => ({
    wrapper: {
        display: "flex",
        justifyContent: "center",
        marginBottom: theme.spacing(2),
    },
    alert: {
        maxWidth: (props) => `${2 * props.cols}rem`,
        flex: 1,
    },
    message: {
        fontWeight: "normal",
    },
    button: {
        color: "white",
    },
}));

const useAlertStyles = makeStyles({
    icon: {
        alignSelf: "center",
    },
});

interface CrosswordCompletedProps {
    className?: string;
    crossword?: CrosswordDetailFragment;
    cols: number;
    score?: number;
}

export function CrosswordCompleted(props: CrosswordCompletedProps) {
    const classes = useStyles(props);
    const alertClasses = useAlertStyles();
    const navigate = useNavigate();
    const deckIds = props.crossword ? props.crossword.decks.map((d) => d.id) : [];
    const [create, { loading: creating, called: created }] = useAddCrosswordMutation({
        onCompleted: (data) => navigate(`/crosswords/${data.addCrossword.id}`),
    });

    return (
        <div className={clsx(classes.wrapper, props.className)}>
            <Alert
                classes={alertClasses}
                severity="success"
                variant="filled"
                className={classes.alert}
                action={
                    <Button
                        autoFocus
                        onClick={() =>
                            create({ deckIds, width: props.crossword?.width, height: props.crossword?.height })
                        }
                        className={classes.button}
                        disabled={created || creating}
                        children="Play again"
                    />
                }
            >
                <span className={classes.message}>
                    Crossword completed! Score: <strong>{props.score}/100</strong>
                </span>
            </Alert>
        </div>
    );
}
