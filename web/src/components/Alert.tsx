import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import makeStyles from '@mui/styles/makeStyles';
import MuiAlert from '@mui/material/Alert';
import React from "react";

const useStyles = makeStyles(theme => ({
    wrapper: {
        textAlign: "center",
    },
}));

interface Props {
    className?: string;
    error?: Error;
    retry?: () => void;
}

export function Alert(props: Props) {
    const classes = useStyles();

    if (!props.error)
        return <div/>;

    return (
        <Grid className={props.className} container alignContent="center" justifyContent="center">
            <Grid item xs={12} lg={8} className={classes.wrapper}>
                <MuiAlert 
                    variant="filled"
                    severity="error" 
                    action={props.retry && <Button color="inherit" size="small" onClick={props.retry}>Retry</Button>}
                >
                    {props.error.message}
                </MuiAlert>
            </Grid>
        </Grid>
    );
};
