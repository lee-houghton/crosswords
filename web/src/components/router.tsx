import { lazy, PropsWithChildren } from "react";
import { createRoutesFromElements, Outlet, Route } from "react-router";
import { createBrowserRouter } from "react-router-dom";
import { Drawer } from "./Drawer";
import { Header } from "./Header";
import { Home } from "./Home";
import { Introduction } from "./Home/Introduction";
import { LoginScreen } from "./LoginScreen";
import { LoginWrapper } from "./LoginWrapper";
import { UserOverview } from "./UserScreen/UserOverview";
import { UserDeckList } from "./UserDeckList";
import { ManageAccount } from "./ManageAccount";

const DeckScreen = lazy(() => import("./DeckScreen"));
const AddDeckScreen = lazy(() => import("./AddDeckScreen"));
const ImportWordList = lazy(() => import("./ImportWordList"));
const EditDeckScreen = lazy(() => import("./EditDeckScreen"));
const CrosswordScreen = lazy(() => import("./CrosswordScreen"));
const RegisterScreen = lazy(() => import("./RegisterScreen"));
const ActivateScreen = lazy(() => import("./ActivateScreen"));
const ForgotPasswordScreen = lazy(() => import("./ForgotPasswordScreen"));
const ConfirmResetPasswordScreen = lazy(() => import("./ConfirmResetPasswordScreen"));
const AddCrosswordScreen = lazy(() => import("./AddCrosswordScreen"));
const NewsScreen = lazy(() => import("./NewsScreen"));
const CrosswordLoading = lazy(() => import("./CrosswordLoading").then((m) => ({ default: m.CrosswordLoading })));
const AdminScreen = lazy(() => import("./AdminScreen"));
const UserScreen = lazy(() => import("./UserScreen"));

function Root({ children }: PropsWithChildren) {
    return (
        <>
            <Drawer />
            <Header />
            {children}
            <Outlet />
        </>
    );
}

export const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path="/" element={<Root />}>
            <Route path="/" element={<Home />} />
            <Route path="/introduction" element={<Introduction />} />
            <Route path="/login" element={<LoginScreen />} />
            <Route path="/activate/:activationCode" element={<ActivateScreen />} />
            <Route path="/register" element={<RegisterScreen />} />
            <Route path="/forgot-password" element={<ForgotPasswordScreen />} />
            <Route path="/reset-password/:code" element={<ConfirmResetPasswordScreen />} />
            <Route
                path="/profile"
                element={
                    <LoginWrapper>
                        <UserScreen />
                    </LoginWrapper>
                }
            >
                <Route path="" element={<UserOverview />} />
                <Route path="lists" element={<UserDeckList />} />
                <Route path="manage/:tab?" element={<ManageAccount />} />
                <Route path="manage/:tab/:code" element={<ManageAccount />} />
            </Route>
            <Route path="/users/:name" element={<UserScreen />} />
            <Route path="/crosswords/loading/14" element={<CrosswordLoading width={14} height={14} />} />
            <Route path="/crosswords/loading/28" element={<CrosswordLoading width={28} height={28} />} />
            <Route path="/crosswords/add" element={<AddCrosswordScreen />} />
            <Route path="/crosswords/:id" element={<CrosswordScreen />} />
            <Route path="/lists/add/:lang?" element={<AddDeckScreen />} />
            <Route path="/lists/import/:lang?" element={<ImportWordList />} />
            <Route path="lists/:id/edit" element={<EditDeckScreen />} />
            <Route path="/lists/:id" element={<DeckScreen />} />
            <Route path="/news" element={<NewsScreen />} />
            <Route path="/admin" element={<AdminScreen />} />
        </Route>
    )
);
