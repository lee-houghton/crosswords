import { IconButton } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import { ArrowBack } from "@mui/icons-material";
import React from "react";
import { useNavigate } from "react-router";
import { isIos } from "../../platform";

const useStyles = makeStyles({
    button: {
        "@media all and (display-mode: browser)": {
            display: "none",
        },
    },
});

export function BackButton() {
    const classes = useStyles();
    const navigate = useNavigate();

    return (
        <IconButton
            hidden={!isIos()}
            color="inherit"
            className={classes.button}
            onClick={() => navigate(-1)}
            aria-label="Back"
            title="Go back"
            children={<ArrowBack />}
            size="large" />
    );
}
