import {
    CircularProgress,
    ClickAwayListener,
    InputBase,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Paper,
    Popper,
} from "@mui/material";
import { alpha } from "@mui/material/styles";
import makeStyles from '@mui/styles/makeStyles';
import { Search as SearchIcon } from "@mui/icons-material";
import React, { memo, useCallback, useEffect, useRef, useState } from "react";
import { useLocation } from "react-router";
import { Link as RouterLink } from "react-router-dom";
import { SearchResultFragment, useGlobalSearchQuery } from "../../graphql/operations";
import { FlagIcon } from "../FlagIcon";

const useStyles = makeStyles((theme) => ({
    search: {
        position: "relative",
        borderRadius: theme.shape.borderRadius,
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        "&:hover": {
            backgroundColor: alpha(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: "100%",
        [theme.breakpoints.up("sm")]: {
            marginLeft: theme.spacing(3),
            width: "auto",
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: "100%",
        position: "absolute",
        pointerEvents: "none",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    inputRoot: {
        color: "inherit",
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create("width"),
        width: "100%",
        [theme.breakpoints.up("md")]: {
            minWidth: "20ch",
            width: "30ch",
        },
    },
    popover: {
        marginTop: theme.spacing(1),
        zIndex: 10,
        width: "30ch",
    },
}));

const inputProps = { "aria-label": "Search" };

export function SearchField() {
    const inputRef = useRef(null);
    const classes = useStyles();
    const [search, setSearch] = useState("");
    const [open, setOpen] = useState(false);
    const { data, loading } = useGlobalSearchQuery({ variables: { search }, skip: !search.trim() });
    const location = useLocation();

    const handleFocus = () => setOpen(search.trim() !== "");
    const close = useCallback(() => setOpen(false), []);

    function handleKeyDown(e: React.KeyboardEvent) {
        if (e.key === "Escape") {
            setOpen(false);
            e.preventDefault();
        }
    }

    // There is an effect which closes the dropdown on navigation, but that
    // doesn't handle the case where you click a link that actually leads to
    // the same page that you're already on.
    function handleLinkClick(e: React.MouseEvent) {
        // Visit all the parent nodes up to the <div> with the listener and look for links.
        // If a link is found, it means the user clicked within it, so we need to close the dropdown.
        for (let element = e.target as HTMLElement | null; element; element = element.parentElement) {
            // We found the <div> with the listener, no need to look further.
            if (element === e.currentTarget) return;

            // It's a link, close the dropdown.
            if (element instanceof HTMLAnchorElement) setOpen(false);
        }
    }

    useEffect(() => {
        setOpen(false);
    }, [location.pathname]);

    return (
        <div className={classes.search} onFocus={handleFocus}>
            <ClickAwayListener onClickAway={close}>
                <div onClickCapture={handleLinkClick}>
                    <div className={classes.searchIcon}>
                        <SearchIcon />
                    </div>
                    <InputBase
                        ref={inputRef}
                        placeholder="Search…"
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                        inputProps={inputProps}
                        value={search}
                        onKeyDown={handleKeyDown}
                        onChange={(e) => {
                            setSearch(e.target.value);
                            setOpen(e.target.value.trim() !== "");
                        }}
                    />
                    <Popper
                        id="search-results"
                        open={open}
                        anchorEl={inputRef.current}
                        onKeyDown={handleKeyDown}
                        className={classes.popover}
                        placement="bottom-start"
                        disablePortal
                    >
                        <Paper elevation={4}>
                            <SearchResultList loading={loading} results={data?.results} />
                        </Paper>
                    </Popper>
                </div>
            </ClickAwayListener>
        </div>
    );
}

const SearchResultList = memo(function SearchResultList({
    loading,
    results,
}: {
    loading: boolean;
    results?: readonly SearchResultFragment[];
}) {
    return (
        <List>
            {loading && (
                <ListItem>
                    <ListItemText>
                        <CircularProgress size="small" /> Loading…
                    </ListItemText>
                </ListItem>
            )}
            {!loading && !results?.length && (
                <ListItem>
                    <ListItemText>No results.</ListItemText>
                </ListItem>
            )}
            {results?.map((result) => (
                <SearchResult key={`${result.__typename}:${result.id}`} result={result} />
            ))}
        </List>
    );
});

function SearchResult({ result }: { result: SearchResultFragment }) {
    if (result.__typename === "Deck")
        return (
            <ListItem button component={RouterLink} to={`/lists/${result.id}`}>
                <ListItemIcon>
                    <FlagIcon lang={result.termLang.id} />
                </ListItemIcon>
                <ListItemText
                    primary={result.title}
                    secondary={`${result.termCount} term${result.termCount === 1 ? "" : "s"}`}
                />
            </ListItem>
        );

    // In practice, search results should always be attached to a deck
    return (
        <ListItem button component={RouterLink} to={`/lists/${result.deck?.id}`}>
            <ListItemIcon>
                <FlagIcon lang={result.deck?.termLang.id} />
            </ListItemIcon>
            <ListItemText
                primary={`${result.term} • ${result.definitions.join(", ")}`}
                secondary={result.deck?.title}
            />
        </ListItem>
    );
}
