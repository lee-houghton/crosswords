import { IconButtonProps } from "@mui/material/IconButton";
import { LinkProps } from "@mui/material/Link";
import { AccountCircle } from "@mui/icons-material";
import React from "react";
import { useMeQuery } from "../../graphql";
import { UserAvatar } from "../Home/UserAvatar";
import { LinkIconButton } from "../LinkIconButton";
import { LinkButton } from "../Links";

export function UserIconLink(props: Omit<IconButtonProps, keyof LinkProps> & LinkProps) {
    const { data } = useMeQuery();
        
    if (data && data.me)
        return (<LinkIconButton {...props as any} to="/profile" color="inherit" aria-label="Profile page">
            <UserAvatar name={data.me.displayName} profileImage={data.me.profileImage || undefined} />
        </LinkIconButton>);

    return (
        <LinkButton {...props as any} variant="outlined" color="inherit" to="/login">
            Log in
        </LinkButton>
    );
}

export function UserIcon() {
    const { data } = useMeQuery();
        
    if (data && data.me)
        return <UserAvatar name={data.me.displayName} profileImage={data.me.profileImage || undefined} />;

    return <AccountCircle />;
}
