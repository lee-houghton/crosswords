import AppBar from "@mui/material/AppBar";
import Badge from "@mui/material/Badge";
import IconButton from "@mui/material/IconButton";
import makeStyles from '@mui/styles/makeStyles';
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { Home } from "@mui/icons-material";
import MenuIcon from "@mui/icons-material/Menu";
import React, { memo } from "react";
import { Link } from "react-router-dom";
import { useTitle } from "../../TitleProvider";
import { useDrawerIsOpen } from "../Drawer/DrawerProvider";
import { useNews } from "../NewsScreen";
import { BackButton } from "./BackButton";
import { SearchField } from "./SearchField";
import { UserIconLink } from "./UserIcon";

const useStyles = makeStyles(theme => ({
    appBar: {
        "@media print": {
            display: "none"
        }
    },
    title: {
        textAlign: "left",
        [theme.breakpoints.only("xs")]: {
            display: "none",
        },
    },
    spacer: {
        flexGrow: 1,
    },
    titleLink: {
        color: "inherit",
        textDecoration: "none",
    },
    homeIcon: {
        [theme.breakpoints.up("sm")]: {
            display: "none",
        },
    }
}));

export const Header = memo(function Header() {
    const classes = useStyles();
    const title = useTitle();
    const news = useNews();

    const [, setIsOpen] = useDrawerIsOpen();
    const toggleDrawer = () => setIsOpen(open => !open);

    return (
        <AppBar id="header" position="static" className={classes.appBar}>
            <Toolbar>
                <IconButton
                    color="inherit"
                    title="Menu"
                    aria-label="Menu"
                    onClick={toggleDrawer}
                    size="large">
                    <Badge invisible={!news || news.length === 0} color="secondary" variant="dot">
                        <MenuIcon/>
                    </Badge>
                </IconButton>
                <BackButton/>
                <IconButton
                    component={Link}
                    to="/"
                    title="Home"
                    aria-label="Home page"
                    color="inherit"
                    className={classes.homeIcon}
                    size="large">
                    <Home/>
                </IconButton>
                <Typography component="h1" variant="h6" color="inherit" className={classes.title}>
                    <Link to="/" className={classes.titleLink}>
                        {title || "Crosswords"}
                    </Link>
                </Typography>
                <SearchField/>
                <div className={classes.spacer}/>
                <UserIconLink size="small"/>
            </Toolbar>
        </AppBar>
    );
});

