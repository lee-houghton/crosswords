import { Avatar, ListItemSecondaryAction, ListItemText } from "@mui/material";
import * as React from "react";
import { DeckCardInfoFragment } from "../../graphql";
import { useToggleDeckFavouriteMutation } from "../../graphql";
import { FlagAvatar } from "../FlagAvatar";
import { ListLink } from "../Links";
import { ToggleFavourite } from "./ToggleFavourite";

interface Props {
    deck: DeckCardInfoFragment;
    favourite: boolean;
}

const noWrap = { noWrap: true };

export function DeckListItem({deck, favourite}: Props) {
    const [toggleFavourite]
        = useToggleDeckFavouriteMutation(deck.id, !favourite);
    
    return (
        <ListLink key={deck.id} to={`lists/${deck.id}`}>
            <Avatar>
                {/* TODO Fix termLang typings */}
                <FlagAvatar lang={(deck as any).termLang}/>
            </Avatar>
            <ListItemText
                primary={deck.title}
                secondary={deck.description}
                secondaryTypographyProps={noWrap}
            />
            <ListItemSecondaryAction>
                <ToggleFavourite favourite={favourite||false} toggleFavourite={toggleFavourite}/>
            </ListItemSecondaryAction>
        </ListLink>
    );
}
