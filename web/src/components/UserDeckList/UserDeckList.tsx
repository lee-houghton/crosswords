import { useOutletContext } from "react-router";
import { UserProfileFragment } from "../../graphql";
import { CardGrid, CardGridItem } from "../CardGrid";
import { DeckCard } from "../DeckCard";
import { DeckCardSkeleton } from "../Home/DeckCardSkeleton";
import { NoDecks } from "./NoDecks";

const skeleton = (
    <CardGrid>
        <CardGridItem>
            <DeckCardSkeleton />
        </CardGridItem>
        <CardGridItem>
            <DeckCardSkeleton />
        </CardGridItem>
        <CardGridItem>
            <DeckCardSkeleton />
        </CardGridItem>
        <CardGridItem>
            <DeckCardSkeleton />
        </CardGridItem>
    </CardGrid>
);

export function UserDeckList() {
    const user = useOutletContext<UserProfileFragment | null>();
    const decks = user?.decks;
    const isMe = user?.isMe;

    if (!decks) return skeleton;

    if (decks.length === 0) return <NoDecks canAdd={isMe || false} />;

    // TODO: On smaller screens show material list view instead of card grid?
    return (
        <CardGrid>
            {decks.map((deck) => (
                <CardGridItem key={deck.id}>
                    <DeckCard deck={deck} />
                </CardGridItem>
            ))}
        </CardGrid>
    );
}
