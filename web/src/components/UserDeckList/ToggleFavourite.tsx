import { Favorite, FavoriteBorder } from "@mui/icons-material";
import { IconButton } from "@mui/material";

export interface ToggleFavouriteProps {
    favourite: boolean;
    toggleFavourite(): void;
}

export function ToggleFavourite(props: ToggleFavouriteProps) {
    return (
        <IconButton aria-label="Add to favourites" onClick={() => props.toggleFavourite()} size="large">
            {props.favourite ? <Favorite /> : <FavoriteBorder />}
        </IconButton>
    );
}
