import makeStyles from '@mui/styles/makeStyles';
import Alert from '@mui/material/Alert';
import React from "react";
import { File } from "react-kawaii";
import { LinkButton } from "../Links";
import { deepPurple } from '@mui/material/colors';

const useStyles = makeStyles(theme => ({
    kawaii: {
        textAlign: "center",
        marginBottom: theme.spacing(1),
    },
    text: {
        textAlign: "center",
    },
    alert: {
        maxWidth: "fit-content",
        margin: "0 auto",
    }
}));

export function NoDecks({ canAdd }: { canAdd: boolean }) {
    const classes = useStyles();
    return <div>
        <div className={classes.kawaii}>
            <File size={160} mood="sad" color={deepPurple["200"]} />
        </div>
        <Alert className={classes.alert} severity="info" icon={false}
            action={canAdd && <LinkButton to="/lists/add">Add word list</LinkButton>}
            >
            It's looking kinda lonely in here..
        </Alert>
    </div>;
}
