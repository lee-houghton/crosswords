import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import SendIcon from "@mui/icons-material/Send";
import Alert from '@mui/material/Alert';
import React, { useState } from "react";
import { useFormStyles } from "../../styles/forms";
import { ActionButton } from "../ActionButton";
import { ScreenWrapper } from "../ScreenWrapper";
import CheckIcon from "@mui/icons-material/Check";
import { useResetPasswordMutation } from "../../graphql";

const sendIcon = <SendIcon/>;

export function ForgotPasswordScreen() {
    const formClasses = useFormStyles();

    const [email, setEmail] = useState("");
    const [resetPassword, { loading, called, error }] = useResetPasswordMutation();
    
    // Form submission
    const canSubmit = Boolean(email) && !loading;
    const onSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        if (canSubmit)
            resetPassword({ variables: { email } })
                .catch(err => undefined); // This is handled by the hooks rather than the Promise rejection
    };

    return <ScreenWrapper>
        <div className={formClasses.wrapper}>
            <Typography component="h1" variant="h5">
                Reset your password
            </Typography>

            <p>
                Use the form below to reset your password. If you enter the correct email address,
                you will receive an email with instructions on how to reset your password.
            </p>

            <form className={formClasses.form} onSubmit={onSubmit}>
                <TextField
                    type="email"
                    value={email}
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    name="email"
                    label="Email address"
                    autoFocus
                    helperText="The email address linked to your account."
                    onChange={e => setEmail(e.target.value)}
                />

                {error && <Alert severity="error">{error.message}</Alert>}
                {called && !error && <Alert severity="success">
                    Success! If the email address matches our records, you will receive 
                    an email with a link to reset your password.
                </Alert>}

                <ActionButton fullWidth
                    icon={sendIcon} children="Send email"
                    loading={loading} loadingText="Requesting"
                    done={called} doneText="Requested" doneIcon={<CheckIcon/>}
                    error={error} errorText="Retry"

                    type="submit"
                    disabled={!canSubmit}
                    variant="contained"
                    color="primary"
                    className={formClasses.submit}
                />
            </form>
        </div>
    </ScreenWrapper>;
}
