import { CircularProgress } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import React from "react";
import { ScreenWrapper } from "./ScreenWrapper";

const useStyles = makeStyles({
    wrapper: {
        textAlign: "center",
    }
})

interface Props {
    loadingText?: string;
}

export function LoadingScreen(props: Props) {
    const classes = useStyles();
    return (
        <ScreenWrapper>
            <div className={classes.wrapper}>
                <CircularProgress title={props.loadingText || "Loading..."} />
            </div>
        </ScreenWrapper>
    );
}
