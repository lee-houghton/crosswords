import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import makeStyles from '@mui/styles/makeStyles';
import Typography from "@mui/material/Typography";
import Skeleton from '@mui/material/Skeleton';
import React, { ReactNode } from "react";
import { Link } from "react-router-dom";
import { UserAvatar } from "../Home/UserAvatar";
import CardActions from "@mui/material/CardActions";
import { BasicUserInfoFragment, UserProfileFragment } from "../../graphql";

const useStyles = makeStyles(theme => ({
    avatar: {
        width: theme.spacing(8),
        height: theme.spacing(8),
    },
    subheader: {
        color: theme.palette.text.secondary,
    },
    logOut: {
        marginLeft: "auto",
    }
}));

export interface UserCardProps {
    user: BasicUserInfoFragment & Partial<UserProfileFragment>;
    actions?: ReactNode;
}

export function UserCardSkeleton() {
    return (
        <Card>
            <CardHeader 
                title={
                    <Typography variant="h5" component="h3">
                        <Skeleton/>
                    </Typography>
                }
                subheader={
                    <Typography>
                        <Skeleton width="70%"/>
                    </Typography>
                }
                avatar={
                    <Skeleton variant="circular"><Avatar/></Skeleton>
                }
            />
            <Box p={1}>
                <Skeleton width="60%"/>
            </Box>
        </Card>
    );
}

export function UserCard({ user, actions }: UserCardProps) {
    const classes = useStyles();
    const decks = user.decks?.length ?? 0;

    return (
        <Card>
            <CardHeader 
                title={
                    <Typography variant="h5" component="h3">{user.displayName} ({user.name})</Typography>
                }
                subheader={
                    <Typography className={classes.subheader}>
                        User since {new Date(user.signUpDate).toLocaleDateString()}
                        {decks > 0 && " • "}
                        {decks > 0 && <Link to={`/users/${user.name}/lists`}>{decks} lists</Link>}
                    </Typography>
                }
                avatar={
                    <UserAvatar 
                        className={classes.avatar} 
                        name={user.displayName} 
                        profileImage={user.profileImage || undefined}
                    />
                }
            />
            {actions && <CardActions>
                {actions}
            </CardActions>}
        </Card>
    );
};
