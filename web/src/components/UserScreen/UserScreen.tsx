import Zoom from "@mui/material/Zoom";
import makeStyles from '@mui/styles/makeStyles';
import AddIcon from "@mui/icons-material/Add";
import Alert from '@mui/material/Alert';
import { Ghost } from "react-kawaii";
import { Outlet, useParams, useResolvedPath } from "react-router";
import { useUserQuery } from "../../graphql";
import { FabLink } from "../Links";
import { TabbedScreen } from "../TabbedScreen";
import { Title } from "../Title";
import { useFabStyles } from "../useFabStyles";
import { deepPurple } from '@mui/material/colors';

const useStyles = makeStyles((theme) => ({
    ghost: {
        textAlign: "center",
        marginBottom: theme.spacing(1),
    },
    text: {
        textAlign: "center",
    },
    alert: {
        maxWidth: "fit-content",
        margin: "0 auto",
    },
    slide: {
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
    },
    tabs: {
        position: "sticky",
        top: 0,
    },
}));

const userTabs = [
    {
        title: "Profile",
        exact: true,
        url: "",
    },
    {
        title: "Lists",
        url: "lists",
    },
];

const meUserTabs = [
    ...userTabs,
    {
        title: "Account",
        url: "manage",
    },
];

export function UserScreen() {
    const { fab } = useFabStyles();
    const classes = useStyles();
    const { name } = useParams<{ name?: string }>();
    const isMe = name === undefined;
    const { data, error } = useUserQuery({ variables: { name, self: isMe } });

    const pathname = useResolvedPath("").pathname;
    const isLists = pathname.endsWith("/lists");

    const user = data?.user;

    const tabs = isMe ? meUserTabs : userTabs;

    return (
        <TabbedScreen baseUrl={pathname} tabs={tabs} visible={!error && data?.user !== null}>
            <Title>{user?.displayName}</Title>
            {error && <Alert severity="error" children={error.message} />}

            {user === null && (
                <>
                    <div className={classes.ghost}>
                        <Ghost size={160} mood="shocked" color={deepPurple["200"]} />
                    </div>
                    <Alert className={classes.alert} severity="info">
                        A user with the name <b>{name}</b> was not found.
                    </Alert>
                </>
            )}

            {/* Should be visible all the time to make the Zoom effect work */}
            {
                <Zoom in={user?.isMe && isLists}>
                    <FabLink className={fab} color="primary" to="/lists/add" aria-label="Add list" title="Add list">
                        <AddIcon />
                    </FabLink>
                </Zoom>
            }

            <Outlet context={user} />
        </TabbedScreen>
    );
}
