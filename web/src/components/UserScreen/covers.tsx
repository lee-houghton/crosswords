import makeStyles from '@mui/styles/makeStyles';
import React, { memo } from "react";

const images = import.meta.glob<true, string, string>(["../../assets/covers/*.(jpg|webp|avif)"], {
    eager: true,
    import: "default",
});

export function getCoverImages(languageId: string, format: "jpg" | "webp" | "avif") {
    return `
        ${images[`../../assets/covers/${languageId}.512.${format}`]} 512w,
        ${images[`../../assets/covers/${languageId}.1024.${format}`]} 1024w,
        ${images[`../../assets/covers/${languageId}.2048.${format}`]} 2048w
    `;
}

const useStyles = makeStyles({
    root: {
        display: "flex",
        width: "100%",
        height: "100%",
    },
    img: {
        maxHeight: 140,
        objectFit: "cover",
        width: "100%",
        height: "auto",
    },
});

interface LanguageCoverImageProps {
    className?: string;
    languageId: string;
}

export const LanguageCoverImage = memo(function LanguageCoverImage({ className, languageId }: LanguageCoverImageProps) {
    const classes = useStyles();

    const normalSize = `../../assets/covers/${languageId}.1024.jpg`;
    const fallbackImage = images[normalSize];
    if (!fallbackImage) return null;

    return (
        <picture className={classes.root}>
            <source type="image/avif" srcSet={getCoverImages(languageId, "avif")} />
            <source type="image/webp" srcSet={getCoverImages(languageId, "webp")} />
            <source type="image/jpeg" srcSet={getCoverImages(languageId, "jpg")} />
            <img className={classes.img} src={fallbackImage} alt="" />
        </picture>
    );
});
