import Button from "@mui/material/Button";
import React from "react";
import { LinkButton } from "../Links";
import Favorite from "@mui/icons-material/Favorite";
import FavoriteBorder from "@mui/icons-material/FavoriteBorder";
import { UserProfileFragment } from "../../graphql";
import { useIsSuperuser, useLogoutMutation, useFollowUserMutation } from "../../graphql";

export function UserActions({ user }: { user: UserProfileFragment }) {
    const isSuperuser = useIsSuperuser();
    const [logOut] = useLogoutMutation();

    const [follow, followState] = useFollowUserMutation(user.id, !user.followed);

    return <>
        {!user.isMe && 
            <Button 
                disabled={followState.loading} 
                color="primary" 
                onClick={follow as any}>
                {user.followed ? <Favorite/> : <FavoriteBorder/>}
                {" "}
                {user.followed ? "Unfollow" : "Follow"}
            </Button>}
        {isSuperuser && <Button>View Activity</Button>}
        {isSuperuser && <Button>Ban Hammer</Button>}
        {user.isMe && <LinkButton to="/profile/manage" color="primary">Manage Account</LinkButton>}
        {user.isMe && <Button onClick={logOut as any}>Log Out</Button>}
    </>;
}
