import Grid from "@mui/material/Grid";
import { useMemo } from "react";
import { useOutletContext } from "react-router";
import { UserProfileFragment, useIsSuperuser } from "../../graphql";
import { UserActions } from "./UserActions";
import { UserCard, UserCardSkeleton } from "./UserCard";
import { UserLanguageCard, UserLanguageCardSkeleton } from "./UserLanguageCard";

export function UserOverview() {
    const user = useOutletContext<UserProfileFragment | null>();

    const isSuperuser = useIsSuperuser();
    const hasActions = user?.isMe || isSuperuser;
    const actions = user && hasActions && <UserActions user={user} />;
    const languages = useMemo(
        () =>
            user?.languages
                ?.slice()
                .sort((a, b) => (a.lastActivity > b.lastActivity ? -1 : b.lastActivity > a.lastActivity ? 1 : 0)),
        [user]
    );

    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                {user ? <UserCard user={user} actions={actions} /> : <UserCardSkeleton />}
            </Grid>
            {!user && (
                <>
                    <Grid item xs={12} md={6}>
                        <UserLanguageCardSkeleton />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <UserLanguageCardSkeleton />
                    </Grid>
                </>
            )}
            {user &&
                languages?.map((lang) => (
                    <Grid key={lang.language.id} item xs={12} md={6}>
                        <UserLanguageCard
                            key={lang.language.id}
                            user={user}
                            language={lang.language}
                            decks={lang.decks}
                            crosswordsCreated={lang.crosswordsCreated}
                            started={lang.started}
                            lastActivity={lang.lastActivity}
                        />
                    </Grid>
                ))}
        </Grid>
    );
}
