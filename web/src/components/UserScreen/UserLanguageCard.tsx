import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardHeader from "@mui/material/CardHeader";
import Typography from "@mui/material/Typography";
import CardMedia from "@mui/material/CardMedia";
import { format } from "date-fns";
import React from "react";
import { FlagAvatar } from "../FlagAvatar";
import { LinkButton } from "../Links";
import makeStyles from '@mui/styles/makeStyles';
import { LanguageCoverImage } from "./covers";
import { useAddCrosswordMutation } from "../../graphql";
import Button from "@mui/material/Button";
import Skeleton from '@mui/material/Skeleton';
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import { BasicUserInfoFragment, LanguageInfoFragment, UserProfileFragment } from "../../graphql";

const useStyles = makeStyles(theme => ({
    avatar: {
        width: theme.spacing(8),
        height: theme.spacing(8),
    },
    subheader: {
        color: theme.palette.text.secondary,
    },
    cover: {
        maxHeight: 140,
    }
}));

interface Props {
    className?: string;
    user: BasicUserInfoFragment & Partial<UserProfileFragment>;
    language: LanguageInfoFragment;
    decks: number;
    crosswordsCreated: number;
    started: Date;
    lastActivity: Date;
}

export function UserLanguageCardSkeleton() {
    return (
        <Card>
            <Skeleton variant="rectangular" height={140}/>
            <CardHeader 
                title={
                    <Typography variant="h5" component="h3">
                        <Skeleton/>
                    </Typography>
                }
                subheader={
                    <Typography>
                        <Skeleton width="70%"/>
                    </Typography>
                }
                avatar={
                    <Skeleton variant="circular"><Avatar/></Skeleton>
                }
            />
            <Box p={1}>
                <Skeleton width="60%"/>
            </Box>
        </Card>
    );
}

export function UserLanguageCard(props: Props) {
    const { className, user, language, decks, crosswordsCreated } = props;
    const [add, { loading: adding }] = useAddCrosswordMutation();

    const handleAddCrossword = () => {
        add({ languageId: props.language.id });
    };

    const classes = useStyles();
    return (
        <Card className={className}>
            <CardMedia
                className={classes.cover}
                component={LanguageCoverImage}
                languageId={language.id}
            />
            <CardHeader 
                title={
                    <Typography variant="h5" component="h3">{language.name}</Typography>
                }
                subheader={
                    <Typography className={classes.subheader}>
                        Since {format(new Date(props.started), "do MMMM yyyy")}
                        {user.decks && ` • ${decks} ${decks === 1 ? "list" : "lists"
                        } • ${crosswordsCreated} ${crosswordsCreated === 1 ? "crossword" : "crosswords"}`}
                    </Typography>
                }
                avatar={
                    <FlagAvatar lang={language.id}/>
                }
            />
            <CardActions>
                <Button disabled={adding} color="primary" onClick={handleAddCrossword}>
                    Create crossword
                </Button>
                <LinkButton to={`lists/add/${language.id}`}>Create list</LinkButton>
            </CardActions>
        </Card>
    );
};
