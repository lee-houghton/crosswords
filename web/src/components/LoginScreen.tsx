import React, { useEffect } from "react";
import { useNavigate } from "react-router";
import { useMeQuery } from "../graphql";
import { LoginForm } from "./LoginForm";
import { ScreenWrapper } from "./ScreenWrapper";

export function LoginScreen() {
    const navigate = useNavigate();
    const { data } = useMeQuery();
    const me = data && data.me;

    useEffect(() => {
        if (me) navigate("/");
    }, [me, navigate]);

    return (
        <ScreenWrapper>
            <LoginForm />
        </ScreenWrapper>
    );
}
