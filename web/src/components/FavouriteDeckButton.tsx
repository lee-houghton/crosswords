import { IconButton } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import { Favorite, FavoriteBorder } from "@mui/icons-material";
import clsx from "clsx";
import React from "react";
import { useToggleDeckFavouriteMutation } from "../graphql";

const useStyles = makeStyles({
    red: {
        color: "#ff6d75"
    }
});

export function FavouriteDeckButton(props: { deckId: number, favourite: boolean }) {
    const classes = useStyles();
    const [toggle, { called, loading }] = useToggleDeckFavouriteMutation(props.deckId, !props.favourite);
    const label = props.favourite ? "Remove from favourites" : "Add to favourites";

    return (
        <IconButton
            title={label}
            aria-label={label}
            onClick={toggle as any}
            disabled={loading}
            size="large">
            {props.favourite 
                ? <Favorite className={clsx(called && classes.red)} /> 
                : <FavoriteBorder/>}
        </IconButton>
    );
}
