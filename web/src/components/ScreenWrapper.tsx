import makeStyles from "@mui/styles/makeStyles";
import clsx from "clsx";
import React, { ReactNode } from "react";

const useStyles = makeStyles(theme => ({
    screenWrapper: {
        width: "auto",
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    tabStrip: {
        marginLeft: theme.spacing(-3),
        marginRight: theme.spacing(-3),
        marginBottom: theme.spacing(3),
    },
    responsiveMargins: {
        [theme.breakpoints.down("md")]: {
            // Need at least 1 spacing to avoid the page scrolling horizontally, due to the negative margin on <Grid container/>.
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
    },
    noMargins: {
        // Need at least 1 spacing to avoid the page scrolling horizontally, due to the negative margin on <Grid container/>.
        marginTop: theme.spacing(0),
        marginRight: theme.spacing(0),
        marginBottom: theme.spacing(0),
        marginLeft: theme.spacing(0),
    },
    topPadding: {
        paddingTop: theme.spacing(3),
    },
}));

interface Props {
    className?: string;

    tabStrip?: ReactNode;

    /** Include a <Header> component. */
    topPadding?: boolean;

    margins?: "normal" | "responsive" | "none";
    children?: React.ReactNode;
}

export function ScreenWrapper({ margins, children, topPadding = true, tabStrip, ...props }: Props) {
    const classes = useStyles();
    const className = clsx(
        classes.screenWrapper,
        topPadding && classes.topPadding,
        props.className,
        margins === "none" && classes.noMargins,
        margins === "responsive" && classes.responsiveMargins
    );

    return (
        <main className={className}>
            {tabStrip && <div className={classes.tabStrip}>{tabStrip}</div>}
            {children}
        </main>
    );
}
