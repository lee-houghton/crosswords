import React, { ReactNode } from "react";
import { useMeQuery } from "../graphql";
import { LoginForm } from "./LoginForm";
import { CircularProgress } from "@mui/material";
import { ScreenWrapper } from "./ScreenWrapper";

export interface Props {
    children: ReactNode;
}

export function LoginWrapper(props: Props) {
    const { data, loading } = useMeQuery();

    if (loading)
        return <CircularProgress title={"Loading..."} />;

    if (data && data.me)
        return <>{props.children}</>;

    return <ScreenWrapper>
        <LoginForm/>
    </ScreenWrapper>;
}
