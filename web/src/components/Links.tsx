import { CardActionArea, CardHeader, ListItem } from "@mui/material";
import Button, { ButtonProps } from "@mui/material/Button";
import ButtonBase, { ButtonBaseProps } from "@mui/material/ButtonBase";
import { CardHeaderProps } from "@mui/material/CardHeader";
import Fab, { FabProps } from "@mui/material/Fab";
import { ListItemProps } from "@mui/material/ListItem";
import React, { forwardRef } from "react";
import { Link, LinkProps } from "react-router-dom";

export const ListLink = (props: LinkProps & Omit<ListItemProps, "button">) =>
    <ListItem button component={Link as any} {...props}/>;

export const LinkButton = (props: LinkProps & ButtonProps) =>
    <Button component={Link as any} {...props}/>;

export const LinkButtonBase = forwardRef(
    (props: LinkProps & ButtonBaseProps, ref) => <ButtonBase component={Link as any} {...props} ref={ref}/>);

export const CardActionAreaLink = (props: LinkProps & ButtonBaseProps) =>
    <CardActionArea component={LinkButtonBase as any} {...props}/>;

export const CardHeaderLink = (props: LinkProps & CardHeaderProps) =>
    <CardHeader component={LinkButtonBase as any} {...props}/>;

export const FabLink = (props: LinkProps & FabProps) =>
    <Fab component={Link as any} {...props}/>;
