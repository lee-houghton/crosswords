import { createStateContext } from "react-use/esm";

export const [useDrawerIsOpen, DrawerIsOpenProvider] = createStateContext(false);
