import Badge from "@mui/material/Badge";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import makeStyles from '@mui/styles/makeStyles';
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import AccountCircle from "@mui/icons-material/AccountCircle";
import Dashboard from "@mui/icons-material/Dashboard";
import ExitToApp from "@mui/icons-material/ExitToApp";
import GridOn from "@mui/icons-material/GridOn";
import NewReleases from "@mui/icons-material/NewReleases";
import React, { memo, useCallback, useEffect } from "react";
import { useNavigate, useLocation } from "react-router";
import { useLogoutMutation, useMeQuery } from "../../graphql";
import { UserIcon } from "../Header/UserIcon";
import { ListLink } from "../Links";
import { useNews } from "../NewsScreen";
import { useDrawerIsOpen } from "./DrawerProvider";
import { Home } from "@mui/icons-material";

const useStyles = makeStyles({
    drawer: {
        "@media print": {
            display: "none",
        },
    },
    list: {
        width: "250px",
    },
});

export const Drawer = memo(function Drawer() {
    const classes = useStyles();

    const { data } = useMeQuery();
    const loggedIn = Boolean(data?.me);
    const [logout, { loading: loggingOut }] = useLogoutMutation();
    const navigate = useNavigate();
    const location = useLocation();
    const news = useNews();

    const [isOpen, setIsOpen] = useDrawerIsOpen();
    const handleOpen = useCallback(() => setIsOpen(true), [setIsOpen]);
    const handleClose = useCallback(() => setIsOpen(false), [setIsOpen]);

    const onLogoutClick = useCallback(() => {
        logout();
        handleClose();
        navigate("/");
    }, [logout, handleClose, navigate]);

    // Close the drawer when the browser navigates away
    useEffect(() => setIsOpen(false), [setIsOpen, location.pathname]);

    return (
        <SwipeableDrawer
            disableBackdropTransition
            className={classes.drawer}
            open={isOpen}
            onOpen={handleOpen}
            onClose={handleClose}
        >
            <List className={classes!.list}>
                {!loggedIn && (
                    <ListLink key="login" to="/login" onClick={handleClose}>
                        <ListItemIcon>
                            <AccountCircle />
                        </ListItemIcon>
                        <ListItemText primary="Log in" />
                    </ListLink>
                )}

                {loggedIn && (
                    <ListLink key="profile" to="/profile" onClick={handleClose}>
                        <ListItemIcon>
                            <UserIcon />
                        </ListItemIcon>
                        <ListItemText primary="Profile" />
                    </ListLink>
                )}

                {loggedIn && (
                    <ListLink key="home" to="/" onClick={handleClose}>
                        <ListItemIcon>
                            <Home />
                        </ListItemIcon>
                        <ListItemText primary="Home" />
                    </ListLink>
                )}

                {loggedIn && (
                    <ListLink key="add-crossword" to="/crosswords/add" onClick={handleClose}>
                        <ListItemIcon>
                            <GridOn />
                        </ListItemIcon>
                        <ListItemText primary="Add crossword" />
                    </ListLink>
                )}

                {loggedIn && (
                    <ListLink key="whats-new" to="/news" onClick={handleClose}>
                        <ListItemIcon>
                            <Badge color="secondary" badgeContent={news?.length} max={9}>
                                <NewReleases />
                            </Badge>
                        </ListItemIcon>
                        <ListItemText primary="What's new" />
                    </ListLink>
                )}

                {data?.me?.superuser && (
                    <ListLink key="admin" to="/admin" onClick={handleClose}>
                        <ListItemIcon>
                            <Dashboard />
                        </ListItemIcon>
                        <ListItemText primary="Admin" />
                    </ListLink>
                )}

                {loggedIn && (
                    <ListItem key="logout" button disabled={loggingOut} onClick={onLogoutClick}>
                        <ListItemIcon>
                            <ExitToApp />
                        </ListItemIcon>
                        <ListItemText primary="Log out" />
                    </ListItem>
                )}
            </List>
        </SwipeableDrawer>
    );
});
