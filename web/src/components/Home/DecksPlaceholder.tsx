import React from "react";
import { Card, CardActions, CardContent, Typography } from "@mui/material";
import { LinkButton } from "../Links";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
    card: {
        height: "100%",
    },
})

export function DecksPlaceholder() {
    const classes = useStyles();

    return <Card className={classes.card}>
        <CardContent>
            <Typography variant="h5" component="h3" gutterBottom>
                New around town?
            </Typography>
            <Typography component="p">
                Let's start by adding some lists of terms to learn
                . Then you'll be able to practice those terms by playing crosswords.
            </Typography>
        </CardContent>
        <CardActions>
            <LinkButton color="primary" to="/lists/add">
                Add a list
            </LinkButton>
            <LinkButton to="/lists/search">
                Search for list
            </LinkButton>
        </CardActions>
    </Card>;
}
