import React from "react";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import Avatar from "@mui/material/Avatar";
import Skeleton from '@mui/material/Skeleton';
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Box from "@mui/material/Box";

export function DeckCardSkeleton() {
    return (
        <Card>
            <CardHeader

                action={<Skeleton variant="circular"><Avatar/></Skeleton>}
                title={<Skeleton width="80%"/>}
                subheader={<Skeleton width="40%" height={10}/>}
            />
            <CardContent>
                <Skeleton/>
                <Skeleton/>
                <Skeleton width="50%"/>
            </CardContent>
            <CardActions>
                <Box p={1}>
                    <Skeleton width="60%"/>
                </Box>
            </CardActions>
        </Card>
    );
}
