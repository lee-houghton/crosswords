import { Add, NoteAdd, PostAdd, Publish } from "@mui/icons-material";
import { SpeedDial, SpeedDialAction, SpeedDialIcon } from '@mui/material';
import React, { useState } from "react";
import { SpeedDialActionLink } from "../DeckScreen/SpeedDialActionLink";
import { useFabStyles } from "../useFabStyles";

export function Actions() {
    const classes = useFabStyles();
    const [fabOpen, setFabOpen] = useState(false);

    return (
        <SpeedDial
            ariaLabel="Actions"
            className={classes.fab}
            icon={<SpeedDialIcon icon={<Add/>} />}
            onClose={() => setFabOpen(false)}
            onOpen={() => setFabOpen(true)}
            open={fabOpen}
        >
            
            <SpeedDialActionLink
                to={`/lists/add`}
                icon={<NoteAdd />}
                tooltipTitle={"Add list"}
                tooltipOpen 
            />

            <SpeedDialActionLink
                to={`/lists/import`}
                icon={<Publish />}
                tooltipTitle={"Import list"}
                tooltipOpen 
            />

            <SpeedDialAction
                icon={<PostAdd />}
                tooltipTitle={"Add entry"}
                tooltipOpen
                onClick={() => setFabOpen(false)} 
            />
        </SpeedDial>
    );
}
