import { Typography } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import React, { useMemo } from "react";
import { Link } from "react-router-dom";
import { AttemptCardInfoFragment, UserLanguageStatsFragment } from "../../graphql/operations";
import { CrosswordCard } from "../DeckScreen/CrosswordCard";
import { AddCrosswordCard } from "./AddCrosswordCard";

interface HorizontalCrosswordListProps {
    attempts: readonly AttemptCardInfoFragment[];
    languages: readonly UserLanguageStatsFragment[];
}

const useStyles = makeStyles(theme => ({
    list: {
        display: "flex",
        flexDirection: "row",
        overflowX: "auto",
    },
    box: {
        width: theme.spacing(12),
        minWidth: theme.spacing(12),
        height: theme.spacing(12),
        display: "grid",
        placeItems: "center",
        textAlign: "center",
        borderRadius: theme.spacing(0.5),
        padding: theme.spacing(1),
    },
    heading: {
        background: theme.palette.secondary.light,
        color: theme.palette.secondary.contrastText,
        userSelect: "none",
    },
    card: {
        marginLeft: theme.spacing(1),
        minWidth: theme.spacing(12),
    },
    button: {
        backgroundColor: theme.palette.grey[300],
        "&:hover": {
            backgroundColor: theme.palette.grey[400],
        },

        marginLeft: theme.spacing(1),
        overflow: "hidden",
        position: "relative", // Allows the flag background to cover it
    },
    link: {
        color: theme.palette.text.primary,
        margin: "-10em",
        padding: "10em",
    },
    addText: {
        fontSize: theme.typography.caption.fontSize,
    }
}));

/** Show most recent languages first */
function compareLanguage(x: UserLanguageStatsFragment, y: UserLanguageStatsFragment) {
    if (x.lastActivity > y.lastActivity)
        return -1;
    if (x.lastActivity < y.lastActivity)
        return 1;
    return 0;
}

const NINETY_DAYS = 90 * 86400 * 1000;

export function HorizontalCrosswordList({ attempts: crosswords, languages }: HorizontalCrosswordListProps) {
    const classes = useStyles();

    // Show languages used within the last 90 days // TODO: In GraphQL
    const sortedLanguages = useMemo(
        () => {
            const cutoff = new Date(Date.now() - NINETY_DAYS).toISOString(); 
            return languages
                .filter(l => l.lastActivity >= cutoff)
                .sort(compareLanguage)
        },
        [languages]
    );

    return <div>
        <div className={classes.list}>
            <div className={classes.box + " " + classes.heading}>
                <Typography component="h2" variant="button">
                    In progress
                </Typography>
            </div>

            {crosswords.map(c => 
                <CrosswordCard small
                    key={c.id} 
                    className={classes.card}
                    crossword={c.crossword}
                />)
            }

            {sortedLanguages.map(({ language }) => 
                <AddCrosswordCard
                    key={language.id}
                    language={language}
                    className={classes.box + " " + classes.button}
                />
            )}
            
            {<div className={classes.box + " " + classes.button}>
                <Link className={classes.link} to="/crosswords/add">
                    <Typography component="span" variant="button" className={classes.addText}>
                        Add new{sortedLanguages.length > 0 && " (other)"}
                    </Typography>
                </Link>
            </div>}
        </div>
    </div>;
}

