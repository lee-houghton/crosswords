import { ApolloError, OperationVariables, QueryResult } from "@apollo/client";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import ListSubheader from "@mui/material/ListSubheader";
import Paper from "@mui/material/Paper";
import makeStyles from "@mui/styles/makeStyles";
import Skeleton from "@mui/material/Skeleton";
import { Link } from "react-router-dom";
import { ScoreInfoFragment, TermsToStudyQuery } from "../../graphql";
import { Alert } from "../Alert";
import { FlagIcon } from "../FlagIcon/FlagIcon";

export interface TermsToStudyProps {
    data?: TermsToStudyQuery;
    error?: ApolloError;
    loading?: boolean;
}

const useStyles = makeStyles({
    empty: {
        textAlign: "center",
    },
});

const skeleton = (
    <>
        <ListSubheader>Recent activity</ListSubheader>
        <ListItem>
            <Skeleton width="100%" />
        </ListItem>
        <ListItem>
            <Skeleton width="100%" />
        </ListItem>
        <ListItem>
            <Skeleton width="100%" />
        </ListItem>
    </>
);

export function TermsToStudy({ data, error, loading }: TermsToStudyProps) {
    const me = data?.me;
    const misses = me?.recentFailures || [];
    const hardest = me?.hardestTerms || [];
    const classes = useStyles();

    return (
        <div>
            {error && <Alert error={error} />}

            <Paper>
                <List>
                    {loading && skeleton}

                    <ScoreList title="Recent misses" scores={misses} />
                    <ScoreList title="Hardest words" scores={hardest} />

                    {!loading && misses.length === 0 && hardest.length === 0 && (
                        <>
                            <ListSubheader disableSticky>Recent activity</ListSubheader>
                            <ListItem>
                                <ListItemText
                                    primary={
                                        <>
                                            <Link to="/crosswords/add">Complete some crosswords</Link> and you'll see
                                            things here. Exciting!
                                        </>
                                    }
                                    className={classes.empty}
                                />
                            </ListItem>
                        </>
                    )}
                </List>
            </Paper>
        </div>
    );
}

function ScoreList({ title, scores }: { title: string; scores: readonly ScoreInfoFragment[] | undefined }) {
    if (!scores || scores.length === 0) return null;

    return (
        <>
            <ListSubheader disableSticky>{title}</ListSubheader>

            {scores.map(score => (
                <ListItem key={score.termId} button component={Link} to={`/lists/${score.term.deck?.id}`}>
                    {score.term.deck?.termLang && (
                        <ListItemIcon>
                            <FlagIcon lang={score.term.deck?.termLang.id} />
                        </ListItemIcon>
                    )}

                    <ListItemText
                        primary={score.term.definitions.join(", ")}
                        secondary={`Accuracy: ${score.successes}/${score.attempts}`}
                    />

                    {/* This doesn't actually do anything yet */}
                    {/* <ListItemSecondaryAction>
                    <IconButton title="Try this word">
                        <PlayIcon/>
                    </IconButton>
                </ListItemSecondaryAction> */}
                </ListItem>
            ))}
        </>
    );
}
