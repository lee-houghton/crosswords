import makeStyles from '@mui/styles/makeStyles';

export const useHomeStyles = makeStyles(theme => ({
    wrapper: {
        textAlign: "center",
    },
    grid: {
        display: "grid",
        gridTemplateColumns: "repeat(12, 1fr)",
        gridGap: theme.spacing(2),
        padding: 0,
        margin: 0,
        alignItems: "stretch",
    },
    content: {
        gridColumnEnd: "span 8",
        [theme.breakpoints.only("xs")]: { gridColumnEnd: "span 12" },
    },
    contentCard: {
        "& + &": {
            marginTop: theme.spacing(1),
        }
    },
    aside: {
        gridColumnEnd: "span 4",
        [theme.breakpoints.only("xs")]: { gridColumnEnd: "span 12" },
    },
    item: {
        listStyleType: "none",
        padding: 0,
        margin: 0,
        gridColumnEnd: "span 4",
        [theme.breakpoints.only("xs")]: { gridColumnEnd: "span 12" },
        [theme.breakpoints.only("sm")]: { gridColumnEnd: "span 6" },
    },
    crosswordItem: {
        listStyleType: "none",
        padding: 0,
        margin: 0,
        [theme.breakpoints.only("xs")]: { gridColumnEnd: "span 12" },
        [theme.breakpoints.only("sm")]: { gridColumnEnd: "span 6" },
        [theme.breakpoints.only("md")]: { gridColumnEnd: "span 6" },
        [theme.breakpoints.only("lg")]: { gridColumnEnd: "span 4" },
    },
    placeholderItem: {
        listStyleType: "none",
        padding: 0,
        margin: 0,
        [theme.breakpoints.only("xs")]: { gridColumnEnd: "span 12" },
        [theme.breakpoints.up("sm")]: { gridColumnEnd: "span 6" },
    },
    card: {
        height: "100%",
    },
}));
