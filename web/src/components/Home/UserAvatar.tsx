import Avatar, { AvatarProps } from "@mui/material/Avatar";
import makeStyles from '@mui/styles/makeStyles';
import clsx from "clsx";
import * as React from "react";

import avatar1 from "../../assets/avatars/colin2.jpg"
import avatar2 from "../../assets/avatars/colin.jpg"
import avatar3 from "../../assets/avatars/224116627386842882_13147478.jpg"
import avatar4 from "../../assets/avatars/225309423154422189_20227743.jpg"
import avatar5 from "../../assets/avatars/229211733215330264_13147478.jpg"
import avatar6 from "../../assets/avatars/232099343050915212_13147478.jpg"
import avatar7 from "../../assets/avatars/281074824785608240_13147478.jpg"
import avatar8 from "../../assets/avatars/324317447347704493_291987.jpg"
import avatar9 from "../../assets/avatars/480495815762060686_1145968.jpg"
import avatar10 from "../../assets/avatars/504419669706221284_1145968.jpg"
import avatar11 from "../../assets/avatars/523933611001886242_261102540.jpg"
import avatar12 from "../../assets/avatars/526434950922497531_392371402.jpg"
import avatar13 from "../../assets/avatars/tabbycolin2.jpg"
import avatar14 from "../../assets/avatars/tabbycolin.jpg"

const avatars = [
    avatar1, avatar2, avatar3, avatar4, avatar5, avatar6, avatar7, 
    avatar8, avatar9, avatar10, avatar11, avatar12, avatar13, avatar14
];

const useStyles = makeStyles(theme => ({
    large: {
        width: 64,
        height: 64,
    },
    tiny: {
        width: "1em",
        height: "1em",
    },
    inline: {
        display: "inline-flex",
        verticalAlign: "sub",
        marginRight: theme.spacing(0.5),
    }
}));

interface UserAvatarProps extends AvatarProps {
    className?: string;
    large?: boolean;
    style?: React.CSSProperties;
    tiny?: boolean;
    inline?: boolean;
    name: string;
    profileImage?: string;
}

export function UserAvatar({ className, inline, large, tiny, style, name, profileImage, ...props }: UserAvatarProps) {
    const classes = useStyles();
    className = clsx(
        className, 
        large && classes.large, 
        tiny && classes.tiny, 
        inline && classes.inline
    );

    return (
        <Avatar 
            {...props}
            className={className}
            style={style}
            title={name}
            src={profileImage || getPlaceholderImage(name)}/>
    );
}

function getPlaceholderImage(name?: string) {
    let hash = 0;

    if (name)
        for (let i = 0; i < name.length; i++)
            hash = ((hash << 5) - hash) + name.charCodeAt(i) | 0; // Convert to 32bit integer

    return avatars[Math.abs(hash) % avatars.length];
}
