import { Card, CardActions, CardContent, Typography } from "@mui/material";
import React from "react";
import { LinkButton } from "../Links";
import { useHomeStyles } from "./useHomeStyles";

export function NothingInProgress() {
    const classes = useHomeStyles();
    
    return (<Card className={classes.card}>
        <CardContent>
            <Typography>
                It looks like you're not playing any crosswords at the moment. Why not try creating one?
                </Typography>
        </CardContent>
        <CardActions>
            <LinkButton color="primary" to="/crosswords/add">Create crossword</LinkButton>
        </CardActions>
    </Card>);
}
