import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import AddIcon from "@mui/icons-material/Add";
import React from "react";
import { useHomeQuery, useTermsToStudyQuery } from "../../graphql";
import { Alert } from "../Alert";
import { DeckCard } from "../DeckCard";
import { LinkIconButton } from "../LinkIconButton";
import { RecentActivity } from "../RecentActivity";
import { ScreenWrapper } from "../ScreenWrapper";
import { Actions } from "./Actions";
import { DeckCardSkeleton } from "./DeckCardSkeleton";
import { DecksPlaceholder } from "./DecksPlaceholder";
import { HorizontalCrosswordList } from "./HorizontalCrosswordList";
import { Introduction } from "./Introduction";
import { TermsToStudy } from "./TermsToStudy";
import { useHomeStyles } from "./useHomeStyles";

export function Home() {
    const classes = useHomeStyles();

    const { data, loading, error } = useHomeQuery({
        // This screen could be updated frequently in ways we can't
        // predict, due to activity of other users.
        fetchPolicy: "cache-and-network",

        partialRefetch: true,
        returnPartialData: true,
    });
    const termsToStudy = useTermsToStudyQuery({ fetchPolicy: "cache-and-network" });

    if (data && !data.me) return <Introduction />;

    const me = data?.me;

    return (
        <ScreenWrapper>
            <Actions />

            <div className={classes.grid}>
                <div className={classes.content}>
                    {me?.recentAttempts && (
                        <div className={classes.contentCard}>
                            <HorizontalCrosswordList attempts={me.recentAttempts} languages={me.languages} />
                        </div>
                    )}
                    <div className={classes.contentCard}>
                        <RecentActivity />
                    </div>
                    <div className={classes.contentCard}>
                        <Box display="flex" alignItems="center" mb={1}>
                            <Typography variant="h5" component="h2">
                                Lists
                            </Typography>
                            <LinkIconButton to="/lists/add">
                                <AddIcon />
                            </LinkIconButton>
                        </Box>

                        <ul className={classes.grid}>
                            {me?.decks?.map(deck => (
                                <li className={classes.item} key={deck.id}>
                                    <DeckCard className={classes.card} deck={deck} />
                                </li>
                            ))}

                            {!loading && me?.decks?.length === 0 && (
                                <li className={classes.placeholderItem}>
                                    <DecksPlaceholder />
                                </li>
                            )}

                            {loading && (
                                <>
                                    <li className={classes.item}>
                                        <DeckCardSkeleton />
                                    </li>
                                    <li className={classes.item}>
                                        <DeckCardSkeleton />
                                    </li>
                                    <li className={classes.item}>
                                        <DeckCardSkeleton />
                                    </li>
                                </>
                            )}

                            {error && (
                                <li className={classes.placeholderItem}>
                                    <Alert error={error} />
                                </li>
                            )}
                        </ul>
                    </div>
                </div>

                <aside className={classes.aside}>
                    <TermsToStudy data={termsToStudy.data} error={termsToStudy.error} loading={termsToStudy.loading} />
                </aside>
            </div>
        </ScreenWrapper>
    );
}
