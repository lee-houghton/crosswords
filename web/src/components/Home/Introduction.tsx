import { Typography } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import React from "react";
import { Backpack } from "react-kawaii";
import { DeckCardInfoFragment, useLanguageInfoQuery, useTopDecksQuery } from "../../graphql/operations";
import { DeckCard } from "../DeckCard";
import { flagImage } from "../FlagAvatar";
import { LoginButtons } from "../LogInButtons";
import { ScreenWrapper } from "../ScreenWrapper";

const useStyles = makeStyles(theme => ({
    [theme.breakpoints.only("xs")]: {
        heading: {
            textAlign: "center",
        },
    },
    signUp: {
        textAlign: "center"
    },
    signUpButton: {
        width: "13em",
    },
    signIn: {
        textAlign: "center",
        "& > *": {
            margin: theme.spacing(1)
        }
    },
    kawaii: {
        [theme.breakpoints.only("xs")]: {
            textAlign: "center",
        },
        [theme.breakpoints.up("sm")]: {
            marginRight: theme.spacing(2),
            float: "left",
        },
        "& #kawaii-face__eyes__arc path": {
            animation: `$blink 8s ${theme.transitions.easing.easeInOut} infinite`,
            transformOrigin: "0 5px",
        },
        "& #kawaii-face__tongue": {
            animation: `$tongue 10s ${theme.transitions.easing.easeInOut} infinite`,
            // transformOrigin: "0 5px",
        },
        "& #kawaii-face__mouth__joy": {
            animation: `$mouth 10s ${theme.transitions.easing.easeInOut} infinite`,
            // transformOrigin: "0 5px",
        }
    },
    "@keyframes blink": {
        "49%": {
            transform: "scaleY(1)"
        },
        "50%": {
            transform: "scaleY(0.3)"
        },
        "51%": {
            transform: "scaleY(1)"
        },
    },
    "@keyframes mouth": {
        "from": {
            transform: "scaleY(1)"
        },
        "47%": {
            transform: "scaleY(1)"
        },
        "50%": {
            transform: "scaleY(0.3)"
        },
        "53%": {
            transform: "scaleY(1)"
        },
        "to": {
            transform: "scaleY(1)"
        },
    },
    "@keyframes tongue": {
        "from": {
            transform: "scaleY(1)"
        },
        "47%": {
            transform: "scaleY(1)"
        },
        "50%": {
            transform: "scaleY(0.0)"
        },
        "53%": {
            transform: "scaleY(1)"
        },
        "to": {
            transform: "scaleY(1)"
        },
    }
}));

export function Introduction() {
    const classes = useStyles();

    return <ScreenWrapper>
        <div className={classes.kawaii}>
            <Backpack size={160} color="lightblue" mood="blissful"/>
        </div>
        <Typography className={classes.heading} variant="h5" component="h2">
            Vocabulary is hard, let's do crosswords
        </Typography>
        <p>
            Practicing a language? Try entering your vocabulary, or search for someone else's vocabulary,
            and create customised crosswords to help you learn.
        </p>
        <LoginButtons showSignUp/>
        <TopDecks lang="fr"/>
        <TopDecks lang="es"/>
    </ScreenWrapper>;
}

const SIZE = 16;

const useTopDecksStyles = makeStyles(theme => ({
    top: {
        marginTop: theme.spacing(4),
        clear: "left",
        display: "flex",
        flexDirection: "row",
        overflowX: "auto",
        padding: theme.spacing(1),
        margin: theme.spacing(1),
        height: theme.spacing(SIZE + 4),
        [theme.breakpoints.down('md')]: {
            maskImage: `gradient(linear, right top, 85% top, from(rgba(0,0,0,0)), to(rgba(0,0,0,1)))`,
        },
        [theme.breakpoints.only("md")]: {
            maskImage: `gradient(linear, right top, 90% top, from(rgba(0,0,0,0)), to(rgba(0,0,0,1)))`,
        },
        [theme.breakpoints.only("lg")]: {
            maskImage: `gradient(linear, right top, 95% top, from(rgba(0,0,0,0)), to(rgba(0,0,0,1)))`,
        }
    }, 
    deck: {
        flex: 0,
        minWidth: "15em",
        marginLeft: theme.spacing(2),
    },
    flag: {
        borderRadius: theme.shape.borderRadius,
        minWidth: theme.spacing(SIZE), minHeight: theme.spacing(SIZE),
        width: theme.spacing(SIZE), height: theme.spacing(SIZE),
        backgroundPosition: "center",
        backgroundSize: "cover",
        display: "grid",
        placeItems: "center",
    },
    languageName: {
        zIndex: 1,
        color: "white",
        fontSize: "1.5em !important",

        // 1px black stroke, 2em text shadow
        // -webkit-text-stroke exists, but it looks terrible
        textShadow: "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000, 0 0 1em black, 0 0 2em black",
        transition: "text-shadow 0.4s ease",
        "&:hover": {
            // More shadow
            textShadow: "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000, 0 0 1em black, 0 0 1em black, 0 0 1em black, 0 0 2em black"
        },
    },
}));

function TopDecks({ lang }: { lang: string }) {
    const classes = useTopDecksStyles();
    const { data: language } = useLanguageInfoQuery({ variables: { id: lang } });
    const { data: top } = useTopDecksQuery({ variables: { termLang: lang } });

    return <section>
        <ol className={classes.top}>
            <li className={classes.flag} style={{backgroundImage: `url(${flagImage(lang)}`}}>
                <Typography variant="button" component="span" className={classes.languageName}>
                    {language?.language?.name}
                </Typography>
            </li>
            {top?.topDecks.map(deck => 
                <DeckCard key={deck.id}
                    component="li" 
                    avatar="small"
                    deck={deck as DeckCardInfoFragment}
                    className={classes.deck} 
                    description={false} 
                    actions={false}
                    flag={false}
                >
                    {deck.terms.length ? <Typography noWrap variant="body1" color="textSecondary">
                        {deck.terms.slice(0, 5).map(t => t.term).join(" \u2022 ")}
                    </Typography> : undefined}
                </DeckCard>
            )}
        </ol>
    </section>;
}
