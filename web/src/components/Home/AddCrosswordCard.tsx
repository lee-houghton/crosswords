import { Avatar, Dialog, DialogTitle, List, ListItem, ListItemAvatar, ListItemText, Typography } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import { GridOn } from "@mui/icons-material";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { LanguageInfoFragment, useAddCrosswordMutation } from "../../graphql";
import { flagImage } from "../FlagAvatar";

interface AddCrosswordCardProps {
    className?: string;
    language: LanguageInfoFragment;
}

const useStyles = makeStyles(theme => ({
    flagBackground: {
        position: "absolute",
        top: 0, right: 0, bottom: 0, left: 0,
        backgroundPosition: "cover",
        
        // This was an idea, but it didn't look as good as I thought.
        // It looked like the buttons were disabled, almost.
        //
        // filter: "saturate(0.5) contrast(0.5) brightness(1.5)"

    },
    link: {
        color: theme.palette.text.primary,
        margin: "-10em",
        padding: "10em",
    },
    shadow: {
        zIndex: 1,
        color: "white",

        // 1px black stroke, 2em text shadow
        // -webkit-text-stroke exists, but it looks terrible
        textShadow: "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000, 0 0 1em black, 0 0 2em black",
        transition: "text-shadow 0.4s ease",
        "&:hover": {
            // More shadow
            textShadow: "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000, 0 0 1em black, 0 0 1em black, 0 0 1em black, 0 0 2em black"
        }
    },
    addText: {
        fontSize: theme.typography.caption.fontSize,
    }
}));

const sizes = [14, 20, 28];

export function AddCrosswordCard({ className, language }: AddCrosswordCardProps) {
    const classes = useStyles();
    const flag = flagImage(language.id);
    const [addCrossword, { loading }] = useAddCrosswordMutation();
    const [isOpen, setIsOpen] = useState(false);

    async function create(size: number) {
        try {
            await addCrossword({ languageId: language.id, width: size, height: size });
            setIsOpen(false);
        } catch(err) {
        }
    }

    return <div className={className}>
        {flag && <div className={classes.flagBackground} style={{ backgroundImage: flag && `url(${flag})` }} />}
        <Link className={classes.link + " " + classes.shadow} to="/crosswords/add" onClick={e => {
            e.preventDefault();
            setIsOpen(true);
        }}>
            <Typography className={classes.addText} component="span" variant="button">
                Add new ({language.name})
            </Typography>
        </Link>
        {isOpen && <Dialog open={isOpen} onClose={() => setIsOpen(false)}>
            <DialogTitle>Add {language.name} crossword</DialogTitle>
            <List>{
                sizes.map(size => 
                    <ListItem key={size} button disabled={loading} onClick={() => create(size)}>
                        <ListItemAvatar>
                            <Avatar>
                                <GridOn color="action"/>
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary={`New ${size}x${size} crossword`}/>
                    </ListItem>
                )
            }</List>
        </Dialog>}
    </div>;
}
