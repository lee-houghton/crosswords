import Paper from "@mui/material/Paper";
import makeStyles from '@mui/styles/makeStyles';
import Typography from "@mui/material/Typography";
import React from "react";
import { NewsItem } from "./useNews";

export const useStyles = makeStyles(theme => ({
    image: {
        borderWidth: "thin",
        borderStyle: "solid",
        borderColor: theme.palette.primary.dark,
        maxWidth: "100%",
        maxHeight: 300,
    },
    imageWrapper: {
        textAlign: "center",
        marginBottom: theme.spacing(2),
        marginLeft: "auto",
        marginRight: "auto",
        padding: theme.spacing(2),
    }, 
    body: {
        padding: theme.spacing(2, 2, 2, 2),
    },
    article: {
        marginBottom: theme.spacing(2),
    }
}));

export function NewsArticle({ item }: { item: NewsItem; }) {
    const classes = useStyles();

    const body = typeof item.body === "string"
        ? <Typography variant="body1" component="p">{item.body}</Typography>
        : item.body;

    return <article className={classes.article}>
        {item.image && <div className={classes.imageWrapper}>
            <img className={classes.image} src={item.image} alt={item.altText || ""} />
        </div>}

        <Paper variant="outlined" className={classes.body}>
            {item.title && <Typography variant="h6" component="h3">
                {item.title}
            </Typography>}

            {body}
        </Paper>

        <Typography gutterBottom variant="subtitle1" color="textSecondary" align="right">
            {typeof item.date === "string" 
                ? item.date
                : new Date(item.date).toLocaleDateString()}
        </Typography>
    </article>;
}
