import React from "react";
import makeStyles from '@mui/styles/makeStyles';

const NUMBER_OF_DOTS = 3;
const DOT_SIZE = 8;

const useStyles = makeStyles(theme => ({
    divider: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        width: DOT_SIZE * (NUMBER_OF_DOTS * 2 - 1),
        height: 0,
        borderTopStyle: "dotted",
        borderTopColor: theme.palette.grey[400],
        borderWidth: `${DOT_SIZE}px 0 0 0`,
    }
}));

export function ArticleDivider() {
    const classes = useStyles();
    return <hr className={classes.divider} />;
}
