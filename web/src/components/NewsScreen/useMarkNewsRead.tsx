import { useEffect } from "react";
import { useMeQuery } from "../../graphql";
import { useMarkNewsReadMutation } from "../../graphql";

export function useMarkNewsRead(itemId: number) {
    const { data } = useMeQuery();
    const lastId = data?.me?.lastReadNewsId;
    const [markRead] = useMarkNewsReadMutation();

    const id = data && (!lastId || lastId < itemId) ? itemId : undefined;

    useEffect(() => {
        if (id)
            markRead({ variables: { id } });
    }, [id, markRead]);
}
