import React, { ReactNode, useMemo } from "react";
import { Link } from "react-router-dom";
import { useMeQuery } from "../../graphql";
import Typography from "@mui/material/Typography";
import HomeScreenImage from "./images/HomeScreen.png";
import FavIconImage from "./images/FavIcon.png";
import ChangePasswordImage from "./images/ChangePassword.png";

export interface NewsItem {
    id: number;
    date: Date | string;
    image?: string;
    altText?: string;
    title?: string;
    body: ReactNode;
}

export const allNewsItems = [
    {
        id: 15,
        date: "Coming soon",
        body: (
            <Typography variant="body1" component="p">
                <strong>Next on the roadmap</strong>: Something completely random, as per usual!
            </Typography>
        ),
    },
    {
        id: 14,
        date: new Date("2020-11-14"),
        body: `
            You can upload Anki .apkg files or .csv files now. The future is here!
        `,
    },
    {
        id: 13,
        date: new Date("2020-11-02"),
        body: `
            It's now easier to quickly add or edit specific words in a word list without editing all of them.
        `,
    },
    {
        id: 12,
        date: new Date("2020-10-30"),
        body: `
            You can now share a link to a crossword with someone and they can complete the crossword anonymous.
            Anonymous users can also create crosswords for a single word list.
        `,
    },
    {
        id: 11,
        date: new Date("2020-09-09"),
        body: `
            Facebook login has been added to add to the existing Sign in with Google option.
        `,
    },
    {
        id: 10,
        date: new Date("2020-08-21"),
        body: `
            You can now register with your Google account, removing the need to
            set a separate password for Crosswords at all. You can also link a
            Google account to your existing Crosswords account. UI improvements
            will come later.
        `,
    },
    {
        id: 9,
        date: new Date("2020-08-15"),
        title: "Size isn't everything",
        body: `
            When solving crosswords on smaller screens, the header and clues
            (apart from the current one) will disappear to leave as much room
            as possible for the crossword puzzle itself. And now even when
            the crossword is too big for the screen, the current clue will
            always be visible at the bottom.
        `,
    },
    {
        id: 8,
        date: new Date("2020-08-08"),
        title: "Download word lists",
        body: "You can now download word lists as CSV.",
    },
    {
        id: 7,
        date: new Date("2020-08-07"),
        title: "Printable crosswords",
        body: "You can now print crosswords. The crossword will print as a blank crossword, even if you have completed it.",
    },
    {
        id: 6,
        date: new Date("2020-08-01"),
        title: "Home screen",
        image: HomeScreenImage,
        body: "I've updated the home screen to highlight unfinished crosswords at the top and show recent activity just below.",
    },
    {
        id: 5,
        date: new Date("2020-07-02"),
        title: "Favourite Icon",
        image: FavIconImage,
        body: "I didn't really feel like doing anything useful today, but here's a favicon.png. I had to get rid of the react atom thing eventually.",
    },
    {
        id: 4,
        date: new Date("2020-07-01"),
        title: "User profiles",
        body: "You can now view other users' profiles, if you know their names (and if this app actually had any users). It's not really very useful for much else yet, but watch this space!",
    },
    {
        id: 3,
        date: new Date("2020-06-23"),
        title: "What's new",
        body: "You'll now see an indicator when new features are added.",
    },
    {
        id: 2,
        date: new Date("2020-06-14"),
        title: "Reset password",
        body: "You can now reset your password from the login page if you forget it. Don't forget your email address!",
    },
    {
        id: 1,
        date: new Date("2020-06-08"),
        title: "Change password",
        image: ChangePasswordImage,
        body: (
            <Typography variant="body1" component="p">
                You can now change your password in your <Link to="/profile/manage/password">account settings</Link>.
                Try using something like <a href="https://1password.com/">1Password</a> to generate secure passwords and
                avoid re-using them.
            </Typography>
        ),
    },
];

export function useNews() {
    const { data } = useMeQuery();
    const lastId = data?.me ? data.me.lastReadNewsId || 0 : undefined;

    return useMemo(() => (lastId !== undefined ? allNewsItems.filter((x) => x.id > lastId) : undefined), [lastId]);
}
