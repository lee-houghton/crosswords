import Typography from "@mui/material/Typography";
import React, { Fragment } from "react";
import { ScreenWrapper } from "../ScreenWrapper";
import { NewsArticle } from "./NewsArticle";
import { useMarkNewsRead } from "./useMarkNewsRead";
import { allNewsItems } from "./useNews";

export function NewsScreen() {
    useMarkNewsRead(allNewsItems[0].id);

    return <ScreenWrapper>
        <Typography gutterBottom variant="h5" component="h2" align="center">
            What's New <span role="img" aria-label="">✨</span>
        </Typography>

        {allNewsItems.map(item => <Fragment key={item.id}>
            <NewsArticle item={item}/>
        </Fragment>)}
    </ScreenWrapper>;
}

