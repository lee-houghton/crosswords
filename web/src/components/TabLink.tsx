import Tab, { TabProps } from "@mui/material/Tab";
import React from "react";
import { Link, LinkProps } from "react-router-dom";

export const TabLink = (props: LinkProps & TabProps) =>
    <Tab component={Link as any} {...props}/>;
