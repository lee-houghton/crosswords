import Edit from "@mui/icons-material/Edit";
import GridOn from "@mui/icons-material/GridOn";
import Share from "@mui/icons-material/Share";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import IconButton from "@mui/material/IconButton";
import { useTheme } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import useMediaQuery from "@mui/material/useMediaQuery";
import { makeStyles } from "@mui/styles";
import clsx from "clsx";
import React, { memo, ReactNode, useCallback } from "react";
import { Link } from "react-router-dom";
import { DeckCardInfoFragment, useAddCrosswordMutation, useFavouriteDeckIdsQuery, useMeQuery } from "../graphql";
import { FavouriteDeckButton } from "./FavouriteDeckButton";
import { FlagAvatar } from "./FlagAvatar";
import { UserAvatar } from "./Home/UserAvatar";
import { LinkIconButton } from "./LinkIconButton";
import { CardActionAreaLink } from "./Links";

interface DeckCardProps {
    component?: React.ElementType<React.HTMLAttributes<HTMLElement>>;
    className?: string;
    title?: boolean;
    deck: DeckCardInfoFragment;
    link?: boolean;
    avatar?: boolean | "small";
    flag?: boolean;
    children?: ReactNode;

    actions?: boolean;
    description?: boolean;
    buttons?: ReactNode;
}

const useStyles = makeStyles({
    root: {
        display: "flex",
        flexDirection: "column",
        flexGrow: 1,
    },
    actionArea: {
        flexGrow: 1,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "start",
    },
    grow: {
        flexGrow: 1,
    },
});

export const DeckCard = memo(function DeckCard(props: DeckCardProps) {
    const { component, deck, link, avatar, flag = true, description = true, actions = true } = props;

    const loggedIn = Boolean(useMeQuery().data?.me);
    const classes = useStyles();
    const favouriteDeckIds = useFavouriteDeckIdsQuery();
    const isSmall = useMediaQuery(useTheme().breakpoints.down("md"));

    // TODO Add link to crossword in snackbar? Or just navigate to the new crossword?
    const [createCrossword, createCrosswordResult] = useAddCrosswordMutation();
    const onCreateCrossword = useCallback(() => createCrossword({ deckIds: [deck.id] }), [createCrossword, deck.id]);

    const headerContent = (
        <>
            <CardHeader
                title={
                    props.title ? (
                        <Typography variant="h5" component="h2">
                            {deck.title}
                        </Typography>
                    ) : (
                        deck.title
                    )
                }
                action={
                    flag && (
                        <IconButton size="large">
                            <FlagAvatar faded small variant="square" lang={deck.termLang.id} />
                        </IconButton>
                    )
                }
                avatar={
                    avatar && (
                        <UserAvatar
                            large={!isSmall && avatar !== "small"}
                            name={deck.createdBy.displayName || deck.createdBy.name}
                            profileImage={deck.createdBy.profileImage || undefined}
                        />
                    )
                }
                subheader={
                    <>
                        {deck.termCount} terms
                        {props.title && " • by "}
                        {props.title && <Link to={`/users/${deck.createdBy.name}`}>{deck.createdBy.displayName}</Link>}
                    </>
                }
            />

            {description && deck.description && (
                <CardContent className={classes.grow}>
                    <Typography variant="body1">{deck.description}</Typography>
                </CardContent>
            )}
            {props.children && <CardContent className={classes.grow}>{props.children}</CardContent>}
        </>
    );

    const header =
        link !== false ? (
            <CardActionAreaLink to={`/lists/${deck.id}`} className={classes.actionArea}>
                {headerContent}
            </CardActionAreaLink>
        ) : (
            headerContent
        );

    return (
        <>
            <Card component={component as "div"} className={clsx(props.className, classes.root)}>
                {header}
                {actions && (
                    <CardActions disableSpacing>
                        {loggedIn && (
                            <FavouriteDeckButton
                                deckId={deck.id}
                                favourite={
                                    // If profile isn't loaded yet, just assume it's not already favourited
                                    Boolean(favouriteDeckIds?.has(deck.id))
                                }
                            />
                        )}
                        {loggedIn && (
                            <LinkIconButton aria-label="Edit" title="Edit" to={`/lists/${deck.id}/edit`}>
                                <Edit />
                            </LinkIconButton>
                        )}
                        <LinkIconButton aria-label="Share" title="Share" to={`/lists/${deck.id}`} onClick={share}>
                            <Share />
                        </LinkIconButton>
                        {props.buttons}
                        {props.title ? (
                            <Box ml="auto">
                                <Button
                                    color="primary"
                                    aria-label="Create crossword"
                                    title="Create crossword"
                                    onClick={onCreateCrossword}
                                    disabled={createCrosswordResult.loading}
                                >
                                    New crossword
                                </Button>
                            </Box>
                        ) : (
                            <IconButton
                                aria-label="Create crossword"
                                title="Create crossword"
                                onClick={onCreateCrossword}
                                disabled={createCrosswordResult.loading}
                                size="large"
                            >
                                <GridOn />
                            </IconButton>
                        )}
                    </CardActions>
                )}
            </Card>
        </>
    );
});

function share(e: React.MouseEvent) {
    if (typeof (navigator as any).share === "function" && e.currentTarget instanceof HTMLAnchorElement) {
        (navigator as any).share({ url: e.currentTarget.href });
        e.preventDefault();
    }
}
