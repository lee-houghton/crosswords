import { Button, Link as MuiLink, TextField, Typography } from "@mui/material";
import makeStyles from '@mui/styles/makeStyles';
import React, { ChangeEvent, useCallback, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import { useDebounce } from "use-debounce";
import { useCheckUserNameQuery, useRegisterMutation } from "../../graphql";
import { Alert } from "../Alert";
import { ScreenWrapper } from "../ScreenWrapper";
import { LoginButtons } from "../LogInButtons";

const useStyles = makeStyles(theme => ({
    wrapper: {
        marginLeft: "auto",
        marginRight: "auto",
        maxWidth: "30em",
    },
    form: {
        display: "flex",
        flexDirection: "column",
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    }
}));

export function RegisterScreen() {
    const classes = useStyles();
    const [email, setEmail] = useState("");
    const [name, setName] = useState("");
    const onNameChange = useCallback((e: ChangeEvent<HTMLInputElement>) => setName(e.target.value), []);
    const onEmailChange = useCallback((e: ChangeEvent<HTMLInputElement>) => setEmail(e.target.value), []);
    
    // User name checking
    const trimmedName = name.trim();
    const [debouncedName] = useDebounce(trimmedName, 500);
    const nameError = validateUserName(trimmedName);
    const { data: existingUser } = useCheckUserNameQuery({
        variables: {
            username: debouncedName
        },
        skip: !!nameError || !debouncedName
    });
    const nameMessage = (existingUser && existingUser.user) 
        ? `The user name ${debouncedName} is already taken` 
        : nameError;

    // Form submission
    const [register, { data: registered, error: registerError, loading: registering }] = useRegisterMutation();
    const onSubmit = useCallback((e: React.FormEvent) => {
        e.preventDefault();
        register({ variables: { username: trimmedName, email } });
    }, [register, trimmedName, email]);

    if (registered) {
        return <ScreenWrapper>
            <p>
                Success! Please check your email inbox at {email} for further instructions on how to verify your account.
            </p>
            {/* TODO: Allow re-sending registration email */}
        </ScreenWrapper>;
    }

    return <ScreenWrapper>
        <div className={classes.wrapper}>
            <Typography gutterBottom component="h1" variant="h5">
                Create an account
            </Typography>

            <Typography variant="body2" color="textSecondary">
                Already have an account? <MuiLink component={RouterLink} to="login">Log in here.</MuiLink>
            </Typography>

            <form className={classes.form} onSubmit={onSubmit}>
                <TextField 
                    value={name}
                    variant="filled"
                    margin="normal"
                    required
                    fullWidth
                    id="username"
                    label="User name"
                    name="username"
                    autoFocus
                    error={Boolean(nameError)}
                    helperText={nameMessage || "3-20 characters long, no special characters"}
                    onChange={onNameChange}
                />

                <TextField 
                    type="email"
                    variant="filled"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email address"
                    name="email"
                    helperText=""
                    onChange={onEmailChange}
                />

                {registerError && <Alert error={registerError}/>}

                <Button
                    type="submit"
                    disabled={registering}
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                    Register
                </Button>

                <LoginButtons/>
            </form>
        </div>
    </ScreenWrapper>;
}

function validateUserName(name?: string) {
    if (!name)
        return;

    if (name.length < 3)
        return "User name must contain at least 3 characters.";
    
    if (name.length > 20)
        return "User name must contain at most 20 characters.";

    if (!/^[\w\d_]+$/.test(name))
        return "User name must contain only letters, digits, and underscores.";

    return;
}
