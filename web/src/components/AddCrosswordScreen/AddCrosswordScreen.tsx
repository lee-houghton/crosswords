import Button from "@mui/material/Button";
import ButtonGroup from "@mui/material/ButtonGroup";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import Grid from "@mui/material/Grid";
import makeStyles from '@mui/styles/makeStyles';
import Typography from "@mui/material/Typography";
import React, { FormEvent, useState } from "react";
import { useAddCrosswordMutation } from "../../graphql";
import { useFormStyles } from "../../styles/forms";
import { Alert } from "../Alert";
import { ScreenWrapper } from "../ScreenWrapper";
import { QuickCreateCrossword } from "./QuickCreateCrossword";
import { SelectDecks } from "./SelectDecks";
import Box from "@mui/material/Box";

const useStyles = makeStyles(theme => ({
    aside: {
        [theme.breakpoints.up("sm")]: {
            order: 2,
        }
    }
}));

const SIZE_NORMAL = 14;
const SIZE_LARGE = 20;
const SIZE_JUMBO = 28;

export function AddCrosswordScreen() {
    const classes = useStyles();
    const formClasses = useFormStyles();
    const [size, setSize] = useState(SIZE_NORMAL);
    const [deckIds, setDeckIds] = useState(() => new Set<number>());
    const [create, { loading: creating, error }] = useAddCrosswordMutation();
    
    const canSubmit = !creating && deckIds.size > 0;

    const handleCreate = () => {
        create({
            deckIds: [...deckIds],
            width: size,
            height: size,
        });
    };

    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
    };

    return <ScreenWrapper>
        <Grid container spacing={2}>
            <Grid className={classes.aside} item xs={12} md={4}>
                <Card>
                    <CardHeader 
                        title={<Typography variant="h5" component="h2">Quick create</Typography>}
                    />
                    <CardContent>
                        <QuickCreateCrossword/>
                    </CardContent>
                </Card>
            </Grid>

            <Grid item xs={12} md={8}>
                <div className={formClasses.wrapper}>
                    <Typography component="h1" variant="h5">
                        Create a crossword
                    </Typography>

                    <Typography paragraph variant="body1">
                        You can create a new crossword from either a single word list, a specific set of word lists,
                        {" "}or all your word lists for a language.
                    </Typography>

                    <Box display="flex" justifyContent="center" marginY={2}>
                        <CrosswordSizeSelector size={size} setSize={setSize}/>
                    </Box>

                    <form className={formClasses.form} onSubmit={handleSubmit}>
                        <SelectDecks deckIds={deckIds} onChange={setDeckIds}/>

                        {error && <Alert error={error} retry={canSubmit ? handleCreate : undefined}/>}

                        <Button
                            type="submit"
                            disabled={!canSubmit}
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={formClasses.submit}
                            onClick={handleCreate}
                        >
                            Create
                        </Button>
                    </form>
                </div>
            </Grid>
        </Grid>
    </ScreenWrapper>;
}

function CrosswordSizeSelector({size, setSize}: { size: number, setSize: (size: number) => void }) {
    return <ButtonGroup color="secondary" aria-label="outlined primary button group">
        <Button variant={size === SIZE_NORMAL ? "contained" : "outlined"} onClick={() => setSize(SIZE_NORMAL)}>
            Normal (14)
        </Button>
        <Button variant={size === SIZE_LARGE ? "contained" : "outlined"} onClick={() => setSize(SIZE_LARGE)}>
            Large (20)
        </Button>
        <Button variant={size === SIZE_JUMBO ? "contained" : "outlined"} onClick={() => setSize(SIZE_JUMBO)}>
            Jumbo (28)
        </Button>
    </ButtonGroup>;
}
