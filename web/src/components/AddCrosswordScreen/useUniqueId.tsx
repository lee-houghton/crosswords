import { useRef } from "react";

let nextId = 1;

export function useUniqueId(prefix = "unique-") {
    const ref = useRef<string>();
    if (!ref.current)
        ref.current = prefix + "-" + (nextId++);
    return ref.current;
}
