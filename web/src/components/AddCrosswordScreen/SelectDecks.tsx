import React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import { useUniqueId } from "./useUniqueId";
import Checkbox from "@mui/material/Checkbox";
import ListItemText from "@mui/material/ListItemText";
import Box from "@mui/material/Box";
import Skeleton from '@mui/material/Skeleton';
import Typography from "@mui/material/Typography";
import { useUserQuery } from "../../graphql";

export interface SelectDecksProps {
    deckIds: Set<number>;
    onChange: (deckIds: Set<number>) => void;
}

export function SelectDecks(props: SelectDecksProps) {
    // This is inefficient, but it's less-often used so I'll let it slide for now
    // Actually $self is a bad name for this query variable, as it simply controls
    // 
    // the presence of the ManageAccountFragment fields (which we don't need here).
    const { data: profile, loading } = useUserQuery({ variables: { self: false } });
    const labelId = useUniqueId("select-decks");

    function handleToggle(id: number) {
        const deckIds = new Set(props.deckIds);
        if (deckIds.has(id))
            deckIds.delete(id);
        else
            deckIds.add(id);
            
        props.onChange(deckIds);
    }

    if (loading)
        return <Box>
            {([1,2,3,4].map(i => <Box key={i} display="flex" alignItems="center" m={2}>
                <Box mr={1}><Skeleton width={20}><Typography>.</Typography></Skeleton></Box>
                <Skeleton width="100%"><Typography>.</Typography></Skeleton>
            </Box>))}
        </Box>;

    return <List>
        {profile?.user?.decks?.map(deck => 
            <ListItem key={deck.id} role={undefined} dense button onClick={() => handleToggle(deck.id)}>
                <Checkbox
                    edge="start"
                    checked={props.deckIds.has(deck.id)}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ 'aria-labelledby': labelId + deck.id }}
                />
                <ListItemText
                    id={labelId + deck.id} 
                    primary={deck.title} 
                    title={deck.description || undefined} 
                />
                {/* <ListItemSecondaryAction>
                <IconButton edge="end" aria-label="comments">
                    <CommentIcon />
                </IconButton>
                </ListItemSecondaryAction> */}
            </ListItem>
        )}
    </List>;
}
