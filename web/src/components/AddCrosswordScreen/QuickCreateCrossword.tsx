import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import React, { useMemo } from "react";
import { Alert } from "../Alert";
import { FlagIcon } from "../FlagIcon";
import Skeleton from "@mui/material/Skeleton";
import ButtonGroup from "@mui/material/ButtonGroup";
import Typography from "@mui/material/Typography";
import { Link } from "react-router-dom";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import makeStyles from "@mui/styles/makeStyles";
import { useUserLanguagesQuery, useAddCrosswordMutation } from "../../graphql";
import { UserLanguageFragment } from "../../graphql";

function compareDates(lang1: UserLanguageFragment, lang2: UserLanguageFragment) {
    if (lang1.lastActivity > lang2.lastActivity) return -1;
    if (lang1.lastActivity < lang2.lastActivity) return 1;
    return 0;
}

const skeleton = (
    <>
        <Skeleton />
        <br />
        <Skeleton />
        <br />
        <Skeleton />
    </>
);

const useStyles = makeStyles((theme) => ({
    empty: {
        textAlign: "center",
    },
}));

export function QuickCreateCrossword() {
    const classes = useStyles();
    const { data, loading } = useUserLanguagesQuery();
    const [addCrossword, { loading: creating, error: createError }] = useAddCrosswordMutation();
    const sortedLanguages = useMemo(() => data?.me?.languages.slice().sort(compareDates), [data]);

    function createForLanguage(languageId: string, size: number) {
        addCrossword({ languageId, width: size, height: size });
    }

    return (
        <div>
            {loading && skeleton}

            {sortedLanguages?.length === 0 && (
                <ListItem>
                    <ListItemText
                        primary={
                            <>
                                <Link to="/lists/add">Add some word lists</Link> or do some crosswords to see some
                                shortcuts here!
                            </>
                        }
                        className={classes.empty}
                    />
                </ListItem>
            )}

            {sortedLanguages?.map(
                (lang) =>
                    lang.decks > 0 && (
                        <div key={lang.language.id}>
                            <Box display="flex" mt={2} alignItems="center">
                                <Box width="3em" textAlign="center">
                                    <FlagIcon space muted lang={lang.language.id} />
                                </Box>
                                <Box flex={1}>
                                    <Typography variant="body1" children={lang.language.name} />
                                </Box>
                                <ButtonGroup variant="outlined" size="small">
                                    <Button
                                        disabled={creating}
                                        onClick={() => createForLanguage(lang.language.id, 14)}
                                        children="14"
                                        title="Normal"
                                    />
                                    <Button
                                        disabled={creating}
                                        onClick={() => createForLanguage(lang.language.id, 20)}
                                        children="20"
                                        title="Large"
                                    />
                                    <Button
                                        disabled={creating}
                                        onClick={() => createForLanguage(lang.language.id, 28)}
                                        children="28"
                                        title="Jumbo"
                                    />
                                </ButtonGroup>
                            </Box>
                        </div>
                    )
            )}

            {createError && <Alert error={createError} />}
        </div>
    );
}
