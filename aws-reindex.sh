#!/bin/bash
aws --profile crosswords ecs run-task \
    --cluster crosswords-dev \
    --task-definition arn:aws:ecs:eu-west-2:613530228088:task-definition/crosswords-api:13 \
    --overrides '{"containerOverrides":[{"name":"api","command":["node","dist/tasks/reindex.js"]}]}' \
    --network-configuration '{"awsvpcConfiguration":{"subnets": ["subnet-02cd38ed4f6c3c15c","subnet-0c613768f66ce069a","subnet-0566911577f48cfc0"],"securityGroups": ["sg-0eb658f6322353f9f"],"assignPublicIp": "ENABLED"}}'
