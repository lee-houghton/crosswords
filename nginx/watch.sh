#!/bin/bash

HASH=$(sha256sum /etc/nginx/conf.d/default.conf)
while true; do

    sleep 1;
    NEWHASH=$(sha256sum /etc/nginx/conf.d/default.conf)

    if [[ "$NEWHASH" != "$HASH" ]]; then 
        echo "Nginx config has changed; reloading..."
        nginx -s reload
        HASH=$NEWHASH
    fi
done 
