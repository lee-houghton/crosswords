server {
    listen 80;

    access_log off;

    location / {
        proxy_pass http://web:3000/;
    }

    # Trailing slash here must be omitted both here and in the proxy_pass directive
    location /sockjs-node {
        proxy_pass http://web:3000/sockjs-node;

        # Web sockets
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $host;
    }

    # Trailing slash here is required both here and in the proxy_pass directive
    # Otherwise the rewritten URL either includes /api or starts with //
    location /api/ {
        proxy_pass http://api:8080/;
    }

    # Trailing slash here must be omitted both here and in the proxy_pass directive
    location /graphql {
        proxy_pass http://api:8080;

        # Web sockets
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $host;
    }

    # Trailing slash here must be omitted both here and in the proxy_pass directive
    location /api/graphql {
        proxy_pass http://api:8080/graphql;

        # Web sockets
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $host;
    }

    location /sockjs-node/ {
        proxy_pass http://web:3000/sockjs-node/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }
}
