This project is a monorepo using Yarn workspaces.
It contains multiple packages which reference each other.
You must use Yarn instead of npm to install the dependencies.

# Components

- `@asztal/crosswords-api` - A REST API using Express, Apollo GraphQL, and TypeORM.
- `@asztal/crosswords-web` - A web front end written with React, Apollo GraphQL Client, and Material UI.
- `@asztal/crosswords-serverless` - Lambda@Edge function for adding security headers to a CloudFront distribution
- Terraform configuration for setting up a load balancer, CloudFront distributions, S3 buckets, and RDS instances.

# Developing

To develop simply run `yarn` at the root of the repository to install the dependencies for all packages,
and then run `yarn dev` to run all the components in Docker. You can run `yarn dev --build` to rebuild the
Docker containers (if, for example, environment variables change).

The development version of the app can then be used at [http://localhost:3000/](http://localhost:3000/).

[![pipeline status](https://gitlab.com/lee-houghton/crosswords/badges/develop/pipeline.svg)](https://gitlab.com/lee-houghton/crosswords/commits/develop)

## Codegen

This project uses [graphql-code-generator](https://graphql-code-generator.com) to generate TypeScript types from the .graphql files. The `.graphql` files in the API define the schema, and the ones in the web front end define the various operations (queries and mutations) that are to be performed. `graphql-code-generator` generates:

- Types in the API so that the Apollo resolvers are accurately type checked.
- Types in the web front end which are specific to the queries being performed.
- Auto-generated react hooks for each query/mutation.

The code generation process runs automatically under docker-compose when running the development environment
with `yarn dev`, however you can still run `yarn codegen` from the root of the repository if required.

# Deploying to dev

Current steps are:

- Run `yarn typeorm migration:generate -c awsdev -n MigrationName` if necessary _(you must have ormconfig set up)_
- Run `yarn typeorm migration:run -c awsdev` if you added a migration
- Push to the `develop` branch
- GitLab CI does the rest

# Deploying the infrastructure

You will need docker to run this (`sudo apt install docker.io`).
Then run `cd ./terraform` and run `./terrform.sh`. (You need to have a `~/.aws/credentials` file set up first).

Then you can run:

- `cd edge && npm install && npm run deploy:dev` to deploy the Lambda@Edge functions
- `terraform init && terraform get && terraform apply` to apply the terraform templates and create the necessary infrastructure (this is also necessary when updating the Lambda@Edge functions)
- If you have made changes to the Lambda@Edge functions or updated files in S3 manually, you can invalidate the distribution using `./invalidate.sh`. By default this only invalidates `/index.html` because asset files are [immutable](https://tools.ietf.org/html/rfc8246).

# Deploying the Lambda@Edge function

Simply run `./edge.sh` from inside the terraform container. This will deploy the Lambda@Edge, apply the terraform templates (you must verify the execution plan yourself and accept it), and then invalidate the CloudFront distribution.
