module.exports = {
    client: {
        service: {
            name: "crosswords",
            localSchemaFile: "./api/graphql.schema.json"
        },
        includes: [
            "./web/src/graphql/operations/**/*.graphql",
            "./web/src/graphql/use*.ts"
        ]
    },
};
