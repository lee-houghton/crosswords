# Open source libraries

## Web app

* Material UI
* React
* Apollo client

## Server

* Express
* Passport
* Apollo Server
* TypeORM
* DataLoader

# Photos

## Language cover images

Language|Source
-|-
Chinese|[*Tom Fisk* from Pexels](https://www.pexels.com/photo/brown-concrete-wall-surrounded-by-trees-1653823//)
